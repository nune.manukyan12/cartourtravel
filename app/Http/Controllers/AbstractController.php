<?php

namespace App\Http\Controllers;


use App\Services\TranslationService;
use Illuminate\Http\Request;

abstract class AbstractController extends Controller
{


    /**
     * @param $request
     * @param $model
     * @param $id
     * @return mixed
     */
    protected function savePrice($request, $model, $id)
    {
        $create = '';


        $create = $model->create([
            'base_price' => $request->input('base_price'),
            'monthly_price' => $request->input('monthly_price'),
            'weekly_discounts' => $request->input('weekly_discount'),
            'two_weekly_discounts' => $request->input('two_weekly_discount'),
            'monthly_discounts' => $request->input('monthly_discount'),
            'pre_payment' => $request->input('pre_payment'),
            'pre_payment_price' => $request->input('pre_payment_price'),
            'extra_cleaning' => $request->input('extra_cleaning'),
            'currency' => $request->input('currency'),
            'parent_id' => $id
        ]);


        return $create;
    }

    public function translate(Request $request)
    {
//        dd($request->all());
        $name = $request->input('name');
        $description = $request->input('description');
        $lang = $request->input('lang');

        $translate = new TranslationService();
        $translate_data = $translate->translate($name, $description, $lang);

        return response()->json([
           'name' => $translate_data['name'],
           'description' => $translate_data['description']
        ]);
    }

}
