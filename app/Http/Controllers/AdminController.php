<?php

namespace App\Http\Controllers;


use App\Models\AjaxPictures;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $model;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function uploadImage()
    {

        $file = Input::file('file');
        $filename = time() . $file->getClientOriginalName();

        $file->move('uploads', $filename);
        $data = [
            'pictures' => $filename
        ];
        AjaxPictures::query()->create($data);
    }
}
