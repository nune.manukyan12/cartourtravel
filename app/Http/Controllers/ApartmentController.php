<?php

namespace App\Http\Controllers;

use App\Http\Requests\Apartment\StoreRequest;
use App\Http\Requests\Apartment\UpdateRequest;
use App\Models\AjaxPictures;
use App\Models\Apartment;
use App\Models\ApartmentAvailable;
use App\Models\ApartmentBedrooms;
use App\Models\ApartmentDetails;
use App\Models\ApartmentPictures;
use App\Models\ApartmentPrices;
use App\Models\ApartmentReservations;
use App\Models\ApartmentTranslation;
use App\Repositories\GeneralRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ApartmentController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $model;
    protected $modelMl;

    public function __construct(Apartment $apartment)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($apartment);
        $this->modelMl = new ApartmentTranslation();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apartments = $this->model->with('translateHasOne')->get();

        return view('admin.apartment.apartments', compact('apartments'));
    }

    public function create()
    {
        return view('admin.apartment.create');
    }

    public function store(StoreRequest $request)
    {
        $data = $request->only($this->model->getModel()->fillable);
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }


        $apartmentStore = $this->model->create($data, $this->modelMl);

        $ApartmentPrices = new ApartmentPrices();
        $this->savePrice($request, $ApartmentPrices, $apartmentStore->id);

        $this->storePictures($apartmentStore->id);

        // details info
        $detail = $this->details($request, $apartmentStore->id);
        // end


        return response()->json([
            'id' => $apartmentStore->id,
        ]);

    }

    public function show($id)
    {

        $apartment = $this->model
            ->with('picture')
            ->with('prices')
            ->with('translate')
            ->with(['details' => function ($query) {
                $query->with('bedrooms');
            }])
            ->findOrFail($id);
        $notAvailableDate = ApartmentAvailable::query()
            ->where('apartment_id', '=', $id)
            ->where('blocked', '=', ApartmentAvailable::BLOCKED)
            ->first();

        $nightlyPrice = ApartmentAvailable::query()
            ->where('apartment_id', '=', $id)
            ->where('blocked', '=', ApartmentAvailable::ACTIVE)
            ->get();

        $countOfNightlyPrice = count($nightlyPrice);

        if ($notAvailableDate) {

            $notAvailableDate = $notAvailableDate->date;

        } else {
            $notAvailableDate = "[]";

        }


        $reservations = $this->getReservation($id);


        return view('admin.apartment.update', compact('apartment', 'notAvailableDate', 'nightlyPrice', 'reservations', 'countOfNightlyPrice'));
    }

    private function getReservation($apartment_id)
    {
        return ApartmentReservations::query()->where('apartment_id', '=', $apartment_id)->get();
    }

    public function update(UpdateRequest $request)
    {
        $id = $request->input('id');
        $data = $request->only($this->model->getModel()->fillable);
        // general picture
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }

        $modelMl = new ApartmentTranslation();
        $updateApartment = $this->model->update($data, $id, $modelMl);
        $apartmentPrices = new ApartmentPrices();
        $apartmentPrices->where('parent_id', '=', $id)->delete();
        $this->savePrice($request, $apartmentPrices, $id);

        $this->storePictures($id);

        $this->details($request, $id);
        if ($updateApartment) {

            return response()->json([
                'status' => 200
            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }

    public function destroy($id)
    {
        $details = ApartmentDetails::query()->where('apartment_id', '=', $id)->first();
        if($details){

            ApartmentBedrooms::query()->where('apartment_detail_id', '=', $details->id)->delete();
        }
        ApartmentDetails::query()->where('apartment_id', '=', $id)->delete();


        return $this->model->delete($id);
    }

    public function addHome($id, $val)
    {
        Apartment::query()->where('id', '=', $id)->update([
            'homepage_order' => $val
        ]);

    }

    protected function storePictures($last_id)
    {
        $pictures = AjaxPictures::query()->get();

        foreach ($pictures as $picture) {
            $data_of_files = [
                'apartment_id' => $last_id,
                'name' => $picture->pictures,
            ];
            ApartmentPictures::query()->insert($data_of_files);
        }

        AjaxPictures::query()->delete();
    }

    protected function details($request, $id)
    {

        $detail = new ApartmentDetails();
        $bedroom = new ApartmentBedrooms();


        $repository = new GeneralRepository($detail);


        $data = $request->only($repository->getModel()->fillable);
        if (!isset($data['detail_id'])) {
            $data['apartment_id'] = $id;
            $detailupdate = $repository->create($data);
            if ($request->input('double')) {


                foreach ($request->input('double') as $key => $value) {
                    $bedroom_data = $bedroom->create([
                        'apartment_detail_id' => $detailupdate->id,
                        'double' => $request->input('double')[$key],
                        'queen' => $request->input('queen')[$key],
                        'single' => $request->input('single')[$key],
                        'sofa_bed' => $request->input('sofa_bed')[$key],
                    ]);
                }
            }
        } else {
            if ($request->input('double')) {
                $bedroom->where('apartment_detail_id', '=', $data['detail_id'])->delete();
                foreach ($request->input('double') as $key => $value) {
                    $bedroom_data = $bedroom->create([
                        'apartment_detail_id' => $data['detail_id'],
                        'double' => $request->input('double')[$key],
                        'queen' => $request->input('queen')[$key],
                        'single' => $request->input('single')[$key],
                        'sofa_bed' => $request->input('sofa_bed')[$key],
                    ]);
                }
            }
            $detailupdate = $repository->update($data, $data['detail_id']);
        }
        return $detailupdate;
    }

    protected function deleteImage($id)
    {
        $model = new ApartmentPictures();

        $repository = new GeneralRepository($model);
        $response = $repository->delete($id);

        return $response;

    }

    public function approval($id)
    {

        $appartment = Apartment::query()
            ->where('id', '=', $id)
            ->update(['status' => Apartment::STATUS_ACTIVE]);
        return $appartment;


    }

    public function available(Request $request)
    {
        $data = $request->all();

        ApartmentAvailable::query()
            ->where('apartment_id', '=', $data['id'])
            ->where('blocked', '=', ApartmentAvailable::BLOCKED)
            ->delete();

        $available = new ApartmentAvailable();
        $available->apartment_id = $data['id'];
        $available->blocked = ApartmentAvailable::BLOCKED;
        $available->date = $data['data'];
        if ($available->save()) {
            $nightly_price = ApartmentAvailable::query()
                ->where('apartment_id', '=', $data['id'])
                ->where('blocked', '=', ApartmentAvailable::ACTIVE)
                ->get();
            $availableDate['blockDay'] = $available->date;
            $availableDate['nightlyPrice'] = $nightly_price;

            return ($availableDate);
        } else {
            return 0;
        }

    }

    public function nightlyPrice(Request $request)
    {
        $data = $request->all();
        $available = new ApartmentAvailable();
        $available->apartment_id = $data['id'];
        $available->blocked = ApartmentAvailable::ACTIVE;
        $available->date = $data['data'];
        $available->nightly_price = $data['price'];


        if ($available->save()) {
            $blockDay = ApartmentAvailable::query()
                ->where('apartment_id', '=', $data['id'])
                ->where('blocked', '=', ApartmentAvailable::BLOCKED)
                ->first();
            $nightlyPrice = ApartmentAvailable::query()
                ->where('apartment_id', '=', $data['id'])
                ->where('blocked', '=', ApartmentAvailable::ACTIVE)
                ->get();
            $availableDate['nightlyPrice'] = $nightlyPrice;
            $availableDate['blockDay'] = $blockDay;
            return $availableDate;
        } else {
            return 0;
        }

    }
}
