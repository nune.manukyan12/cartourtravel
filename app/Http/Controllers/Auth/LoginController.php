<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function logout(Request $request)
    {
        /*
         * Boilerplate needed logic
         */

        /*
         * Remove the socialite session variable if exists
         */
        if (app('session')->has(config('access.socialite_session_name'))) {
            app('session')->forget(config('access.socialite_session_name'));
        }

        /*
         * Remove any session data from backend
         */
//        app()->make(Auth::class)->flushTempSession();

        /*
         * Fire event, Log out user, Redirect
         */
//        dd($this->guard());
//        event(new UserLoggedOut($this->guard()->user()));

        /*
         * Laravel specific logic
         */
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect('/');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.

        $cative_user = User::query()->where('email', $request->input('email'))->where('status', '=', User::STATUS_ACTIVE)->first();
       if($cative_user){
           if ($this->hasTooManyLoginAttempts($request)) {
               $this->fireLockoutEvent($request);

               return $this->sendLockoutResponse($request);
           }

           if ($this->attemptLogin($request)) {
               return $this->sendLoginResponse($request);
           }

           // If the login attempt was unsuccessful we will increment the number of attempts
           // to login and redirect the user back to the login form. Of course, when this
           // user surpasses their maximum number of attempts they will get locked out.
           $this->incrementLoginAttempts($request);

           return $this->sendFailedLoginResponse($request);
       }

    }

}
