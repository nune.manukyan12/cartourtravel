<?php

namespace App\Http\Controllers;

//use App\Http\Controllers\Controller;

use App\Http\Requests\Car\StoreRequest;
use App\Http\Requests\Car\UpdateRequest;
use App\Models\AjaxPictures;
use App\Models\Car;
use App\Models\CarPictures;
use App\Models\CarReservations;
use App\Models\CarPrices;
use App\Models\CarTranslation;
use App\Models\CarType;
use App\Repositories\GeneralRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CarController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $model;
    protected $modelMl;

    public function __construct(Car $car)
    {
        $this->middleware('auth');
        $this->modelMl = new CarTranslation();
        $this->model = new GeneralRepository($car);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = $this->model->all();

        return view('admin.car.cars', compact('cars'));
    }

    public function create()
    {
        $types =CarType::query()->get();

        return view('admin.car.create', compact('types'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreRequest $request)
    {

        $data = $request->only($this->model->getModel()->fillable);

        // general picture
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }
        $carStore = $this->model->create($data, $this->modelMl);

        $this->storePictures($carStore->id);

        return response()->json([
            'id' => $carStore->id,
        ]);
    }


    /**
     * @param $fromdate
     * @param $todate
     * @return \DatePeriod
     * @throws \Exception
     */
    private function returnDates($fromdate, $todate)
    {
        $fromdate = \DateTime::createFromFormat('d/m/Y', $fromdate);
        $todate = \DateTime::createFromFormat('d/m/Y', $todate);
        return new \DatePeriod(
            $fromdate,
            new \DateInterval('P1D'),
            $todate->modify('+1 day')
        );
    }



    public function show($id)
    {
        $reservationTimeArray=array();
        $car = $this->model
            ->with('picture')
            ->with('prices')
            ->with('translate')
            ->with('reservations')
            ->findOrFail($id);

        for ($i=0; $i<count($car->reservations); $i++){
            if ($car->reservations[$i]['status'] == Car::STATUS_ACTIVE){
                $datePeriod = $this->returnDates($car->reservations[$i]['pick_up_date'], $car->reservations[$i]['return_date']);
                foreach ($datePeriod as $date) {
                    $rezult = $date->format('Y/m/d');
                    array_push($reservationTimeArray, $rezult);
                }
            }

        }
        $types = CarType::query()->get();

        return view('admin.car.update', compact('car', 'types','reservationTimeArray'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request)
    {

        $id = $request->input('id');
        $data = $request->only($this->model->getModel()->fillable);
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }



        $updateCar = $this->model->update($data, $id, $this->modelMl);
//        foreach ($request->input('lang') as $key => $lang) {
//            $data_translate = CarTranslation::query()->updateOrCreate(
//                [
//                    'car_id' => $id,
//                    'locale' => $lang,
//                ],
//                [
//                    'name' => $request->input('name')[$key],
//                    'description' => $request->input('description')[$key],
//                ]);
//        }
        $this->storePictures($id);

        if ($updateCar) {

            return response()->json([
                'status' => 200,


            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }

    public function destroy($id)
    {
        $delete = $this->model->delete($id);
        if ($delete) {

            return response()->json([
                'status' => 200
            ]);
        }
    }


    public function storePictures($last_id)
    {
        $pictures = AjaxPictures::query()->get();

        foreach ($pictures as $picture) {
            $data_of_files = [
                'car_id' => $last_id,
                'name' => $picture->pictures,
            ];
            CarPictures::query()->insert($data_of_files);
        }

        AjaxPictures::query()->delete();
    }

    protected function deleteImage($id)
    {
        $model = new CarPictures();

        $repository = new GeneralRepository($model);
        $response = $repository->delete($id);

        return $response;

    }

    public function approval($id)
    {
        $car = Car::query()
            ->where('id', '=', $id)
            ->update(['status' => Car::STATUS_ACTIVE]);
        return $car;

    }
}
