<?php

namespace App\Http\Controllers;

use App\Models\CarSeat;
use App\Models\CarSeatPictures;
use App\Models\AjaxPictures;
use App\Repositories\GeneralRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\CarSeat\CarSeatRequest;


class CarSeatController extends Controller
{
    protected $model;
    public function __construct(CarSeat $carSeat)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($carSeat);
    }

    public function index()
    {   $carSeats = $this->model->all();
        return view('admin.car_seat.car_seat',compact('carSeats'));
    }
    public function create()
    {
        return view('admin.car_seat.create');
    }

    public  function store(CarSeatRequest $request)
    {
        $data = $request->all();
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }
        $carSeat=$this->model->create($data);
        $this->storePictures($carSeat->id);

        return $carSeat;

    }
    public function storePictures($last_id)
    {
        $pictures = AjaxPictures::query()->get();

        foreach ($pictures as $picture) {
            $data_of_files = [
                'car_seat_id' => $last_id,
                'name' => $picture->pictures,
            ];
            CarSeatPictures::query()->insert($data_of_files);
        }

        AjaxPictures::query()->delete();
    }

    public function show($id)
    {
        $carSeat = $this->model
            ->with('picture')
            ->find($id);
        return view('admin.car_seat.update', compact('carSeat'));
    }
    public function update(Request $request)
    {

        $id = $request->input('id');
        $data = $request->only($this->model->getModel()->fillable);
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }
        $updateCarSeat = $this->model->update($data, $id);
        $this->storePictures($id);

        if ($updateCarSeat) {

            return response()->json([
                'status' => 200,


            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }
    public function destroy( $id)
    {
        $delete = $this->model->delete($id);
        if ($delete) {

            return response()->json([
                'status' => 200
            ]);
        }

    }
}
