<?php

namespace App\Http\Controllers\Frontend;

use Artesaos\SEOTools\Facades\SEOMeta;
use App\Http\Controllers\Controller;


abstract class AbstractController extends Controller
{

    public function __construct()
    {
        SEOMeta::setTitle('CarTourTravel');
        SEOMeta::setDescription('Car Tour Travel');
        SEOMeta::setCanonical('http://cartour.travel/');
    }
}
