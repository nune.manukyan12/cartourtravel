<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Apartment;
use App\Models\ApartmentBedrooms;
use App\Models\ApartmentDetails;
use App\Models\ApartmentAvailable;
use App\Models\ApartmentPictures;
use App\Repositories\GeneralRepository;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\DB;

class ApartmentController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;

    public function __construct(Apartment $apartment)
    {
        SEOMeta::setTitle('Apartments');
        SEOMeta::setDescription('comfortable apartment');
        SEOMeta::setCanonical('http://cartour.travel/apartments');
        $this->model = new GeneralRepository($apartment);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($type = null)
    {

        $bedrooms = ApartmentBedrooms::query()
            ->select( DB::raw('apartment_detail_id,SUM(`double`) AS `double`, SUM(`queen`) AS `queen`, SUM(`single`) AS `single`, SUM(`sofa_bed`) AS `sofa_bed`'))
            ->groupBy('apartment_detail_id')
            ->get();

        if ($type) {
            $apartments = $this->model
                ->with('prices')
                ->with('translateHasOne')
                ->whereHas('details', function ($query) use ($type) {
                    $query->where('property_type', '=', $type);
                })
                ->orderBy('id', 'DESC')
                ->get();
            return view('apartment.apartment', compact('apartments','bedrooms'));
        } else {
            $apartments = $this->model
                ->with('prices')
                ->with('translateHasOne')
                ->orderBy('id', 'DESC')
                ->get();


            return view('apartment.apartment', compact('apartments','bedrooms'));
        }

    }

    public function search(Request $request)
    {

        $bedrooms = ApartmentBedrooms::query()
            ->select( DB::raw('apartment_detail_id,SUM(`double`) AS `double`, SUM(`queen`) AS `queen`, SUM(`single`) AS `single`, SUM(`sofa_bed`) AS `sofa_bed`'))
            ->groupBy('apartment_detail_id')
            ->get();

        $block_date_server = array();
        $block_day_ids_arr = array();

        $address = $request->input('address');

        $adult = $request->input('adult');
        $child = $request->input('child');
        $date_check_in = $request->input('date_check_in');
        $date_check_out = $request->input('date_check_out');

        //block day if user chose $resulte
        $notAvailableDate = ApartmentAvailable::query()
            ->where('blocked', '=', ApartmentAvailable::BLOCKED)
            ->get();
        foreach ($notAvailableDate as $notAvailable) {
            $block_day = array($notAvailable->date);
            $block_day_date = json_decode($block_day[0]);
            for ($m = 0; $m < count($block_day_date); $m++) {
                $timestamp = $block_day_date[$m] / 1000;
                $date = date('d/m/Y', strtotime('+1 day', $timestamp));
                array_push($block_date_server, $date);
            }

            $i = 0;
            while ($i < count($block_date_server)):
                if ($block_date_server[$i] > $date_check_in && $block_date_server[$i] < $date_check_out) {
                    array_push($block_day_ids_arr, $notAvailable->apartment_id);
                    break;
                }
                $i++;
            endwhile;

        }
        if (!$address) {
            $address = 'Armenia Yerevan';
        }
        $total_guests = $adult + $child;
        $apartments_data = Apartment::query()
            ->join('apartment_details', 'apartments.id', '=', 'apartment_details.apartment_id')
            ->select('apartments.*')
            ->where('apartment_details.number_of_quests', '>=', $total_guests);

//        if ($request->input('garden')) {
//            $apartments_data = $apartments_data->where('apartment_details.garden', '=', $request->input('garden'));
//        }
        if ($request->input('wifi')) {
            $apartments_data = $apartments_data
                ->whereNotNull('apartment_details.wifi');
        }
        if ($request->input('central_cooling')) {
            $apartments_data = $apartments_data
                ->where(function ($query){
                    $query->whereNotNull('apartment_details.central_cooling')
                        ->orWhereNotNull('apartment_details.air_conditioning');
                });
        }
//        if ($request->input('parking')) {
//            $apartments_data = $apartments_data
//                ->whereNotNull('apartment_details.parking_type');
//        }
//        if ($request->input('cable_tv')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.cable_tv', '=', $request->input('cable_tv'));
//        }
//        if ($request->input('balcony')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.balcony', '=', $request->input('balcony'));
//        }
//        if ($request->input('terrace')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.terrace', '=', $request->input('terrace'));
//        }
//        if ($request->input('key_card_access')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.key_card_access', '=', $request->input('key_card_access'));
//        }
//        if ($request->input('towels')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.towels', '=', $request->input('towels'));
//        }
//        if ($request->input('bed_sheets')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.bed_sheets', '=', $request->input('bed_sheets'));
//        }
//        if ($request->input('pillows')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.pillows', '=', $request->input('pillows'));
//        }
//        if ($request->input('shampoo')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.shampoo', '=', $request->input('shampoo'));
//        }
//
//        if ($request->input('hot_water')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.hot_water', '=', $request->input('hot_water'));
//        }
//        if ($request->input('kitchen_ware')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.kitchen_ware', '=', $request->input('kitchen_ware'));
//        }
//        if ($request->input('electric_kettle')) {
//            $apartments_data = $apartments_data
//                ->where('apartment_details.electric_kettle', '=', $request->input('electric_kettle'));
//        }
//        if ($request->input('central_heating')) {
//            $apartments_data = $apartments_data
//                ->whereNotNull('apartment_details.central_heating');
//        }


        if (isset($date_check_in) && isset($date_check_out)) {
            $apartments_data->whereNotIn('apartments.id', $block_day_ids_arr);
        }
        if (isset($address)) {
            $addressArr = explode('/', $address);
            $apartments_data->where(function ($query) use ($addressArr) {
                $query->where('apartments.country', 'like', '%' . $addressArr[0] . '%');
                $query->orWhere('apartments.city', 'like', '%' . $addressArr[0] . '%');
                $query->orWhere('apartments.district', 'like', '%' . $addressArr[0] . '%');
                $query->orWhere('apartments.address', 'like', '%' . $addressArr[0] . '%');
                if (isset($addressArr[1])) {

                    $query->orWhere('apartments.country', 'like', '%' . $addressArr[1] . '%');
                    $query->orWhere('apartments.city', 'like', '%' . $addressArr[1] . '%');
                    $query->orWhere('apartments.district', 'like', '%' . $addressArr[1] . '%');
                    $query->orWhere('apartments.address', 'like', '%' . $addressArr[1] . '%');
                }
            });
        }




        $apartments_data->with('prices');
        $apartments_data->with('translateHasOne');
        $apartments = $apartments_data->get();
        return view('apartment.apartment', compact('apartments', 'date_check_out', 'date_check_in', 'adult', 'child', 'address','bedrooms'));
    }

    public function detail($id)
    {


        $apartment = $this->model
            ->with(['picture' => function ($query) use ($id) {
                $query->where('apartment_id', '=', $id)
                    ->orderBy('id', 'ASC')
                    ->limit(3);
            }])
            ->with(['details' => function ($query) {
                $query->sortable();
                $query->with('bedrooms');
            }])
            ->with('users')
            ->with('translateHasOne')
//            ->with('available_apartment')
            ->findOrFail($id);

        SEOMeta::setTitle($apartment->name);
        SEOMeta::setDescription($apartment->description);

        SEOMeta::setCanonical('http://cartour.travel/apartment/' . $apartment->id);
        $sort = ApartmentDetails::sortable();
        $details = ApartmentDetails::query()->where('apartment_id', '=', $id)->first()->toArray();
//        $sortDetails = [];
        foreach ($sort as $key => $value) {
            $sortDetails [$value] = $details[$value];
//            dd($key, $value);
        }


        return view('apartment.detail', compact('apartment', 'sortDetails'));
    }

    public function imgView(Request $request)
    {
        $data = $request->all();
        $picture = ApartmentPictures::query()
            ->where('apartment_id', '=', $data['id'])
            ->get();
        return $picture;


    }
}
