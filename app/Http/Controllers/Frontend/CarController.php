<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Car;
use App\Models\CarType;
use App\Repositories\GeneralRepository;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;

class CarController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;

    public function __construct(Car $car)
    {
        SEOMeta::setTitle('Cars');
        SEOMeta::setDescription('comfortable car');
        SEOMeta::setCanonical('http://cartour.travel/cars');
        
        $this->model = new GeneralRepository($car);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($type = null)
    {

        $date = date('Y-m-d');
        $query =  $this->model
            ->with('translateHasOne')
            ->with('picture')
            ->with('type');
        if ($type) {
            $query = $query->where('type_id', '=', $type);
        }
        $cars = $query->get();


        $types = CarType::query()->get();

        return view('car.car', compact('cars', 'types'));
    }

    public function detail($id)
    {
        $car = $this->model->with('picture')->findOrFail($id);

        return view('car.detail', compact('car'));
    }

    public function search(Request $request)
    {
        $carSearch = Car::query();
        if (isset($request->fuel)) {
            $carSearch->where('fuel', '=', $request->fuel);
        }
        if (isset($request->transmission_drive)) {
            $carSearch->where('transmission_drive', '=', $request->transmission_drive);
        }
        if (isset($request->doors_vehicle_type)) {
            $carSearch->where('doors_vehicle_type', '=', $request->doors_vehicle_type);
        }
        if (isset($request->car_category)) {
            $carSearch->where('car_category', '=', $request->car_category);
        }
        $carSearch
            ->with('picture')
            ->with('translateHasOne')
            ->with('type');
        $cars = $carSearch->get();
        $types = CarType::query()->get();
        return view('car.car', compact('cars', 'types'));
    }


}
