<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\CarSeat;
use App\Repositories\GeneralRepository;


class CarSeatController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;

    public function __construct(CarSeat $carSeat)
    {
        $this->model = new GeneralRepository($carSeat);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {


        $carSeats = CarSeat::query()
//            ->with('picture')
//            ->with('type')
            ->get();




        return view('car_seat.car_seat', compact('carSeats'));
    }




}
