<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\ForgeApi;


/**
 * Class LanguageController.
 */
class CurrencyController extends AbstractController
{
    const CURRENCY_USD = 'USD';
    const CURRENCY_EURO = 'EUR';
    const CURRENCY_RUB = 'RUB';
    const CURRENCY_AMD = 'AMD';

    const CURRENCY_USD_SYMBOL = '$';
    const CURRENCY_EURO_SYMBOL = '€';
    const LANGUAGE_RUB_SYMBOL = '₽';
    const LANGUAGE_AMD_SYMBOL = '֏';

    /**
     * Method for changing language of website
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change($currencyWeSell)
    {
        $api = new ForgeApi();
        $access_key = '72c030f508eb7a2fb6a3a2882a683b7c';


        try {
            $ch = curl_init('http://apilayer.net/api/live?access_key=' . $access_key);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $json = curl_exec($ch);
            curl_close($ch);
            $conversionResult = json_decode($json, true);

            $result = $conversionResult['quotes'];

            if (!$result) {
                \Log::warning('wasnt able to get exchange rate from API ' . self::class);


            }
        } catch (\Exception $e) {

            \Log::error($e);
        }
        if ($currencyWeSell == CurrencyController::CURRENCY_USD) {

            session()->put('currency', CurrencyController::CURRENCY_EURO);
            session()->put('currency_symbol', CurrencyController::CURRENCY_EURO_SYMBOL);
            if(isset($result)){
                $rate = $result['USD' . CurrencyController::CURRENCY_EURO];
                session()->put('rate', $rate);

            }
        }
        if ($currencyWeSell == CurrencyController::CURRENCY_EURO) {

            session()->put('currency', CurrencyController::CURRENCY_RUB);
            session()->put('currency_symbol', CurrencyController::LANGUAGE_RUB_SYMBOL);
            if(isset($result)) {
                $rate = $result['USD' . CurrencyController::CURRENCY_RUB];
                session()->put('rate', $rate);
            }
        }
        if ($currencyWeSell == CurrencyController::CURRENCY_RUB) {

            session()->put('currency', CurrencyController::CURRENCY_AMD);
            session()->put('currency_symbol', CurrencyController::LANGUAGE_AMD_SYMBOL);
            if(isset($result)) {
                $rate = $result['USD' . CurrencyController::CURRENCY_AMD];
                session()->put('rate', $rate);
            }
        }
        if ($currencyWeSell == CurrencyController::CURRENCY_AMD) {

            session()->put('currency', CurrencyController::CURRENCY_USD);
            session()->put('currency_symbol', CurrencyController::CURRENCY_USD_SYMBOL);
            if(isset($result)) {
                $rate = $result['USD' . CurrencyController::CURRENCY_USD];
                session()->put('rate', $rate);
            }
        }




        return redirect()->back();
    }
}
