<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Apartment;
use App\Models\ApartmentReservations;
use App\Models\Car;
use App\Models\CarType;
use App\Models\Hotel;
use App\Models\Slider;
use App\Models\Tour;
use App\Models\Transfer;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class HomeController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apartment_city = (Input::get('apartment_city')) ? Input::get('apartment_city') : 'Yerevan';
        $apartment_housing = Input::get('housing');
        $apartment_building = Input::get('building');
        $apartment_date_from = Input::get('apartment_date_from');
        $apartment_date_to = Input::get('apartment_date_to');
        $apartment_adult = (int)Input::get('apartment_adult');
        $apartment_child = (int)Input::get('apartment_child');

        $car_date_from = Input::get('car_date_from');
        $car_date_to = Input::get('car_date_to');
        $car_adult = (int)substr(Input::get('car_adult'), 0, 1);
        $car_child = (int)substr(Input::get('car_child'), 0, 1);

        $tour_type = Input::get('tour_type');
        $tour_date_from = Input::get('tour_date_from');
        $tour_date_to = Input::get('tour_date_to');
        $tour_adult = (int)Input::get('tour_adult');
        $tour_child = (int)Input::get('tour_child');

        $car_type_transfer = Input::get('car_type_transfer');
        $transfer_type = Input::get('transfer_type');
        $transfer_date_from = Input::get('transfer_date_from');
        $transfer_date_to = Input::get('transfer_date_to');
        $transfer_adult = (int)Input::get('transfer_adult');
        $transfer_child = (int)Input::get('transfer_child');

        $apartment = new Apartment();
        $hotel = new Hotel();
        $tour = new Tour();
        $car = new Car();
        if ($apartment_city || $apartment_adult || $apartment_child) {
            $guests = $apartment_adult + $apartment_child;
            $apartment_city = str_replace('/',' ',$apartment_city);
            $apartment = $apartment
                ->join('apartment_details', 'apartments.id', '=', 'apartment_details.apartment_id')
                ->where('apartment_details.number_of_quests', '>=', $guests)
                ->where(function ($query) use($apartment_city){
                    $query->where(DB::raw('concat(country," ",city)'), 'like', $apartment_city)
                        ->orWhere( 'apartments.city','like', $apartment_city)
                        ->orWhere('apartments.country','like', $apartment_city);
                });

        }

        if ($apartment_date_from || $apartment_date_to) {
            $ApartmentReservations = ApartmentReservations::where(function ($query) use($apartment_date_from,$apartment_date_to){
                if ($apartment_date_from && $apartment_date_to) {
                    $query->where('date_from','>=',strtotime($apartment_date_from)*1000)
                        ->where('date_to','<=',strtotime($apartment_date_to)*1000);
                } else if($apartment_date_from) {
                    $query->where('date_from','>=',strtotime($apartment_date_from)*1000)
                        ->where('date_to','<=',time()*1000);
                } else {
                    $query->where('date_to','<=',strtotime($apartment_date_to)*1000);
                }
            })->pluck('id');

            $apartment = $apartment
                ->join('apartment_reservations', 'apartments.id', '=', 'apartment_reservations.apartment_id')
                ->whereNotIn('apartment_reservations.id',$ApartmentReservations);
        }

        if ($apartment_housing) {
            $apartment = $apartment
                ->where('apartment_details.property_type', '=', $apartment_housing);
        }
        if ($apartment_building) {
            $apartment = $apartment
                ->where('apartment_details.property_type', '=', $apartment_building);
        }
        $apartments = $apartment
            ->with('prices')
            ->where('apartments.homepage_order', '=', '1')
            ->select('apartments.*')
            ->orderBy('apartments.id', 'DESC')
            ->limit(4)
            ->get();

        $hotels = $hotel->with(['prices' => function ($query) {

        }])
            ->orderBy('id', 'DESC')
            ->limit(4)
            ->get();


//        tour search
        if ($tour_type || $tour_date_from || $tour_date_to || $tour_adult || $tour_child) {
            if (empty(!$tour_type))
                $tour = $tour->where('type_id',$tour_type);
        }

        $tours = $tour
            ->orderBy('tours.id', 'DESC')
            ->limit(4)
            ->get();

//        Car search
        if ($car_date_from || $car_date_to || $car_adult || $car_child) {
            $persons = $car_adult + $car_child;
            $car = $car->where('persons', '>=', $persons);
//                ->where('type_id', '=', $car_type);
        }

        $cars = $car
            ->orderBy('id', 'DESC')
            ->limit(4)
            ->get();
        $transfers = [];
        if ($transfer_type || $car_type_transfer || $transfer_date_from || $transfer_date_to || $transfer_adult || $transfer_child) {
            $transfer_persons = $transfer_adult + $transfer_child;
            $transfers = Transfer::query()->where('persons', '>=', $transfer_persons)->get();
        }
        $slider = Slider::query()->first();
        $types = CarType::query()->get();
        return view('home', compact('apartments', 'hotels', 'tours', 'cars', 'types', 'slider', 'transfers'));
    }
}
