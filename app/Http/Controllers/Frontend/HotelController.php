<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Hotel;
use App\Repositories\GeneralRepository;

class HotelController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;

    public function __construct(Hotel $hotel)
    {
        $this->model = new GeneralRepository($hotel);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $date = date('Y-m-d');
        $hotels  = Hotel::query()
            ->with(['prices' => function ($query) use ($date) {

//                $query->where('from',  '<', $date);
//                $query->where('to', '>', $date);
            }])
            ->orderBy('id', 'DESC')
            ->get();
        return view('hotel.hotel', compact('hotels'));
    }

    public function detail($id)
    {
        $hotel = $this->model->with('picture')->findOrFail($id);

        return view('hotel.detail', compact('hotel'));
    }
}
