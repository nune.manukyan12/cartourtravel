<?php

namespace App\Http\Controllers\Frontend;

use App\Models\About;
use App\Services\EmailSendService;
use Illuminate\Http\Request;
use App\Models\Transfer;
use App\Repositories\GeneralRepository;


class InfoController extends AbstractController
{

    protected $model;

    public function __construct(Transfer $transfers)
    {
        $this->model = new GeneralRepository($transfers);
    }


    public function about()
    {
        $about = About::query()->firstOrFail();

        return view('about.about', compact('about'));
    }
    public function contact()
    {
        return view('contact.contact');
    }

    public function contactEmail(Request $request)
    {
        $data = $request->all();
        $send = new EmailSendService();

        $body = $send->generateEmailContact($data);
        $subject = 'Contact Us';
        $email = $send->send($subject, $body);

        if ($email) {
            return response()->json([
                'status' => 200,
                'message' => 'ok',
            ]);
        } else {

        }
    }

}
