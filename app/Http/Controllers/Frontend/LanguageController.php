<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;


/**
 * Class LanguageController.
 */
class LanguageController extends Controller
{

    const LANGUAGE_EN = 'en';
    const LANGUAGE_RU = 'ru';
    const LANGUAGE_HY = 'hy';

    /**
     * Method for changing language of website
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function swap($lang)
    {
        if ($lang == LanguageController::LANGUAGE_EN) {

            session()->put('locale', LanguageController::LANGUAGE_HY);
        } elseif ($lang == LanguageController::LANGUAGE_HY) {
            session()->put('locale', LanguageController::LANGUAGE_RU);
        } elseif ($lang == LanguageController::LANGUAGE_RU) {
            session()->put('locale', LanguageController::LANGUAGE_EN);
        }

        return redirect()->back();
    }
}
