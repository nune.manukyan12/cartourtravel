<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Medical;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\GeneralRepository;
use Artesaos\SEOTools\Facades\SEOMeta;
use App\Services\EmailSendService;
use App\Http\Requests\Transfer\StoreServiceRequest;
use App\Models\MedicalReservations;


class MedicalController  extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;

    public function __construct(Medical $medical)
    {
        SEOMeta::setTitle('Medical');
        SEOMeta::setDescription('MEDICAL TOURISM');
        SEOMeta::setCanonical('http://cartour.travel/medical/tourism');

        $this->model = new GeneralRepository($medical);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $medicals = $this->model->all();

        return view('medical.medical_service', compact('medicals'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details($id)
    {
        $medical = $this->model->find($id);
        $next = Medical::where('id', '<', $id)->orderBy('id','desc')->first();
        $prev = Medical::where('id', '>', $id)->orderBy('id','desc')->first();

        return view('medical.medical_details', compact('medical','next','prev'));
    }


    /**
     * @return \Illuminate\Http\Response
     */
    public function basket(Request $request)
    {

        if (session()->exists('medical_basket')) {
            if (in_array($request->id, session()->get('medical_basket'))) {
                $item = array_search($request->id, session()->get('medical_basket'));
                session()->pull('medical_basket'.".".$item);
            } else {
                session()->push('medical_basket',$request->id);
            }
        } else {
            session()->push('medical_basket',$request->id);
        }

        return Response()->json(count(session()->get('medical_basket')));
    }


    public function store_medical(StoreServiceRequest $request)
    {
        $data = $request->all();
        if (session()->exists('medical_basket')) {
            foreach (session()->get('medical_basket') as $medical) {
                if (!empty($medical)) {
                    $data['medical_id'] = $medical;
                    $data["status"] = MedicalReservations::STATUS_REQUEST;
                    $reservation = MedicalReservations::query()->create($data);

                    $email = new EmailSendService();
                    $body = $email->generateEmailCreateMedicalService($data);
                    $subject = 'Medical Service Booking';
                    $email->send($subject, $body);
                }
            }
        }

        session()->pull('medical_basket');
        return $reservation;

    }
}
