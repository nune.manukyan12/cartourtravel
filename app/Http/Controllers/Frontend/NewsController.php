<?php

namespace App\Http\Controllers\Frontend;

use App\Models\NewsFeed;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\GeneralRepository;
use Artesaos\SEOTools\Facades\SEOMeta;


class NewsController  extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;

    public function __construct(NewsFeed $newsFeed)
    {
        SEOMeta::setTitle('News');
        SEOMeta::setDescription('News Feed');
        SEOMeta::setCanonical('http://cartour.travel/news');

        $this->model = new GeneralRepository($newsFeed);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $news = $this->model->all();

        return view('news.news_feed', compact('news'));
    }
}
