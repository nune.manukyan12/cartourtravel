<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Reservation\StorePrivateRequest;
use App\Http\Requests\Reservation\StoreRequest;
use App\Http\Requests\Reservation\StoreCarRequest;

use App\Http\Requests\Reservation\StoreTourPackageRequest;
use App\Models\Apartment;
use App\Models\Reservation;
use App\Models\TourPackageReservation;
use App\Models\TourReservation;
use App\Services\EmailSendService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ApartmentReservations;
use App\Models\CarReservations;
use App\Models\Car;

use App\Models\ApartmentAvailable;
use App\Models\ApartmentPrices;


class ReservationController extends Controller
{
    public function store(StoreRequest $request)
    {
        $data = $request->all();
        $data['date_from'] = strtotime($data['date_from'])*1000;
        $data['date_to'] = strtotime($data['date_to']) * 1000;

        $data['status'] = ApartmentReservations::STATUS_REQUEST;
        $reservation = ApartmentReservations::query()->create($data);

        $email = new EmailSendService();
        $body = $email->generateEmailApartmentReservation($data);
        $subject = 'Reservation Apartment';
        $email->send($subject, $body);

        return $reservation;
    }


    public function calculation(Request $request)
    {

        $date_to = strtotime($request->input('date_to'));
        $date_form = strtotime($request->input('date_from'));

        //        interval@ gtnelu hamar
        $time_date = array();
        $interval = new \DateInterval('P1D');
        $date1 = new \DateTime($request->input('date_from'));
        $date2 = new \DateTime($request->input('date_to'));
        $datePeriod = new \DatePeriod($date1, $interval, $date2->modify('+1 day'));
//        $interval = $datePeriod->getDateInterval();
//        dd($interval);
        foreach ($datePeriod as $date) {
            $result = $date->format('d-m-Y');
            array_push($time_date, $result);
        }
        $id = $request->id;

        $block_date_server = array();
        $blockDay = array();
        $nightli_date_price = array();
        $nightlyDayPrice = array();


        $notAvailableDate = ApartmentAvailable::query()
            ->where('apartment_id', '=', $id)
            ->where('blocked', '=', ApartmentAvailable::BLOCKED)
            ->first();
//        if ($notAvailableDate) {
//            $block_day = array($notAvailableDate->date);
//            $block_day_date = json_decode($block_day[0]);
//            dd($block_day_date);
//            foreach ($block_day_date as $day){
//                if($day > $date_form ){
//
//                }
//            }
////            for ($m = 0; $m < count($block_day_date); $m++) {
////                $date = date_create_from_format("U", $block_day_date[$m] / 1000);
////                array_push($block_date_server, date_format($date, "d-m-Y"));
////            }
////            for ($i = 0; $i < count($time_date); $i++) {
////                if (in_array($time_date[$i], $block_date_server)) {
////                    array_push($blockDay, $time_date[$i]);
////                }
////            }
//        }

        $nightlyPrice = ApartmentAvailable::query()
            ->where('apartment_id', '=', $id)
            ->where('blocked', '=', ApartmentAvailable::ACTIVE)
            ->get();
//        dd($nightlyPrice);
//        $nightly_price = json_decode($nightlyPrice[0]);
        for ($y = 0; $y < count($nightlyPrice); $y++) {
            $json_nightly = $nightlyPrice[$y]->date;
            $nightly_price = json_decode($json_nightly);

            for ($t = 0; $t < count($nightly_price); $t++) {
                $time = date_create_from_format("U", $nightly_price[$t] / 1000);
                array_push($nightli_date_price, date_format($time, "d-m-Y"));


                if ($date_form > $nightly_price[$t] / 1000 && $date_to < $nightly_price[$t] / 1000) {
                    array_push($nightlyDayPrice, $nightlyPrice[$t]->nightly_price);
                }
            }
//            for ($x = 0; $x < count($time_date); $x++) {

//            }
            $nightli_date_price = array();
        }

        $this_price = ApartmentPrices::query()->where('parent_id', '=', $id)->first();
//dd($nightlyDayPrice);
//dd($time_date,count($time_date));
        $final_price=0;
        if (Apartment::BOOK_INTERVAL_MONTH < (count($time_date) - 1) && 32 > (count($time_date) - 1)) {

            if ($this_price->monthly_price) {

                $discount_mountly = $this_price->monthly_price * $this_price->monthly_discounts / 100;
                $final_price = $this_price->monthly_price - $discount_mountly;
            } else {
                $discount_mountly = $this_price->base_price * $this_price->monthly_discounts / 100;
                $final_price = $this_price->base_price - $discount_mountly;
            }
        } else {
            if (Apartment::BOOK_INTERVAL_TWO_WEEK < (count($time_date) - 1)) {

                $basePriceDayCount = count($time_date) - 1;
                $total_price = $basePriceDayCount * $this_price->base_price;
                $discount_two_weekly = $total_price * $this_price->two_weekly_discounts / 100;
                $final_price = $total_price - $discount_two_weekly;
//                $all_nightly_price = 0;
//                $count_nightly_day = count($nightlyDayPrice);
//                $count_block_day = count($blockDay);
//                $count_block_nightly_day = $count_nightly_day + $count_block_day;
//                $basePriceDayCount = count($time_date) - $count_block_nightly_day;
//                $bacePriceDay = $basePriceDayCount * $this_price->base_price;

//                for ($v = 0; $v < count($nightlyDayPrice); $v++) {
//                    $all_nightly_price += $nightlyDayPrice[$v];
//                }
//                $all_price = $all_nightly_price + $bacePriceDay;
//                $discount_two_weekly = $all_price * $this_price->two_weekly_discounts / 100;
//                $final_price = $all_price - $discount_two_weekly;

            } else {
                if (6 < count($time_date) && (count($time_date) -1) < 8) {

//                    $all_nightly_price = 0;
//                    $count_nightly_day = count($nightlyDayPrice);
//                    $count_block_day = count($blockDay);
//                    $count_block_nightly_day = $count_nightly_day + $count_block_day;
//                    $basePriceDayCount = count($time_date) - $count_block_nightly_day;
//                    $bacePriceDay = $basePriceDayCount * $this_price->base_price;
//                    for ($v = 0; $v < count($nightlyDayPrice); $v++) {
//                        $all_nightly_price += $nightlyDayPrice[$v];
//                    }
                    $basePriceDayCount = count($time_date) - 1;
//                    dd();
                    $total_price = $basePriceDayCount * $this_price->base_price;
                    $discount_two_weekly = $total_price * $this_price->weekly_discounts / 100;
                    $final_price = $total_price - $discount_two_weekly;
//                    $all_price = $all_nightly_price + $bacePriceDay;
//                    $discount_weekly = $all_price * $this_price->weekly_discounts / 100;
//                    $final_price = $all_price - $discount_weekly;

                } else {
                    if (count($time_date) == 2) {

                        $basePriceDayCount = count($time_date);
                    } else {
                        $basePriceDayCount = count($time_date) - 1;
                    }
                    if (count($nightlyDayPrice)) {
                        $total_price = 0;
                        foreach ($nightlyDayPrice as $price) {
                            $basePriceDayCount = count($time_date) - 1;
                            $total_price += $price;
                        }
                        $final_price = $total_price;
                        $count_nightly_day = count($nightlyDayPrice);
                        $basePriceDayCount = $basePriceDayCount - $count_nightly_day;
                    }
                    $final_price = $final_price + ($basePriceDayCount * $this_price->base_price);
//                    $all_nightly_price = 0;
//                    $count_nightly_day = count($nightlyDayPrice);
//                    $count_block_day = count($blockDay);
//                    $count_block_nightly_day = $count_nightly_day + $count_block_day;
//                    $basePriceDayCount = count($time_date) - $count_block_nightly_day;
//                    $bacePriceDay = $basePriceDayCount * $this_price->base_price;
//                    for ($v = 0; $v < count($nightlyDayPrice); $v++) {
//                        $all_nightly_price += $nightlyDayPrice[$v];
//                    }
//                    $final_price = $all_nightly_price + $bacePriceDay;
                }
            }

        }
//        $return['block_day'] = $blockDay;
//        $return['final_price'] = $final_price;

         if(\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$final_price)){
             $return['symbol'] = strtoupper(session()->get('currency_symbol'));
             $return['final_price'] = \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$final_price);
         }else {
             $return['symbol'] = '$';
             $return['final_price'] = $final_price;
         }
        return $return;


    }

    public function store_car(StoreCarRequest $request)
    {
        $data = $request->all();
        $data["status"] = CarReservations::STATUS_REQUEST;
        $data['type'] =  CarReservations::TYPE_RESERVATION;
        $reservation = CarReservations::query()->create($data);

        $email = new EmailSendService();
        $body = $email->generateEmailCarReservation($data);
        $subject = 'Reservation Car';
        $email->send($subject, $body);
        return $reservation;


    }

    /**
     * @param StoreTourPackageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function packageSave(StoreTourPackageRequest $request)
    {
        $data = $request->all();
        $reservation = TourPackageReservation::query()->create($data);

        $email = new EmailSendService();
        $body = $email->generateEmailCreateTourPackage($data);
        $subject = 'Create tour package';
        $email->send($subject, $body);
        if ($reservation) {

            return response()->json([
                'status' => 200
            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }

    public function tourSave(StorePrivateRequest $request)
    {
        $data = $request->all();

        $tour = json_decode($data['tour']);

        if (is_array($tour)) {
            foreach ($tour as $value) {

                $data['tour_destination_id'] = $value->id;
                if ($value->guide == 'true') {
                    $data['guide'] = true;

                } elseif ($value->guide == 'false') {
                    $data['guide'] = false;

                }

                $data['status'] = TourReservation::STATUS_REQUEST;
                if ($value->type_car == 'sedan') {
                    $data['car_type'] = Car::SEDAN;

                } elseif ($value->type_car == 'minivan') {
                    $data['car_type'] = Car::MINIVAN;

                } elseif ($value->type_car == 'mini_bus') {
                    $data['car_type'] = Car::MINI_BUS;

                } elseif ($value->type_car == 'bus') {
                    $data['car_type'] = Car::BUS;

                }

                $response = TourReservation::query()->create($data);


            }
        } else {
            $data['tour_destination_id'] = $tour;
            $response = TourReservation::query()->create($data);
        }
        if ($response) {
            $email = new EmailSendService();

            $body = $email->generateEmailCreatePrivateTour($data);

            $subject = 'Book private tour';
            $email->send($subject, $body);
        }
        return response()->json([
            'message' => 'success',
            'status' => 'ok',
        ]);


    }
}
