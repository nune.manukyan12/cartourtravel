<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Apartment;
use App\Models\ApartmentDetails;
use App\Models\Car;
use App\Models\TourDestination;
use App\Models\Tour;
use App\Repositories\GeneralRepository;
use App\Services\CountPriceService;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;

class TourController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;

    public function __construct(Tour $tour)
    {
        SEOMeta::setTitle('Tours');
        SEOMeta::setDescription('Regular and Private Tour');
        SEOMeta::setCanonical('http://cartour.travel/tour/2');

        $this->model = new GeneralRepository($tour);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($type)
    {

        if (Tour::TYPE_EXTREAM == $type) {
            return view('tour.extream_tour');
        }

        $tours = Tour::query()
            ->where('type_id', '=', $type)
            ->with('destination')
            ->with('type')
            ->orderBy('id', 'DESC')
            ->first();
//        dd($type);
        if ($type == Tour::TYPE_PRIVATE || $type == Tour::TYPE_REGULAR) {

            $carSedan = Car::query()
                ->where('type_id', '=', Car::SEDAN)
                ->with('picture')
                ->get();

            $carMinivan = Car::query()
                ->where('type_id', '=', Car::MINIVAN)
                ->with('picture')
                ->get();

        } else {
            $carSedan = null;
            $carMinivan = null;
        }
        if ($type == Tour::TYPE_PRIVATE) {
//            dd($tours->destination);
            return view('tour.tour', compact('tours', 'carSedan', 'carMinivan'));
        } elseif ($type == Tour::TYPE_REGULAR) {
            return view('tour.regular', compact('tours', 'carSedan', 'carMinivan'));

        }

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $tour = $this->model->with('picture')->findOrFail($id);

        return view('tour.detail', compact('tour'));
    }

    /**
     * @return array
     */
    public function createPackage()
    {
//        return view('tour.create_package');
        $apartment = new Apartment();
        $tour = new Tour();
        $apartmentTourCreate = $apartment
            ->select('apartments.id',  'apartment_details.property_type', 'apartments.general_pic', 'apartments.address', 'apartments.city')
            ->join('apartment_details', 'apartments.id', '=', 'apartment_details.apartment_id')
            ->where('apartment_details.property_type', '=', 'building')
            ->with('translateHasOne')
            ->get();
        $houseTourCreate = $apartment
            ->select('apartments.id',  'apartment_details.property_type', 'apartments.general_pic', 'apartments.address', 'apartments.city')
            ->join('apartment_details', 'apartments.id', '=', 'apartment_details.apartment_id')
            ->where('apartment_details.property_type', '=', 'house')
            ->with('translateHasOne')
            ->get();
        $tourPrivate = $tour
            ->where('type_id', '=', Tour::TYPE_PRIVATE)
            ->with('destination')
            ->get();
//        dd($tourPrivate);
        $tourRegular = $tour
            ->where('type_id', '=', Tour::TYPE_REGULAR)
            ->with('destination')
            ->get();
        $tourExtream = $tour
            ->where('type_id', '=', Tour::TYPE_EXTREAM)
            ->with('destination')
            ->get();
        return view('tour.create_package', compact( 'tourPrivate', 'tourRegular', 'tourExtream', 'apartmentTourCreate', 'houseTourCreate'));

    }

    /**
     * @param $fromdate
     * @param $todate
     * @return \DatePeriod
     */
    private function returnDates($fromdate, $todate)
    {
        $fromdate = \DateTime::createFromFormat('d/m/Y', $fromdate);
        $todate = \DateTime::createFromFormat('d/m/Y', $todate);
        return new \DatePeriod(
            $fromdate,
            new \DateInterval('P1D'),
            $todate->modify('+1 day')
        );
    }

    /**
     * @param Request $request
     * @return array
     */


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function apartmentPackage($id)
    {
        $apartment = Apartment::query()
            ->with(['picture' => function ($query) use ($id) {
                $query->where('apartment_id', '=', $id);
            }])
            ->with(['details' => function ($query) {
                $query->sortable();
                $query->with('bedrooms');
            }])
            ->with('users')
            ->with('available_apartment')
            ->findOrFail($id);
        $sort = ApartmentDetails::sortable();
        $details = ApartmentDetails::query()->where('apartment_id', '=', $id)->first()->toArray();

        foreach ($sort as $key => $value) {
            $sortDetails [$value] = $details[$value];

        }
        return view('_partial.package_apartment', compact('apartment', 'sortDetails'));
    }

    public function apartmentPricePackage(Request $request)
    {
        $data = $request->all();
        $priceService = new CountPriceService();
        $apartmentPrice = $priceService->apartmentPrice($data['id'], $data['dateArrival'], $data['dateDeparture']);

        return $apartmentPrice;
    }

    public function tourPricePackage(Request $request)
    {
        $data = $request->all();
//        dd($data);
        $priceService = new CountPriceService();
        $tourPrice = $priceService->tourPrice($data);

        return $tourPrice;
    }

    public function getDestinationInfo($id)
    {
        $destinationInfo = TourDestination::query()
            ->join('tour_destination_picture', 'tour_destination.id', '=', 'tour_destination_picture.tour_destination_id')
            ->where('tour_destination.id', '=', $id)
            ->select('tour_destination_picture.*', 'tour_destination.description')->get();

        return response()->json([
            'destinationInfo' => $destinationInfo
        ]);

    }


}
