<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Tourism;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\GeneralRepository;
use Artesaos\SEOTools\Facades\SEOMeta;
use App\Services\EmailSendService;
use App\Http\Requests\Transfer\StoreServiceRequest;
use App\Models\TourismReservations;


class TourismController  extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;

    public function __construct(Tourism $tourism)
    {
        SEOMeta::setTitle('Tourism');
        SEOMeta::setDescription('MEDICAL TOURISM');
        SEOMeta::setCanonical('http://cartour.travel/tourism/tourism');

        $this->model = new GeneralRepository($tourism);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $tourisms = $this->model->all();

        return view('tourism.tourism_service', compact('tourisms'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details($id)
    {
        $tourism = $this->model->find($id);
        $next = Tourism::where('id', '<', $id)->orderBy('id','desc')->first();
        $prev = Tourism::where('id', '>', $id)->orderBy('id','desc')->first();

        return view('tourism.tourism_details', compact('tourism','next','prev'));
    }


    /**
     * @return \Illuminate\Http\Response
     */
    public function basket(Request $request)
    {

        if (session()->exists('tourism_basket')) {
            if (in_array($request->id, session()->get('tourism_basket'))) {
                $item = array_search($request->id, session()->get('tourism_basket'));
                session()->pull('tourism_basket'.".".$item);
            } else {
                session()->push('tourism_basket',$request->id);
            }
        } else {
            session()->push('tourism_basket',$request->id);
        }

        return Response()->json(count(session()->get('tourism_basket')));
    }


    public function store_tourism(StoreServiceRequest $request)
    {
        $data = $request->all();
        if (session()->exists('tourism_basket')) {
            foreach (session()->get('tourism_basket') as $tourism) {
                if (!empty($tourism)) {
                    $data['tourism_id'] = $tourism;
                    $data["status"] = TourismReservations::STATUS_REQUEST;
                    $reservation = TourismReservations::query()->create($data);

                    $email = new EmailSendService();
                    $body = $email->generateEmailCreateTourismService($data);
                    $subject = 'Tourism Service Booking';
                    $email->send($subject, $body);
                }
            }
        }

        session()->pull('tourism_basket');
        return $reservation;

    }
}
