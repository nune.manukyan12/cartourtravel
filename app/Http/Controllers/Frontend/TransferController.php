<?php

namespace App\Http\Controllers\Frontend;

use App\Models\ChauffeurServices;
use App\Models\Transfer;
use App\Models\CarReservations;
use App\Repositories\GeneralRepository;
use Artesaos\SEOTools\Facades\SEOMeta;
use App\Services\EmailSendService;
use App\Http\Requests\Transfer\StoreServiceRequest;



class TransferController extends AbstractController
{

    protected $model;

    public function __construct(Transfer $transfers)
    {
        SEOMeta::setTitle('Transfer and Chauffeur');
        SEOMeta::setDescription('Transfer and Chauffeur Services');
        SEOMeta::setCanonical('http://cartour.travel/transfer');
        $this->model = new GeneralRepository($transfers);
    }


    public function show()
    {
        $transfers = $this->model->all();
        $chauffeurService = ChauffeurServices::query()->first();
        return view('transfer.transfer', compact('transfers', 'chauffeurService'));
    }

    public function transferDetails($id)
    {
        $transfer = $this->model->find($id);
        $chauffeur_service = ChauffeurServices::query()->first();

        return view('transfer.transfer_details', compact('transfer', 'chauffeur_service'));

    }

    public function chauffeurDetails($id)
    {

        $chauffeur_service = ChauffeurServices::query()->findOrFail($id);
        $transfer = $this->model->first();

        return view('transfer.chauffeur_service_details', compact('chauffeur_service', 'transfer'));

    }


    public function store_transfer(StoreServiceRequest $request)
    {
        $data = $request->all();
        $data["status"] = CarReservations::STATUS_REQUEST;
        $data['type'] =  CarReservations::TYPE_TRANSFER;
        $reservation = CarReservations::query()->create($data);

        $email = new EmailSendService();
        $body = $email->generateEmailTransferBooking($data);
        $subject = 'Transfer Booking';
        $email->send($subject, $body);
        return $reservation;

    }


    public function store_chauffeur(StoreServiceRequest $request)
    {
        $data = $request->all();
        $data["status"] = CarReservations::STATUS_REQUEST;
        $data['type'] =  CarReservations::TYPE_CHAUFFEUR;
        $reservation = CarReservations::query()->create($data);

        $email = new EmailSendService();
        $body = $email->generateEmailChauffeurBooking($data);
        $subject = 'Chauffeur Booking';
        $email->send($subject, $body);
        return $reservation;

    }

}
