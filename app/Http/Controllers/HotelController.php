<?php

namespace App\Http\Controllers;

use App\Http\Requests\Hotel\UpdateRequest;
use App\Http\Requests\Tour\StoreRequest;
use App\Models\AjaxPictures;
use App\Models\Hotel;
use App\Models\HotelPictures;
use App\Models\HotelPrices;
use App\Repositories\GeneralRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HotelController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $model;

    public function __construct(Hotel $hotel)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($hotel);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotels = $this->model->all();
        return view('admin.hotel.hotels', compact('hotels'));
    }

    public function create()
    {
        return view('admin.hotel.create');
    }

    public function store(StoreRequest $request)
    {
        $data = $request->only($this->model->getModel()->fillable);
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
//            $events->general_pic = $general_pic_name;
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }

        $hotelStore = $this->model->create($data);

//        $HotelPrices = new HotelPrices();
//        $this->savePrice($request, $HotelPrices, $hotelStore->id);


        $this->storePictures($hotelStore->id);
        return $hotelStore;

//        if ($hotelStore) {
//            return \redirect('/admin/hotel');
//        } else {
//
//            return $this->model->create($request->only($this->model->getModel()->fillable));
//        }
    }

    public function show($id)
    {
        $hotel = $this->model
            ->with('picture')
            ->with('prices')
            ->findOrFail($id);

        return view('admin.hotel.update', compact('hotel'));
    }

    public function update(UpdateRequest $request)
    {
        $id = $request->input('id');
        $data = $request->only($this->model->getModel()->fillable);
        // general picture
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }
        $updateHotel = $this->model->update($data, $id);

//        $hotelPrices = new HotelPrices();
//        $hotelPrices->where('parent_id', '=', $id)->delete();
//        $this->savePrice($request, $hotelPrices, $id);

        $this->storePictures($id);

        if ($updateHotel) {

            return response()->json([
                'status' => 200
            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }

    public function destroy($id)
    {
        return $this->model->delete($id);
    }

    public function storePictures($last_id)
    {
        $pictures = AjaxPictures::query()->get();

        foreach ($pictures as $picture) {
            $data_of_files = [
                'hotel_id' => $last_id,
                'name' => $picture->pictures,
            ];
            HotelPictures::query()->insert($data_of_files);
        }

        AjaxPictures::query()->delete();
    }

    public function deleteImage($id)
    {
        $model = new HotelPictures();

        $repository = new GeneralRepository($model);
        $response = $repository->delete($id);

        return $response;

    }

    protected function savePrice($request, $model, $id)
    {
//dd($request->all());
        foreach ($request->input('single_price') as $key => $value) {
            if ($request->input('single_price')[$key]) {
                $create = $model->create([
                    'from' => isset($request->input('date_from')[$key]) ? $request->input('date_from')[$key] : null,
                    'to' => isset($request->input('date_to')[$key]) ? $request->input('date_to')[$key] : null,
                    'single_price' => $request->input('single_price')[$key],
                    'double_price' => $request->input('double_price')[$key],
                    'superior_price' => $request->input('superior_price')[$key],
                    'superior_double_price' => $request->input('superior_double_price')[$key],
                    'family_price' => $request->input('family_price')[$key],
                    'executive_price' => $request->input('executive_price')[$key],
                    'deluxe_price' => $request->input('deluxe_price')[$key],
                    'deluxe_double_price' => $request->input('deluxe_double_price')[$key],
                    'currency' => $request->input('currency')[$key],
                    'parent_id' => $id
                ]);
            }
        }

        return $create;
    }
    public function approval($id)
    {

        $hotel = Hotel::query()
            ->where('id', '=', $id)
            ->update(['status' => Hotel::STATUS_ACTIVE]);
        return $hotel;


    }
}
