<?php

namespace App\Http\Controllers;


use App\Models\About;
use App\Repositories\GeneralRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class InfoController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $model;

    public function __construct(About $about)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($about);
    }

    public function about()
    {
        $about = $this->model->first();

        return view('admin.about', compact('about'));

    }

    public function storeAbout(Request $request)
    {
        $data = $request->all();
//dd()
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }
        if ($data['about_id']) {
            $response = $this->model->update($data, $data['about_id']);
        } else {
            $response = $this->model->create($data);
        }
        if ($response) {

            return response()->json([
                'status' => 200,
                'message' => 'OK',
            ]);
        }else{
            return response()->json([
                'status' => 500,
                'message' => 'Failed',
            ]);
        }

    }

    public function contact($id)
    {

    }


}
