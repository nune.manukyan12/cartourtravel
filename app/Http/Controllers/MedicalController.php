<?php

namespace App\Http\Controllers;

use App\Models\Medical;
use Illuminate\Http\Request;
use App\Repositories\GeneralRepository;
use App\Http\Requests\Medical\StoreRequest;
use App\Http\Requests\Medical\UpdateRequest;
use Illuminate\Support\Facades\Input;

class MedicalController extends Controller
{
    protected $model;

    public function __construct(Medical $medical)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($medical);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $medicals = $this->model->all();
        return view('admin.medical.medical', compact('medicals'));
    }

    public function show($id)
    {
        $medical = $this->model->find($id);

        return view('admin.medical.update', compact('medical'));
    }

    public function update(UpdateRequest $request)
    {
        $id = $request->input('id');

        $medicalUpdate = Medical::find($id);
        $medicalUpdate->name = $request->input('name');
        $medicalUpdate->description = $request->input('description');

        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('img', $general_pic_name);


            $medicalUpdate->img = $general_pic_name;
        }

        if ($medicalUpdate->save()) {

            return response()->json([
                'id' => $id
            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }


    public function create()
    {

        return view('admin.medical.create');
    }


    public function store(StoreRequest $request)
    {

        $medicalStore = new Medical();
        $medicalStore->name = $request->input('name');
        $medicalStore->description = $request->input('description');

        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('img', $general_pic_name);


            $medicalStore->img = $general_pic_name;
        }


        if ($medicalStore->save()) {
            return response()->json([
                'id' => $medicalStore->id
            ]);
        } else {

            return response()->json([
                'status' => 500
            ]);
        }
    }

    public function destroy($id)
    {
        return $this->model->delete($id);
    }
}
