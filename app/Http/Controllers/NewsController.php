<?php

namespace App\Http\Controllers;

use App\Models\NewsFeed;
use Illuminate\Http\Request;
use App\Repositories\GeneralRepository;
use App\Http\Requests\News\StoreRequest;
use App\Http\Requests\News\UpdateRequest;
use Illuminate\Support\Facades\Input;

class NewsController extends Controller
{
    protected $model;

    public function __construct(NewsFeed $newsFees)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($newsFees);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $news = $this->model->all();
        return view('admin.news.news_feed', compact('news'));
    }

    public function show($id)
    {
        $news = $this->model->find($id);

        return view('admin.news.update', compact('news'));
    }

    public function update(UpdateRequest $request)
    {
        $id = $request->input('id');

        $newsUpdate = NewsFeed::find($id);
        $newsUpdate->name = $request->input('name');
        $newsUpdate->description = $request->input('description');

        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('img', $general_pic_name);


            $newsUpdate->img = $general_pic_name;
        }

        if ($newsUpdate->save()) {

            return response()->json([
                'id' => $id
            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }


    public function create()
    {

        return view('admin.news.create');
    }


    public function store(StoreRequest $request)
    {

        $newsStore = new NewsFeed();
        $newsStore->name = $request->input('name');
        $newsStore->description = $request->input('description');

        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('img', $general_pic_name);


            $newsStore->img = $general_pic_name;
        }


        if ($newsStore->save()) {
            return response()->json([
                'id' => $newsStore->id
            ]);
        } else {

            return response()->json([
                'status' => 500
            ]);
        }
    }

    public function destroy($id)
    {
        return $this->model->delete($id);
    }
}
