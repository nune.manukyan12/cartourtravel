<?php

namespace App\Http\Controllers;


use App\Models\Slider;
use App\Repositories\GeneralRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SliderController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $model;

    public function __construct(Slider $slider)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($slider);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider_Photos = $this->model->all();
        return view('admin.slider.slider', compact('slider_Photos'));
    }

    public function store(Request $request)
    {
        $file = Input::file('file');
        $filename = time() . $file->getClientOriginalName();

        $file->move('uploads', $filename);
        $data = [
            'photo' => $filename
        ];
            $store = $this->model->create($data);

            return $store;

    }
    public function delete($id)
    {
        $delete = $this->model->delete($id);

    return $delete;
    }


}
