<?php

namespace App\Http\Controllers;

//use App\Http\Controllers\Controller;

use App\Http\Requests\Tour\StoreRequest;
use App\Http\Requests\Tour\UpdateRequest;
use App\Models\AjaxPictures;
use App\Models\Apartment;
use App\Models\Tour;
use App\Models\TourDestinationPicture;
use App\Models\TourPackageReservation;
use App\Models\TourPictures;
use App\Models\TourDestination;

use App\Models\TourPrices;
use App\Models\TourType;
use App\Repositories\GeneralRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mockery\Exception;

class TourController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $model;

    public function __construct(Tour $tour)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($tour);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tours = $this->model->all();
        return view('admin.tour.tours', compact('tours'));
    }

    public function indexPackage()
    {
        $packages = TourPackageReservation::query()->get();
        return view('admin.tour.package.tourPackage', compact('packages'));

    }

    public function showPackage($id)
    {
        $apartment = null;
        $house = null;
        $tourId = array();
        $destinations = null;
        $data = TourPackageReservation::query()
            ->findOrFail($id);

        if ($data->apartment) {
            $apartment = Apartment::query()
                ->findOrFail($data->apartment);
        }
        if ($data->house) {
            $house = Apartment::query()
                ->findOrFail($data->house);
        }
        $tour = json_decode($data->tour);
        foreach ($tour as $value) {
            foreach ($value as $id) {
                if (gettype($id) == "string") {
                    if (in_array($id, $tourId)) {

                    } else {
                        array_push($tourId, $id);
                        $destinations = TourDestination::query()
                            ->whereIn('id', $tourId)
                            ->get();
                    }

                }

            }
        }


        return view('admin.tour.package.detail', compact('data', 'apartment', 'house', 'destinations', 'tour'));
    }

    public function create()
    {
        $types = TourType::query()->get();

        return view('admin.tour.create', compact('types'));
    }

    public function store(StoreRequest $request)
    {
        $data = $request->only($this->model->getModel()->fillable);
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }
        $tourStore = $this->model->create($data);

        $this->storeDestination($tourStore->id, $data, $tourStore->type_id);
        $tourPrices = new TourPrices();
        $this->savePrice($request, $tourPrices, $tourStore->id);
        $this->storePictures($tourStore->id);
        if ($tourStore) {
            return response()->json([
                'id' => $tourStore->id
            ]);
        } else {

            return response()->json([
                'status' => 500
            ]);
        }
    }

    public function storeDestination($last_id, $data, $type)
    {
        if ($type == Tour::TYPE_REGULAR) {
            if (isset($data['regular_tour_destination'])) {
                foreach ($data['regular_tour_destination'] as $key => $value) {
                    $data_of_destination = [
                        'parent_id' => $last_id,
                        'destination' => $value,
                        'km' => $data['regular_km'][$key],
                        'duration' => $data['regular_duration'][$key],
                        'date' => $data['regular_date'][$key],
                        'clock' => $data['regular_clock'][$key],
                        'price_person' => $data['regular_price_person'][$key],
                    ];
                    if (isset($data['destination_id'][$key])) {

                        TourDestination::query()->updateOrCreate(
                            ['id' => $data['destination_id'][$key]],
                            $data_of_destination);
                    } else {
                        TourDestination::query()->create($data_of_destination);
                    }
                }
            }

        } elseif ($type == Tour::TYPE_PRIVATE) {
            if (isset($data['tour_destination'])) {
                foreach ($data['tour_destination'] as $key => $value) {
                    $data_of_destination = [
                        'parent_id' => $last_id,
                        'destination' => $value,
                        'km' => $data['km'][$key],
                        'duration' => $data['duration'][$key],
                        'sedan_price' => $data['sedan_price'][$key],
                        'sedan_price_guide' => $data['sedan_price_guide'][$key],
                        'minivan_price' => $data['minivan_price'][$key],
                        'minivan_price_guide' => $data['minivan_price_guide'][$key],
                        'mini_bus' => $data['mini_bus'][$key],
                        'mini_bus_guide' => $data['mini_bus_guide'][$key],
                        'bus' => $data['bus'][$key],
                        'bus_guide' => $data['bus_guide'][$key],
                    ];
                    if (isset($data['destination_id'][$key])) {

                        TourDestination::query()->updateOrCreate(
                            ['id' => $data['destination_id'][$key]],
                            $data_of_destination);
                    } else {
                        TourDestination::query()->create($data_of_destination);
                    }
                }
            }

        } else {

        }


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeDestinationInfo(Request $request)
    {
        $parent_id = $request->input('parent_id');
        $description = $request->input('description');
//        TourDestinationPicture::query()->where('tour_destination_id', '=', $parent_id)->delete();

        TourDestination::query()->where('id', '=', $parent_id)->update([
            'description' => $description
        ]);
        try {
            if ($request->file()) {
                foreach ($request->file() as $files) {
                    foreach ($files as $file) {
                        $pic_name_realy = $file->getClientOriginalName();
                        $pic_name = time() . $file->getClientOriginalName();
                        $file->move('uploads', $pic_name);
                        $data['tour_destination_id'] = $parent_id;
                        $data['picture'] = $pic_name;
                        $data['realy_picture'] = $pic_name_realy;
                        $tourDestinationPicture = TourDestinationPicture::query()->create($data);

                    }
                }
            }
            return response()->json([
                'message' => 'Message: OK',
                'status' => 'success',]);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Message: ' . $e->getMessage(),
                'status' => 'failed',
            ]);
        }
    }

    public function showDestinationInfo($id)
    {
        $destinationPictures = TourDestinationPicture::query()->where('tour_destination_id', '=', $id)->get();
        $description = TourDestination::query()->where('id', '=', $id)->first();

        return response()->json([
            'destinationPictures' => $destinationPictures,
            'description' => $description,
        ]);


    }

    public function show($id)
    {
        $tour = $this->model
            ->with('picture')
            ->with('prices')
            ->findOrFail($id);

        $types = TourType::query()->get();

        return view('admin.tour.update', compact('tour', 'types'));
    }

    public function update(UpdateRequest $request)
    {
        $id = $request->input('id');
        $data = $request->only($this->model->getModel()->fillable);
        // general picture
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }
        $updateTour = $this->model->update($data, $id);

        $tourPrices = new TourPrices();
        $tourPrices->where('parent_id', ' = ', $id)->delete();
        $this->savePrice($request, $tourPrices, $id);
        $tourDestination = new TourDestination();
        $tourDestination->where('parent_id', ' = ', $id)->delete();
        $this->storeDestination($id, $data, $data['type_id']);

        $this->storePictures($id);

        if ($updateTour) {

            return response()->json([
                'id' => $id
            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }

    public function destroy($id)
    {
        return $this->model->delete($id);
    }

    public function storePictures($last_id)
    {
        $pictures = AjaxPictures::query()->get();

        foreach ($pictures as $picture) {
            $data_of_files = [
                'tour_id' => $last_id,
                'name' => $picture->pictures,
            ];
            TourPictures::query()->insert($data_of_files);
        }

        AjaxPictures::query()->delete();
    }

    public function deleteImage($id)
    {
        $model = new TourPictures();

        $repository = new GeneralRepository($model);
        $response = $repository->delete($id);

        return $response;

    }

    public function approval($id)
    {
        $tour = Tour::query()
            ->where('id', ' = ', $id)
            ->update(['status' => Tour::STATUS_ACTIVE]);
        return $tour;


    }

    public function deleteDestination(Request $request)
    {
        $data = $request->all();
        $tourDestination = new TourDestination();
        $tourDestination->where('id', '=', $data['id'])->delete();
        if ($tourDestination) {
            return response()->json([
                'status' => 200
            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }
}
