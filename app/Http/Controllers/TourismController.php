<?php

namespace App\Http\Controllers;

use App\Models\Tourism;
use Illuminate\Http\Request;
use App\Repositories\GeneralRepository;
use App\Http\Requests\Tourism\StoreRequest;
use App\Http\Requests\Tourism\UpdateRequest;
use Illuminate\Support\Facades\Input;

class TourismController extends Controller
{
    protected $model;

    public function __construct(Tourism $tourism)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($tourism);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tourisms = $this->model->all();
        return view('admin.tourism.tourism', compact('tourisms'));
    }

    public function show($id)
    {
        $tourism = $this->model->find($id);

        return view('admin.tourism.update', compact('tourism'));
    }

    public function update(UpdateRequest $request)
    {
        $id = $request->input('id');

        $tourismUpdate = Tourism::find($id);
        $tourismUpdate->name = $request->input('name');
        $tourismUpdate->description = $request->input('description');

        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('img', $general_pic_name);


            $tourismUpdate->img = $general_pic_name;
        }

        if ($tourismUpdate->save()) {

            return response()->json([
                'id' => $id
            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }


    public function create()
    {

        return view('admin.tourism.create');
    }


    public function store(StoreRequest $request)
    {

        $tourismStore = new Tourism();
        $tourismStore->name = $request->input('name');
        $tourismStore->description = $request->input('description');

        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('img', $general_pic_name);


            $tourismStore->img = $general_pic_name;
        }


        if ($tourismStore->save()) {
            return response()->json([
                'id' => $tourismStore->id
            ]);
        } else {

            return response()->json([
                'status' => 500
            ]);
        }
    }

    public function destroy($id)
    {
        return $this->model->delete($id);
    }
}
