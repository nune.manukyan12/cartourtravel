<?php

namespace App\Http\Controllers;


use App\Http\Requests\Transfer\StoreRequest;
use App\Http\Requests\Transfer\UpdateRequest;
use App\Models\AjaxPictures;
use App\Models\ChauffeurServices;
use App\Models\Transfer;
use App\Models\TransferPictures;
use App\Models\TransferPrices;
use App\Repositories\GeneralRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TransferController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $model;

    public function __construct(Transfer $transfer)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($transfer);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transfers = $this->model->all();
        return view('admin.transfer.transfer', compact('transfers'));
    }

    public function create()
    {

        return view('admin.transfer.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreRequest $request)
    {

        $data = $request->only($this->model->getModel()->getFillable());

        // general picture
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }
//        $transferStore = $this->model->create($data);

        $transferStore = new Transfer();
        $transferStore->persons = $data['persons'];
        $transferStore->name = $data['name'];
        $transferStore->description = $data['description'];
        $transferStore->general_pic =  $data['general_pic'];
        $transferStore->admin_id = \Auth::user()->id;
        $transferStore->save();

        $transferPrices = new TransferPrices();
        $this->savePrice($request, $transferPrices, $transferStore->id);

        $this->storePictures($transferStore->id);
        if ($transferStore) {
            return \redirect('/admin/transfer');
        } else {

            return $this->model->create($request->only($this->model->getModel()->fillable));
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {

        $transfer = $this->model
            ->with('picture')
            ->with('prices')
            ->findOrFail($id);


        return view('admin.transfer.update', compact('transfer'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request)
    {


        $id = $request->input('id');

        $data = $request->only($this->model->getModel()->getFillable());
        // general picture

        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }

        $updateCar = $this->model->update($data, $id);

        $transfer = Transfer::find($id);
        $transfer->name = $data['name'];
        $transfer->description = $data['description'];
        $transfer->save();

//        $transferPrices = new TransferPrices();
//        $transferPrices->where('parent_id', '=', $id)->delete();
//        $this->savePrice($request, $transferPrices, $id);
//
//        $this->storePictures($id);

        if ($updateCar && $transfer) {

            return response()->json([
                'status' => 200
            ]);
        } else {
            return response()->json([
                'status' => 500
            ]);
        }
    }

    public function destroy($id)
    {
        $delete = $this->model->delete($id);
        if ($delete) {

            return response()->json([
                'status' => 200
            ]);
        }
    }


    public function storePictures($last_id)
    {
        $pictures = AjaxPictures::query()->get();

        foreach ($pictures as $picture) {
            $data_of_files = [
                'transfer_id' => $last_id,
                'name' => $picture->pictures,
            ];
            TransferPictures::query()->insert($data_of_files);
        }

        AjaxPictures::query()->delete();
    }

    public function deleteImage($id)
    {
        $model = new TransferPictures();

        $repository = new GeneralRepository($model);
        $response = $repository->delete($id);

        return $response;

    }

    public function approval($id)
    {

        $tour = Transfer::query()
            ->where('id', '=', $id)
            ->update(['status' => Transfer::STATUS_ACTIVE]);
        return $tour;


    }

    public function chauffeur()
    {
        $chauffeur = ChauffeurServices::query()->first();
        if ($chauffeur) {

            return view('admin.transfer.chauffeur', compact('chauffeur'));
        } else {
            return view('admin.transfer.chauffeur_create');
        }


    }

    public function chauffeurCreate()
    {
        return view('admin.transfer.chauffeur_create');
    }

    public function chauffeurStore(Request $request)
    {
        $data = $request->all();
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }

        $save = ChauffeurServices::query()->create($data);
        if ($save) {
            return response()->json([
                'message' => 'success',
                'status' => '200'
            ]);
        } else {
            return response()->json([
                'message' => 'error',
                'status' => '500'
            ]);
        }

    }
    public function chauffeurEdit(Request $request)
    {
        $data = $request->all();
        if (Input::hasFile('general_pic')) {
            $general_pic = Input::file('general_pic');
            $general_pic_name = time() . $general_pic->getClientOriginalName();
            $general_pic->move('uploads', $general_pic_name);
            $data['general_pic'] = $general_pic_name;
        }

        $save = ChauffeurServices::query()->where('id', '=', $data['chauffeur_id'])->update(
            [
                'name' => $data['name'],
                'description' => $data['description'],
//                'general_pic' => $data['general_pic'],

            ]);
        if ($save) {
            return response()->json([
                'message' => 'success',
                'status' => '200'
            ]);
        } else {
            return response()->json([
                'message' => 'error',
                'status' => '500'
            ]);
        }

    }
}
