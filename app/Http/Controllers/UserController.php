<?php

namespace App\Http\Controllers;

//use App\Http\Controllers\Controller;


use App\Repositories\GeneralRepository;
use App\Services\EmailSendService;
use App\User;


class UserController extends AbstractController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $model;

    public function __construct(User $user)
    {
        $this->middleware('auth');

        $this->model = new GeneralRepository($user);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->model->all();
        return view('admin.users.list', compact('users'));
    }


    public function activate($id)
    {
        $update = User::query()->where('id', '=', $id)
            ->update([
                'status' => User::STATUS_ACTIVE
            ]);
        $user = User::query()->where('id', '=', $id)->first();
        $send_email = new EmailSendService();

        $body = $send_email->generateEmailBodyActivate();
        $subject = 'CarTourTravel Account';
        if ($update) {
           $send = $send_email->send($user, $subject, $body);
        }
        return $send;
    }

    public function deactivate($id)
    {
        $update = User::query()->where('id', '=', $id)
            ->update([
                'status' => User::STATUS_DELETED
            ]);

        $user = User::query()->where('id', '=', $id)->first();
        $send_email = new EmailSendService();

        $body = $send_email->generateEmailBodyDeactivate();
        $subject = 'CarTourTravel Account';
        if ($update) {
            $send = $send_email->send($user, $subject, $body);
        }
        return $send;
    }


}
