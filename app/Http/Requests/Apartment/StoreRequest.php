<?php

namespace App\Http\Requests\Apartment;

use App\Http\Requests\Request;

/**
 * Class UpdateProfileRequest.
 */
class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        $rules['country'] = 'required';
        $rules['city'] = 'required';
        $rules['address'] = 'required';
        $rules['name'] = 'required';
        $rules['general_pic'] = 'required';
        $rules['base_price'] = 'required';

        return $rules;

    }
}
