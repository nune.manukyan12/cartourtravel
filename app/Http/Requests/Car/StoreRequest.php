<?php

namespace App\Http\Requests\Car;

use App\Http\Requests\Request;

/**
 * Class UpdateProfileRequest.
 */
class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules['name'] = 'required';
        $rules['car_day_prices'] = 'required';


        return $rules;

    }
}
