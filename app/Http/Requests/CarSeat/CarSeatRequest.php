<?php

namespace App\Http\Requests\CarSeat;

use App\Http\Requests\Request;

/**
 * Class UpdateProfileRequest.
 */
class CarSeatRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules['name'] = 'required';
        $rules['general_pic'] = 'required';
        $rules['car_seat_price'] = 'required';
        $rules['child_age'] = 'required';



        return $rules;

    }
}
