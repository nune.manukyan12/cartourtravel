<?php

namespace App\Http\Requests\Reservation;

use App\Http\Requests\Request;

/**
 * Class UpdateProfileRequest.
 */
class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules['first_name'] = 'required';
        $rules['last_name'] = 'required';
        $rules['email']='required|email';
        $rules['phone']='required';
        $rules['date_from']='required';
        $rules['date_to']='required';

        return $rules;

    }
}
