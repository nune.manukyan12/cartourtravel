<?php

namespace App\Http\Requests\Reservation;

use Illuminate\Foundation\Http\FormRequest;

class StoreTourPackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules['first_name'] = 'required';
        $rules['last_name'] = 'required';
        $rules['country'] = 'required';
        $rules['city'] = 'required';
        $rules['address'] = 'required';
        $rules['email'] = 'required|email';
        $rules['phone'] = 'required';
        $rules['viber'] = 'required';
        $rules['whatsapp'] = 'required';
        $rules['notes'] = 'required';



        return $rules;
    }
}
