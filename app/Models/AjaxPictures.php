<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AjaxPictures extends Model
{
    protected $table ='ajax_pictures';


    protected $fillable = [
        'pictures',
    ];
}
