<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
class Apartment extends Model
{
    use Translatable;
    public $translationModel = 'App\Models\ApartmentTranslation';
    protected $table = 'apartments';

    const STATUS_PENDING = 1;
    const STATUS_ACTIVE = 2;

    const BOOK_INTERVAL_WEEK = 7;
    const BOOK_INTERVAL_MONTH = 29;
    const BOOK_INTERVAL_TWO_WEEK = 14;
    public $translatedAttributes = ['name', 'description'];
    public $translationForeignKey = 'parent_id';
    protected $fillable = [
        'id',
        'country',
        'city',
        'general_pic',
        'address',
        'price_status',
        'admin_id',
        'status',
        'district',
        'homepage_order',
    ];
    public function picture()
    {
        return $this->hasMany('App\Models\ApartmentPictures', 'apartment_id', 'id');
    }

    public function prices()
    {
        return $this->hasMany('App\Models\ApartmentPrices', 'parent_id', 'id');
    }

    public function details()
    {
        return $this->hasMany('App\Models\ApartmentDetails', 'apartment_id', 'id');
    }
    public function users()
    {
        return $this->belongsTo('App\User', 'admin_id', 'id');
    }
    public function translate()
    {
        return $this->hasMany('App\Models\ApartmentTranslation', 'parent_id', 'id');
    }
    public function  translateHasOne()
    {
        if(\App::getLocale()){

            return $this->hasOne('App\Models\ApartmentTranslation','parent_id')->where('locale', '=', \App::getLocale());
        }else{
            return $this->hasOne('App\Models\ApartmentTranslation','parent_id')->where('locale', '=', 'en');

        }
    }


}
