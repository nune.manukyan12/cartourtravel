<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApartmentAvailable extends Model
{
    protected $table = 'apartment_available';

    const BLOCKED = 1;
    const ACTIVE = 2;

    protected $fillable = [
        'apartment_id',
        'blocked',
        'date',
        'nightly_price'
    ];
}
