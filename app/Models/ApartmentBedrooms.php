<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApartmentBedrooms extends Model
{
    protected $table = 'apartment_bedrooms';

    protected $fillable = [
        'apartment_detail_id',
        'double',
        'queen',
        'single',
        'sofa_bed'

    ];
}
