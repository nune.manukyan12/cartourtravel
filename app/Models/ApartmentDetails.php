<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ApartmentDetails extends Model
{
    use Sortable;
    protected $table = 'apartment_details';

    protected $fillable = [
        'hot_water',
        'elevator',
        'apartment_id',
        'balcony',
        'hair_dryer',
        'washing_machine',
        'shampoo',
        'iron',
        'tv',
        'iron_table',
        'cable_tv',
        'wifi',
        'pillows',
        'property_type',
        'number_of_quests',
        'total_number_of_beds',
        'total_number_of_bedrooms',
        'bathrooms',
        'towels',
        'bed_sheets',
        'soap',
        'toilet_paper',
        'heating',
        'number_air_conditioner',
        'air_conditioning',
        'youtube',
        'refrigerator',
        'micro_wave',
        'oven',
        'kitchen_ware',
        'electric_kettle',
        'key_card_access',
        'terrace',
        'free_parking',
        'underground_parking',
        'city_view',
        'street_view',
        'yard_view',
        'number_sofa_bed',
        'central_cooling',
        'pool',
        'garden',
        'sauna',
        'barbeque_place',
        'hot_tube'
    ];
    public static function  sortable()
    {
        return $sortable = ['tv', 'cable_tv', 'wifi', 'youtube', 'hair_dryer', 'iron', 'iron_table', 'washing_machine',
            'refrigerator', 'micro_wave', 'oven', 'kitchen_ware', 'electric_kettle', 'key_card_access', 'elevator', 'balcony',
            'terrace', 'towels', 'bed_sheets', 'soap','toilet_paper', 'shampoo','air_conditioning', 'heating', 'hot_water','free_parking', 'underground_parking'];
    }

    public function bedrooms()
    {
        return $this->hasMany('App\Models\ApartmentBedrooms', 'apartment_detail_id', 'id');
    }
}
