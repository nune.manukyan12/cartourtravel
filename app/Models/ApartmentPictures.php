<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApartmentPictures extends Model
{
    protected $table = 'apartment_pictures';

    protected $fillable = [
        'apartment_id',
        'name',
    ];
}
