<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApartmentPrices extends Model
{
    protected $table = 'apartment_prices';

    protected $fillable = [
        'parent_id',
        'currency',
        'base_price',
        'monthly_price',
        'weekly_discounts',
        'two_weekly_discounts',
        'two_weekly_discounts',
        'monthly_discounts',
        'pre_payment',
        'extra_cleaning',
        'pre_payment_price'
    ];
}
