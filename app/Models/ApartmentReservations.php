<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApartmentReservations extends Model
{
    protected $table = 'apartment_reservations';

    const STATUS_REQUEST = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_DECLINED = 3;
    const STATUS_EXPAINED = 4;
    const STATUS_CANCELED = 5;

    protected $fillable = [
        'id',
        'apartment_id',
        'first_name',
        'last_name',
        'email',
        'viber',
        'telegram',
        'status',
        'date_from',
        'date_to',
        'guest_child',
        'guest_adult',
        'guests_rooms',
        'phone'
    ];
}
