<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApartmentTranslation extends Model
{


    const en = 'en';
    const hy = 'hy';
    const ru = 'ru';

    protected $table = 'apartment_ml';


    protected $fillable = [
        'parent_id',
        'description',
        'name',
        'locale',
    ];

}
