<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use Translatable;

    const SEDAN = 2;
    const MINIVAN = 4;
    const SUV = 3;
    const MINI_BUS = 5;
    const BUS = 6;


    const SAME_PRICE = 1;
    const DATE_PRICE = 2;

    const STATUS_PENDING = 1;
    const STATUS_ACTIVE = 2;

    protected $table = 'cars';

    public $translatedAttributes = ['name', 'description'];
    public $translationForeignKey = 'parent_id';


    protected $fillable = [
        'persons',
        'type_id',
        'winter_wheel',
        'general_pic',
        'price_status',
        'conditioner',
        'door_count',
        'transmission',
        'admin_id',
        'status',
        'car_day_prices',
        'currency',
        'fuel',
        'transmission_drive',
        'doors_vehicle_type',
        'car_category'
    ];

    public function picture()
    {
       return $this->hasMany('App\Models\CarPictures', 'car_id', 'id');
    }

    public function prices()
    {
        return $this->hasMany('App\Models\CarPrices', 'parent_id', 'id');
    }
    public  function  type()
    {
        return $this->hasOne('App\Models\CarType','id','type_id');
    }
    public function reservations()
    {
        return $this->hasMany('App\Models\CarReservations', 'car_id', 'id');
    }
    public function translate()
    {
        return $this->hasMany('App\Models\CarTranslation', 'parent_id', 'id');
    }
    public function  translateHasOne()
    {
        return $this->hasOne('App\Models\CarTranslation','parent_id')->where('locale', '=', \App::getLocale());
    }
}
