<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarPictures extends Model
{
    protected $table = 'car_pictures';

    protected $fillable = [
        'car_id',
        'name',
    ];
}
