<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarPrices extends Model
{
    protected $table = 'car_prices';

    protected $fillable = [
        'parent_id',
        'from',
        'to',
        'price',
        'currency'
    ];
}
