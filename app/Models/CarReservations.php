<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarReservations extends Model
{
    protected $table = 'car_reservations';
    const STATUS_REQUEST = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_DECLINED = 3;
    const STATUS_EXPAINED = 4;
    const STATUS_CANCELED = 5;

    const TYPE_RESERVATION = 1;
    const TYPE_TRANSFER = 2;
    const TYPE_CHAUFFEUR = 3;


    protected $fillable = [
        'last_name',
        'first_name',
        'email',
        'viber',
        'telegram',
        'whatsap',
        'phone',
        'car_id',
        'type',
        'pick_up_locations',
        'return_locations',
        'pick_up_date',
        'return_date',
        'pick_up_time',
        'return_time',
        'status'
    ];


}
