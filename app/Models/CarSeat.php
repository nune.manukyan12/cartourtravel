<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarSeat extends Model
{
    protected $table = 'car_seat';

    protected $fillable = [
        'id',
        'name',
        'description',
        'currency',
        'general_pic',
        'child_age',
        'car_seat_price',
        'admin_id'

    ];
    public function picture()
    {
        return $this->hasMany('App\Models\CarSeatPictures', 'car_seat_id', 'id');
    }
}
