<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarSeatPictures extends Model
{
    protected $table = 'car_seat_pictures';

    protected $fillable = [
        'id',
        'name',
        'car_seat_id',
    ];
}
