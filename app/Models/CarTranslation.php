<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarTranslation extends Model
{


    const en = 'en';
    const hy = 'hy';
    const ru = 'ru';

    protected $table = 'car_ml';


    protected $fillable = [
        'parent_id',
        'description',
        'name',
        'locale',
    ];

}
