<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChauffeurServices extends Model
{
    protected $table = 'chauffeur_services';

    const STATUS_PENDING = 1;
    const STATUS_ACTIVE = 2;

    protected $fillable = [
        'name',
        'general_pic',
        'description',
        'admin_id',
    ];

}
