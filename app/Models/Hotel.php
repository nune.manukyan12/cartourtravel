<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = 'hotels';

    const STATUS_PENDING = 1;
    const STATUS_ACTIVE = 2;

    protected $fillable = [
        'description',
        'country',
        'name',
        'city',
        'general_pic',
        'address',
        'price_status',
        'admin_id',
        'status'
    ];
    public function picture()
    {
        return $this->hasMany('App\Models\HotelPictures', 'hotel_id', 'id');
    }
    public function prices()
    {
        return $this->hasMany('App\Models\HotelPrices', 'parent_id', 'id');
    }
}
