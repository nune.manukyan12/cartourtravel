<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelPictures extends Model
{
    protected $table = 'hotel_pictures';

    protected $fillable = [
        'hotel_id',
        'name',
    ];
}
