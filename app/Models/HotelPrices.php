<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelPrices extends Model
{
    protected $table = 'hotel_prices';

    protected $fillable = [
        'parent_id',
        'from',
        'to',
        'price',
        'type',
        'single_price',
        'family_price',
        'double_price',
        'executive_price',
        'superior_price',
        'superior_double_price',
        'deluxe_price',
        'deluxe_double_price',
        'currency'
    ];
}
