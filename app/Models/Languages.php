<?php
/**
 * Created by PhpStorm.
 * User: realizeit
 * Date: 5/15/19
 * Time: 5:56 PM
 */


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Languages extends Model
{
    const LANGUAGE_EN = 'en';

    protected $table = 'languages';


    protected $fillable = ['local', 'language'];

}
