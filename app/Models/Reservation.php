<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table ='reservation';


    protected $fillable = [
        'user_name',
        'user_email',
        'country',
        'city',
        'contact',
        'guests_adult',
        'guests_kids',
        'apartment_id',
        'date'
    ];
}
