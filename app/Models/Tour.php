<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $table = 'tours';

    const STATUS_PENDING = 1;
    const STATUS_ACTIVE = 2;

    const TYPE_PRIVATE = 3;
    const TYPE_REGULAR = 2;
    const TYPE_EXTREAM = 4;

    protected $fillable = [
        'description',
        'country',
        'name',
        'city',
        'general_pic',
        'address',
        'price_status',
        'type_id',
        'admin_id',
        'status'
    ];
    public function picture()
    {
        return $this->hasMany('App\Models\TourPictures', 'tour_id', 'id');
    }
    public function destination()
    {
        return $this->hasMany('App\Models\TourDestination', 'parent_id', 'id');

    }
    public function prices()
    {
        return $this->hasMany('App\Models\TourPrices', 'parent_id', 'id');
    }
    public function type()
    {
        return $this->hasOne('App\Models\TourType', 'id', 'type_id');
    }
}
