<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourDestination extends Model
{
    protected $table = 'tour_destination';

    protected $fillable = [
        'id',
        'parent_id',
        'destination',
        'description',
        'km',
        'duration',
        'sedan_price',
        'minivan_price',
        'sedan_price_guide',
        'minivan_price_guide',
        'mini_bus',
        'mini_bus_guide',
        'bus',
        'bus_guide',
        'date',
        'clock',
        'price_person',

    ];

}
