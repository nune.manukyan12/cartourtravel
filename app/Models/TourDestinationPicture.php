<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourDestinationPicture extends Model
{
    protected $table = 'tour_destination_picture';

    protected $fillable = [
//        'id',
        'tour_destination_id',
        'picture',
        'realy_picture',
//        'picture_size',

    ];

}
