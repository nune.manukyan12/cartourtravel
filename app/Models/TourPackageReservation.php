<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourPackageReservation extends Model
{
    protected $table = 'tour_package_reservation';

    protected $fillable = [
        'id',
        'date_arrival',
        'date_departure',
        'adults',
        'kids',
        'people_arrival',
        'people_departure',
        'apartment',
        'house',
        'tour',
        'last_name',
        'first_name',
        'country',
        'city',
        'address',
        'email',
        'phone',
        'viber',
        'whatsapp',
        'notes'
    ];
}
