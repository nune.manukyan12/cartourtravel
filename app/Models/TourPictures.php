<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourPictures extends Model
{
    protected $table = 'tour_pictures';

    protected $fillable = [
        'tour_id',
        'name',
    ];
}
