<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourPrices extends Model
{
    protected $table = 'tour_prices';

    protected $fillable = [
        'parent_id',
        'from',
        'to',
        'price',
        'currency'

    ];
}
