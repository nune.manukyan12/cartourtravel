<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourismReservations extends Model
{
    protected $table = 'tourism_reservation';

    const STATUS_REQUEST = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_DECLINED = 3;
    const STATUS_EXPAINED = 4;
    const STATUS_CANCELED = 5;

    protected $fillable = [
        'id',
        'tourism_id',
        'car_type',
        'first_name',
        'last_name',
        'email',
        'viber',
        'whatsapp',
        'telegram',
        'status',
        'phone'
    ];

}
