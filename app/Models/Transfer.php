<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $table = 'transfers';

    const STATUS_PENDING = 1;
    const STATUS_ACTIVE = 2;

    protected $fillable = [
        'persons',
        'name',
        'winter_wheel',
        'general_pic',
        'description',
        'price_status',
        'admin_id',
        'status'
    ];
    public function picture()
    {
        return $this->hasMany('App\Models\TransferPictures', 'transfer_id', 'id');
    }
    public function prices()
    {
        return $this->hasMany('App\Models\TransferPrices', 'parent_id', 'id');
    }
}
