<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferPictures extends Model
{
    protected $table = 'transfer_pictures';

    protected $fillable = [
        'transfer_id',
        'name',
    ];

}
