<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferPrices extends Model
{
    protected $table = 'transfer_prices';

    protected $fillable = [
        'parent_id',
        'from',
        'to',
        'price',
        'currency'

    ];
}
