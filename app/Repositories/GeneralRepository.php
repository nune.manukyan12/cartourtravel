<?php namespace App\Repositories;

use App\Models\Car;
use App\User;
use Illuminate\Database\Eloquent\Model;

class GeneralRepository implements GeneralRepositoryInterfase
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Model $model)
    {

        $this->model = $model;
    }

    // Get all instances of model
    public function all()
    {
        $user = \Auth::user();
        if ($user && $user->type == User::ADMIN) {
            $items = $this->model->where('admin_id', '=', $user->id)->orderBy('id', 'DESC')->get();
        } else {
            $items = $this->model->orderBy('id', 'DESC')->get();
        }

        return $items;
    }

    // create a new record in the database
    public function create(array $data, $modelMl = null)
    {

        $user = \Auth::user();
        if ($user && $user->type == User::ADMIN) {
            $data['admin_id'] = $user->id;
            $data['status'] = Car::STATUS_PENDING;
        } else {
            $data['admin_id'] = $user->id;
        }
        $dataCreate =  $data;
        unset($dataCreate['name']);
        unset($dataCreate['description']);
        unset($dataCreate['lang']);
        $created = $this->model->create($dataCreate);

        if ($this->model->translatedAttributes) {
            $this->createMl($data, $created->id, $modelMl);
        }
        return $created;
    }

    // update record in the database
    public function update(array $data, $id, $modelMl = null)
    {
        $user = \Auth::user();
        if ($user) {
            $data['admin_id'] = $user->id;
        }
        $record = $this->model->find($id);

        if ($this->model->translatedAttributes) {
            $this->updateMl($data, $id, $modelMl);
        }
        unset($data['name']);
        unset($data['description']);
        unset($data['lang']);

        return $record->update($data);
    }

    protected function updateMl($data, $id, $modelMl)
    {
        foreach ($data['lang'] as $key => $lang) {
            $data_translate = $modelMl
                ->where('parent_id', '=', $id)
                ->where('locale', '=', $lang)
                ->update(
                    [
                        'name' => $data['name'][$key],
                        'description' => $data['description'][$key],
                    ]);
        }
    }

    protected function createMl($data, $id, $modelMl)
    {
        foreach ($data['lang'] as $key => $lang) {
            $data_translate = $modelMl
                ->create(
                    [
                        'locale' => $lang,
                        'parent_id' => $id,
                        'name' => $data['name'][$key],
                        'description' => $data['description'][$key],
                    ]);
        }
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function first()
    {
        return $this->model->first();
    }

    // Get the associated modelG
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

//    public function getMl()
//    {
//        return $this->model->join();
//    }
//    public function join(){
//
//    }
    public function lastId()
    {
        $last = $this->model->orderBy('id', 'DESC')->first();
        if ($last) {
            $id = $last;
        } else {
            $id = 0;
        }
        return $id;
    }
}