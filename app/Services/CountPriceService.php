<?php

namespace App\Services;

use App\Models\Apartment;
use App\Models\ApartmentAvailable;
use App\Models\ApartmentPrices;
use App\Models\TourDestination;
use Illuminate\Database\Eloquent\Collection;
use PHPUnit\Util\Json;

class CountPriceService
{
    public function apartmentPrice($id, $dateStart, $dateEnd)
    {

//        all book day $book_date
        $book_date = array();
        $block_date_server = array();
        $blockDay = array();
        $nightli_date_price = array();
        $nightlyDayPrice = array();

//        all book day $time_date

        $datePeriod = $this->returnDates($dateStart, $dateEnd);
        foreach ($datePeriod as $date) {
            $rezult = $date->format('d/m/Y');
            array_push($book_date, $rezult);
        }


        $nightlyPrice = ApartmentAvailable::query()
            ->where('apartment_id', '=', $id)
            ->where('blocked', '=', ApartmentAvailable::ACTIVE)
            ->get();


        for ($i = 0; $i < count($nightlyPrice); $i++) {
            $nightly_price = json_decode($nightlyPrice[$i]->date);
            for ($t = 0; $t < count($nightly_price); $t++) {
                $time = date_create_from_format("U", $nightly_price[$t] / 1000);
                array_push($nightli_date_price, date_format($time, "d/m/Y"));
            }

            for ($x = 0; $x < count($book_date); $x++) {
                if (in_array($book_date[$x], $nightli_date_price)) {
                    array_push($nightlyDayPrice, $nightlyPrice[$i]->nightly_price);
                }
            }

            $nightli_date_price = array();
        }
        $apartment_price = ApartmentPrices::query()->where('parent_id', '=', $id)->first();
        $apartment = Apartment::query()
            ->where('id', '=', $id)
            ->first();
        $apartment_name = $apartment->name;

        if (29 < count($book_date)) {
            $final_price = $this->monthlyDiscount($apartment_price, $apartment_name, $id);

        } else {
            if (13 < count($book_date)) {
                $final_price = $this->twoWeeklyDiscount($apartment_price, $book_date, $nightlyDayPrice, $apartment_name, $id);

            } else {
                if (6 < count($book_date)) {
                    $final_price = $this->weeklyDiscount($apartment_price, $book_date, $nightlyDayPrice, $apartment_name, $id);

                } else {
                    $final_price = $this->dailyPrice($apartment_price, $book_date, $nightlyDayPrice, $apartment_name, $id);

                }
            }

        }
        $final_price = $this->exchangeFromUsd($final_price);

        return $final_price;

    }

    public function monthlyDiscount($apartment_price, $apartment_name, $id)
    {
        $discount_mountly = $apartment_price->monthly_price * $apartment_price->monthly_discounts / 100;
        $final_price = $apartment_price->monthly_price - $discount_mountly;
        $discount = $apartment_price->monthly_discounts;
        return compact('final_price', 'discount', 'apartment_name', 'id');

    }

    public function twoWeeklyDiscount($apartment_price, $book_date, $nightlyDayPrice, $apartment_name, $id)
    {
        $all_nightly_price = 0;
        $basePriceDayCount = count($book_date) - count($nightlyDayPrice);
        $bacePriceDay = $basePriceDayCount * $apartment_price->base_price;
        for ($i = 0; $i < count($nightlyDayPrice); $i++) {
            $all_nightly_price += $nightlyDayPrice[$i];
        }
        $all_price = $all_nightly_price + $bacePriceDay;
        $discount_two_weekly = $all_price * $apartment_price->two_weekly_discounts / 100;
        $discount = $apartment_price->two_weekly_discounts;

        $final_price = $all_price - $discount_two_weekly;
        return compact('final_price', 'discount', 'apartment_name', 'id');


    }

    public function weeklyDiscount($apartment_price, $book_date, $nightlyDayPrice, $apartment_name, $id)
    {
        $all_nightly_price = 0;
        $basePriceDayCount = count($book_date) - count($nightlyDayPrice);
        $bacePriceDay = $basePriceDayCount * $apartment_price->base_price;
        for ($i = 0; $i < count($nightlyDayPrice); $i++) {
            $all_nightly_price += $nightlyDayPrice[$i];
        }
        $all_price = $all_nightly_price + $bacePriceDay;
        $discount_weekly = $all_price * $apartment_price->weekly_discounts / 100;
        $discount = $apartment_price->weekly_discounts;
        $final_price = $all_price - $discount_weekly;
        return compact('final_price', 'discount', 'apartment_name', 'id');

    }

    public function dailyPrice($apartment_price, $book_date, $nightlyDayPrice, $apartment_name, $id)
    {
        $all_nightly_price = 0;
        $basePriceDayCount = count($book_date) - count($nightlyDayPrice);
        $bacePriceDay = $basePriceDayCount * $apartment_price->base_price;
        for ($v = 0; $v < count($nightlyDayPrice); $v++) {
            $all_nightly_price += $nightlyDayPrice[$v];
        }
        $final_price = $all_nightly_price + $bacePriceDay;
        $discount = null;
        return compact('final_price', 'discount', 'apartment_name', 'id');
    }

    private function returnDates($fromdate, $todate)
    {
        $fromdate = \DateTime::createFromFormat('d/m/Y', $fromdate);
        $todate = \DateTime::createFromFormat('d/m/Y', $todate);
        return new \DatePeriod(
            $fromdate,
            new \DateInterval('P1D'),
            $todate->modify('+1 day')
        );
    }

    public function tourPrice($data)
    {
        $tourId = array();
        $privatePrices = array();
        $groupPrices = array();

        foreach ($data['data'] as $tour) {
            if (in_array($tour['id'], $tourId)) {

            } else {
                array_push($tourId, $tour['id']);
            }
        }
        $destinations = TourDestination::query()->whereIn('id', $tourId)
            ->get();
        foreach ($data['data'] as $tour) {
            foreach ($destinations as $destination) {
                if ($tour['id'] == $destination->id) {
                    if ($tour['type_tour'] == 'private') {
                        if ($tour['type_car'] == 'sedan') {
                            if ($tour['guide'] == 'false') {
                                array_push($privatePrices, $destination->sedan_price);

                            } else {
                                array_push($privatePrices, $destination->sedan_price_guide);

                            }
                        } elseif ($tour['type_car'] == 'minivan') {
                            if ($tour['guide'] == 'false') {
                                array_push($privatePrices, $destination->minivan_price);

                            } else {
                                array_push($privatePrices, $destination->minivan_price_guide);

                            }
                        } elseif ($tour['type_car'] == 'minibus') {
                            if ($tour['guide'] == 'false') {
                                array_push($privatePrices, $destination->mini_bus);

                            } else {
                                array_push($privatePrices, $destination->mini_bus_guide);

                            }
                        } elseif ($tour['type_car'] == 'bus') {
                            if ($tour['guide'] == 'false') {
                                array_push($privatePrices, $destination->bus);

                            } else {
                                array_push($privatePrices, $destination->bus_guide);

                            }
                        }
                    } elseif ($tour['type_tour'] == 'group') {
                        array_push($groupPrices, $destination->price_person);

                    }

                }
            }
        }
//        dd($privatePrices);

        $private_price = 0;
        foreach ($privatePrices as $value) {
            $private_price = $private_price + $value;
        }

        $group_price = 0;
        foreach ($groupPrices as $value) {
            $group_price = $group_price + $value;
        }
        $group_price = $this->exchangeFromAmd($group_price);
        $private_price = $this->exchangeFromAmd($private_price);
        return response()->json([
            'private_price' => $private_price,
            'group_price' => $group_price
        ]);
    }

    public function carPrice($id, $dateStart, $dateEnd)
    {
        return 2;
    }

    protected function exchangeFromUsd($price)
    {
        $exchange_price = \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'), $price);
        if ($exchange_price) {
            $symbol = strtoupper(session()->get('currency_symbol'));
            $price = $exchange_price;
        } else {
            $symbol = '$';
            $price = $price;
        }

        return response()->json([
           'price'  => $price,
           'symbol'  => $symbol
        ]);
    }
    protected function exchangeFromAmd($price)
    {
        $exchange_price = \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'), $price);
        if ($exchange_price) {
            $symbol = strtoupper(session()->get('currency_symbol'));
            $price = $exchange_price;
        } else {
            $symbol = '$';
            $price = $price;
        }

        return response()->json([
            'price'  => $price,
            'symbol'  => $symbol
        ]);
    }
}