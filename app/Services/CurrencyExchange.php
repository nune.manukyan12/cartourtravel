<?php

namespace  App\Services;


use App\Models\Currency;
use App\Models\CurrencyExcangeRate;

class CurrencyExchange
{
    /**
     * @param Currency $from
     * @param Currency $to
     * @return float
     */
    public  function exchange($rate , $price)
    {


        $new_price = (float)$price / $rate;


        return round($new_price, 2);
    }
}