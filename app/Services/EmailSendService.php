<?php

namespace App\Services;


use App\Categories;
use App\EventsSchedules;
use PHPMailer\PHPMailer\PHPMailer;


class EmailSendService
{


    /**
     * @param $to
     * @param $subject
     * @param $body
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function send($subject, $body)
    {
        $mail = new PHPMailer();

//        $mail->SMTPDebug = 3;

        $mail->CharSet = 'UTF-8';
//Set PHPMailer to use SMTP.
        $mail->isSMTP();
//Set SMTP host name
        $mail->Host = "smtp.mail.ru";
//Set this to true if SMTP host requires authentication to send email
        $mail->SMTPAuth = true;

//Provide username and password
        $mail->Username = "work.realizeit@mail.ru";
        $mail->Password = "123123NM";
//If SMTP requires TLS encryption then set it
        $mail->SMTPSecure = "tls";
//Set TCP port to connect to
        $mail->Port = 587;

        $mail->From = "work.realizeit@mail.ru";
        $mail->FromName = "CarTourTravel";

//        if(isset($to->email))
//        {
//            $mail->addAddress($to->email, $to->name);
//        }else{
        $mail->addAddress('cartourtravel.info@gmail.com', 'Tour');
//        }


        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AltBody = "This is the plain text version of the email content";


        if (!$mail->send()) {
            return "Mailer Error: " . $mail->ErrorInfo;
        } else {
            return "Message has been sent successfully";
        }

    }

    public function generateEmailCarReservation($data)
    {

        $url = env('APP_URL');
        $body = '<h1>CarTourTravel</h1>
                       <h2>User</h2>
                  <p>
                  Name: ' . $data['last_name'] . '  ' . $data['first_name'] . '
                  </p>
                  <p>
                  Email: ' . $data['email'] . '
                  </p>
                  <p>
                  Phone: ' . $data['phone'] . ' 
                  </p>
                  <p>
                  viber: ' . $data['viber'] . ' 
                  </p>
                  <p>
                  Telegram: ' . $data['telegram'] . ' 
                  </p>
                   <p>
                  Whatsapp: ' . $data['whatsapp'] . ' 
                  </p>
                    <hr>
                    <h3>User want reserve Car <bold> from  ' . $data['pick_up_date'] . ' to  ' . $data['return_date'] . ' </bold></h3>
                    <h2><a href="' . $url . '/cars">Click there for see car</a></h2> ';

        return $body;

    }public function generateEmailChauffeurBooking($data)
    {

        $url = env('APP_URL');
        $body = '<h1>CarTourTravel</h1>
                       <h2>User</h2>
                  <p>
                  Name: ' . $data['last_name'] . '  ' . $data['first_name'] . '
                  </p>
                  <p>
                  Email: ' . $data['email'] . '
                  </p>
                  <p>
                  Phone: ' . $data['phone'] . ' 
                  </p>
                  <p>
                  viber: ' . $data['viber'] . ' 
                  </p>
                  <p>
                  Telegram: ' . $data['telegram'] . ' 
                  </p>
                   <p>
                  Whatsapp: ' . $data['whatsapp'] . ' 
                  </p>
                    <hr>
                    <h2><a href="' . $url . '/chauffeur/details/'.$data['car_id'].'">Click there for see chauffeur</a></h2> ';

        return $body;

    }

    public function generateEmailTransferBooking($data)
    {

        $url = env('APP_URL');
        $body = '<h1>CarTourTravel</h1>
                       <h2>User</h2>
                  <p>
                  Name: ' . $data['last_name'] . '  ' . $data['first_name'] . '
                  </p>
                  <p>
                  Email: ' . $data['email'] . '
                  </p>
                  <p>
                  Phone: ' . $data['phone'] . ' 
                  </p>
                  <p>
                  viber: ' . $data['viber'] . ' 
                  </p>
                  <p>
                  Telegram: ' . $data['telegram'] . ' 
                  </p>
                   <p>
                  Whatsapp: ' . $data['whatsapp'] . ' 
                  </p>
                    <hr>
                    <h2><a href="' . $url . '/transfer/details/'.$data['car_id'].'">Click there for see transfer</a></h2> ';

        return $body;

    }

    public function generateEmailApartmentReservation($data)
    {

        $url = env('APP_URL');

        $fromdate = date('d-m-Y', $data['date_from'] / 1000);
        $todate = date('d-m-Y', $data['date_to'] / 1000);
        $body = '<h1>CarTourTravel</h1>
                       <h2>User</h2>
                  <p>
                  Name: ' . $data['last_name'] . '  ' . $data['first_name'] . '
                  </p>
                  <p>
                  Email: ' . $data['email'] . '
                  </p>
                  <p>
                  Phone: ' . $data['phone'] . ' 
                  </p>
                  <p>
                  viber: ' . $data['viber'] . ' 
                  </p>
                  <p>
                  Telegram/Whatsapp: ' . $data['telegram'] . ' 
                  </p>
                    <hr>
                    <h3>User want reserve Apartment <bold> from  ' . $fromdate . ' to  ' . $todate . ' </bold></h3>
                    <h2><a href="' . $url . '/apartment/' . $data['apartment_id'] . '">Click there for see apartment</a></h2> ';

        return $body;

    }

    public function generateEmailCreateTourPackage($data)
    {

        $url = env('APP_URL');

        $body = '<h1>CarTourTravel</h1>
                       <h2>User</h2>
                  <p>
                  Name: ' . $data['last_name'] . '  ' . $data['first_name'] . '
                  </p>
                  <p>
                  Email: ' . $data['email'] . '
                  </p>
                  <p>
                  Phone: ' . $data['phone'] . ' 
                  </p>
                  <p>
                  viber: ' . $data['viber'] . ' 
                  </p>
                   <p>
                  Whatsapp: ' . $data['whatsapp'] . ' 
                  </p>
                    <hr>
                    <h3>User create tour package</bold></h3>
                     <h2><a href="' . $url . 'apartments">See created package</a></h2> ';


        return $body;

    }

    public function generateEmailCreatePrivateTour($data)
    {
        $url = env('APP_URL');
        $guide = 'Without Guid';
        if (isset( $data['guide']) && $data['guide']) {
            $guide = 'With Guid';
        }

        $body = '<h1>CarTourTravel</h1>
                       <h2>User</h2>
                  <p>
                  Name: ' . $data['last_name'] . '  ' . $data['first_name'] . '
                  </p>
                  <p>
                  Email: ' . $data['email'] . '
                  </p>
                  <p>
                  Phone: ' . $data['phone'] . ' 
                  </p>
                  <p>
                  viber: ' . $data['viber'] . ' 
                  </p>
                   <p>
                  Whatsapp: ' . $data['whatsapp'] . ' 
                  </p>
                      <h3>'.$data['last_name'] . '  ' . $data['first_name'] . ' User want reserve Private tour  <bold>' . $guide . ' </bold ></h3 >

                    <hr >
                    <h3 > User book private tour </bold ></h3 >
                     <h2 ><a href = "' . $url . '/tours/3" > See booking tour </a ></h2 > ';


        return $body;

    }

    public function generateEmailCreateMedicalService($data) {
        $url = env('APP_URL');

        $body = '<h1>CarTourTravel</h1>
                       <h2>User</h2>
                  <p>
                  Name: ' . $data['last_name'] . '  ' . $data['first_name'] . '
                  </p>
                  <p>
                  Email: ' . $data['email'] . '
                  </p>
                  <p>
                  Phone: ' . $data['phone'] . ' 
                  </p>
                  <p>
                  viber: ' . $data['viber'] . ' 
                  </p>
                  <p>
                  Telegram/Whatsapp: ' . $data['telegram'] . ' 
                  </p>
                    <hr>
                    <h2><a href="' . $url . '/medical/details/' . $data['medical_id'] . '">Click there for see medical</a></h2> ';

        return $body;

    }


    public function generateEmailCreateTourismService($data) {
        $url = env('APP_URL');

        $body = '<h1>CarTourTravel</h1>
                       <h2>User</h2>
                  <p>
                  Name: ' . $data['last_name'] . '  ' . $data['first_name'] . '
                  </p>
                  <p>
                  Email: ' . $data['email'] . '
                  </p>
                  <p>
                  Phone: ' . $data['phone'] . ' 
                  </p>
                  <p>
                  viber: ' . $data['viber'] . ' 
                  </p>
                  <p>
                  Telegram/Whatsapp: ' . $data['telegram'] . ' 
                  </p>
                    <hr>
                    <h2><a href="' . $url . '/tourism/details/' . $data['tourism_id'] . '">Click there for see tourism</a></h2> ';

        return $body;

    }


    public function generateEmailContact($data){
        $url = env('APP_URL');


        $body = '<h1>CarTourTravel</h1>
                       <h2>User</h2>
                  <p>
                  Name: ' . $data['last_name'] . '  ' . $data['first_name'] . '
                  </p>
                  <p>
                  Email: ' . $data['email'] . '
                  </p>
                  <p>
                  Phone: ' . $data['phone'] . ' 
                  </p>
                 
                      <h3>'.$data['last_name'] . '  ' . $data['first_name'] . ' User want contact with Me</h3 >

                    <hr >
                    <h3 >Message </bold ></h3 >
                     <p>'.$data['message'].'</p > ';


        return $body;
    }
}