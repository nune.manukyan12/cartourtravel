<?php

namespace App\Services;


class ForgeApi
{
    /**
     * @param Currency $from
     * @param Currency $to
     * @return float
     */
    public static function getRate()
    {
        $access_key = '72c030f508eb7a2fb6a3a2882a683b7c';


        try {
            $ch = curl_init('http://apilayer.net/api/live?access_key=' . $access_key);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $json = curl_exec($ch);
            curl_close($ch);
            $conversionResult = json_decode($json, true);

            $rate = $conversionResult['quotes'];

            if (!$rate) {
                \Log::warning('wasnt able to get exchange rate from API ' . self::class);

                return $rate;
            }
        } catch (\Exception $e) {

            \Log::error($e);
        }


    }

    public static function changeCurrencyFromUsd($to, $amount)
    {
        $rate = session()->get('rate');
//        dd($rate);
        $to = strtoupper($to);
        if ($rate) {
            $price = $rate['USD' . $to] * $amount;

            return round($price, 2);
        }else{
            return 0;
        }
    }

    public static function changeCurrencyFromAmd($to, $amount)
    {
        $to = strtoupper($to);
        $rate = ForgeApi::getRate($to);

        if ($rate) {
            $price_usd = $amount / $rate['USDAMD'];

            $price = $price_usd * $rate['USD' . $to];

            return round($price, 2);
        }else{

            return 0;
        }
    }
}