<?php

namespace App\Services;


use App\Models\Languages;


class TranslationService
{
    public function translate($name, $description, $translate_lang)
    {


        $default_lang = Languages::LANGUAGE_EN;

        $apiKey = env('GOOGLE_API_KEY');

        // description translate
        $url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($description) . '&source=' . $default_lang . '&target=' . $translate_lang;

        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($handle);
        $responseDecodedDesc = json_decode($response, true);
        curl_close($handle);


        // name translate
        $url_name = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($name) . '&source=' . $default_lang . '&target=' . $translate_lang;

        $handle_name = curl_init($url_name);
        curl_setopt($handle_name, CURLOPT_RETURNTRANSFER, true);
        $response_name = curl_exec($handle_name);
        $responseDecodedName = json_decode($response_name, true);
        curl_close($handle_name);



        return
            [
                'description' => $responseDecodedDesc['data']['translations'][0]['translatedText'],
                'name' => $responseDecodedName['data']['translations'][0]['translatedText'],
            ];
    }

}
