<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type_id');
            $table->foreign('type_id')
                ->references('id')->on('car_types');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->integer('persons')->nullable();
            $table->string('winter_wheel')->nullable();
            $table->integer('door_count')->nullable();
            $table->string('conditioner')->nullable();
            $table->string('transmission')->nullable();
            $table->float('price_status')->nullable();
            $table->string('general_pic')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
