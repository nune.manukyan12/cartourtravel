<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartment_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('apartment_id');
            $table->foreign('apartment_id')
                ->references('id')->on('apartments')
                ->onDelete('cascade');
            $table->integer('floors')->nullable();
            $table->integer('apartment_floor')->nullable();
            $table->string('building_type')->nullable();
            $table->string('elevator')->nullable();
            $table->string('hot_water')->nullable();
            $table->string('bedroom')->nullable();
            $table->integer('double_bedroom')->nullable();
            $table->integer('simple_bedroom')->nullable();
            $table->string('furnishing')->nullable();
            $table->string('dish_washer')->nullable();
            $table->string('new_building')->nullable();
            $table->string('parking')->nullable();
            $table->string('repairing')->nullable();
            $table->string('air_conditioner')->nullable();
            $table->string('garden')->nullable();
            $table->string('pool')->nullable();
            $table->string('sauna')->nullable();
            $table->string('barbeque_place')->nullable();
            $table->string('hot_tube')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//
    }
}
