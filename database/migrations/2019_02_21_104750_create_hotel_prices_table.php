<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id');
            $table->foreign('parent_id')
                ->references('id')->on('hotels')
                ->onDelete('cascade');
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->string('currency')->nullable();
            $table->float('single_price')->nullable();
            $table->float('family_price')->nullable();
            $table->float('double_price')->nullable();
            $table->float('executive_price')->nullable();
            $table->float('superior_price')->nullable();
            $table->float('superior_double_price')->nullable();
            $table->float('deluxe_price')->nullable();
            $table->float('deluxe_double_price')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_prices');
    }
}
