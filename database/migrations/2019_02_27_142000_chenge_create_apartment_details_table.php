<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChengeCreateApartmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apartment_details', function (Blueprint $table) {
            $table->string('balcony')->after('air_conditioner')->nullable();
            $table->integer('person')->after('balcony')->nullable();
            $table->string('kitchen')->after('person')->nullable();
            $table->string('hair_dryer')->after('kitchen')->nullable();
            $table->string('washing_machine')->after('hair_dryer')->nullable();
            $table->string('shampoo')->after('washing_machine')->nullable();
            $table->string('tv')->after('shampoo')->nullable();
            $table->string('bed_liners')->after('tv')->nullable();
            $table->string('iron_table')->after('bed_liners')->nullable();
            $table->string('cable_tv')->after('iron_table')->nullable();
            $table->string('wifi')->after('cable_tv')->nullable();
            $table->string('iron')->after('wifi')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apartment_details', function (Blueprint $table) {
            $table->dropColumn(['balcony', 'person', 'kitchen','hair_dryer','washing_machine','shampoo','tv','bed_liners','iron_table','cable_tv','wifi','iron']);
        });
    }
}
