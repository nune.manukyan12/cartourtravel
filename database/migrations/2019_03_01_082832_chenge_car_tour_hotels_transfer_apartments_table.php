<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChengeCarTourHotelsTransferApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {


            $table->integer('type')->after('name')->nullable();
            $table->integer('status')->after('type')->nullable();
            $table->string('role')->after('type')->nullable();


        });
        Schema::table('cars', function (Blueprint $table) {
            $table->unsignedInteger('admin_id')->after('type_id')->nullable();
            $table->foreign('admin_id')->references('id')->on('users');

            $table->integer('status')->after('type_id')->nullable();


        });
        Schema::table('hotels', function (Blueprint $table) {

            $table->integer('admin_id')->unsigned()->after('id')->nullable();;
            $table->foreign('admin_id')->references('id')->on('users');

            $table->integer('status')->after('admin_id')->nullable();

        });
        Schema::table('tours', function (Blueprint $table) {
            $table->integer('admin_id')->unsigned()->after('id')->nullable();;
            $table->foreign('admin_id')->references('id')->on('users');

            $table->integer('status')->after('admin_id')->nullable();


        });
        Schema::table('transfers', function (Blueprint $table) {

            $table->integer('admin_id')->unsigned()->after('id')->nullable();;
            $table->foreign('admin_id')->references('id')->on('users');

            $table->integer('status')->after('admin_id')->nullable();
        });
        Schema::table('apartments', function (Blueprint $table) {
            $table->integer('admin_id')->unsigned()->after('id')->nullable();;
            $table->foreign('admin_id')->references('id')->on('users');

            $table->integer('status')->after('admin_id')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['type']);
        });
        Schema::table('apartments', function (Blueprint $table) {
            $table->dropColumn(['admin_id', 'status']);
        });
        Schema::table('transfers', function (Blueprint $table) {
            $table->dropColumn(['admin_id', 'status']);
        });
        Schema::table('tours', function (Blueprint $table) {
            $table->dropColumn(['admin_id', 'status']);
        });
        Schema::table('hotels', function (Blueprint $table) {
            $table->dropColumn(['admin_id', 'status']);
        });
        Schema::table('cars', function (Blueprint $table) {
            $table->dropColumn(['admin_id', 'status']);
        });
    }
}
