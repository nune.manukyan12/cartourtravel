<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChengeApartmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apartment_details', function (Blueprint $table) {

            $table->dropColumn([ 'parking','kitchen','bedroom','bed_liners', 'person', 'repairing', 'floors', 'apartment_floor', 'building_type', 'double_bedroom', 'simple_bedroom', 'furnishing', 'dish_washer', 'new_building', 'air_conditioner',]);


            $table->string('property_type')->after('apartment_id')->nullable();
            $table->integer('number_of_quests')->after('property_type')->nullable();
            $table->integer('total_number_of_beds')->after('number_of_quests')->nullable();
            $table->integer('total_number_of_bedrooms')->after('total_number_of_beds')->nullable();
            $table->float('bathrooms')->after('total_number_of_bedrooms')->nullable();
            $table->string('towels')->after('bathrooms')->nullable();
            $table->string('bed_sheets')->after('towels')->nullable();
            $table->string('pillows')->after('bed_sheets')->nullable();
            $table->string('soap')->after('pillows')->nullable();
            $table->string('toilet_paper')->after('soap')->nullable();
            $table->string('heating')->after('toilet_paper')->nullable();
            $table->string('air_conditioning')->after('heating')->nullable();
            $table->integer('number_air_conditioner')->after('air_conditioning')->nullable();
            $table->string('youtube')->after('number_air_conditioner')->nullable();
            $table->string('refrigerator')->after('youtube')->nullable();
            $table->string('micro_wave')->after('refrigerator')->nullable();
            $table->string('oven')->after('micro_wave')->nullable();
            $table->string('kitchen_ware')->after('oven')->nullable();
            $table->string('electric_kettle')->after('kitchen_ware')->nullable();
            $table->string('key_card_access')->after('electric_kettle')->nullable();
            $table->string('terrace')->after('key_card_access')->nullable();
            $table->string('parking_type')->after('terrace')->nullable();
            $table->string('city_view')->after('parking_type')->nullable();
            $table->string('street_view')->after('city_view')->nullable();
            $table->string('yard_view')->after('street_view')->nullable();
            $table->integer('number_sofa_bed')->after('yard_view')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apartment_details', function (Blueprint $table) {
            $table->dropColumn(['view','parking_type','terrace','key_card_access',
                'electric_kettle','kitchen_ware','oven','micro_wave','refrigerator',
                'youtube','number_air_conditioner','air_conditioning','heating',
                'toilet_paper','soap','pillows','bed_sheets','towels','property_type',
                'number_of_quests','total_number_of_beds','total_number_of_bedrooms','bathrooms','number_sofa_bed']);
        });
    }
}
