<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentBedrooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartment_bedrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apartment_detail_id')->unsigned();
            $table->foreign('apartment_detail_id')->references('id')->on('apartment_details')->onDelete('cascade');
            $table->integer('double')->nullable();
            $table->integer('queen')->nullable();
            $table->integer('single')->nullable();
            $table->integer('sofa_bed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartment_bedrooms');
    }
}
