<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChengeTourPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_prices', function (Blueprint $table) {
            $table->dropColumn([ 'from', 'to', 'price',]);

            $table->integer('base_price')->after('parent_id')->nullable();
            $table->integer('monthly_price')->after('base_price')->nullable();
            $table->integer('weekly_discounts')->after('monthly_price')->nullable();
            $table->integer('two_weekly_discounts')->after('weekly_discounts')->nullable();
            $table->integer('monthly_discounts')->after('two_weekly_discounts')->nullable();
            $table->string('pre_payment')->after('monthly_discounts')->nullable();
            $table->integer('extra_cleaning')->after('pre_payment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_prices', function (Blueprint $table) {
            $table->dropColumn([ 'base_price', 'monthly_price', 'weekly_discounts',
                'two_weekly_discounts','monthly_discounts','pre_payment','extra_cleaning']);
        });
    }
}
