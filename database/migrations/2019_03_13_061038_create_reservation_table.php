<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_name')->nullable();
            $table->string('user_email')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('contact')->nullable();
            $table->string('guests_adult')->nullable();
            $table->string('guests_kids')->nullable();
            $table->integer('apartment_id')->nullable();
            $table->text('date')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation');
    }
}
