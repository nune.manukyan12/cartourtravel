<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->string('fuel')->after('currency')->nullable();
            $table->string('transmission_drive')->after('fuel')->nullable();
            $table->string('doors_vehicle_type')->after('transmission_drive')->nullable();
            $table->string('car_category')->after('doors_vehicle_type')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropColumn([ 'fuel']);
            $table->dropColumn([ 'transmission_drive']);
            $table->dropColumn([ 'doors_vehicle_type']);
            $table->dropColumn([ 'car_category']);

        });
    }
}
