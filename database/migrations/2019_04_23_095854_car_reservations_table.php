<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CarReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('viber')->nullable();
            $table->string('whatsap')->nullable();
            $table->string('telegram')->nullable();
            $table->string('status')->nullable();
            $table->string('pick_up_locations')->nullable();
            $table->string('return_locations')->nullable();
            $table->string('pick_up_date')->nullable();
            $table->string('return_date')->nullable();
            $table->string('pick_up_time')->nullable();
            $table->string('return_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_reservations');

    }
}
