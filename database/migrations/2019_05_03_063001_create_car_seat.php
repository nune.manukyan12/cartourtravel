<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarSeat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_seat', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();
            $table->string('name')->nullable();
            $table->string('currency')->nullable();
            $table->string('general_pic')->nullable();
            $table->integer('child_age')->nullable();
            $table->integer('car_seat_price')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_seat');
    }
}
