<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourDestinationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_destination', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->text('tour_destination')->nullable();
            $table->text('description')->nullable();
            $table->integer('km')->nullable();
            $table->string('duration')->nullable();
            $table->integer('sedan_price')->nullable();
            $table->integer('minivan_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_tour_destination');
    }
}
