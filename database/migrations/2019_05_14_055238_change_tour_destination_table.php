<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTourDestinationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_destination', function (Blueprint $table) {

            $table->text('destination')->after('parent_id')->nullable();
            $table->string('sedan_price_guide')->after('sedan_price')->nullable();
            $table->string('minivan_price_guide')->after('minivan_price')->nullable();
            $table->string('mini_bus')->after('minivan_price_guide')->nullable();
            $table->string('mini_bus_guide')->after('mini_bus')->nullable();
            $table->string('bus')->after('mini_bus_guide')->nullable();
            $table->string('bus_guide')->after('bus')->nullable();
            $table->dropColumn([ 'tour_destination']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_destination', function (Blueprint $table) {

            $table->text('tour_destination')->after('parent_id')->nullable();
            $table->dropColumn([ 'destination']);
            $table->dropColumn([ 'sedan_price_guide']);
            $table->dropColumn([ 'minivan_price_guide']);
            $table->dropColumn([ 'mini_bus']);
            $table->dropColumn([ 'mini_bus_guide']);
            $table->dropColumn([ 'bus']);
            $table->dropColumn([ 'bus_guide']);

        });
    }
}
