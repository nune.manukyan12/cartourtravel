<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTourDestinationTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_destination', function (Blueprint $table) {
            $table->string('price_person')->after('bus_guide')->nullable();
            $table->string('clock')->after('price_person')->nullable();
            $table->string('date')->after('clock')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_destination', function (Blueprint $table) {
            $table->dropColumn([ 'price_person','clock','date']);
        });
    }
}
