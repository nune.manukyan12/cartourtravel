<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TourPackageReservationCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_package_reservation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date_arrival')->nullable();
            $table->string('date_departure')->nullable();
            $table->string('adults')->nullable();
            $table->string('kids')->nullable();
            $table->string('people_arrival')->nullable();
            $table->string('people_departure')->nullable();
            $table->string('apartment')->nullable();
            $table->string('house')->nullable();
            $table->text('tour')->nullable();
            $table->string('last_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('viber')->nullable();
            $table->string('whatsapp')->nullable();
            $table->text('notes')->nullable();
            $table->string('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_package_reservation');
    }
}
