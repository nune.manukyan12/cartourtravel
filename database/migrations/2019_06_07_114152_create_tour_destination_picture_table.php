<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourDestinationPictureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_destination_picture', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tour_destination_id');
            $table->foreign('tour_destination_id')
                ->references('id')->on('tour_destination')
                ->onDelete('cascade');
            $table->string('picture')->nullable();
            $table->string('realy_picture')->nullable();
            $table->string('picture_size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_destination_picture');
    }
}
