<?php

use Illuminate\Database\Seeder;

class CarTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('car_types')->insert([
            'name' => 'Sedan',
        ]);
        DB::table('car_types')->insert([
            'name' => 'SUV',
        ]);
        DB::table('car_types')->insert([
            'name' => 'Minivan',
        ]);
    }
}
