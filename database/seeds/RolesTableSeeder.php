<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Role::query()->delete();
       \Spatie\Permission\Models\Role::query()->insert(
           array (
               0 =>
                   array (
                       'name' => 'Super',
                       'guard_name' => 'Super admin'

                   ),
               1 =>
                   array (

                       'name' => 'Admin',
                       'guard_name' => 'Admin'
                   ),
           ));
    }
}
