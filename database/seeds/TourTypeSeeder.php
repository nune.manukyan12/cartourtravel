<?php

use Illuminate\Database\Seeder;

class TourTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tour_types')->insert(
            [
                'name' => 'Regular Tours',
            ]
        );
        DB::table('tour_types')->insert(

            [
                'name' => 'Private Tours',
            ]
        );
        DB::table('tour_types')->insert(

            [
                'name' => 'Extrams Tours'
            ]
        );
    }
}
