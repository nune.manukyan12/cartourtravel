$(document).ready(function () {

    $('#translate-btn').on('click', function () {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var name = $('#en_name').val();
        var lang = $(this).attr('lang');
        var description = $('#en_description').val();
        $.ajax({
            url: '/admin/translate',
            type: 'POST',
            headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
            data: {name:name, description:description, lang:lang},
            success: function (result) {
                console.log(result);
                $('#'+lang+'_name').val(result.name);
                var desc_id = lang+'_description'
                CKEDITOR.instances[desc_id].setData(result.description);

            },
            error: function (errors) {

            }
        })
    })

    $('.translate-tab').on('click', function () {
        var lang = $(this).attr('lang');
        $('#translate-btn').css('display', 'block')
        $('#translate-btn').attr('lang', lang)
    })

    $('.picture-delete-btn').on('click', function () {
        var model = $(this).attr('model');
        var id = $(this).attr('data-id');
        if (confirm('Do you want to delete this item?')) {
            $.ajax({
                url: '/admin/' + model + '/image/delete/' + id,
                type: 'GET',
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                success: function (result) {
                    $('.error-update').css('display', 'none')
                    $('.success-update').text('')
                    $('.success-update').css('display', 'block')
                    $('.success-update').text('Picture deleted successfully')
                    $('#pic_container' + id).remove();
                },
                error: function (errors) {


                }
            })
        }
    })

    // price block
    $('#price-date').on('click', function () {
        $('.price-block').css('display', 'block')
        $('.price-block-without-date').css('display', 'none')
    })
    $('#same-price').on('click', function () {
        $('.price-block').css('display', 'none')
        $('.price-block-without-date').css('display', 'block')
    })
    $('.plus_dates_hotel').click(function () {
        var x = (Math.random().toString()).substr(2)
        $('.many_dates').append('<div class="row row_number_' + x + '" style="margin-bottom: 10px">\n' +
            '                                                    <div class="col-md-8">\n' +
            '                                                        <div class="row">\n' +
            '                                                            <div class="col-md-3">' +
            '                                                                <label style="margin-right: 3px">From</label>' +
            '                                                                <input class="date-from" type="date" name="date_from[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-3">\n' +
            '                                                                <label style="margin-right: 3px">To</label><br>' +
            '                                                                <input class="date-to" type="date" name="date_to[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-3">\n' +
            '                                                                <label style="margin-right: 3px">From</label>' +
            ' <select class="currency-select form-control"  name="currency[' + x + ']">\n' +
            '                                    <option value="usd">USD</option>\n' +
            '                                    <option value="amd">AMD</option>\n' +
            '                                    <option value="rub">RUB</option>\n' +
            '                                </select>' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-2">\n' +
            '                                                                <a class="btn btn-danger delete_date" title="Delete" number="' + x + '"><i\n' +
            '                                                                            class="fa fa-times" aria-hidden="true"></i></a>\n' +
            '                                                            </div>\n' +
            '                                                        </div>\n' +
            '                                                        <div class="row price-block-one">\n' +
            '                                                            <div class="col-md-3">' +
            '                                                                <label style="margin-right: 3px">Single</label><br>' +
            '                                                                <input class="date-from" type="text" name="single_price[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-3">\n' +
            '                                                                <label style="margin-right: 3px">Double/Twin</label><br>' +
            '                                                                <input class="date-to" type="text" name="double_price[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-3">\n' +
            '                                                                <label style="margin-right: 3px">Superior Single</label><br>' +
            '                                                                <input class="date-to" type="text" name="superior_price[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-3">\n' +
            '                                                                <label style="margin-right: 3px">Superior Double</label><br>' +
            '                                                                <input class="date-to" type="text" name="superior_double_price[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                        </div>\n' +
            '                                                        <div class="row price-block-one">\n' +
            '                                                            <div class="col-md-3">' +
            '                                                                <label style="margin-right: 3px">Family Suite</label><br>' +
            '                                                                <input class="date-from" type="text" name="family_price[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-3">\n' +
            '                                                                <label style="margin-right: 3px">Executive Suite</label><br>' +
            '                                                                <input class="date-to" type="text" name="executive_price[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-3">\n' +
            '                                                                <label style="margin-right: 3px">Deluxe Single</label><br>' +
            '                                                                <input class="date-to" type="text" name="deluxe_price[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-3">\n' +
            '                                                                <label style="margin-right: 3px">Deluxe Double</label><br>' +
            '                                                                <input class="date-to" type="text" name="deluxe_double_price[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                        </div>\n' +
            '                                                        <hr>\n' +
            '                                                        </div>\n' +
            '                                                </div>'
        )
        ;
    })
    $('.plus_dates').click(function () {
        var x = (Math.random().toString()).substr(2)
        $('.many_dates').append('<div class="row row_number_' + x + '" style="margin-bottom: 10px">\n' +
            '                                                    <div class="col-md-8">\n' +
            '                                                        <div class="row">\n' +
            '                                                            <div class="col-md-3">' +
            '                                                                <label style="margin-right: 3px">From</label>' +
            '                                                                <input class="date-from" type="date" name="date_from[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-3">\n' +
            '                                                                <label style="margin-right: 3px">To</label><br>' +
            '                                                                <input class="date-to" type="date" name="date_to[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-2">\n' +
            '                                                                <a class="btn btn-danger delete_date" title="Delete" number="' + x + '"><i\n' +
            '                                                                            class="fa fa-times" aria-hidden="true"></i></a>\n' +
            '                                                            </div>\n' +
            '                                                        </div>\n' +
            '                                                        <div class="row price-block-one">\n' +
            '                                                            <div class="col-md-3">' +
            '                                                                <label style="margin-right: 3px">Price</label><br>' +
            '                                                                <input class="date-from" type="text" name="price[' + x + ']">\n' +
            '                                                            </div>\n' +
            '                                                            <div class="col-md-2 form-group">' +
            '                                                                <label style="margin-right: 3px" name="currency[' + x + ']">Currency</label><br>' +
            '                                                                <select class="currency-select form-control"  name="currency[' + x + ']">\n' +
            '                                                                       <option value="usd">USD</option>\n' +
            '                                                                       <option value="amd">AMD</option>\n' +
            '                                                                       <option value="rub">RUB</option>\n' +
            '                                                                </select>\n' +
            '                                                            </div>\n' +
            '                                                        </div>\n' +
            '                                                        <hr>\n' +
            '                                                        </div>\n' +
            '                                                </div>');

    });
    $('.delete_date').click(function () {
        var number = $(this).attr('number')
        $('.row_number_' + number).remove()
    })
    $('.date-from').change(function () {

        var dateValue = $(this).val();
        $('.date-to').val(dateValue);
    })
    // end price block


// apartment create and update


    $('#content-content').on('click', '.buttonbtn', function () {

        var x = 1;
        var y = $(this).parent().find("input").val();
        console.log(y);
        var num = parseInt(y);
        var z = x + num;
        $(this).parent().find("input").val(z);


    });


    $('#content-content').on('click', '#bathrooms-plus', function () {
        var x = 0.5;
        var y = $(this).parent().find("input").val();
        var num = parseFloat(y);
        var z = x + num;

        $(this).parent().find("input").val(z);

    });


    $('#content-content').on('click', '.add-bedroom', function () {

        var bedrooms = $('#total-number-of-bedrooms').val();

        if (bedrooms > 0) {
            $('.bedroom-big-body').append(
                '                                           <div class="bedroom-body-' + bedrooms + '">\n' +
                '                                               <ul>\n' +
                '                                                   <p class="title-title">Bedroom  ' + bedrooms + '</p>\n' +
                '                                                   <li>\n' +
                '                                                       <p class="p-articale">Double</p>\n' +
                '                                                       <button type="button" class="btn btn-primary buttonminus"><i class="fas fa-minus"></i></button>\n' +
                '                                                       <input type="text" name="double[' + bedrooms + ']" class="input-num" value="0">\n' +
                '                                                       <button type="button" class="btn btn-primary buttonbtn"><i class="fas fa-plus"></i></button>\n' +
                '                                                   </li>\n' +
                '                                                   <li>\n' +
                '                                                       <p class="p-articale" >Queen</p>\n' +
                '                                                       <button type="button" class="btn btn-primary buttonminus"><i class="fas fa-minus"></i></button>\n' +
                '                                                       <input type="text" name="queen[' + bedrooms + ']" class="input-num" value="0">\n' +
                '                                                       <button type="button" class="btn btn-primary buttonbtn"><i class="fas fa-plus"></i></button>\n' +
                '                                                   </li>\n' +
                '                                                   <li>\n' +
                '                                                       <p class="p-articale">Single</p>\n' +
                '                                                       <button type="button" class="btn btn-primary buttonminus"><i class="fas fa-minus"></i></button>\n' +
                '                                                       <input type="text" name="single[' + bedrooms + ']" class="input-num" value="0">\n' +
                '                                                       <button type="button" class="btn btn-primary buttonbtn"><i class="fas fa-plus"></i></button>\n' +
                '                                                   </li>\n' +
                '                                                   <li>\n' +
                '                                                       <p class="p-articale">Sofa bed</p>\n' +
                '                                                       <button type="button" class="btn btn-primary buttonminus"><i class="fas fa-minus"></i></button>\n' +
                '                                                       <input type="text" name="sofa_bed[' + bedrooms + ']" class="input-num" value="0">\n' +
                '                                                       <button type="button" class="btn btn-primary buttonbtn"><i class="fas fa-plus"></i></button>\n' +
                '                                                   </li>\n' +
                '                                               </ul>\n' +
                '                                           </div>\n')
        }


    })


    $('#content-content').on('click', '.remove-bedroom', function () {
        if ($('#total-number-of-bedrooms').val() > 0) {
            var bedroomHide = $('#total-number-of-bedrooms').val();
            $(".bedroom-body-" + bedroomHide).remove();
        }
    });

    $('#content-content').on('click', '.buttonminus', function () {
        var x = -1;
        var y = $(this).parent().find("input").val();
        var num = parseInt(y);
        if (num > 0) {
            var z = x + num;
            $(this).parent().find("input").val(z);
        }
    });

    $('#content-content').on('click', '#bathrooms-minus', function () {
        var m = -0.5;
        var n = $(this).parent().find("input").val();
        var num = parseFloat(n);
        if (num > 0) {
            var z = m + num;
            $(this).parent().find("input").val(z);
        }
    });


    $('#air-conditioner').on('change', function () {
        if(this.checked){
            $('.number-air-conditioner-big').append(
                '<div class="number-air-conditioner">\n' +
                '                <button type="button" class="btn btn-primary buttonminus">\n' +
                '                <i class="fas fa-minus"></i>\n' +
                '                </button>\n' +
                '                <input type="text" name="number_air_conditioner" class="input-num" value="1">\n' +
                '                <button type="button" class="btn btn-primary buttonbtn">\n' +
                '                <i class="fas fa-plus"></i>\n' +
                '                </button>\n' +
                '                </div> \n'
            )
        }else {
            $('.number-air-conditioner').remove();

        }


    });
    // function tabcontrol() {
        var pageNumber=1;
        $('.next-button').click(function () {
            pageNumber++;
            if(pageNumber==2){
                $('.back-button').show();
            }
            var present=$('.nav-tabs').find(".active").attr('tab');
            var next= $('.nav-tabs').find(".active").attr('next');
            var finish= $('.nav-tabs').find(".active").attr('finish');
            if(finish){
                $('.next-button').hide();
                $('.save_item').show()
            }
            $('.nav-tabs').find(".active").children().removeClass();
            $('.nav-tabs').find(".active").children().attr('aria-expanded','false');
            $('.nav-tabs').find(".active").removeClass('active');

            $('.'+next).addClass('active');
            $('.'+next).children().addClass('active');
            $('.'+next).children().addClass('show');
            $('.'+next).children().attr('aria-expanded','true');

            $('.tab-content').find('#'+present).removeClass( 'active');
            $('.tab-content').find('#'+present).removeClass( 'show');

            $('.tab-content').find('#'+next).addClass( 'active');

        });
        $('.back-button').click(function () {
            pageNumber--;
            if (pageNumber==1){
                $('.back-button').hide();
            }
            var present=$('.nav-tabs').find(".active").attr('tab');
            var back= $('.nav-tabs').find(".active").attr('back');
            var finish= $('.nav-tabs').find(".active").attr('finish');

            if(finish){
                $('.next-button').show();
            }else{
                $('.next-button').show();
            }
            $('.nav-tabs').find(".active").children().removeClass();
            $('.nav-tabs').find(".active").children().attr('aria-expanded','false');
            $('.nav-tabs').find(".active").removeClass('active');

            $('.'+back).addClass('active');
            $('.'+back).children().addClass('active');
            $('.'+back).children().addClass('show');
            $('.'+back).children().attr('aria-expanded','true');

            $('.tab-content').find('#'+present).removeClass( 'active');
            $('.tab-content').find('#'+present).removeClass( 'show');

            $('.tab-content').find('#'+back).addClass( 'active');

        });
        $('.page-number-date').click(function () {
            pageNumber=$(this).attr('id');

            if (pageNumber==1){
                $('.back-button').hide();
                $('.next-button').show();
                $('.save_apartment').hide();
            }else if(pageNumber==2){
                $('.back-button').show();
                $('.next-button').show();
                $('.save_apartment').hide();
            }else if(pageNumber==3){
                $('.back-button').show();
                $('.next-button').show();
                $('.save_apartment').hide();
            }else if(pageNumber==4){
                $('.back-button').show();
                $('.next-button').hide();
                $('.save_apartment').show();
            }
        });


    function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 48.864716, lng: 2.349014},
            zoom: 15,
            scrollwheel: true,
        });

        var geocoder = new google.maps.Geocoder();
        // $(document).ready(function () {
        //     geocodeAddress(geocoder, map);
        //     number = 1;
        // });
        // document.getElementById('add_marker').addEventListener('click', function () {
        //     geocodeAddress(geocoder, map);
        //     number = 1;
        // });
    }


    function geocodeAddress(geocoder, resultsMap) {
        var address = $('#country').val() + ' ' + $('#city').val() + ' ' + $('#street').val();
        console.log(address);
        if (number == 0) {
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                    resultsMap.setCenter(results[0].geometry.location);
                    marker = new google.maps.Marker({
                        map: resultsMap,
                        position: results[0].geometry.location,
                        zoom: 12,

                    });
                    markers.push(marker);
                    number = 1;
                } else {

                }
            });
        } else {
            marker.setMap(null);
            markers = [];
            number = 0;
            geocodeAddress(geocoder, resultsMap);
        }
    }
    $( ".select-tour-type" ).change(function () {
        var str = [];
        $( "select option:selected" ).each(function() {
            str.push( $( this ).attr('value')) ;
        });
        $(".table-tour").hide();
        if (str[0]==2){
            $(".group-regular-type").show();

        }else if(str[0]==3){
            $(".private-type").show();

        }else {
            $(".extream-type").show();

        }
    });
    if (numberTour!== 'undefined'){
        var numRegular=numberTour;

    }else {
        var numRegular=1;

    }

    $('#regular_type').click(function () {
        $('.group-regular-type').append(
            ' <tr class="tour-info-tr">\n' +
            '                                       <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="regular_tour_destination['+numRegular+']">\n' +
            '                                       </td>\n' +
            '                                       <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="regular_km['+numRegular+']">\n' +
            '                                       </td>\n' +
            '                                       <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="regular_date['+numRegular+']">\n' +
            '                                       </td>\n' +
            '                                       <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="regular_clock['+numRegular+']">\n' +
            '                                       </td>\n' +
            '                                       <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="regular_duration['+numRegular+']">\n' +
            '                                       </td>\n' +
            '                                       <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="regular_price_person['+numRegular+']">\n' +
            '                                       </td>\n' +
            '                                      <td>\n'+
            '                                              <button type="button" attr="regular" class="btn btn-danger tour_info_delete" id=" "><i class="fa fa-trash"></i></button>\n'+
            '                                      </td>\n'+
            '                                   </tr>'
        );
        deleteDestination();
        numRegular++

    });




    if (numberTour){
        var numPrivate=numberTour;

    }else {
        var numPrivate=1;

    }
    $('#tour_info').click(function () {

        $('.private-type').append('<tr class="tour-info-tr">\n' +
            '                                    <td>\n' +
            '                                        <input type="text" class="input-tour-info " name="tour_destination['+numPrivate+']">\n' +
            '                                    </td>\n' +
            '                                    <td>\n' +
            '                                        <input type="text" class="input-tour-info " name="km['+numPrivate+']">\n' +
            '                                    </td>\n' +
            '                                    <td>\n' +
            '                                        <input type="text" class="input-tour-info " name="duration['+numPrivate+']">\n' +
            '                                    </td>\n' +
            '                                    <td>\n' +
            '                                        <input type="text" class="input-tour-info" name="sedan_price['+numPrivate+']">\n' +
            '                                    </td>\n' +
            '                                    <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="sedan_price_guide['+numPrivate+']">\n' +
            '                                     </td>\n'+
            '                                    <td>\n' +
            '                                        <input type="text" class="input-tour-info" name="minivan_price['+numPrivate+']">\n' +
            '                                    </td>\n' +
            '                                    <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="minivan_price_guide['+numPrivate+']">\n' +
            '                                       </td>\n'+
            '                                   <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="mini_bus['+numPrivate+']">\n' +
            '                                      </td>\n' +
            '                                       <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="mini_bus_guide['+numPrivate+']">\n' +
            '                                       </td>\n' +
            '                                       <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="bus['+numPrivate+']">\n' +
            '                                       </td>\n' +
            '                                       <td>\n' +
            '                                           <input type="text" class="input-tour-info" name="bus_guide['+numPrivate+']">\n' +
            '                                       </td>'+
            '                                    <td>\n' +
            '                                        <button type="button"  attr="private" class="btn btn-danger tour_info_delete" id=" "><i class="fa fa-trash"></i></button>\n' +
            '                                    </td>\n' +
            '                                </tr>');
        deleteDestination();
        numPrivate++;
    });

    deleteDestination();
    function deleteDestination() {
        $('.tour-info-tr').on('click', '.tour_info_delete', function () {
            // console.log( $(this).attr('attr'));
            if ($(this).attr('attr')=='regular'){
                numRegular--;
            }else {
                // console.log(numPrivate);
                numPrivate--;
            }

            if (confirm('Do you want to delete this item?')){
                $(this).parent().parent().remove();

                var id=$(this).attr('id');
                // console.log(id);

                $.ajax({
                    url: '/admin/tour/destination/delete',
                    type: 'POST',
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    data: {
                        id:id
                    },
                    success: function (result) {
                    },
                    error: function (errors) {

                    }
                })
            }
        });
    }
});





