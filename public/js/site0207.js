$(document).keydown(function (e) {
    switch (e.which) {
        case 37: // left
            $('.owl-next').click();
            break;

        case 38: // up
            break;

        case 39: // right
            $('.owl-prev').click();
            break;

        case 40: // down
            break;

        default:
            return;
    }
    e.preventDefault();
});
$(document).ready(function () {


    $('ul.navbar-nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });


    $('.accommodation-div-book').on('click', '.apartment-choose', function () {
        var dateArrival = $('#datepicker_arrival').val();
        var dateDeparture = $('#datepicker_departure').val();
        var id = $(this).attr('id');

        if (dateArrival && dateArrival) {
            $.ajax({
                url: '/accommodation/package/price',
                type: 'POST',
                dataType: "JSON",
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: {
                    dateArrival: dateArrival,
                    dateDeparture: dateDeparture,
                    id: id
                },
                success: function (result) {
                    console.log(result);
                    $('.date-apartment-package').html(dateArrival + ' -' + dateDeparture);
                    $('.name-apartment-package').html(result.price.apartment_name);
                    $('#apartment_package_price').show();
                    var origin = window.location.origin;
                    var link = origin + '/apartment/' + result.price.id;
                    $('.name-apartment-package').attr('href', link);

                    $('.price-apartment-package').html(result.price.final_price + ' ' + result.symbol);
                    if (Number.isInteger(result.discount)) {
                        $('.discount-apartment-package').html(result.price.discount + ' %');
                    }
                },
                error: function (errors) {


                }
            });
        }
        if ($(this).attr('attr') == 'apartment') {
            var id = $(this).attr('id');
            $('.apartment-hidden-input').val(id);
            $('.hover-div-apartment').hide();
            $(this).parent().parent().find('.hover-div-tour').show();
        } else {
            var id = $(this).attr('id');
            $('.house-hidden-input').val(id);
            $('.hover-div-house').hide();
            $(this).parent().parent().find('.hover-div-house').show();
        }
    });
    $('.accommodation-div-book').on('click', '#package_apartment_see', function () {

        $("#webPackageModal").modal("show");
        var id = $(this).attr('attr');
        var origin = window.location.origin;
        var path = origin + '/accommodation/package/' + id;
        $('.iframe-modal-page').attr('src', path);

    });


    $('.package-close-apartment').click(function () {
        $("#webPackageModal").modal("hide");

    });
    $('.package-close').click(function () {
        $("#modal_tour_create").modal("hide");
    });

    $('.info-tab-new').on('click', function () {

        $('.info-tab-new').removeClass('activ-link');
        $(this).addClass('activ-link');
        var active_content = $(this).attr('tab-content');

        $('.content-tab').css('display', 'none');
        $('#' + active_content).css('display', 'inline-block')
    });

    $('.with_read_more_dots').each(function () {
        $(this).html($(this).text().substr(0, 110).concat(' . . .'));
    });
    $('.with_read_more_dots_chauffeur').each(function () {
        $(this).html($(this).text().substr(0, 400).concat(' . . .'));
    });

    // $('.apartment-name').each(function (e) {
    //
    //     var countName = $(this).html($(this).text());
    //     if (countName.text().length > 25) {
    //         $(this).html($(this).text().substr(0, 45).concat(' . . .'));
    //
    //     }
    //
    // });

    $('.example').jdSlider();


    $('.scroll-to-top').on('click', function () {
        $('html, body').animate({scrollTop: 0}, 'slow');
    })

});
var map;
var number = 0;
var markers = [];

function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 48.864716, lng: 2.349014},
        zoom: 15,
        scrollwheel: true,
    });

    var geocoder = new google.maps.Geocoder();
    $(document).ready(function () {
        geocodeAddress(geocoder, map);
        number = 1;
    });
    // document.getElementById('add_marker').addEventListener('click', function () {
    //     geocodeAddress(geocoder, map);
    //     number = 1;
    // });
}


function geocodeAddress(geocoder, resultsMap) {
    var address = $('#country').val() + ' ' + $('#city').val() + ' ' + $('#street').val();
    console.log(address);
    if (number == 0) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    zoom: 12,

                });
                markers.push(marker);
                number = 1;
            } else {

            }
        });
    } else {
        marker.setMap(null);
        markers = [];
        number = 0;
        geocodeAddress(geocoder, resultsMap);
    }
}

function callendarInterval() {
    value = $('.click-calendar-val').val();
    checkInterval.push(value.trim());

    if (checkInterval[0]) {
        $('#check-in').text(checkInterval[0]);
        $('.date_from').val(checkInterval[0]);
    }
    if (checkInterval[1]) {
        $('#check-out').text(checkInterval[1]);
        $('.date_to').val(checkInterval[1]);

    }
    if (checkInterval.length == 2) {
        var day_from = checkInterval[0].split("-")[0];
        var day_to = checkInterval[1].split("-")[0];

        var trData = $('.dp_body').find('tr');


        trData.each(function (key, value) {

            var tdData = $(value).find('td');
            tdData.each(function (tdKey, tdValue) {
                if (!$(this).hasClass('dp_disabled')) {

                    var day = $(tdValue).html();
                    if (day < 10) {

                        day = '0' + day;
                    }
                    if (day >= day_from && day <= day_to) {
                        if ($(this).hasClass('bloke-calendar-day')) {
                            checkInterval = [];
                            bookDayDate = [];
                            $('td').css('background-color', 'white');
                            alert('You selected blocked day. Please choose other date')
                            return false;
                        } else {
                            $(this).css('background-color', '#337ab7');
                        }
                    }

                }
            })
        });
        checkInterval = [];
    } else {
        // checkInterval = [];
        // checkInterval.push($('.click-calendar-val').val().trim());
        // console.log(checkInterval);
        $('td').css('background-color', 'white');
        // $(this).css('background-color', '#337ab7');
    }
}