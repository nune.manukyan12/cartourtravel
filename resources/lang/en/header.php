<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'home' => 'Home',
    'apartment' => 'Apartments',
    'tour' => 'Tours',
    'hotel' => 'Hotels',
    'car' => 'Cars',
    'info' => 'Info',
    'medical' => 'MEDICAL TOURISM'

];