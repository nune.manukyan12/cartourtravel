<div class="global-search-block">
    <div class="container">

        <form action="{{url('/')}}" method="GET" class="">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav global-search-tab">
                        <li class="nav-item active">
                            <a class="nav-link active" data-toggle="tab" href="#accommodations">Accommodations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#rental_car">Rental Car</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tours">Tours</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#transfer">Transfer</a>
                        </li>
                    </ul>
                    <div class="tab-content tab-private-general">
                        <div id="accommodations" class="tab-pane active">
                            <div class="row mt-3">

                                <div class="col-md-12">
                                    <div class="col-sm-12   col-md-2 form-group">
                                        <input type="checkbox" class="" value="housing" name="housing"> <p class="p-checkbox">Housing</p>
                                    </div>
                                    <div class="col-sm-12   col-md-3 form-group">
                                        <input type="checkbox" class="" value="building" name="building"> <p class="p-checkbox" style="width: auto">Apartment in building </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 search-inputs">
                                    <div class="row">
                                        <div class="col-md-4 form-group search-calendar-div-row">
                                            <input id="apartment_city" type="text" class="form-control" placeholder="City"
                                                   autocomplete="off"
                                                   name="apartment_city" value="Armenia/Yerevan">
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row date_search_from">
                                            <div class="input-group">
                                                    <span class="input-group-addon"><i
                                                                class="far fa-calendar-alt not-close-calendar"></i></span>
                                                <input type="text" id="date-from" class="form-control date-from" name="apartment_date_from" placeholder="From" style="width: 100%">

                                            </div>
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row date_search">
                                            <div class="input-group">
                                                <input type="text" id="date-to" class="form-control date-to" name="apartment_date_to" placeholder="To" style="width: 100%">

                                            </div>
                                        </div>
                                        <div class="col-md-3 form-group search-calendar-div-row people-number-click" search-type="apartment">
                                            {{--<div class="input-group">--}}
                                                    {{--<span class="input-group-addon">  <i--}}
                                                                {{--class="fas fa-user-alt user-alt"></i></span>--}}
                                                {{--<input class="input-by-search text-center search-adults" type="text"--}}
                                                       {{--autocomplete="off"--}}
                                                       {{--value="{{(isset($adult))? $adult : '1 adult'}}"--}}
                                                       {{--name="apartment_adult">--}}
                                                {{--<input class="input-by-search text-center search-child" type="text"--}}
                                                       {{--autocomplete="off"--}}
                                                       {{--value="{{(isset($child))? $child : '0 child'}}"--}}
                                                       {{--name="apartment_child">--}}
                                            {{--</div>--}}
                                            <div class="input-group">
                                                    <span class="input-group-addon">  <i
                                                                class="fas fa-user-alt user-alt"></i></span>
                                                <input class="input-by-search text-center search-adults adultPopup" type="number"
                                                       autocomplete="off"
                                                       value="{{(isset($adult))? $adult : '1'}}"
                                                       name="apartment_adult" >
                                                <span class="popuptext" id="adultPopup">adult</span>
                                                <input class="input-by-search text-center search-child childPopup" type="number"
                                                       autocomplete="off"
                                                       value="{{(isset($child))? $child : '0'}}"
                                                       name="apartment_child">
                                                <span class="popuptext" id="childPopup">child</span>
                                            </div>
                                        </div>
                                        <div class="col-md-1 search-calendar-div-row">
                                            <button id="global-search" class="form-control btn  search-button">Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="rental_car" class="tab-pane fade">
                            <div class="row mt-3">

                                <div class="col-md-12">
                                    <div class="row" style="padding: 0 15px;">
                                        <div class="col-md-3 form-group">
                                            <input type="checkbox" class=""> <p class="p-checkbox" style="width: auto">Return to same location</p>
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <input type="checkbox" class=""> <p class="p-checkbox" style="width: auto">Return to different location</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 search-inputs">
                                    <div class="row">
                                        <div class="col-md-2 form-group search-calendar-div-row">
                                            <select id="pick_up" class="form-control" autocomplete="off" name="car_type" style="height: 36px !important">
                                                <option value="">Pick up</option>
                                                <option value="">Zvartnotz</option>
                                                <option value="">Gyumri</option>
                                                <option value="">Tbilisi</option>
                                            </select>
                                            {{--<input id="pick_up" type="text" class="form-control" placeholder="Pick up"--}}
                                            {{--autocomplete="off"--}}
                                            {{--name="car_type">--}}
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row">
                                            <select id="pick_up" class="form-control" autocomplete="off" name="drop_off" style="height: 36px !important">
                                                <option value="">Drop off</option>
                                                <option value="">Zvartnotz</option>
                                                <option value="">Gyumri</option>
                                                <option value="">Tbilisi</option>
                                            </select>
                                            {{--<input id="pick_up" type="text" class="form-control" placeholder="Pick up"--}}
                                            {{--autocomplete="off"--}}
                                            {{--name="car_type">--}}
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row">
                                            <select name="type_off_car"  class="form-control" autocomplete="off" style="height: 36px !important">
                                                <option value="" selected  >Type of car</option>
                                                <option value="M">M-Mlni</option>
                                                <option value="N">N-Mini Elite</option>
                                                <option value="E">E-Econamy</option>
                                                <option value="C">C-Compact</option>
                                                <option value="I">I-Intermediate</option>
                                                <option value="S">S-Standard</option>
                                                <option value="F">F-Full-size</option>
                                                <option value="P">P-Premium</option>
                                                <option value="V">V-Van</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row date_search_from">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i
                                                            class="far fa-calendar-alt not-close-calendar"></i></span>
                                                <input type="text" class="form-control date-from" name="car_date_from" placeholder="From" style="width: 100%">

                                            </div>
                                        </div>
                                        <div class="col-md-1 form-group search-calendar-div-row date_search">
                                            <div class="input-group">
                                                <input type="text" class="form-control date-to" name="car_date_to" placeholder="To" style="width: 100%">

                                            </div>
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row"
                                             search-type="car">
                                            <div class="input-group">
                                                <span class="input-group-addon">  <i
                                                            class="fas fa-user-alt user-alt"></i> Age</span>
                                                <input class="input-by-search text-center form-control" type="number"
                                                       value="18" name="car_age" min="18">
                                            </div>
                                        </div>
                                        <div class="col-md-1 search-calendar-div-row">
                                            <button id="global-search" class="form-control btn  search-button">Search
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div id="tours" class="tab-pane fade">
                            <div class="row mt-3">
                                <div class="col-md-12 search-inputs">
                                    <div class="row">
                                        <div class="col-md-2 form-group search-calendar-div-row">
                                                <select name="tour_type" id="tour_type" class="form-control select-car-search h-100">
                                                    <option value="" >Tour Type</option>
                                                    <option value="2">Regular Tours</option>
                                                    <option value="3">Private Tours</option>
                                                    <option value="4">Extream Tours</option>
                                                </select>
                                            {{--<input id="tour_type" type="text" class="form-control" placeholder="Tour Type" autocomplete="off">--}}
                                            {{--<input id="tour_type_value" type="hidden" class="form-control" placeholder="Tour Type"--}}
                                                   {{--name="tour_type" autocomplete="off">--}}
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row">
                                            <select name="tour_type_car" class="form-control select-car-search h-100">
                                                <option value="" >Tour Type Car</option>
                                                <option value="sedan">Sedan</option>
                                                <option value="minivan">Minivan</option>
                                                <option value="mini_bus">Mini bus</option>
                                                <option value="bus">Bus</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row date_search_from" >
                                            <div class="input-group">
                                                <span class="input-group-addon"><i
                                                            class="far fa-calendar-alt not-close-calendar "></i></span>
                                                <input type="text" class="form-control date-from" name="tour_date_from" placeholder="From" style="width: 100%">

                                            </div>
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row date_search">
                                            <div class="input-group">
                                                <input type="text" class="form-control date-to" name="tour_date_to" placeholder="To" style="width: 100%">

                                            </div>
                                        </div>
                                        <div class="col-md-3 form-group search-calendar-div-row people-number-click"
                                             search-type="tour">
                                            <div class="input-group">
                                                <span class="input-group-addon">  <i
                                                            class="fas fa-user-alt user-alt"></i></span>
                                                <input class="input-by-search search-adults text-center adultPopup" type="number" autocomplete="off"
                                                       value="{{(isset($adult))? $adult : '1'}}"
                                                       name="tour_adult">
                                                <span class="popuptext" id="adultPopup">adult</span>
                                                <input class="input-by-search search-child text-center childPopup" type="number"
                                                       value="{{(isset($child))? $child : '0'}}" autocomplete="off"
                                                       name="tour_child">
                                                <span class="popuptext" id="childPopup">child</span>
                                            </div>
                                        </div>
                                        <div class="col-md-1 search-calendar-div-row">
                                            <button id="global-search" class="form-control btn  search-button">Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="transfer" class="tab-pane fade">
                            <div class="row mt-3">

                                {{--<div class="col-md-12">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-2 form-group">--}}
                                            {{--<input type="checkbox" class="" name="transfer_pick_up"> Pick up--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-2 form-group">--}}
                                            {{--<input type="checkbox" class="" name="transfer_drop_off"> Drop off--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="row">
                                <div class="col-md-12 search-inputs">
                                    <div class="row">
                                        <div class="col-md-2 form-group search-calendar-div-row">
                                            {{--<input id="transfer_pick_up" type="text" class="form-control"--}}
                                                   {{--placeholder="Pick up"--}}
                                                   {{--name="transfer_pick_up">--}}
                                            <select id="pick_up" class="form-control" autocomplete="off" name="transfer_pick_up" style="height: 36px !important">
                                                <option value="">Pick up</option>
                                                <option value="">Zvartnotz</option>
                                                <option value="">Gyumri</option>
                                                <option value="">Tbilisi</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row">
                                            {{--<input id="transfer_drop_off" type="text" class="form-control"--}}
                                                   {{--placeholder="Drop off"--}}
                                                   {{--name="transfer_drop_off">--}}
                                            <select id="pick_up" class="form-control" autocomplete="off" name="transfer_drop_off" style="height: 36px !important">
                                                <option value="">Drop off</option>
                                                <option value="">Zvartnotz</option>
                                                <option value="">Gyumri</option>
                                                <option value="">Tbilisi</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row">
                                            <select name="car_type_transfer" class="form-control h-100">
                                                <option value="M">M-Mlni</option>
                                                <option value="N">N-Mini Elite</option>
                                                <option value="E">E-Econamy</option>
                                                <option value="C">C-Compact</option>
                                                <option value="I">I-Intermediate</option>
                                                <option value="S">S-Standard</option>
                                                <option value="F">F-Full-size</option>
                                                <option value="P">P-Premium</option>
                                                <option value="V">V-Van</option>
                                            </select>
                                            {{--<input id="car_type_value" type="hidden" class="form-control" placeholder="Car Type"--}}
                                                   {{--name="car_type_transfer">--}}
                                        </div>
                                        <div class="col-md-3  search-calendar-div-row">
                                            <div class="row">
                                                <div class="col-md-7 form-group date_search_from">
                                                    <div class="input-group">
                                                <span class="input-group-addon"><i
                                                            class="far fa-calendar-alt not-close-calendar"></i></span>
                                                        <input type="text" class="form-control date-from" name="transfer_date_from" placeholder="From" style="width: 100%">

                                                    </div>
                                                </div>
                                                <div class="col-md-5 form-group search-calendar-div-row date_search">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control date-to" name="transfer_date_to" placeholder="To" style="width: 100%">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 form-group search-calendar-div-row people-number-click"
                                             search-type="transfer">
                                            <div class="input-group">
                                                <span class="input-group-addon">  <i
                                                            class="fas fa-user-alt user-alt"></i></span>
                                                <input class="input-by-search search-adults text-center adultPopup" type="number"
                                                       value="{{(isset($adult))? $adult : '1'}}" autocomplete="off"
                                                       name="transfer_adult">
                                                <span class="popuptext adultPopupMini" id="adultPopup">adult</span>
                                                <input class="input-by-search  search-child text-center childPopup" type="number"
                                                       value="{{(isset($child))? $child : '0'}}" autocomplete="off"
                                                       name="transfer_child">
                                                 <span class="popuptext" id="childPopup">child</span>
                                            </div>
                                        </div>
                                        <div class="col-md-1 search-calendar-div-row">
                                            <button id="global-search" class="form-control btn  search-button">Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6 text-right">
                            <div class="room-adult-children display-block-none" style="display: none"
                                 id="people-number" search-type="">
                                <div class="display-block-none">
                                    <p class="display-inline-block adult-p display-block-none">Adult</p>
                                    <div class="display-inline-block display-block-none">
                                        <button type="button"
                                                class="buttons-padding btn btn-primary button-minus display-block-none">
                                            <i class="fas fa-minus display-block-none"></i></button>
                                        <input class="input-by-number text-center adult-input display-block-none"
                                               type="text" value="{{(isset($adult))? $adult : '1'}}">
                                        <button type="button"
                                                class="buttons-padding btn btn-primary button-plus display-block-none">
                                            <i class="fas fa-plus display-block-none"></i></button>
                                    </div>
                                </div>
                                <div class="display-block-none">
                                    <p class="display-inline-block children-p display-block-none">Children</p>
                                    <div class="display-inline-block display-block-none">
                                        <button type="button"
                                                class="buttons-padding btn btn-primary button-minus display-block-none">
                                            <i class="fas fa-minus display-block-none"></i></button>
                                        <input class="input-by-number text-center children-input display-block-none"
                                               type="text" value="{{(isset($child))? $child : '0'}}">
                                        <button type="button"
                                                class="buttons-padding btn btn-primary button-plus display-block-none">
                                            <i class="fas fa-plus display-block-none"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $('.date-from').Zebra_DatePicker({
        format: ' d/m/Y',
        custom_classes: {
            'not-close-calendar': ['1-31']
        }

    });
    $('.date-to').Zebra_DatePicker({
        format: ' d/m/Y',
        custom_classes: {
            'not-close-calendar': ['1-31']
        }

    });

    $(document).ready(function () {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth() + 1;
        var y = date.getFullYear();
        $(".date-from").attr("placeholder", d + '/' + m + '/' + y);
        $(".date-to").attr("placeholder", d + '/' + m + '/' + y);

    });
    $('.button-minus').on('click', function () {
        var y = $(this).parent().find("input").val();
        var search_cat = $('#people-number').attr('search-type');
        console.log(search_cat);
        if (y > 0) {
            var x = -1;
            var num = parseInt(y);
            var z = x + num;
            $(this).parent().find("input").val(z);
            var numberChildren = $('input[name="' + search_cat + '_child"]').val().slice(0, 1);
            var numberAdult = $('input[name="' + search_cat + '_adult"]').val().slice(0, 1);
            var adultIinput = $('.adult-input').val();
            var childrenIinput = $('.children-input').val();

            if (childrenIinput != numberChildren) {
                $('.children-number').val(z);
                $('input[name="' + search_cat + '_child"]').val(z + ' child')
            }
            if (adultIinput != numberAdult) {
                $('.adult-number').val(z);
                $('input[name="' + search_cat + '_adult"]').val(z + ' adults')

            }
        }
    });
    $('.button-plus').on('click', function () {
        var x = 1;
        var y = $(this).parent().find("input").val();
        var search_cat = $('#people-number').attr('search-type');
        var nu = parseInt(y);
        var z = x + nu;
        $(this).parent().find("input").val(z);
        var numberChildren = $('input[name="' + search_cat + '_child"]').val().slice(0, 1);
        var numberAdult = $('input[name="' + search_cat + '_adult"]').val().slice(0, 1);
        console.log(numberChildren);
        var adultIinput = $('.adult-input').val();
        var childrenIinput = $('.children-input').val();
        console.log(childrenIinput);
        if (childrenIinput != numberChildren) {
            $('.children-number').val(z);
            $('input[name="' + search_cat + '_child"]').val(z + ' child')
        }
        if (adultIinput != numberAdult) {
            $('.adult-number').val(z);
            $('input[name="' + search_cat + '_adult"]').val(z + ' adults')
        }
    });

    $('.nav-link').on('click', function () {
        $('.tab-pane').css('display', '');
        $('.add-search').css('background-color', '#fff');

    });

    $('body').on('click', function (e) {
        if (!$(event.target).closest('.people-number-click').length) {
            if (!$(event.target).closest('#people-number').length) {
                $('#people-number').css('display', 'none')
                $('#people-number').attr('search-type', '')
            }
        }

    })


    function autocomplete(inp, arr, value_element) {

        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function (e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
//            if (!val) { return false;}
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            console.log(arr);
            $.each(arr, function(key, value) {

                $(this).val(value);
//            })
            /*for each item in the array...*/
//            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
//                val=key
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + value + "</strong>";

                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + value + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
//                    inp.text = value;
                    inp.value = value;
                    document.getElementById(value_element).value = key;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            })

        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("click", function (e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");

        });

        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;

            removeActive(x);

            x[currentFocus].classList.add("autocomplete-active");
        }

        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }

        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }

        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }

    var apartment_city = {"Armenia Yerevan":"Armenia Yerevan", "Armenia Hrazdan":"Armenia Hrazdan", 'Armenia Gyumri':'Armenia Gyumri'};
    autocomplete(document.getElementById("apartment_city"), apartment_city);

    var car_type = {
        "2":"Sedan",
        "3":"SUV",
        "4":'Minivan'
       };

    autocomplete(document.getElementById("car_type"), car_type, 'car_type_value');
    var tour_type = {"3":"Private Tour", "2":"Group Tour", "4":"Extream Tour"};
    autocomplete(document.getElementById("tour_type"), tour_type, 'tour_type_value');

    var transfer_pick_up = {"Zvartnots International airport":"Zvartnots International airport","Gyumri airport":"Gyumri airport","Tbilisi airport":"Tbilisi airport","Yerevan downtown":"Yerevan downtown", "Gyumri downtown":"Gyumri downtown", "Tbilisi downtown":"Tbilisi downtown"};
    autocomplete(document.getElementById("transfer_pick_up"), transfer_pick_up);
    autocomplete(document.getElementById("pick_up"), transfer_pick_up);

</script>


<style>

    .autocomplete {
        position: relative;
        display: inline-block;
    }

    * {
        box-sizing: border-box;
    }

    .global-search-block {
        font: 16px Arial;
    }

    .autocomplete-items {
        position: absolute;
        border: 1px solid #d4d4d4;
        border-bottom: none;
        border-top: none;
        z-index: 99;
        /*position the autocomplete items to be the same width as the container:*/
        top: 100%;
        left: 0;
        right: 0;
    }

    .autocomplete-items div {
        padding: 10px !important;
        cursor: pointer;
        background-color: #fff;
        border-bottom: 1px solid #d4d4d4;
    }

    /*when hovering an item:*/
    .autocomplete-items div:hover {
        background-color: #e9e9e9;
    }

    .autocomplete-active {
        background-color: DodgerBlue !important;
        color: #ffffff;
    }

    .global-search-block {
        padding: 2%;
        background-color: #ffffff8f;
        height: auto;
    }

    .global-search-select {
        height: 10% !important;
    }

    .label-inherit {
        display: inherit;
    }

    .global-search-div {
        text-align: right;
    }

    .tab-private-general div.active, .global-search-tab li.active a {
        background-color: #d0e0e8 !important;
        margin-bottom: 0px !important;
        margin-left: 0px !important;
    }

    .global-search-tab li.active {
        margin: 0px;
    }

    .global-search-tab li.active a {
        height: 105%;
    }

    .search-calendar-div-row {
        border-top: none;
        border-right: none;
        border-left: none;
        height: 38px;
    }

    .tab-pane {
        padding-top: 1%;
    }

    .global-search-block .far, .global-search-block .fas {
        color: #d02d2d;
    }

    #accommodations, #rental_car, #tours {
        margin-bottom: 10px !important;
    }
    .search-adults,.search-child{
        border: 1px solid #ccd0d2;
    }
    .search-calendar-div-row{
        border: none;
    }

    .search-inputs div {
        margin: 0px;
        padding: 0px;
    }
    .date-to{
        width: 129%;
    }
    .popuptext {
        visibility: hidden;
        width: 120px;
        background-color: #555;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        position: absolute;
        z-index: 1;
        bottom: 125%;
        left: 36%;
        margin-left: -60px;
        opacity: 0;
        transition: opacity 0.3s;
    }

    .popuptext::after {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: #555 transparent transparent transparent;
    }

    .adultPopup:hover ~ #adultPopup{
        visibility: visible;
        opacity: 1;
    }
    .childPopup:hover ~ #childPopup{
        visibility: visible;
        opacity: 1;
    }
    #childPopup{
        left: 77%;
    }
    .adultPopupMini{
        left: 41%;
    }
</style>










