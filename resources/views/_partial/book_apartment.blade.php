<div class="container-fluid big-background-img" style="position:relative;" id="book-block-apartment">
    <div class="fixed-price">
        <div class="row">
            <div class="col-sm-3">
                <div class="div-fixed-enter-text">
                    <p class="enter-apartment-in">Enter apartment in {{$apartment->name}}</p>
                </div>
            </div>
            <div class="col-sm-3"></div>
            <div class="col-sm-3"></div>
            <div class="col-sm-3">
                <div class="div-fixed-button">
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-4">
                            <p class="night-price"><span
                                        class="curr">{{ (\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? strtoupper(session()->get('currency_symbol')).' ' : '$ '}}</span>
                                {{(\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price):$apartment->prices[0]->base_price}}
                                /NIGHT</p>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-4">
                            <a href="#book-block-apartment" type="button" class="btn btn-primary">Book</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="{{ asset("uploads/".$apartment->general_pic)}} " alt="" style="width:100%;height: 600px;">
    <div class="search-calendar-choose">
        <div class="apartment-book">
            <p class="apartment-search-title text-center">Book this Apartment</p>
            <div class="parameter-book-apartment container">
                <form id="book-form">
                    <div class="first-block">
                        <div class="row" style="    margin-left: 10px;">
                            <div class="col-md-6 book-calendar-div-apartment col-4-div-button col-div-calendar-open not-close-calendar">
                                <div class="col-div-button not-close-calendar">
                                    <i class="fa fa-calendar-alt not-close-calendar" style="color: #636b6f;"></i>
                                    <p class="search-adults not-close-calendar"
                                       id="check-in">{{(isset($date_check_in))? $date_check_in : 'Check-in'}}</p>-
                                    <p class="search-adults not-close-calendar"
                                       id="check-out">{{(isset($date_check_out))? $date_check_out : 'Check-out'}}</p>
                                </div>
                            </div>
                            <div class="col-md-5 form-group book-calendar-div-apartment people-number-click"
                                 id="people-number-click">
                                <input type="hidden" value="1" class="hidden-people-number display-block-none">
                                <div class="input-group">
                                                <span class="input-group-addon">  <i
                                                            class="fas fa-user-alt user-alt"></i></span>
                                    <input class="input-by-search text-center search-adults" type="text"
                                           value="{{(isset($adult))? $adult : '1 adult'}}"
                                           name="apartment_adult">
                                    <input class="input-by-search text-center search-child" type="text"
                                           value="{{(isset($child))? $child : '0 child'}}"
                                           name="apartment_child">
                                </div>
                            </div>
                            <input type="hidden" class="click-calendar-val" id="demo" data-zdp_direction="[1,365]">
                            <input type="hidden" class="calendar-hidden-input" value="0">
                            <input type="hidden" class="date_from" name="date_from"
                                   value="{{(isset($date_check_in))? $date_check_in : ''}}">
                            <input type="hidden" class="date_to" name="date_to"
                                   value="{{(isset($date_check_out))? $date_check_out : ''}}">

                            <div id="calendar" style="display: none"></div>
                        </div>
                        <div class="row">
                            <div class="room-adult-children display-block-none" style="display: none"
                                 id="people-number">
                                <div class="display-block-none">
                                    <p class="display-inline-block adult-p display-block-none">Adult</p>
                                    <div class="display-inline-block display-block-none">
                                        <button type="button"
                                                class="buttons-padding btn btn-primary button-minus display-block-none">
                                            <i
                                                    class="fas fa-minus display-block-none"></i></button>
                                        <input class="input-by-number text-center adult-input display-block-none"
                                               type="text"
                                               value="{{(isset($adult))? $adult : '1'}}">
                                        <button type="button"
                                                class="buttons-padding btn btn-primary button-plus display-block-none">
                                            <i
                                                    class="fas fa-plus display-block-none"></i></button>
                                    </div>
                                </div>
                                <div class="display-block-none">
                                    <p class="display-inline-block children-p display-block-none">Children</p>
                                    <div class="display-inline-block display-block-none">
                                        <button type="button"
                                                class="buttons-padding btn btn-primary button-minus display-block-none">
                                            <i
                                                    class="fas fa-minus display-block-none"></i></button>
                                        <input class="input-by-number text-center children-input display-block-none"
                                               type="text"
                                               value="{{(isset($child))? $child : '0'}}">
                                        <button type="button"
                                                class="buttons-padding btn btn-primary button-plus display-block-none">
                                            <i
                                                    class="fas fa-plus display-block-none"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4" style="margin-left: 15px;border-bottom: 1px solid;">
                                <span class="pull-left" style="font-size: 20px;">Nightly Price</span>
                                <p class="pull-right"
                                   style="font-size: 20px;color: #52f7ef">{{ (\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? strtoupper(session()->get('currency_symbol')).' ' : '$ '}}</span>
                                    {{(\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price):$apartment->prices[0]->base_price}}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4" style="margin-left: 15px;border-bottom: 1px solid;">
                                <span class="pull-left" style="font-size: 20px;">Total Price</span>
                                <p class="pull-right span-for-price" style="font-size: 20px;color: #52f7ef"></p>
                            </div>
                        </div>
                    </div>

                    <div class="next-block" style="display: none">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                                <p class="apartment-search-title text-center">Please enter your info for booking. </p>
                            </div>
                        </div>
                        <input type="hidden" value="{{$apartment->id}}" name="apartment_id">
                        <div class="row">

                            <div class="col-sm-5 div-modal-book div-first-block ">
                                <label for="">Last name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control" id="last-name" placeholder="Last name"
                                       name="last_name">
                                <p class="error-last-name"></p>

                            </div>
                            <div class="col-sm-5 div-modal-book div-second-block ">
                                <label for="">First name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control" id="first-name" placeholder="First name"
                                       name="first_name">
                                <p class="error-first-name"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 div-modal-book div-first-block">
                                <label for="">Email address<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="email" class="form-control email" id="email" placeholder="Email address"
                                       name="email">
                                <p class="error-email"></p>

                            </div>
                            <div class="col-sm-5 div-modal-book div-second-block">
                                <label for="">Phone<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control phone" id="phone" placeholder="Phone"
                                       name="phone">
                                <p class="error-phone"></p>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5  div-modal-book div-first-block">
                                <label for="">Viber/Whatsapp:</label>
                                <input type="text" class="form-control viber" id="viber" placeholder="Viber"
                                       name="viber">
                            </div>
                            <div class="col-sm-5  div-modal-book div-second-block">
                                <label for="">Telegram:</label>
                                <input type="text" class="form-control" id="telegram" placeholder="Telegram"
                                       name="telegram">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4" style="margin-left: 15px;border-bottom: 1px solid;">
                                <span class="pull-left" style="font-size: 20px;">Nightly Price</span>
                                <p class="pull-right"
                                   style="font-size: 20px;color: #52f7ef">{{ (\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? strtoupper(session()->get('currency_symbol')).' ' : '$ '}}</span>
                                    {{(\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price):$apartment->prices[0]->base_price}}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4" style="margin-left: 15px;border-bottom: 1px solid;">
                                <span class="pull-left" style="font-size: 20px;">Total Price</span>
                                <p class="pull-right span-for-price" style="font-size: 20px;color: #52f7ef"></p>
                            </div>
                        </div>

                    </div>
                    <div class="row" style="margin-top: 5px">
                        <div class="col-md-8"></div>
                        <div class="col-3 col-md-2 back-step" style="display: none">
                            <button type="button" class="btn btn-danger back-step"><i
                                        class="fas fa-arrow-left"></i> Back
                            </button>
                        </div>
                        <div class="col-3 col-md-2 next-step">
                            <button type="button" class="btn btn-success">Next
                            </button>
                        </div>
                        <div class="col-3 col-md-2 send" style="display: none">
                            <button type="button" class="btn btn-primary " id="book-book">
                                Finish
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    input{
        color: black;
    }
    .people-number-click {
        background: transparent;
    }
</style>
<script>
    //function by convert milisecunds to data
    var availableApartment = [];
    availableApartment =<?php
    if ($apartment->available_apartment) {
        echo $apartment->available_apartment;
    } else {
        echo 1;
    }
    ?>;
    var blokedDayDisable = [];
    var bookDays = [];// bazayum pahelu hamar erku or@
      bookDayDate = [];// lriv orer@ gtnelu hamar
    var chooseDate = [];//lriv orer@ sccesum ekac
    var chooseDataNigPr = [];
    var nightlyDayPrice = {};
    for (var i = 0; i < availableApartment.length; i++) {
        if (availableApartment[i].blocked == 2) {
            var nightlyDay = JSON.parse("[" + availableApartment[i]['date'] + "]");
            // console.log(nightlyDay);
            for (var n = 0; n < nightlyDay[0].length; n++) {
                var datetime = nightlyDay[0][n];
                var dateobj = new Date(datetime);

                function pad(n) {
                    return n < 10 ? "0" + n : n;
                }

                var result = pad(dateobj.getDate()) + " " + pad(dateobj.getMonth() + 1) + " " + dateobj.getFullYear();
                nightlyDayPrice[n + "key-" + availableApartment[i].nightly_price] = [result];
                // console.log(nightlyDayPrice);
            }


        } else {
            // console.log(JSON.parse("[" + availableApartment[i]['date'] + "]"));
            var blockedDay = JSON.parse("[" + availableApartment[i]['date'] + "]");
            for (var m = 0; m < blockedDay[0].length; m++) {
                var datetime = blockedDay[0][m];
                var dateobj = new Date(datetime);

                function pad(n) {
                    return n < 10 ? "0" + n : n;
                }

                var result = pad(dateobj.getDate()) + " " + pad(dateobj.getMonth() + 1) + " " + dateobj.getFullYear();
                blokedDayDisable.push(result);
                // console.log(blokedDayDisable);

            }
        }
    }


    checkInterval = [];

    $(".container-fluid").on('click', '.dp_body>tr>td', function (e) {
        if (!$(e.target).hasClass('dp_disabled')) {
            if (!$(e.target).hasClass('bloke-calendar-day')) {
                if (bookDayDate.length < 2) {
                    if ($('.click-calendar-val').val() != 0) {
                        bookDayDate.push($('.click-calendar-val').val());
                        callendarInterval();
                        if (bookDayDate.length > 1) {
                            var date_from = bookDayDate[0];
                            var date_to = bookDayDate[1];
                            var id = '<?php echo $apartment->id ?>';

                            $.ajax({
                                url: '/reservation/calculation',
                                type: 'POST',
                                dataType: "JSON",
                                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                                data: {
                                    date_from: date_from,
                                    date_to: date_to,
                                    id: id,
                                },
                                success: function (response) {
                                    console.log(response.symbol)
                                    bookDayDate =[];
                                        $('.span-for-base-price').css('display', 'none');
                                        $(".span-for-price").text(response.symbol+ ' '+ response.final_price);

                                },
                                error: function (errors) {

                                }
                            });

                        }
                    }
                } else {
                    bookDays = [];
                    bookDayDate = [];
                }
            }else{
                alert('You select blocked date');
            }
        }
    });


    var calendar = $('#demo').Zebra_DatePicker({
        always_visible: $('#calendar'),
        format: 'd-m-Y',
        custom_classes: {
            'bloke-calendar-day': blokedDayDisable,
        },
        onSelect: function (a, b, c) {
            $(this).addClass('test');

        },
        onChange: function (view, elements) {
            if (view === 'days') {
                // console.log(view);
                elements.each(function () {
                    for (var i = 1; i < 32; i++) {
                        if ($(this).data('date').match(/[1]/g))
                            $(this).addClass('not-close-calendar');
                    }

                });
            }
        }

    });


    $('.col-div-calendar-open').on('click', function () {
        var num = $('.calendar-hidden-input').val();
        if (num == 0) {
            $('#calendar').css('display', 'block');
            $('.calendar-hidden-input').val(1);

        } else {
            $('#calendar').css('display', 'none');
            $('.calendar-hidden-input').val(0);


        }
    });



//


    $('body').on('click', function () {
        if (!$(event.target).closest('.people-number-click').length) {
            if (!$(event.target).closest('#people-number').length) {
                $('#people-number').css('display', 'none')
            }
        }
        if (!$(event.target).closest('.book-calendar-div-apartment').length) {
            if (!$(event.target).closest('#calendar').length) {
                $('#calendar').css('display', 'none')
            }
        }

    })
    $('.button-minus').on('click', function () {
        var y = $(this).parent().find("input").val();
        var search_cat = $('#people-number').attr('search-type');
        console.log(search_cat);
        if (y > 0) {
            var x = -1;
            var num = parseInt(y);
            var z = x + num;
            $(this).parent().find("input").val(z);
            var numberChildren = $('input[name="apartment_child"]').val().slice(0, 1);
            var numberAdult = $('input[name="apartment_adult"]').val().slice(0, 1);
            var adultIinput = $('.adult-input').val();
            var childrenIinput = $('.children-input').val();

            if (childrenIinput != numberChildren) {
                $('.children-number').val(z);
                $('input[name="apartment_child"]').val(z + ' child')
            }
            if (adultIinput != numberAdult) {
                $('.adult-number').val(z);
                $('input[name="apartment_adult"]').val(z + ' adults')

            }
        }
    });
    $('.button-plus').on('click', function () {
        var x = 1;
        var y = $(this).parent().find("input").val();
        var search_cat = $('#people-number').attr('search-type');
        var nu = parseInt(y);
        var z = x + nu;
        $(this).parent().find("input").val(z);
        var numberChildren = $('input[name="apartment_child"]').val().slice(0, 1);
        var numberAdult = $('input[name="apartment_adult"]').val().slice(0, 1);
        console.log(numberChildren);
        var adultIinput = $('.adult-input').val();
        var childrenIinput = $('.children-input').val();
        console.log(childrenIinput);
        if (childrenIinput != numberChildren) {
            $('.children-number').val(z);
            $('input[name="apartment_child').val(z + ' child')
        }
        if (adultIinput != numberAdult) {
            $('.adult-number').val(z);
            $('input[name="apartment_adult"]').val(z + ' adults')
        }
    });
    $('#see_more_filter').click(function () {
        if ($('.see-more-filter').val() == 0) {
            $(".apartment-search").animate({width: '700px'}, 1000);
            $(".button-car-s").animate({width: '640px'}, 1000);
            $('#see_more_filter').text('CLOSE');
            $('.see-more-filter').val(1);
            // $('.more-search-detalis-car').show(500);
            setTimeout(function () {
                $('.more-search-div').show(700);
                setTimeout(function () {
                    $('.more-search-div-1').show(1000);

                }, 200);

            }, 200);
        } else {
            $(".apartment-search").animate({width: '366px'}, 1000);
            $(".button-car-s").animate({width: '297px'}, 1000);
            $('#see_more_filter').text('MORE');
            $('.see-more-filter').val(0);

            $('.more-search-div-1').hide(1000);

            setTimeout(function () {
                $('.more-search-div').hide(800);
            }, 200);

        }
    })

    $('.next-step').click(function () {
        $('.next-block').css('display', 'block')
        $('.back-step').css('display', 'block')
        $('.send').css('display', 'block')
        $('.first-block').css('display', 'none')
        $(this).css('display', 'none')
        $('.apartment-book').animate({height: '80%'}, 1000);
    })
    $('.back-step').click(function () {
        $('.next-block').css('display', 'none')
        $('.first-block').css('display', 'block')
        $('.send').css('display', 'none')
        $(this).css('display', 'none')
        $('.next-step').css('display', 'block')
        $('.apartment-book').animate({height: '40%'}, 1000);

    })

</script>