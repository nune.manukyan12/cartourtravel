@extends('layouts.app')
@section('content')

    <div class="container-fluid container-padding">
        <div class="row">
            <div class="col-sm-7">
                <p class="apartment-title">Tour this apartment</p>

                <div class="owl-theme owl-height  owl-carousel">
                    @foreach($apartment->picture as $picture)
                        <div >
                            <img  src="{{asset('uploads/'.$picture->name)}}" style="width: 380px; height: 220px;">
                        </div>
                    @endforeach

                </div>


            </div>
            <div class="col-sm-5">
                <h4 class="comp-tour-travel">{{$apartment->name}}</h4>
                @foreach($apartment->details as $detail)
                    <p class="text-capitalize font-weight-bold">{{$detail->property_type}}</p>
                @endforeach
                <p><?=$apartment->description?></p>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-8">
                <?php $numberBedroom = 1; ?>
                @foreach($apartment->details[0]->bedrooms as $bedroom)

                    @if($bedroom->double!=0)
                        <p class="p-rooms-class"><span class="color-black-blod">Bedroom {{$numberBedroom}}
                                :</span> {{$bedroom->double}} <span>double bed</span>
                            <img src="{{asset("img/queen-bed.png")}}" style="width: 20px; height: 20px" alt="">
                            {{--                                <img src="{{asset("img/single-bed.png")}}" style="width: 20px; height: 20px" alt="">--}}
                        </p>
                    @endif

                    @if($bedroom->queen!=0)
                        <p class="p-rooms-class"><span class="color-black-blod">Bedroom {{$numberBedroom}}
                                :</span> {{$bedroom->queen}} <span>queen bed</span> <img
                                    src="{{asset("img/queen-bed.png")}}" style="width: 20px; height: 20px" alt="">
                        </p>
                    @endif
                    @if($bedroom->single!=0)
                        <p class="p-rooms-class"><span class="color-black-blod">Bedroom {{$numberBedroom}}
                                :</span> {{$bedroom->single}} <span>single bed</span>
                            <img src="{{asset("img/single-bed.png")}}" style="width: 20px; height: 20px" alt="">
                            @if($bedroom->single ==2)
                                <img src="{{asset("img/single-bed.png")}}" style="width: 20px; height: 20px" alt="">
                            @endif
                            @if($bedroom->single ==3)
                                <img src="{{asset("img/single-bed.png")}}" style="width: 20px; height: 20px" alt="">
                            @endif
                        </p>
                    @endif
                    @if($bedroom->sofa_bed!=0)
                        <p class="p-rooms-class"><span class="color-black-blod">Bedroom {{$numberBedroom}}
                                :</span> {{$bedroom->sofa_bed}} <span>sofa bed</span> <img
                                    src="{{asset("img/sofa-bed.png")}}" style="width: 20px; height: 20px" alt="">
                        </p>
                    @endif
                    <?php $numberBedroom++; ?>
                @endforeach
                @if($apartment->details[0]->number_sofa_bed != 0)
                    <p class="p-rooms-class"><span class="color-black-blod">Living space
                                    :</span> {{$apartment->details[0]->number_sofa_bed}} <span>sofa bed</span> <img
                                src="{{asset("img/sofa-bed.png")}}" style="width: 20px; height: 20px" alt="">
                    </p>
                @endif
            </div>
            <div class="col-sm-4"></div>
        </div>

        <div class="row margin-left-0-px">
            <p class="apartment-title amenities-p">Amenities</p>
        </div>
        <div class="row margin-left-0-px">
            <p>These amenites are available to you.</p>
        </div>
        <div class="row div-by-img-serch">

            <?php
            $detail = $apartment->details[0]->toArray();
            $array = array_sort_recursive($detail);
            $see_detalis = 0;
            ?>
            @foreach($sortDetails as $key => $value)
                @if ($value === "yes")
                    <?php $see_detalis++; ?>
                    @if($see_detalis>7)
                        <div class="div-detail-8" style="display: none">
                            @endif
                            <div class="div-by-img">
                                <img src="{{ asset("img/$key.jpg")}}" alt="" style="width: 146px; height: 130px;">
                                <p class="text-capitalize name-amenities{{$see_detalis}}" id="{{$key}}"></p>
                            </div>
                            @if($see_detalis>7)
                        </div>
                    @endif
                @endif
            @endforeach


        </div>
        <div class="row row-by-button-see-all-detail margin-left-0-px" style="width: 100%;">
            <button type="button" class="btn  button-by-button-see-all-detail">See more</button>
            <input type="hidden" value="0" class="hidden-by-see-all">
        </div>
        <p><br></p>
        <div class="row  margin-left-0-px">
            <p class="apartment-title">Location</p>
        </div>

        <div>

            @foreach($apartment->details as $detail)
                <div class="row margin-left-0-px">
                    <p >{{$apartment->address}},{{$apartment->city}}, {{$apartment->country}}</p>
                </div>
            @endforeach

            <div class="row">
                <div class="content-tab" id="location" style=" height: auto">
                    <div id="map" style="height: 500px;width: 100%" class="text"></div>
                    <input id="country" value="{{$apartment->country}}" type="hidden">
                    <input id="city" value="{{$apartment->city}}" type="hidden">
                    <input id="area" value="{{$apartment->district}}" type="hidden">
                    <input id="street" value="{{$apartment->address}}" type="hidden">
                </div>
            </div>
        </div>
    </div>
    </div>


    <style>
        .navbar-expand-lg {
            display: none;
        }
        .footer{
            display: none;
        }
        .div-by-img-serch {
            padding-top: 0px !important;
            padding-bottom: 0px;
            padding: 4%;
        }
        .container-padding{
            padding-left: 30px;
        }
        .content-tab{
            margin-right: 20px;
            margin-left: 20px;
            width: 100%;
            height: auto;
        }
        .owl-nav {
            width: 95%;
            top: 32%;
            position: absolute;
            font-size: 30px;
        }
        .owl-height {
            height: 250px;
        !important: ;
        }
        .margin-left-0-px{
            margin-left: 0px !important;
        }
    </style>
    <script>
$('.navbar-expand-lg').remove();
        $(document).ready(function () {
            $('.owl-carousel').owlCarousel({
                items: 1,
                loop: true,
                nav: true,
                smartSpeed: 10,
                navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                navClass: ['owl-prev', 'owl-next'],

            });
            ymaps.ready(init);

            function init() {
                myMap = new ymaps.Map('map', {
                    center: [40.1791857, 44.4991029],
                    zoom: 9
                });
                var address = $('#country').val() + ' ' + $('#city').val() + ' ' + $('#area').val() + ' ' + $('#address').val();

                // Поиск координат центра Нижнего Новгорода.
                ymaps.geocode(address, {
                    /**
                     * Опции запроса
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                     */
                    // Сортировка результатов от центра окна карты.
                    // boundedBy: myMap.getBounds(),
                    // strictBounds: true,
                    // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
                    // Если нужен только один результат, экономим трафик пользователей.
                    results: 1
                }).then(function (res) {
                    // Выбираем первый результат геокодирования.
                    var firstGeoObject = res.geoObjects.get(0),
                        // Координаты геообъекта.
                        coords = firstGeoObject.geometry.getCoordinates(),
                        // Область видимости геообъекта.
                        bounds = firstGeoObject.properties.get('boundedBy');

                    firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
                    // Получаем строку с адресом и выводим в иконке геообъекта.
                    firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());

                    // Добавляем первый найденный геообъект на карту.
                    myMap.geoObjects.add(firstGeoObject);
                    // Масштабируем карту на область видимости геообъекта.
                    myMap.setBounds(bounds, {
                        // Проверяем наличие тайлов на данном масштабе.
                        checkZoomRange: true
                    });


                });

            }

            var seeDetail =<?php echo $see_detalis; ?>;
            seeDetail = seeDetail + 1;
            console.log(seeDetail);
            for (var i = 1; i < seeDetail; i++) {
                var text = $('.name-amenities' + i + '').attr('id');
                // console.log(text);
                var arrayText = text.split("_");
                for (var m = 0; m < arrayText.length; m++) {
                    var text = $('.name-amenities' + i + '').append(arrayText[m] + ' ');

                }

            }

        });
        $(document).ready(function () {
            var seeDetail =<?php echo $see_detalis; ?>;
            if (seeDetail > 7) {
                $('.div-detail-8').css('display', 'none');
            } else {
                $('.row-by-button-see-all-detail').css('display', 'none');
            }
        });
        $('.button-by-button-see-all-detail').click(function () {
            if ($('.hidden-by-see-all').val() == 0) {
                $('.div-detail-8').show(1500);
                $('.hidden-by-see-all').val(1);
                $('.button-by-button-see-all-detail').text('Close');
            } else {
                $('.div-detail-8').hide(1000);
                $('.hidden-by-see-all').val(0);
                $('.button-by-button-see-all-detail').text('See more');
            }

        });


        var request = 'yes';
        $(document).keydown(function (e) {
            switch (e.which) {
                case 37: // left
                    $('.owl-next').click();
                    break;

                case 38: // up
                    break;

                case 39: // right
                    $('.owl-prev').click();
                    break;

                case 40: // down
                    break;

                default:
                    return;
            }
            e.preventDefault();
        });
        $('.img-apartment-view').click(function () {
            var id = '<?php echo $apartment->id ?>';
            if (request == 'yes') {
                $.ajax({
                    url: '/apartments/img',
                    type: 'POST',
                    dataType: "JSON",
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    data: {
                        id: id
                    },
                    success: function (success) {
                        for (var i = 0; i < success.length; i++) {
                            $('.photo-slider').append(
                                '          <div>\n' +
                                '                <img alt="" src="/uploads/' + success[i].name + '" style="height:620px">\n' +
                                '           </div>'
                            )
                        }
                        $('.owl-carousel').owlCarousel({
                            items: 1,
                            loop: true,
                            nav: true,
                            smartSpeed: 900,
                            navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                            navClass: ['owl-prev', 'owl-next'],

                        });
                        $("#myModal").modal({backdrop: true});
                        request = '';

                    },
                    error: function (errors) {

                    }
                })

            } else {
                $("#myModal").modal({backdrop: true});

            }

        })

    </script>

@endsection