<!-- Modal create your tour -->
<div class="modal fade" id="modal_tour_create" role="dialog">
    <div class="modal-dialog modal-lg " style="margin-left: 19% !important;">

        <!-- Modal content-->
        <div class="modal-content" style="width: 960px">
            <div>
                <div class="div-by-package-price">

                </div>
                <div class="div-by-package-4-step">
                    <div>
                        <h3 class="text-center">Create Your Tour Package</h3>
                        <p class="text-center step-page margin-bottom-0">1st step</p>
                    </div>
                </div>
                <div class="div-by-package-price">
                    <button type="button" class="package-close"><span style="font-size: 18px;color: black;">x</span>
                    </button>
                </div>
            </div>
            <hr style="margin: 0px;">

            {!! Form::open(['files'=>true, 'id' => 'mainForm','enctype'=>'multipart/form-data']) !!}
            <div class="modal-body">
                <div class="row page-create">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="margin-bottom-10-px">
                            <p class="title-tour-create">1. Choose your dates</p>
                            <div class="margin-left-10-px">
                                <div style="position: relative">
                                    <p class="p-tour-create">Arrival</p>
                                    <input id="datepicker_arrival" type="text"
                                           class="input-date-tour-create not-close-calendar"
                                           name="date_arrival">
                                    <div id="container_arrival" class="container-arrival-data"
                                         style="display: none"></div>
                                    <input type="hidden" class="input-hidden-by-arrival" value="0">

                                </div>
                                <div style="position:relative;">
                                    <p class="p-tour-create">Departure</p>
                                    <input id="datepicker_departure" type="text"
                                           class="input-date-tour-create not-close-calendar"
                                           name="date_departure">
                                    <div id="container_departure" class="container-departure-data"
                                         style="display: none"></div>
                                    <input type="hidden" class="input-hidden-by-departure" value="0">
                                </div>
                            </div>
                        </div>
                        <div class="margin-bottom-10-px">
                            <p class="title-tour-create">2. How many person</p>
                            <div class="margin-left-10-px">
                                <span class="persons-tour-create"> Adults</span>
                                <input type="text" name="adults" id="adults" class="input-number-tour-create">
                                <span class="persons-tour-create">Kids</span>
                                <input type="text" name="kids" id="kids" class="input-number-tour-create">
                            </div>
                        </div>
                        <div class="margin-bottom-10-px">
                            <p class="title-tour-create">3. Airport Transfer Service</p>
                            <div class="margin-left-10-px">
                                <div>
                                    <p class="p-number-people-tour-create">Number of people on arrival</p>
                                    <input type="text" name="people_arrival" id="people_arrival"
                                           class="input-number-tour-create">
                                </div>
                                <div>
                                    <p class="p-number-people-tour-create">Number of people on departure</p>
                                    <input type="text" name="people_departure" id="people_departure"
                                           class="input-number-tour-create">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row page-2" style="display: none">
                    <div class="col-sm-12">
                        <p class="margin-top-0 text-center title-tour-create">Choose your accommodation</p>
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#apartment">Apartment</a></li>
                            <li><a href="#house">House</a></li>
                            <li><a href="#menu2">Hotel</a></li>
                            <li><a href="#menu3">Hostel</a></li>
                        </ul>

                        <div class="tab-content accommodation-div-book " style="width: 100% !important;">
                            <input type="hidden" name="apartment" class="apartment-hidden-input">
                            <div id="apartment" class="tab-pane fade in active ">
                                <div class="apartment-tour-create">
                                    @foreach($apartmentTourCreate as $apartment)

                                        <div class="div-apartment-tour-create">
                                            <div class="div-checked-apartment-tour">
                                                <img src="/uploads/' + result.apartmentTourCreate[i].general_pic + '"
                                                     alt="" class="img-tour-create ">
                                                <div class="hover-div-tour hover-div-apartment"><img
                                                            src="/img/icon-check.png" class="icon-check-apartment"
                                                            alt="check"></div>
                                            </div>
                                            <p class="apartment-name-tour-create ">{{$apartment->name}}</p>
                                            <i class="city-tour-create">{{$apartment->city.' '.$apartment->address}}</i>
                                            <p>
                                                <button type="button" attr="{{$apartment->id}}"
                                                        id="package_apartment_see" class="btn btn-success">See
                                                </button>
                                                <button type="button" attr="apartment"
                                                        class="apartment-book btn btn-warning"
                                                        id="' + result.apartmentTourCreate[i].id + '">Choose
                                                </button>
                                            </p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <input type="hidden" name="house" class="house-hidden-input">
                            <div id="house" class="tab-pane fade">
                                <div class="house-tour-create">
                                    @foreach($houseTourCreate as $house)
                                        <div class="div-apartment-tour-create">
                                            <div class="div-checked-apartment-tour">
                                                <img src="/uploads/' + result.houseTourCreate[m].general_pic + '" alt=""
                                                     class="img-tour-create">
                                                <div class="hover-div-tour hover-div-house"><img
                                                            src="/img/icon-check.png" class="icon-check-apartment"
                                                            alt="check"></div>
                                            </div>
                                            <p class="apartment-name-tour-create">{{$house->name}}</p>
                                            <i class="city-tour-create">{{$house->city.' '.$house->address}}</i>
                                            <p>
                                                <button type="button" attr="{{$house->id}}"
                                                        id="package_apartment_see" class=" btn btn-success">See
                                                </button>
                                                <button type="button" attr="house"
                                                        class="apartment-book btn btn-warning"
                                                        id="' + result.houseTourCreate[m].id + '">Choose
                                                </button>
                                            </p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <h3>Menu 2</h3>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam.</p>
                            </div>
                            <div id="menu3" class="tab-pane fade">
                                <h3>Menu 3</h3>
                                <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta
                                    sunt explicabo.</p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row page-3" style="display: none">
                    <div class="col-sm-12">
                        <p class="margin-top-0 text-center title-tour-create margin-bottom-0">Choose your tour type</p>
                        <ul class="nav nav-tabs" style="border-bottom: 3px solid white;">
                            <li class="active"><a href="#private">Private</a></li>
                            <li><a href="#group">Group</a></li>
                            <li><a href="#extream">Extream</a></li>
                        </ul>

                        <div class="tab-content " id="content_tour" style="width: 100% !important;">
                            <div id="private" class="tab-pane fade in active">
                                <p class="margin-top-0 text-center title-tour-create margin-bottom-0">Choose type of
                                    Travel car</p>
                                <ul class="nav nav-tabs nav-tab-private" style="border-bottom: 3px solid white;">
                                    <li class="active"><a href="#sedan">Sedan<p class="person-count">1-3per.</p></a>
                                    </li>
                                    <li><a href="#minivan">Minivan<p class="person-count">4-7per.</p></a></li>
                                    <li><a href="#minibus">Mini bus<p class="person-count">8-21per.</p></a></li>
                                    <li><a href="#bus"> Bus<p class="person-count">22-40per.</p></a></li>
                                </ul>
                                <div class="tab-content " style="width: 100% !important;">
                                    <input type="hidden" name="tour" class="tour-json-input">
                                    <div id="sedan" class="tab-pane fade in active">
                                        <table class="table  ">
                                            <thead class="thead-package-private-tour">
                                            <tr>
                                                <th class="text-center private-package-destination">Destination</th>
                                                <th>Duration</th>
                                                <th class="text-center">Km</th>
                                                <th>Without guide</th>
                                                <th>With guide</th>
                                            </tr>
                                            </thead>

                                            <tbody class="sedan-table tbody-private-tour">

                                            </tbody>

                                        </table>
                                    </div>
                                    <div id="minivan" class="tab-pane fade">

                                        <table class="table  ">
                                            <thead class="thead-package-private-tour">
                                            <tr>
                                                <th class="text-center private-package-destination">Destination</th>
                                                <th>Duration</th>
                                                <th>Km</th>
                                                <th>Without guide</th>
                                                <th>With guide</th>

                                            </tr>
                                            </thead>
                                            <tbody class="minivan-table tbody-private-tour">

                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="minibus" class="tab-pane fade">
                                        <table class="table  ">
                                            <thead class="thead-package-private-tour">
                                            <tr>
                                                <th class="text-center private-package-destination">Destination</th>
                                                <th>Duration</th>
                                                <th>Km</th>
                                                <th>Without guide</th>
                                                <th>With guide</th>

                                            </tr>
                                            </thead>
                                            <tbody class="minibus-table tbody-private-tour">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="bus" class="tab-pane fade">
                                        <table class="table  ">
                                            <thead class="thead-package-private-tour">
                                            <tr>
                                                <th class="text-center private-package-destination">Destination</th>
                                                <th>Duration</th>
                                                <th>Km</th>
                                                <th>Without guide</th>
                                                <th>With guide</th>

                                            </tr>
                                            </thead>
                                            <tbody class="bus-table tbody-private-tour">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="group" class="tab-pane fade">
                                <div class="table-group">

                                </div>
                            </div>
                            <div id="extream" class="tab-pane fade">
                                <div class="deals row">
                                    <!--accommodation item-->
                                    <div class="col-md-4">
                                        <article class="accommodation_item">
                                            <div>
                                                <figure class="pic-figure">
                                                    <img src="{{asset("img/winter-tour.jpg")}}"
                                                         alt="City loft">
                                                </figure>
                                                <div class="detail-etxream">
                                                    <p class="apartment-name">Winter Tours</p>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-md-4">
                                        <article class="accommodation_item">
                                            <div>
                                                <figure class="pic-figure">
                                                    <img src="{{asset("img/hiking-tour.jpg")}}"
                                                         alt="City loft">
                                                </figure>
                                                <div class="detail-etxream">
                                                    <p class="apartment-name">Hiking Tours</p>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-md-4">
                                        <article class="accommodation_item">
                                            <div>
                                                <figure class="pic-figure">
                                                    <img src="{{asset("img/jeep-tour.jpg")}}"
                                                         alt="City loft">
                                                </figure>
                                                <div class="detail-etxream">
                                                    <p class="apartment-name">Jeep Tours</p>
                                                    <hr>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                                <div class="deals row">
                                    <div class="col-md-4">
                                        <article class="accommodation_item">
                                            <div>
                                                <figure class="pic-figure">
                                                    <img src="{{asset("img/bike-tour.jpg")}}"
                                                         alt="City loft">
                                                </figure>
                                                <div class="detail-etxream">
                                                    <p class="apartment-name">Bike Tours</p>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-md-4">
                                        <article class="accommodation_item">
                                            <div>
                                                <figure class="pic-figure">
                                                    <img src="{{asset("img/zipline-tour.jpg")}}"
                                                         alt="City loft">
                                                </figure>
                                                <div class="detail-etxream">
                                                    <p class="apartment-name">Zipline Tours</p>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-md-4">
                                        <article class="accommodation_item">
                                            <div>
                                                <figure class="pic-figure">
                                                    <img src="{{asset("img/enduro-tour.jpg")}}"
                                                         alt="City loft">
                                                </figure>
                                                <div class="detail-etxream">
                                                    <p class="apartment-name">Enduro Tours</p>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!--//accommodation item-->
                                </div><!--//deals-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row page-4 page-4-package" style="display: none">
                    <table class="table ">
                        <tbody>
                        <tr>
                            <td class="text-center border-top-none"></td>
                            <td class="text-center border-top-none">Date</td>
                            <td class="text-center border-top-none">Discount</td>
                            <td class="text-center border-top-none">Price</td>
                        </tr>
                        <tr class="tr-package-border" style="display: none" id="apartment_package_price">
                            <td class="text-center td-package-border">
                                <p class="apartment-p-package">Apartment</p>
                                <a href="" class="name-apartment-package" target="_blank"></a>
                            </td>
                            <td class="text-center date-apartment-package td-package-border"></td>
                            <td class="text-center discount-apartment-package td-package-border"></td>
                            <td class="text-center price-apartment-package td-package-border"></td>
                        </tr>
                        <tr style="display: none" id="private_package_price">
                            <td class="text-center td-package-border">Tour private</td>
                            <td class="text-center td-package-border date-apartment-package"></td>
                            <td class="text-center td-package-border"></td>
                            <td class="text-center td-package-border private-price-package"></td>
                        </tr>
                        <tr style="display: none" id="group_package_price">
                            <td class="text-center td-package-border">Tour regular</td>
                            <td class="text-center td-package-border date-apartment-package"></td>
                            <td class="text-center td-package-border"></td>
                            <td class="text-center td-package-border group-price-package"></td>
                        </tr>
                        <tr>
                            <td class="text-center td-package-border"></td>
                            <td class="text-center td-package-border"></td>
                            <td class="text-center td-total-price-package td-package-border"
                                style="border-bottom: solid 1px black;">Total price

                            </td>
                            <td class="text-center td-total-price-package td-package-border price-all-package"
                                style="border-bottom: solid 1px black;">

                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <div class="row page-5" style="display: none">
                    <div class="col-sm-12">
                        <p class="margin-top-0 text-center title-tour-create">Fill the information about you</p>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="div-modal-book-package div-first-block ">
                                    <label for="">Last name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name"
                                           placeholder="Last name">
                                    <p class="error-last-name"></p>
                                </div>
                                <div class="div-modal-book-package div-second-block ">
                                    <label for="">First name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name"
                                           placeholder="First name">
                                    <p class="error-first-name"></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="div-modal-book-package div-first-block ">
                                    <label for="">Country<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control" name="country" id="country"
                                           placeholder="Country">
                                    <p class="error-country"></p>
                                </div>
                                <div class="div-modal-book-package div-second-block">
                                    <label for="">City<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control" name="city" id="city" placeholder="City">
                                    <p class="error-city"></p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="div-modal-book-package div-first-block ">
                                    <label for="">Address<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control" name="address" id="address"
                                           placeholder="Address">
                                    <p class="error-country"></p>
                                </div>
                                <div class="div-modal-book-package div-second-block">
                                    <label for="">Email address<sup><i
                                                    class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="email" class="form-control" name="email" id="email"
                                           placeholder="Email address">
                                    <p class="error-email"></p>
                                </div>
                            </div>
                            <div class=" ">
                                <div class=" div-by-tour-create-phone">
                                    <label for="">Phone<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone">
                                    <p class="error-phone"></p>
                                </div>
                                <div class="div-by-tour-create-phone">
                                    <label for="">Viber<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control" name="viber" id="viber" placeholder="Viber">
                                    <p class="error-viber"></p>
                                </div>
                                <div class="div-by-tour-create-phone">
                                    <label for="">Whatsapp<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control" name="whatsapp" id="whatsapp"
                                           placeholder="Whatsapp">
                                    <p class="error-whatsapp"></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div>
                                        <label for="">Notes<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    </div>
                                    <textarea class="textarea-tour-create" name="notes" id="notes"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal  fade" id="webPackageModal" role="dialog">
                    <div class="modal-dialog modal-lg ">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div>
                                <button type="button" class="package-close-apartment"><span
                                            style="font-size: 18px;color: black;">x</span></button>
                            </div>
                            <div class="modal-body modal-apartment-package">
                                <iframe src="" class="iframe-modal-page">
                                    <p>Your browser does not support iframes.</p>
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger back-step" style="display: none"> Back
                </button>
                <button type="button" class="btn btn-success next-step">Next step
                </button>
                <button type="button" class="btn btn-success next-step">Skip</button>
                <button type="button" class="btn btn-primary send" style="display: none">Finish</button>
            </div>
            {!! Form::close() !!}

        </div>

    </div>
</div>
<style>
    .active {
        color: #2579a9 !important;
    }
</style>

<script>
    var tourArray = [];
$('#content_tour').on('click','.choose-group-tour-package',function () {
    $(this).hide();
    $(this).parent().find('img').show();
    var id = $(this).attr('id');
    var typeCar = $(this).attr('car');
    var guide = $(this).attr('guide');
    var typeTour = $(this).attr('tour');
    var tour = {id: id, guide: guide, type_car: typeCar, type_tour: typeTour};
    tourArray.push(tour);
    var tourJSON = JSON.stringify(tourArray);
    $('.tour-json-input').val(tourJSON);
});



    $('#content_tour').on('click','.choose-tour-package-private',function () {

        var id = $(this).attr('id');
        var typeCar = $(this).attr('car');
        var guide = $(this).attr('guide');
        var typeTour = $(this).attr('tour');
        if (tourArray.length > 0) {
            var bool = true;
            for (var i = 0; i < tourArray.length; i++) {
                if (tourArray[i].id == id && tourArray[i].type_car == typeCar) {
                    $(this).hide();
                    $(this).parent().find('.package-check-img-private').show();
                    $(this).parent().parent().find('.' + guide + '-td').children('img').hide();
                    $(this).parent().parent().find('.' + guide + '-td').children('button').show();
                    tourArray[i].guide = guide;
                    bool = false;
                }
            }
            if (bool === true) {
                var tour = {id: id, guide: guide, type_car: typeCar, type_tour: typeTour};
                $(this).hide();
                $(this).parent().find('.package-check-img-private').show();
                tourArray.push(tour);
                var tourJSON = JSON.stringify(tourArray);
                $('.tour-json-input').val(tourJSON);
            }
        } else {
            var tour = {id: id, guide: guide, type_car: typeCar, type_tour: typeTour};
            $(this).hide();
            $(this).parent().find('.package-check-img-private').show();
            tourArray.push(tour);
            var tourJSON = JSON.stringify(tourArray);
            $('.tour-json-input').val(tourJSON);

        }
    });
    $('#content_tour').on('click','.package-check-img-private',function () {
        var id = $(this).attr('id');
        var typeCar = $(this).attr('car');
        $(this).hide();
        $(this).parent().find('button').show();
        console.log(tourArray)

        for (var i = 0; i < tourArray.length; i++) {
            if (tourArray[i].id == id && tourArray[i].type_car == typeCar) {
                tourArray.splice(i,1)
            }
        }
        var tourJSON = JSON.stringify(tourArray);
        $('.tour-json-input').val(tourJSON);
    });

    var PageNumberCreateTour = 1;
    $('#modal_tour_create').on('click', '.next-step', function () {

        if (PageNumberCreateTour < 5) {
            PageNumberCreateTour++;
            if (PageNumberCreateTour == 2) {
                $('.modal-body').find('.page-create').hide();
                $('.page-2').show();
                $('.back-step').show();
                $('.step-page').html('2st step');
            } else if (PageNumberCreateTour == 3) {
                $('.modal-body').find('.page-2').hide();
                $('.page-3').show();
                $('.step-page').html('3st step');
            } else if (PageNumberCreateTour == 4) {
                $('.modal-body').find('.page-3').hide();
                $('.page-4').show();
                $('.next-step').show();
                $('.step-page').html('4st step');
                if (tourArray.length > 0) {
                    $.ajax({
                        url: '/tour/package/price',
                        type: 'POST',
                        dataType: "JSON",
                        headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                        data: {
                            data: tourArray
                        },
                        success: function (result) {
                            var integer = parseInt($('.price-apartment-package').html());

                            if(result.price.private_price>0){
                                $('.private-price-package').html(result.price.private_price + " $");
                                $('#private_package_price').show();
                            }
                            if (result.price.group_price > 0) {
                                $('.group-price-package').html(result.price.group_price + " $");
                                $('#group_package_price').show();
                            }
                            var dateArrival = $('#datepicker_arrival').val();
                            var dateDeparture = $('#datepicker_departure').val();
                            $('.date-apartment-package').html(dateArrival + ' -' + dateDeparture);
                            console.log(dateArrival)

                            if (integer == 0 || integer > 0) {
                                $('.price-all-package').html(result.price.private_price + result.price.group_price + integer + " $");
                            } else {
                                $('.price-all-package').html(result.price.private_price + result.price.group_price + " $");
                            }

                        },
                        error: function (errors) {

                        }
                    });
                } else {
                    var integer = parseInt($('.price-apartment-package').html());
                    if (integer == 0 || integer > 0) {
                        $('.price-all-package').html(integer + " $");
                    }
                }

            } else if (PageNumberCreateTour == 5) {
                $('.modal-body').find('.page-4').hide();
                $('.page-5').show();
                $('.next-step').hide();
                $('.step-page').html('5st step');
                $('.send').show();
            }
        }
    });
    $('.back-step').click(function () {
        if (PageNumberCreateTour == 5) {
            $('.send').hide();
            $('.next-step').show();
            $('.modal-body').find('.page-5').hide();
            $('.page-4').show();
            $('.step-page').html('4st step');
        } else if (PageNumberCreateTour == 4) {
            // $('.send').hide();
            $('.next-step').show();
            $('.modal-body').find('.page-4').hide();
            $('.page-3').show();
            $('.step-page').html('3st step');

        } else if (PageNumberCreateTour == 3) {
            $('.modal-body').find('.page-3').hide();
            $('.page-2').show();
            $('.step-page').html('2st step');
        } else if (PageNumberCreateTour == 2) {
            $('.modal-body').find('.page-2').hide();
            $('.page-create').show();
            $('.back-step').hide();

            $('.step-page').html('1st step');
        }
        PageNumberCreateTour--;

    });
    $('.crete-tour').click(function () {
        $.ajax({
            url: '/tour/create',
            type: 'GET',
            dataType: "JSON",
            headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
            data: {
                create: 'create'
            },
            success: function (result) {
                $('.apartment-tour-create').empty();
                $('.house-tour-create').empty();
                $('.sedan-table').empty();
                $('.minivan-table').empty();
                $('.minibus-table').empty();
                $('.bus-table').empty();

                for (var i = 0; i < result.apartmentTourCreate.length; i++) {
                    $('.apartment-tour-create').append(
                        '  <div class="div-apartment-tour-create">\n' +
                        '                                        <div class="div-checked-apartment-tour" >\n' +
                        '                                        <img   src="/uploads/' + result.apartmentTourCreate[i].general_pic + '" alt="" class="img-tour-create " >\n' +
                        '          <div class="hover-div-tour hover-div-apartment"><img src="/img/icon-check.png" class="icon-check-apartment" alt="check"></div>\n' +
                        '                                        </div>\n' +
                        '                                        <p class="apartment-name-tour-create ">' + result.apartmentTourCreate[i].name + ' </p>\n' +
                        '                                        <i class="city-tour-create">' + result.apartmentTourCreate[i].city + ' ' + result.apartmentTourCreate[i].address + '</i>\n' +
                        '                                   <p><button type="button" attr="' + result.apartmentTourCreate[i].id + '" id="package_apartment_see" class="btn btn-success">See</button><button type="button" attr="apartment" class="apartment-book btn btn-warning" id="' + result.apartmentTourCreate[i].id + '">Choose</button></p>\n' +
                        '                                    </div>')
                }
                for (var m = 0; m < result.houseTourCreate.length; m++) {
                    $('.house-tour-create').append(
                        '  <div class="div-apartment-tour-create">\n' +
                        '                                        <div class="div-checked-apartment-tour"  >\n' +
                        '                                        <img src="/uploads/' + result.houseTourCreate[m].general_pic + '" alt="" class="img-tour-create" >\n' +
                        '                                        <div class="hover-div-tour hover-div-house"><img src="/img/icon-check.png" class="icon-check-apartment" alt="check"></div>\n' +
                        '                                        </div>\n' +
                        '                                        <p class="apartment-name-tour-create">' + result.houseTourCreate[m].name + ' </p>\n' +
                        '                                        <i class="city-tour-create">' + result.houseTourCreate[m].city + ' ' + result.houseTourCreate[m].address + ' </i>\n' +
                        '                                      <p> <button type="button" attr="' + result.houseTourCreate[m].id + '" id="package_apartment_see" class=" btn btn-success">See</button><button type="button" attr="house" class="apartment-book btn btn-warning" id="' + result.houseTourCreate[m].id + '">Choose</button></p>\n' +
                        '                                    </div>')
                }
                for (var n = 0; n < result.tourPrivate.length; n++) {
                    var destination = result.tourPrivate[n].destination;
                    for (var l = 0; l < destination.length; l++) {

                        var sedanGuidePrice = "";
                        var minivanGuidePrice = "";
                        var miniBusGuidePrice = "";
                        var busGuidePrice = "";
                        var miniBusPrice = "";
                        var busPrice = "";


                        if (destination[l].sedan_price_guide) {
                            sedanGuidePrice = destination[l].sedan_price_guide;
                        }
                        if (destination[l].minivan_price_guide) {
                            minivanGuidePrice = destination[l].minivan_price_guide;
                        }
                        if (destination[l].mini_bus_guide) {
                            miniBusGuidePrice = destination[l].mini_bus_guide;
                        }
                        if (destination[l].bus_guide) {
                            busGuidePrice = destination[l].bus_guide;
                        }
                        if (destination[l].mini_bus) {
                            miniBusPrice = destination[l].mini_bus;
                        }
                        if (destination[l].bus) {
                            busPrice = destination[l].bus;
                        }
                        $('.sedan-table').append(
                            '                                            <tr class="checkbox-tour-create sedan-private-tr">\n' +
                            '                                                <td>' + destination[l].destination + '</td>\n' +
                            '                                                <td class="text-center">' + destination[l].duration + '</td>\n' +
                            '                                                <td>' + destination[l].km + '</td>\n' +
                            '                                                <td class="true-td">' + destination[l].sedan_price + ' ' +
                            '          <img src="/img/icon-check.png" class="package-check-img-private" style="display: none" alt="Check" car="sedan" id="' + destination[l].id + '">' +
                            '          <button type="button" tour="private" car="sedan" guide="false" class="btn choose-tour-package-private" id="' + destination[l].id + '">Choose</button></td>\n' +
                            '                                                <td class="false-td">' + sedanGuidePrice + '' +
                            '           <img src="/img/icon-check.png" class="package-check-img-private" style="display: none" alt="Check" car="sedan" id="' + destination[l].id + '">' +
                            '           <button type="button" tour="private" car="sedan" guide="true" class="btn choose-tour-package-private" id="' + destination[l].id + '">Choose</button></td>\n' +

                            '                                            </tr>'
                        );
                        $('.minivan-table').append(
                            '                                           <tr class="checkbox-tour-create sedan-private-tr">\n' +
                            '                                                <td>' + destination[l].destination + '</td>\n' +
                            '                                                <td class="text-center">' + destination[l].duration + '</td>\n' +
                            '                                                <td>' + destination[l].km + '</td>\n' +
                            '                                                <td class="true-td">' + destination[l].minivan_price + '' +
                            '<img src="/img/icon-check.png" style="display: none" class="package-check-img-private" alt="Check" car="minivan" id="' + destination[l].id + '">' +
                            '<button type="button" tour="private" car="minivan" guide="false" class="btn choose-tour-package-private" id="' + destination[l].id + '">Choose</button></td>\n' +
                            '                                                <td class="false-td">' + minivanGuidePrice + '' +
                            ' <img src="/img/icon-check.png" style="display: none" class="package-check-img-private" alt="Check" car="minivan" id="' + destination[l].id + '">' +
                            ' <button type="button" tour="private" car="minivan" guide="true" class="btn choose-tour-package-private" id="' + destination[l].id + '">Choose</button></td>\n' +
                            '                                            </tr>'
                        );
                        $('.minibus-table').append(
                            '                                           <tr class="checkbox-tour-create sedan-private-tr">\n' +
                            '                                                <td>' + destination[l].destination + '</td>\n' +
                            '                                                <td class="text-center">' + destination[l].duration + '</td>\n' +
                            '                                                <td>' + destination[l].km + '</td>\n' +
                            '                                                <td class="true-td">' + miniBusPrice + '' +
                            '<img src="/img/icon-check.png" style="display: none" alt="Check" class="package-check-img-private" car="minibus" id="' + destination[l].id + '">' +
                            '<button type="button" tour="private" car="minibus" guide="false" class="btn choose-tour-package-private" id="' + destination[l].id + '">Choose</button></td>\n' +
                            '                                                <td class="false-td">' + miniBusGuidePrice + '' +
                            '<img src="/img/icon-check.png" style="display: none" alt="Check" class="package-check-img-private" car="minibus" id="' + destination[l].id + '">' +
                            '<button type="button" tour="private" car="minibus" guide="true" class="btn choose-tour-package-private" id="' + destination[l].id + '">Choose</button></td>\n' +
                            '                                            </tr>'
                        );
                        $('.bus-table').append(
                            '                                           <tr class="checkbox-tour-create sedan-private-tr">\n' +
                            '                                                <td>' + destination[l].destination + '</td>\n' +
                            '                                                <td class="text-center">' + destination[l].duration + '</td>\n' +
                            '                                                <td>' + destination[l].km + '</td>\n' +
                            '                                                <td class="true-td">' + busPrice + '' +
                            '<img src="/img/icon-check.png" style="display: none" alt="Check" class="package-check-img-private" car="bus" id="' + destination[l].id + '">' +
                            '<button type="button" tour="private" car="bus" guide="false" class="btn choose-tour-package-private" id="' + destination[l].id + '">Choose</button></td>\n' +
                            '                                                <td class="false-td">' + busGuidePrice + '' +
                            '<img src="/img/icon-check.png" style="display: none" alt="Check" class="package-check-img-private" car="bus" id="' + destination[l].id + '">' +
                            '<button type="button" tour="private" car="bus" guide="true" class="btn choose-tour-package-private" id="' + destination[l].id + '">Choose</button></td>\n' +
                            '                                            </tr>'
                        )

                    }
                }

                for (var r = 0; r < result.tourRegular.length; r++) {
                    var destination = result.tourRegular[r].destination;
                    for (var i = 0; i < destination.length; i++) {

                        $('.table-group').append(
                            ' <div  class="package-group-title"><span class="span-destination"> ' + destination[i].destination + ' </span></div>\n' +
                            '<div class="row div-group-package margin-0">\n' +
                            '    \n' +
                            '    <div class="col-xl-2">\n' +
                            '        <img src="https://onewaytour.com/wp-content/uploads/2019/02/featured-khndzoresk-180x136.jpg" style="width: 145px; height: 100px" alt="">\n' +
                            '    </div>\n' +
                            '    <div class="offset-sm-1 offset-xl-0 col-sm-2 ">\n' +
                            '        <p class="text-center margin-top-group">Date</p>\n' +
                            '        <p class="text-center">' + destination[i].date + '</p>\n' +
                            '    </div>\n' +
                            '    <div class="col-sm-2 col-border-group-package">\n' +
                            '        <p class="text-center margin-top-group">Start</p>\n' +
                            '        <p class="text-center">' + destination[i].clock + '</p>\n' +
                            '    </div>\n' +
                            '    <div class="col-sm-2 col-border-group-package">\n' +
                            '        <p class="text-center margin-top-group">Duration</p>\n' +
                            '        <p class="text-center">' + destination[i].duration + '</p>\n' +
                            '    </div>\n' +
                            '    <div class="col-sm-2 col-border-group-package">\n' +
                            '        <p class="text-center margin-top-group">Price Per Person</p>\n' +
                            '        <p class="text-center">' + destination[i].price_person + '</p>\n' +
                            '    </div>\n' +
                            '    <div class="col-sm-2">\n' +
                            '<img src=" /img/icon-check.png" class="change-group-package-img package-check-img-private" car="group" id="' + destination[i].id + '">\n' +
                            '        <button type="button" class="choose-group-tour-package" id="' + destination[i].id + '" car="group" tour="group" guide="group">Choose</button>\n' +
                            '    </div>\n' +
                            '</div>')
                    }

                }


                $("#modal_tour_create").modal({backdrop: "static"});
                $("#modal_tour_create").modal("show");

            },
            error: function (errors) {

                console.log(errors);
            }
        })
    });


    $('.all-body').on('click', '.dp_body>tr>td', function () {
        var dateArrival = $('#datepicker_arrival').val();
        var dateDeparture = $('#datepicker_departure').val();
        if (dateArrival) {
            if (dateDeparture) {
                $.ajax({
                    url: '/package/date/search',
                    type: 'POST',
                    dataType: "JSON",
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    data: {
                        dateArrival: dateArrival,
                        dateDeparture: dateDeparture,
                    },
                    success: function (result) {
                        $('.apartment-tour-create').empty();
                        $('.house-tour-create').empty();
                        for (var i = 0; i < result.apartmentTourCreate.length; i++) {
                            $('.apartment-tour-create').append(
                                '  <div class="div-apartment-tour-create">\n' +
                                '                                        <div class="div-checked-apartment-tour" >\n' +
                                '                                               <img src="/uploads/' + result.apartmentTourCreate[i].general_pic + '" alt="" class="img-tour-create">\n' +
                                '          <div class="hover-div-tour hover-div-apartment"><img src="/img/icon-check.png" class="icon-check-apartment" alt="check"></div>\n' +
                                '                                        </div>\n' +
                                '                                        <p class="apartment-name-tour-create">' + result.apartmentTourCreate[i].name + ' </p>\n' +
                                '                                        <i class="city-tour-create">' + result.apartmentTourCreate[i].city + ' ' + result.apartmentTourCreate[i].address + '</i>\n' +
                                '                                      <p><button type="button" attr="' + result.apartmentTourCreate[i].id + '" id="package_apartment_see" class="btn btn-success">See</button><button type="button" attr="apartment" class="apartment-book btn btn-warning" id="' + result.apartmentTourCreate[i].id + '">Choose</button></p>\n' +
                                '                                    </div>')
                        }
                        for (var m = 0; m < result.houseTourCreate.length; m++) {
                            $('.house-tour-create').append(
                                '  <div class="div-apartment-tour-create">\n' +
                                '                                        <div class="div-checked-apartment-tour"  >\n' +
                                '                                               <img src="/uploads/' + result.houseTourCreate[m].general_pic + '" alt="" class="img-tour-create">\n' +
                                '                                        <div class="hover-div-tour hover-div-house"><img src="/img/icon-check.png" class="icon-check-apartment" alt="check"></div>\n' +
                                '                                        </div>\n' +
                                '                                        <p class="apartment-name-tour-create">' + result.houseTourCreate[m].name + ' </p>\n' +
                                '                                        <i class="city-tour-create">' + result.houseTourCreate[m].city + ' ' + result.houseTourCreate[m].address + ' </i>\n' +
                                '                                        <p> <button type="button" attr="' + result.houseTourCreate[m].id + '" id="package_apartment_see" class=" btn btn-success">See</button><button type="button" attr="house" class="apartment-book btn btn-warning" id="' + result.houseTourCreate[m].id + '">Choose</button></p>\n' +
                                '                                    </div>')
                        }

                    },
                    error: function (errors) {


                    }
                });
            }
        }
    });

    function saveTourSuccess() {
        $('#modal_tour_create').find('input').val(' ');
        $("#modal_tour_create").modal("hide");
        $('.modal-body').find('.page-4').hide();
        $('.back-step').hide();
        $('.next-step').show();
        $('.send').hide();
        $('.page-create').show();
        $('.step-page').html('1st step');
        PageNumberCreateTour = 1;
    }

    $('.send').click(function () {
        var form = document.getElementById('mainForm');
        var formData = new FormData(form);
        $.ajax({
            url: '/reservation/package/save',
            type: 'POST',
            headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.status == 200) {
                    saveTourSuccess();
                }
            },
            error: function (errors) {

            }
        })
    });
    $(document).ready(function () {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
        });
    });
    $('#datepicker_arrival').Zebra_DatePicker({
        always_visible: $('#container_arrival'),
        format: 'd/m/Y',
        custom_classes: {
            'not-close-calendar': ['1-31']
        },

    });
    $('#datepicker_departure').Zebra_DatePicker({
        always_visible: $('#container_departure'),
        format: 'd/m/Y',
        custom_classes: {
            'not-close-calendar': ['1-31']
        }
    });
    $('#datepicker_arrival').click(function () {

        if ($('.input-hidden-by-arrival').val() < 1) {

            $('.container-arrival-data').show();
            $('.input-hidden-by-arrival').val(1);
        }
        if ($('.input-hidden-by-departure').val() > 0) {
            $('.container-departure-data').hide();
            $('.input-hidden-by-departure').val(0);
        }
    });
    $('#datepicker_departure').click(function () {

        if ($('.input-hidden-by-departure').val() < 1) {

            $('.container-departure-data').show();
            $('.input-hidden-by-departure').val(1);
        }
        if ($('.input-hidden-by-arrival').val() > 0) {
            $('.container-arrival-data').hide();
            $('.input-hidden-by-arrival').val(0);
        }
    });
    $('.all-body').on('click', function (e) {
        if (!$(e.target).hasClass('not-close-calendar')) {

            if ($('.input-hidden-by-departure').val() > 0) {
                $('.container-departure-data').css('display', 'none');
                $('.input-hidden-by-departure').val(0);

            }
            if ($('.input-hidden-by-arrival').val() > 0) {
                $('.container-arrival-data').css('display', 'none');
                $('.input-hidden-by-arrival').val(0);
            }

        }
    });

</script>
