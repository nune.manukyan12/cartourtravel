<div class="apartment-search">
    <form action="{{url('/apartments/search')}}" method="POST" class="pb-3">
        {{ csrf_field() }}
        {{--<p class="apartment-search-title">Find your apartment</p>--}}
        <div class="parameter-search-apartment container p-4">
            <div class="row" style="color: white">
                <div class="col-sm-4 col-md-4">
                    <input type="checkbox" class="" value="housing" name="housing">
                    <p class="p-checkbox" style="width: auto;font-size: 14px">Housing</p>
                </div>
                <div class="col-sm-8 col-md-8">
                    <input type="checkbox" class="" value="building" name="building">
                    <p class="p-checkbox" style="width: auto;font-size: 14px">Apartment in building </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group" >
                    <input class="input-by-search-apartment text-left form-control" name="address"
                           value="{{(isset($address))? $address : 'Armenia/Yerevan'}}"
                           placeholder="Search by name, description, city..." type="text">
                </div>
            </div>
            <div class="row" style="color: white"   >
                <div class="col-md-6">
                    <p> Adult
                        {{--<label for="adult-input">Adult</label>--}}
                    {{--<button type="button"--}}
                            {{--class="btn btn-primary button-minus display-block-none"><i--}}
                                {{--class="fas fa-minus display-block-none"></i></button>--}}
                    <select class="children-input adult-input form-control h-100" name="adult" id="adult-input">
                        @for($i=0; $i<=100; $i++)
                            <option {{isset($child) && $child== $i?'selected':''}}>{{ $i }}</option>
                        @endfor
                    </select>
                    </p>
                    {{--<input class="text-center children-input adult-input"--}}
                           {{--type="number" min="0" name="adult"--}}
                           {{--value="{{(isset($adult))? $adult : '0'}}">--}}
                    {{--<button type="button"--}}
                            {{--class="btn btn-primary button-plus display-block-none"><i--}}
                                {{--class="fas fa-plus display-block-none"></i></button>--}}
                </div>
                <div class="col-md-6">
                    {{--<label for="child-input">Child</label>--}}
                    {{--<button type="button"--}}
                            {{--class="btn btn-primary button-minus display-block-none"><i--}}
                                {{--class="fas fa-minus display-block-none"></i></button>--}}

                    <p> Child
                    <select class="children-input adult-input form-control h-100" name="child" id="child-input">
                        @for($i=0; $i<=100; $i++)
                            <option {{isset($child) && $child== $i?'selected':''}}>{{ $i }}</option>
                        @endfor
                    </select>
                    </p>
                    {{--<input class="text-center children-input adult-input"--}}
                           {{--type="number" min="0" name="child"--}}
                           {{--value="{{(isset($child))? $child : '0'}}">--}}
                    {{--<button type="button"--}}
                            {{--class="btn btn-primary button-plus display-block-none"><i--}}
                                {{--class="fas fa-plus display-block-none"></i></button>--}}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 div-by-days-car">
                    <div class="calendar-div-button">
                        <p class="pick-up-return-location"> Check-in date and time <sup class="red-icon">*</sup>
                        </p>
                        <div class="row">
                            <div class="col-7 col-sm-7">
                                <input type="text" class="input-car-calendar input-by-pick-up not-close-calendar"
                                       id="datepicker-pick-up"
                                       data-zdp_direction="[1,365]" name="date_check_in">

                                <div id="datepicker-pick-up" style="display: none"></div>
                                <div id="datepicker-pick-up-div" style="display: none"></div>
                                <input type="hidden" class="input-hidden-by-pick-up" value="0">
                            </div>
                            <div class="col-5 col-sm-5">
                                <div class="time-div">
                                    <input type="time" class="timepicker-pick-up" name="date_check_in_time" value="12:00">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="calendar-div-button">
                        <p class="pick-up-return-location"> Check-out date and time<sup class="red-icon">*</sup>
                        </p>
                        <div class="row">
                            <div class="col-7">
                                <input type="text" class="input-car-calendar input-by-return not-close-calendar"
                                       id="datepicker-return"
                                       data-zdp_direction="[1,365]" name="date_check_out">

                                <div id="datepicker-return" style="display: none"></div>
                                <div id="datepicker-return-div" style="display: none"></div>
                                <input type="hidden" class="input-hidden-by-return" value="0">
                            </div>
                            <div class="col-5">
                                <div class="time-div">
                                    <input type="time" class="timepicker-return" name="date_check_out_time" value="12:00">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="col-4 col-lg-12 col-xl-4">--}}
                    {{--<div class="row mt-5 p-2 h-100">--}}
                        {{--<div class="col-12 col-sm-12 col-lg-6 col-xl-12">--}}
                            {{--<label >--}}
                                {{--<input type="checkbox" name="wifi" value="1">--}}
                                {{--<span class="text-white"><i class="fa fa-wifi" style="    font-size: 16px;color: #51b7f1;"></i></span>--}}
                            {{--</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-12 col-sm-12 col-lg-6 col-xl-12">--}}
                            {{--<label >--}}
                                {{--<input type="checkbox" name="central_cooling" value="1">--}}
                                {{--<span class="text-white"><i class="fa fa-snowflake" style="    font-size: 16px;color: #51b7f1;"></i></span>--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

            </div>
            {{--<div class="row" style="color: white">--}}
                {{--<div class="pl-0 pr-0 col-6 col-xl-6 " >--}}
                    {{--<div class="p-1">--}}
                        {{--<div class="p-3 not-close-calendar w-100 col-div-calendar-open search-calendar-div-apartment col-10-div-button">--}}
                            {{--<i class="ml-2 mr-2 far fa-calendar-alt not-close-calendar"></i>--}}
                            {{--<span class=" search-adults not-close-calendar"--}}
                               {{--id="check-in">{{(isset($date_check_in))? $date_check_in : 'Check-in'}}</span>--}}

                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="pl-0 pr-0 col-6 col-xl-6 ">--}}
                    {{--<div class="p-1">--}}
                        {{--<div class="p-3 not-close-calendar w-100  col-div-calendar-open search-calendar-div-apartment col-10-div-button">--}}
                            {{--<i class="ml-2 mr-2 far fa-calendar-alt not-close-calendar"></i>--}}
                            {{--<span class="search-adults not-close-calendar"--}}
                                  {{--id="check-out">{{(isset($date_check_out))? $date_check_out : 'Check-out'}}</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<input type="hidden" class="click-calendar-val" id="demo" data-zdp_direction="[1,365]">--}}
                {{--<input type="hidden" class="calendar-hidden-input" value="0">--}}
                {{--<input type="hidden" class="calendar-check-in date_from" name="date_check_in"--}}
                       {{--value="{{(isset($date_check_in))? $date_check_in : ''}}">--}}
                {{--<input type="hidden" class="calendar-check-out date_to" name="date_check_out"--}}
                       {{--value="{{(isset($date_check_out))? $date_check_out : ''}}">--}}


                {{--<div id="calendar" style="display: none"></div>--}}
            {{--</div>--}}
        </div>
        <div class="buttons-div-by-serch-car">
            <div>
                <input type="hidden" class="see-more-filter" value="0">
            </div>
            <button type="submit" class="btn btn-primary button-car-s" style="width: 89%;margin-left:17px">SEARCH</button>
        </div>
    </form>
</div>
{{--<div class="apartment-description">--}}
    {{--Car Tour Travel Club with reliable partners give you the best selection of short-term, long-term and luxury rentals--}}
    {{--apartments and houses.--}}
    {{--We do our best for making your vacation unbelievable.--}}
    {{--Our properties privilegius is comfort with cheap prices.--}}
    {{--You can do early reservation before high season with prepayment and warranty.--}}
{{--</div>--}}
<style>
    .col-div-calendar-open{
        background-color: #3097d1;
    }
    #check-in, #check-out{
        color: white;
    }
    /*.adult-input{*/
        /*background-color: black;*/
        /*border: none;*/
        /*color: white;*/
        /*width: 40px;*/
    /*}*/
    .button-plus,.button-minus{
        text-align: center;
        /*width: 23%;*/
        /*height: 67%;*/
        padding: 3px !important;
    }
    .input-by-number::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }
    .input-by-number{
        width: 20px;
    }
</style>
<script>
    var dateSearch = [];
//    $('#datepicker-pick-up').Zebra_DatePicker({
//        always_visible: $('#datepicker-pick-up-div'),
//        format: 'd/m/Y',
//        custom_classes: {
//            'not-close-calendar': ['1-31']
//        }
//    });
//    $('#datepicker-return').Zebra_DatePicker({
//        always_visible: $('#datepicker-return-div'),
//        format: 'd/m/Y',
//        custom_classes: {
//            'not-close-calendar': ['1-31']
//        }
//    });
    $('#demo').Zebra_DatePicker({
        always_visible: $('#calendar'),
        // disabled_dates: ['* * * 0,6'],
        format: ' d/m/Y',
        custom_classes: {
            'not-close-calendar': ['1-31']
        }

    });


    $('.col-div-calendar-open').on('click', function () {
        var num = $('.calendar-hidden-input').val();
        if (num == 0) {
            $('#calendar').css('display', 'block');
            $('.calendar-hidden-input').val(1);

        } else {
            $('#calendar').css('display', 'none');
            $('.calendar-hidden-input').val(0);


        }
    });
    checkInterval = [];
    $(".container-fluid").on('click', '.dp_body>tr>td', function (e) {
        console.log(7897);
        callendarInterval();
    });
    $('.search-calendar-choose').on('click', function (e) {

        if (!$(e.target).hasClass('not-close-calendar')) {
            var num = $('.calendar-hidden-input').val();
            if (num == 1) {
                $('#calendar').css('display', 'none');
                $('.calendar-hidden-input').val(0);

            }
        } else {
            if ($('.click-calendar-val').val() != 0) {
                var calendarVal = $('.click-calendar-val').val();
                dateSearch.push(calendarVal);
                $('.calendar-check-in').val(dateSearch[0]);
                if (dateSearch.length == 2) {
                    $('.calendar-check-out').val(dateSearch[1]);
                    dateSearch = [];
                }


            }

        }
    });



    $('body').on('click', function () {
        if (!$(event.target).closest('.people-number-click').length) {
            if (!$(event.target).closest('#people-number').length) {
                $('#people-number').css('display', 'none')

            }


        }
        if (!$(event.target).closest('.search-calendar-div-apartment').length) {
            if (!$(event.target).closest('#calendar').length) {
                $('#calendar').css('display', 'none')
            }
        }

    });
    $('.button-minus').on('click', function () {
        var y = $(this).parent().find("input").val();
        var search_cat = $('#people-number').attr('search-type');
        console.log(search_cat);
        if (y > 0) {
            var x = -1;
            var num = parseInt(y);
            var z = x + num;
            $(this).parent().find("input").val(z);
            var numberChildren = $('input[name="apartment_child"]').val().slice(0, 1);
            var numberAdult = $('input[name="apartment_adult"]').val().slice(0, 1);
            var adultIinput = $('.adult-input').val();
            var childrenIinput = $('.children-input').val();

            if (childrenIinput != numberChildren) {
                $('.children-number').val(z);
                $('input[name="apartment_child"]').val(z + ' child')
            }
            if (adultIinput != numberAdult) {
                $('.adult-number').val(z);
                $('input[name="apartment_adult"]').val(z + ' adults')

            }
        }
    });
    $('.button-plus').on('click', function () {
        var x = 1;
        var y = $(this).parent().find("input").val();
        var search_cat = $('#people-number').attr('search-type');
        var nu = parseInt(y);
        var z = x + nu;
        $(this).parent().find("input").val(z);
        var numberChildren = $('input[name="apartment_child"]').val().slice(0, 1);
        var numberAdult = $('input[name="apartment_adult"]').val().slice(0, 1);
        console.log(numberChildren);
        var adultIinput = $('.adult-input').val();
        var childrenIinput = $('.children-input').val();
        console.log(childrenIinput);
        if (childrenIinput != numberChildren) {
            $('.children-number').val(z);
            $('input[name="apartment_child').val(z + ' child')
        }
        if (adultIinput != numberAdult) {
            $('.adult-number').val(z);
            $('input[name="apartment_adult"]').val(z + ' adults')
        }
    });
    $('#see_more_filter').click(function () {
        if ($('.see-more-filter').val() == 0) {
            $(".apartment-search").animate({width: '64%'}, 1000);
            $(".apartment-description").animate({width: '35%'}, 1000);
            $(".button-car-s").animate({width: '98%'}, 1000);
            $('#see_more_filter').text('CLOSE');
            $('.see-more-filter').val(1);
            // $('.more-search-detalis-car').show(500);
            setTimeout(function () {
                $('.more-search-div').show(700);
                setTimeout(function () {
                    $('.more-search-div-1').show(1000);

                }, 200);

            }, 200);
        } else {
            $(".apartment-search").animate({width: '43%'}, 1000);
            $(".button-car-s").animate({width: '95%'}, 1000);
            $(".apartment-description").animate({width: '55%'}, 1000);

            $('#see_more_filter').text('MORE');
            $('.see-more-filter').val(0);

            $('.more-search-div-1').hide(1000);

            setTimeout(function () {
                $('.more-search-div').hide(800);
            }, 200);

        }
    })


</script>













