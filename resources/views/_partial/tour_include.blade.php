@if($carSedan)
    <div class="row div-sedan_img hide-class-div car-info">
        <div class="col-sm-5 car-type-tour">
            <div class="row">
                <div class="owl-theme owl-height  owl-carousel">
                    @foreach($carSedan as $car)
                        <div>
                            <img class="img-apartment" src="{{asset('uploads/'.$car->general_pic)}}" alt=""
                                 style="width: 450px; height: 300px;">
                        </div>
                        @if($car->picture)
                            @foreach($car->picture as $picture)
                                @if($picture->id != 33)
                                <div>
                                    <img src="{{asset('uploads/'.$picture->name)}}"
                                         style="width: 450px; height: 300px;">
                                </div>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-sm-7" style="padding-top: 4%;">
            <div class="row">
                <div class="col-md-6">

                    <p class="car-offer-front">This offer includes:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">

                    <ul class="ul-offer-car">
                        <li>Free WiFi in vehicles</li>
                        <li>Pick up (Hotel, Apartment)</li>
                        <li>Air Conditioning in vehicles</li>


                    </ul>
                </div>
                <div class="col-sm-6">

                    <ul class="ul-offer-car">
                        <li>Bottled water</li>
                        <li>Entrance Tickets</li>
                        <li>Vehicles and passenger insurance</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
@if($carMinivan)
    <div class="row div-minivan_img  hide-class-div car-info" style="display: none">
        <div class="col-sm-5 car-type-tour">
            <div class="row">
                <div class="owl-theme owl-height  owl-carousel">
                    @foreach($carMinivan as $minivan)
                        <div>
                            <img class="img-apartment" src="{{asset('uploads/'.$minivan->general_pic)}}" alt=""
                                 style="width: 450px; height: 300px;">
                        </div>
                        @if($minivan->picture)
                            @foreach($minivan->picture as $minivan_pic)
                                <div>
                                    <img src="{{asset('uploads/'.$minivan_pic->name)}}"
                                         style="width: 450px; height: 300px;">
                                </div>
                            @endforeach
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-sm-7" style="padding-top: 4%;">
            <div class="row">
                <p class="car-offer-front">This offer includes:</p>
            </div>
            <div class="row">
                <div class="col-sm-6">

                    <ul class="ul-offer-car">
                        <li>Free WiFi in vehicles</li>
                        <li>Pick up (Hotel, Apartment)</li>
                        <li>Air Conditioning in vehicles</li>


                    </ul>
                </div>
                <div class="col-sm-6">

                    <ul class="ul-offer-car">
                        <li>Bottled water</li>
                        <li>Entrance Tickets</li>
                        <li>Vehicles and passenger insurance</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="row div-mini_bus_img  hide-class-div car-info" style="display: none">
    <div class="col-sm-5 car-type-tour">
        <div class="row">
            <img class="img-apartment" src="/img/Minibus.jpg" alt=""
                 style="width: 450px; height: 300px;">
        </div>
    </div>
    <div class="col-sm-7" style="padding-top: 4%;">
        <div class="row">
            <p class="car-offer-front">This offer includes:</p>
        </div>
        <div class="row">
            <div class="col-sm-6">

                <ul class="ul-offer-car">
                    <li>Free WiFi in vehicles</li>
                    <li>Pick up (Hotel, Apartment)</li>
                    <li>Air Conditioning in vehicles</li>


                </ul>
            </div>
            <div class="col-sm-6">

                <ul class="ul-offer-car">
                    <li>Bottled water</li>
                    <li>Entrance Tickets</li>
                    <li>Vehicles and passenger insurance</li>

                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row div-bus_img  hide-class-div car-info" style="display: none">
    <div class="col-sm-5 car-type-tour">
        <div class="row">
            <img class="img-apartment" src="/img/Bus.jpg" alt=""
                 style="width: 450px; height: 300px;">
        </div>
    </div>
    <div class="col-sm-7" style="padding-top: 4%;">
        <div class="row">
            <p class="car-offer-front">This offer includes:</p>
        </div>
        <div class="row">
            <div class="col-sm-6">

                <ul class="ul-offer-car">
                    <li>Free WiFi in vehicles</li>
                    <li>Pick up (Hotel, Apartment)</li>
                    <li>Air Conditioning in vehicles</li>


                </ul>
            </div>
            <div class="col-sm-6">

                <ul class="ul-offer-car">
                    <li>Bottled water</li>
                    <li>Entrance Tickets</li>
                    <li>Vehicles and passenger insurance</li>

                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $('.owl-carousel').owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        autoplay:true,
        smartSpeed: 900,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        navClass: ['owl-prev', 'owl-next'],

    });
</script>
<style>
    .owl-nav {
        width: 96%;
        top: 41%;
        position: absolute;
        font-size: 30px;
    }
    ul li{
        display: inline;
    }
</style>