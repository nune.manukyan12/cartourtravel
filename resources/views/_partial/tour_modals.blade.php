{{--reservation Tour--}}
<div class="modal fade" id="book" role="dialog">
    <div class="modal-dialog book-dialog">
        <!-- Modal content-->
        <div class="modal-content book-content">
            <div class="modal-header">
                <h4 class="modal-title">Please enter your info for booking. </h4>
            </div>
            <div class="modal-body">
                <form id="tour-reservation">
                    <div class="row">
                        <input type="hidden" value="" id="tour_type">
                        <input type="hidden" value="" id="regulra_tour">
                        <div class="div-modal-book div-first-block ">
                            <label for="">Last name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                            <input type="text" class="form-control" id="last-name" placeholder="Last name"
                                   name="last_name">
                            <p class="error-last-name"></p>
                        </div>
                        <div class="div-modal-book div-second-block ">
                            <label for="">First name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                            <input type="text" class="form-control" id="first-name" placeholder="First name"
                                   name="first_name">
                            <p class="error-first-name"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="div-modal-book div-first-block ">
                            <label for="">Email address<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                            <input type="email" class="form-control email" id="email" placeholder="Email address"
                                   name="email">
                            <p class="error-email"></p>
                        </div>
                        <div class="div-modal-book div-second-block">
                            <label for="">Phone<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                            <input type="text" class="form-control phone" id="phone" placeholder="Phone" name="phone">
                            <p class="error-phone"></p>

                        </div>
                    </div>
                    <div>
                        <label for="">Viber:</label>
                        <input type="text" class="form-control" id="viber" placeholder="Viber" name="viber">
                    </div>
                    <div class="">
                        <label for="">Telegram:</label>
                        <input type="text" class="form-control" id="telegram" placeholder="Telegram" name="telegram">
                    </div>
                    <div class="">
                        <label for="">Whatsapp:</label>
                        <input type="text" class="form-control" id="whatsapp" placeholder="Whatsapp" name="whatsapp">
                    </div>
                    <p class="date-from"></p>
                    <p class="date-to"></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="book-tour">Book</button>
            </div>
        </div>

    </div>
</div>

{{--Tour destination info--}}
<div class="modal fade" id="modalDestination">
    <div class="modal-dialog modal-lg ">

        <!-- Modal content-->
        <div class="modal-content" style="height: 650px">
            <div class="modal-body modal-picture-destination">

                <div class="row" style="height: 490px">
                    <div class="owl-carousel owl-theme owl-height photo-slider">


                    </div>
                </div>
                <div class="row">
                    <div class="destination-description">

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="modal-messenger" role="dialog">
    <div class="modal-dialog modal-messege-dialog">
        <!-- Modal content-->
        <div class="modal-content modal-messege">
            <div class="modal-body text-center">
                <p class="book-successful">Your booking successful</p>
                <button type="button" class="close " data-dismiss="modal">
                    <i class="fas fa-times close-button-x"></i>
                </button>
            </div>

        </div>

    </div>
</div>
<script>
    $('#book-tour').on('click', function () {

        var data = $('#tour-reservation').serializeArray();
        var type = $('#tour_type').val();
        if (type != 'regular') {

            var tour = JSON.stringify(dataPrivate);

//            if (dataPrivate.length > 0) {
        } else {
            tour = $('#regulra_tour').val();
            console.log(tour);
        }
        data.push({name: "tour", value: tour});
        $.ajax({
            url: '/reservation/tour/save',
            type: 'POST',
            dataType: "JSON",
            headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
            data: data,
            success: function (success) {
                $("#book").modal("hide");
                $("#tour-reservation").find('input').val(' ');
                $("#modal-messenger").modal("show");
                setTimeout(function () {
                    $("#modal-messenger").modal("hide");
                }, 10000);
            },
            error: function (errors) {

                if (errors.responseJSON.errors.last_name) {
                    $(".error-last-name").html(errors.responseJSON.errors.last_name);
                }
                if (errors.responseJSON.errors.first_name) {
                    $(".error-first-name").html(errors.responseJSON.errors.first_name);
                }
                if (errors.responseJSON.errors.email) {
                    $(".error-email").html(errors.responseJSON.errors.email);
                }
                if (errors.responseJSON.errors.phone) {
                    $(".error-phone").html(errors.responseJSON.errors.phone);
                }
                if (errors.responseJSON.errors.pick_up_date || errors.responseJSON.errors.return_date) {

                    $("#modal_error").modal("show");
                    if (errors.responseJSON.errors.pick_up_date) {
                        $(".error-puke-up-date-car").html(errors.responseJSON.errors.pick_up_date);

                    }
                    console.log(errors.responseJSON.errors.return_date);
                    if (errors.responseJSON.errors.return_date) {
                        $(".error-return-date-car").html(errors.responseJSON.errors.return_date);

                    }
                    setTimeout(function () {
                        $("#modal_error").modal("hide");
                    }, 5000);
                }


            }
        })
//        } else {
//            alert('Please choose tour')
//        }

    })
</script>