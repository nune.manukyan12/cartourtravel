@extends('layouts.app')

@section('content')
    @if($about)
        <div class="page_top_wrap">
            <h1 class="page_top_title">
                {{$about->name}}    </h1>

            {{--<h2 class="page_top_desc">--}}
                {{--Affordable, comfortable, enjoyable </h2>--}}
        </div>

        <section class="sections_content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 cust_link_cols">
                        <div class="content_left_link_wrap">
                            <div class="title">
                                Products &amp; Services
                            </div>
                                <a class="item" href="{{asset('contact')}}">
                                   Contact Us </a>
                            <a class="item active_link" href="{{asset('About')}}">
                                About Us </a>

                        </div>
                    </div>

                    <div class="col-lg-9 col-md-8">
                        <div class="content_right">
                            <img src="{{asset('uploads/'.$about->general_pic)}}"
                                 alt="car rental in yerevan chauffeur service" title="" class="img-responsive">


                            <div class="main_content_text">
                                <p style="margin-left:.25in;"><?=$about->description?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    <style>
        .page_top_wrap {
            text-align: center;
            padding-top: 5%;
            padding-bottom: 22px;
        }

        .page_top_wrap .page_top_title {
            color: #343434;
            font-size: 34px;
            font-family: 'sans_semibold';
            line-height: 46px;
            margin: 0;
            padding: 0;
        }

        .page_top_wrap .page_top_desc {
            color: #343434;
            font-family: 'sans_reg';
            font-size: 20px;
            line-height: 28px;
            margin: 0;
            padding: 0;
        }

        .sections_content {
            border-top: 6px solid #2979b2;
        }

        .content_left_link_wrap {
            background-color: #ededed;
        }

        .content_left_link_wrap .title {
            color: #343434;
            font-family: 'sans_semibold';
            font-size: 20px;
            padding-left: 23px;
            padding-bottom: 9px;
            padding-top: 8px;
            border-bottom: 2px solid #ddd;
        }

        .content_left_link_wrap .item {
            display: block;
            text-decoration: none;
            color: #525252;
            font-size: 16px;
            line-height: 20px;
            font-family: 'sans_semibold';
            padding-left: 30px;
            padding-right: 10px;
            padding-top: 15px;
            padding-bottom: 16px;
            border-bottom: 1px solid #ddd;
        }

        .content_left_link_wrap .active_link {
            background-color: white;
            color: #2979b2;
        }
    </style>
@endsection
