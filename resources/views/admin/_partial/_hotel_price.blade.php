<!-- Price tab -->
<div class="tab-pane menue-content" id="price">

    <div class="form-group">
        <b style="font-size: 25px;">Prices</b>
    </div>

    <div class="row">
        <div class="col-md-4 col-lg-2 col-xs-5">
            <label class="radio-inline">
                <input type="radio" name="price_status" id="same-price" value="1" checked>
                Always same price
            </label>
        </div>
        <div class="col-md-6 col-lg-10 col-xs-7 price-block-without-date">
            <div class="many_dates">
                <div class="row row_number_1" style="margin-bottom: 10px">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-2 form-group">
                                <label style="margin-right: 3px">Currency</label><br>
                                <select class="currency-select form-control"  name="currency[0]">
                                    <option value="usd">USD</option>
                                    <option value="amd">AMD</option>
                                    <option value="rub">RUB</option>
                                </select>
                            </div>

                        </div>
                        <div class="row price-block-one">
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Single</label><br>
                                <input class="" type="text" name="single_price[0]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Double/Twin</label><br>
                                <input class="" type="text" name="double_price[0]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Superior Single</label><br>
                                <input class="" type="text" name="superior_price[0]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Superior Double</label><br>
                                <input class="" type="text" name="superior_double_price[0]">
                            </div>
                        </div>
                        <div class="row price-block-one">
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Family Suite</label><br>
                                <input class="" type="text" name="family_price[0]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Executive Suite</label><br>
                                <input class="" type="text" name="executive_price[0]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Deluxe Single</label><br>
                                <input class="" type="text" name="deluxe_price[0]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Deluxe Double</label><br>
                                <input class="" type="text" name="deluxe_double_price[0]">
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-lg-2 col-xs-5">
            <label class="radio-inline">
                <input type="radio" name="price_status" id="price-date" value="2">
                Select date
            </label>
        </div>
        <div class="col-md-8 col-lg-10 col-xs-7 price-block" style="display: none">

            <div class="form-group">
                <label>Add date</label> &nbsp;&nbsp;
                <button type="button" class="btn btn-success btn-sm plus_dates_hotel"><i
                            class="fa fa-plus"></i></button>
            </div>
            <div class="many_dates">
                <div class="row row_number_1" style="margin-bottom: 10px">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-3">
                                <label style="margin-right: 3px">From</label>
                                <input class="date-from" type="date" name="date_from[1]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">To</label><br>
                                <input class="date-to" type="date" name="date_to[1]">
                            </div>
                            <div class="col-md-2 form-group">
                                <label style="margin-right: 3px">Currency</label><br>
                                <select class="currency-select form-control"  name="currency[1]">
                                    <option value="usd">USD</option>
                                    <option value="amd">AMD</option>
                                    <option value="rub">RUB</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <a class="btn btn-danger delete_date" title="Delete" number="1"><i
                                            class="fa fa-times" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row price-block-one">
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Single</label><br>
                                <input class="" type="text" name="single_price[1]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Double/Twin</label><br>
                                <input class="" type="text" name="double_price[1]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Superior Single</label><br>
                                <input class="" type="text" name="superior_price[1]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Superior Double</label><br>
                                <input class="" type="text" name="superior_double_price[1]">
                            </div>
                        </div>
                        <div class="row price-block-one">
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Family Suite</label><br>
                                <input class="" type="text" name="family_price[1]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Executive Suite</label><br>
                                <input class="" type="text" name="executive_price[1]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Deluxe Single</label><br>
                                <input class="" type="text" name="deluxe_price[1]">
                            </div>
                            <div class="col-md-3">
                                <label style="margin-right: 3px">Deluxe Double</label><br>
                                <input class="" type="text" name="deluxe_double_price[1]">
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<!--End price tab-->