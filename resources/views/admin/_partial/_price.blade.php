<!-- Price tab -->
<div class="tab-pane menue-content" id="price">
    <div class="form-group">
        <b style="font-size: 25px;">Prices</b>
    </div>
    <div class="row div-row-price-general">
        <div class="col-md-3 div-price-padding-0">
            <div>
                <p class="title-title">Pricing</p>
            </div>
            <div class="row">
                <div class="col-sm-4 div-price-padding-0" >
                    {{--<p>Currency</p>--}}
                    <p >Base Price</p>
                    <p>Monthly Price</p>
                    <div class="">
                        <label for="">Prepayment</label>
                        <input type="checkbox" class="pre-payment-checkbox" value="yes" name="pre_payment">
                    </div>
                </div>
                <div class="col-sm-8 div-price-padding-0">
                    <input type="hidden" name="currency" value="USD">
                    {{--<div class="div-price-by-imput">--}}
                        {{--<select name="currency" class="price-select">--}}
                            {{--<option value="usd">USD</option>--}}
                            {{--<option value="euro">EURO</option>--}}
                            {{--<option value="amd">AMD</option>--}}
                            {{--<option value="rub">RUB</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    <div class="div-price-by-imput">
                        <input type="text" name="base_price" class="input-price-1" placeholder="Base Price"> <span class="usd-span">USD</span>
                    </div>
                    <div class="div-price-by-imput">
                        <input type="text" name="monthly_price" class="input-price-1" placeholder="Monthly Price"> <span class="usd-span">USD</span>
                    </div>
                    <div class="div-price-by-imput price-payment" style="display: none">
                        <input type="text" name="pre_payment_price" class="input-price-1" placeholder="Prepayment Price"> <span class="usd-span">USD</span>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3 div-price-big-col div-price-padding-0">
            <div>
                <p class="title-title">Length of stay discount</p>
            </div>
            <div class="row">
                <div class="col-sm-3 div-price-padding-0">
                    <p>Weekly</p>
                    <p >14 nights</p>
                    <p>Monthly</p>
                </div>
                <div class="col-sm-9 div-price-padding-0">
                    <div>
                        <input class="input-length" type="text" name="weekly_discount">
                        <label for="">%</label>
                    </div>
                    <div>
                        <input class="input-length" type="text" name="two_weekly_discount">
                        <label for="">%</label>
                    </div>
                    <div>
                        <input class="input-length" type="text" name="monthly_discount">
                        <label for="">%</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 div-price-big-col">
            <div>
                <h5>Cleaning Free<span class="description">*</span></h5>
            </div>
            <div class="form-group">
                <div class="">
                    <label for="">Extra Cleaning</label>
                    <input type="text" class="input-price-1" name="extra_cleaning"> <span class="usd-span">USD</span>
                </div>
            </div>
            <div class="form-group">
                <div class="description-text">
                    <span class="description">*</span><span>This will be free for every reservation</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End price tab-->
<script>
    $('.pre-payment-checkbox').on('change', function () {
        if (this.checked){
            $('.price-payment').show()
        }else {
            $('.price-payment').hide()
        }
    })
</script>