<!-- Price tab -->
<div class="tab-pane menue-content" id="price">

    <div class="form-group">
        <b style="font-size: 25px;">Prices</b>
    </div>
    @foreach($prices as $price)
        <div class="row div-row-price-general">
            <div class="col-md-3 div-price-padding-0">
                <div>
                    <p class="title-title">Pricing</p>
                </div>
                <div class="row">
                    <div class="col-sm-4 div-price-padding-0">
                        {{--<p >Currency</p>--}}
                        <p >Base Price</p>
                        <p>Monthly Price</p>
                        <div class="">
                            <label for="">Prepayment</label>
                            <input type="checkbox" name="pre_payment" class="pre-payment-checkbox" value="yes" {{($price->pre_payment == 'yes') ? 'checked' : ''}}>
                        </div>
                    </div>
                    <div class="col-sm-8 div-price-padding-0">
                        <input type="hidden" name="currency" value="USD">
                        {{--<div class="div-price-by-imput">--}}
                            {{--<select name="currency" class="price-select">--}}
                                {{--<option value="usd" {{($price->currency == 'usd')? 'selected' : ''}}>USD</option>--}}
                                {{--<option value="euro" {{($price->currency == 'euro')? 'selected' : ''}}>EURO</option>--}}
                                {{--<option value="amd" {{($price->currency == 'amd')? 'selected' : ''}}>AMD</option>--}}
                                {{--<option value="rub" {{($price->currency == 'rub')? 'selected' : ''}}>RUB</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        <div class="div-price-by-imput">
                            <input class="input-price-1" type="text" name="base_price" value="{{$price->base_price}}"> <span class="usd-span">USD</span>
                        </div>
                        <div class="div-price-by-imput">
                            <input class="input-price-1" type="text" name="monthly_price" value="{{$price->monthly_price}}"> <span class="usd-span">USD</span>
                        </div>

                        <div class="div-price-by-imput price-payment" >
                            @if($price->pre_payment == 'yes')
                                <input type="text" name="pre_payment_price" class="input-price-1" value="{{$price->pre_payment_price}}" > <span class="usd-span">USD</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">

                </div>
            </div>
            <div class="col-md-3 div-price-padding-0">
                <div>
                    <p class="title-title">Length of stay discount</p>
                </div>
                <div class="row">
                    <div class="col-sm-3 div-price-padding-0">
                        <p>Weekly</p>
                        <p>14 nights</p>
                        <p>Monthly</p>

                    </div>
                    <div class="col-sm-9 div-price-padding-0">
                        <div>
                            <input type="text" class="input-length"  name="weekly_discount" value="{{$price->weekly_discounts}}">
                            <label for="">%</label>
                        </div>
                        <div>
                            <input type="text" class="input-length"  name="two_weekly_discount" value="{{$price->two_weekly_discounts}}">
                            <label for="">%</label>
                        </div>
                        <div>
                            <input type="text" class="input-length"  name="monthly_discount" value="{{$price->monthly_discounts}}">
                            <label for="">%</label>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 div-price-padding-0">
                <div>
                    <h5>Cleaning Free<span class="description">*</span></h5>
                </div>

                <div class="form-group">
                    <div class="">
                        <label for="">Extra Cleaning</label>
                        <input type="text" class="input-price-1" name="extra_cleaning" value="{{$price->extra_cleaning}}"> <span class="usd-span">USD</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="description-text">
                        <span class="description">*</span><span>This will be free for every reservation</span>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
<!--End price tab-->
<script>
    $('.pre-payment-checkbox').on('change', function () {
        if (this.checked){
            $('.price-payment').append(
                ' <input type="text" name="pre_payment_price" class="input-price-1" > <span class="usd-span">USD</span>\n'
            )
        }else {
            $('.price-payment').empty()
        }
    })
</script>