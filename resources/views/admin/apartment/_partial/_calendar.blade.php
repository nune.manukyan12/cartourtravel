<!-- Price tab -->

<div class="tab-pane menue-content" id="calendar">
    <br>
    <div class="div-button-calendar">
        <button type="button" class="btn btn-success available" >Available</button>
        <button type="button" class="btn btn-danger available"  >Blocked</button>
        <label for="" class="nightly-price-label">Nightly price:</label>
        <input type="text" placeholder="Nightly price" class="nightly-price-input" name="nightly_price">
        <button type="button" class="btn btn-success add-price">Add price</button>
    </div>
    <div class="form-group">
        <b style="font-size: 25px;">Calendar</b>
    </div>
    <div class="row">

    </div>

    <div class="row">
        <div class="calendarCreate">

        </div>
    </div>


</div>
<script>
    var dates = [];

    $('.calendarCreate').calendar({
        startYear: new Date().getFullYear(),
        showHeaders: false,
        renderEnd: function (e) {
            var currentMonth = new Date().getMonth();
            $('.calendar .month-container').each(function (idx, el) {
                if (idx < currentMonth) {
                    $(this).remove();
                }
                if (idx > currentMonth + 5) {
                    $(this).remove();
                }
            });
        },
        language: 'en', // or 'fr'
        allowOverlap: true,
        displayWeekNumber: false,
        displayDisabledDataSource: true,
        displayHeader: true,
        alwaysHalfDay: false,
        dataSource: [], // an array of data
        style: 'border',
        enableRangeSelection: true,
        disabledDays: [],
        disabledWeekDays: [],
        hiddenWeekDays: [],
        roundRangeLimits: false,
        contextMenuItems: [], // an array of menu items,
        clickDay: function (e) {
            var element = $(e.element).children('div');
            var status = element.attr('data-status');
            var oldStatus = element.attr('old-status');

            // if (!status || status == 0) {

            if (oldStatus == 'red') {
                $(element).css('background-color', 'rgb(0, 164, 166)');
                $(element).css('color', 'white');
                $(element).css('font-weight', '400');
                element.attr('old-status', 'black');

            } else {
                $(element).css('background-color', 'black');
                $(element).css('color', 'white');
                $(element).css('font-weight', 'bold');
                var year = new Date(e.date).getFullYear();
                var month = new Date(e.date).getMonth() + 1;
                var day = new Date(e.date).getDate();
                var a = day + "-" + month + "-" + year;
                dates.push(a);
                console.log(dates);
                element.attr('old-status', 'red');


            }

            $('.buttons').css('display', 'inline-block');


            // } else {
            //     if (!oldStatus) {
            //         $(element).css('background-color', 'rgb(0, 164, 166)');
            //         $(element).css('color', '#fff');
            //         $(element).css('font-weight', 'normal');
            //         var a = new Date(e.date).getTime();
            //         var index = redDays.indexOf(a);
            //         redDays.splice(index, 1);
            //         element.attr('data-status', 0);
            //     } else {
            //         $(element).css('background-color', 'red');
            //         $(element).css('color', '#fff');
            //         $(element).css('font-weight', 'normal');
            //         var a = new Date(e.date).getTime();
            //         var index = workingDays.indexOf(a);
            //         workingDays.splice(index, 1);
            //         element.attr('data-status', 0);
            //     }
            //     $('.buttons').css('display', 'none');
            // }
            // ;
        },
        // textMenu: null,
        // selectRange: null,
        // mouseOnDay: false,
        // mouseOutDay: null,
        // renderEnd: true,
    });

    $('.available').on('click', function () {
        alert('Please Save your apartment, Then choose date');
        // var myJsonString = JSON.stringify(dates);
        // $.ajax({
        //     url: '/admin/apartment/available',
        //     type: 'POST',
        //     dataType: "JSON",
        //     headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
        //     data: {data:myJsonString, id:apartment_id},
        //
        //     success: function (success) {
        //
        //     },
        //     error: function (errors) {
        //
        //
        //     }
        // })
    })
    $('.add-price').on('click', function () {
        alert('Please Save your apartment, Then choose date');

    })


</script>
<!--End price tab-->