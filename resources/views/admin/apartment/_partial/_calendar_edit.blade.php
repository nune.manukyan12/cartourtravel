<!-- Price tab -->


<div class="tab-pane menue-content" id="calendar">
    <div class="saved-successfully-bloke-day">
        <p class="text-saved">Apartment Saved successfully</p>
    </div>

    <div class="div-button-calendar"><br>
        <button type="button" class="btn btn-success available">Available</button>
        <button type="button" class="btn btn-danger available">Blocked</button>
        <label for="" class="nightly-price-label">Nightly price:</label>
        <input type="text" placeholder="Nightly price" class="nightly-price-input" name="nightly_price">
        <button type="button" class="btn btn-success " id="nightly-price">Nightly price</button>

        <div class="rand-div-color">
            <h4>Existing nightly price</h4>
        </div>
        <div class="existing-nightly-price">
            @for($i=0; $i<$countOfNightlyPrice; $i++)
                <div class="nightly-price-color-div">
                    <div class="existing-nightly-price-color-div existing-nightly-color-{{$nightlyPrice[$i]['nightly_price']}}"></div>
                    <p style="display: inline-block">{{$nightlyPrice[$i]['nightly_price']}}    <span>$</span></p>
                </div>
            @endfor
        </div>


    </div>
    <div class="form-group">
        <b style="font-size: 25px;">Calendar</b>
    </div>
    <div class="row">
        <div class="close" style="width: 3%;
    height: 10px;
    background: #159e9f;margin: 5px;"></div>
        Available
    </div>
    <div class="row">
        <div class="available" style="width: 3%;
    height: 10px;
    background: red;margin: 5px;"></div>
        Closed

    </div>

    <div class="row">
        <div class="calendarDay">
        </div>
    </div>


    <script>
        var redDateTimeArray = [];
        redDateTimeArray =<?php
        if ($notAvailableDate) {
            echo $notAvailableDate;
        } else {
            echo 1;
        }
        ?>;
        var nightlyPrice = <?php
            if ($nightlyPrice) {
                echo $nightlyPrice;
            } else {
//                echo 1;
            }
            ?>;
        var countNigtlyPrice =<?php
            if ($countOfNightlyPrice) {
                echo $countOfNightlyPrice;
            } else {
                echo 0;
            }
            ?>;

        if (redDateTimeArray) {
            var countOfNotWorkingDays = redDateTimeArray.length;
        }
        else {
            var countOfNotWorkingDays = 0;
        }


        // workingDays = [];
        // redDays = [];
        // selectAllMonth = [];
        var dates = [];

        console.log(redDateTimeArray);

        function calendarfun() {
            $('.calendarDay').calendar({
                startYear: new Date().getFullYear(),
                showHeaders: false,
                language: 'en', // or 'fr'
                allowOverlap: true,
                displayWeekNumber: false,
                displayDisabledDataSource: true,
                displayHeader: true,
                alwaysHalfDay: false,
                dataSource: [], // an array of data
                style: 'border',
                enableRangeSelection: true,
                disabledDays: [],
                disabledWeekDays: [],
                hiddenWeekDays: [],
                roundRangeLimits: false,
                contextMenuItems: [], // an array of menu items,
                customDayRenderer: function (element, date) {
                    for (var i = 0; i < countOfNotWorkingDays; i++) {

                        if (date.getTime() == redDateTimeArray[i]) {
                            $(element).css('border', '2px solid white');
                            $(element).css('background-color', 'red');
                            $(element).attr('old-status', 'red');
                        }
                    }
                    for (var n = 0; n < countNigtlyPrice; n++) {
                        var array = JSON.parse("[" + nightlyPrice[n]['date'] + "]");
                        for (var m = 0; m < array[0].length; m++) {
                            if (date.getTime() == array[0][m]) {
                                $(element).addClass('existing-nightly-color-' + nightlyPrice[n]['nightly_price']);
                            }
                        }
                    }
                },
                clickDay: function (e) {

                    var element = $(e.element).children('div');

                    var oldStatus = element.attr('old-status');
                    if (oldStatus == 'red') {

                    } else {
                        element.attr('old-status', 'black');

                    }
                    if (oldStatus == 'red') {
                        $(element).css('background-color', 'rgb(0, 164, 166)');
                        $(element).css('color', 'white');
                        $(element).css('font-weight', '400');
                        element.attr('old-status', 'black');
                        var clickedDay = new Date(e.date).getTime();
                        // console.log(clickedDay);
                        for (var i = 0; i < redDateTimeArray.length; i++) {
                            if (redDateTimeArray[i] == clickedDay) {
                                redDateTimeArray.splice(i, 1);
                                dates.push(clickedDay);
                                console.log(dates);

                            }
                        }


                    } else {

                        $(element).css('background-color', 'black');
                        $(element).css('color', 'white');
                        $(element).css('font-weight', 'bold');
                        var a = new Date(e.date).getTime();
                        redDateTimeArray.push(a);
                        dates.push(a);
                        element.attr('old-status', 'red');
                    }

                    $('.buttons').css('display', 'inline-block');


                },
            });
        }

        calendarfun();
        $('.available').on('click', function () {
            if (0 < dates.length) {
                dates = [];
                console.log(redDateTimeArray);

                var myJsonString = JSON.stringify(redDateTimeArray);
                var apartment_id = $('#apartment_id').val();
                $.ajax({
                    url: '/admin/apartment/available',
                    type: 'POST',
                    dataType: "JSON",
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    data: {data: myJsonString, id: apartment_id},
                    success: function (success) {
                        console.log(success);
                        $('.calendar').calendar({
                            customDayRenderer: function (element, date) {
                                var zang = JSON.parse("[" + success.blockDay + "]");
                                for (var i = 0; i < zang[0].length; i++) {
                                    if (date.getTime() == zang[0][i]) {

                                        $(element).css('border', '2px solid white');
                                        $(element).css('background-color', 'red');
                                        $(element).attr('old-status', 'red');
                                    }
                                }
                                for (var n = 0; n < success.nightlyPrice.length; n++) {
                                    var array = JSON.parse("[" + success.nightlyPrice[n].date + "]");
                                    for (var m = 0; m < array[0].length; m++) {
                                        if (date.getTime() == array[0][m]) {
                                            $(element).addClass('existing-nightly-color-' + success.nightlyPrice[n]['nightly_price']);

                                        }
                                    }
                                }

                            },
                        });
                        $(document).ready(function () {
                            for (var n = 0; n < success.nightlyPrice.length; n++) {
                                var r = Math.floor(Math.random() * 255);
                                var g = Math.floor(Math.random() * (255 - 85)) + 85;
                                var b = Math.floor(Math.random() * 255);
                                $('.existing-nightly-color-' + success.nightlyPrice[n].nightly_price).css('background-color', "rgb(" + r + "," + g + "," + b + ")");

                            }
                        });
                        //
                        $('.saved-successfully-bloke-day').css('display', 'block');
                        $('.text-saved').text('Your change saved successfully');
                        setTimeout(function () {
                            $('.saved-successfully-bloke-day').css('display', 'none');
                            $('.text-saved').text('Your change saved successfully');
                        }, 2000);

                    },
                    error: function (errors) {


                    }
                })
            } else {
                alert('Days are not selected');
            }
        });


        $('#nightly-price').on('click', function () {
            if (0 < dates.length) {
                if ($('.nightly-price-input').val()) {
                    var myJsonString = JSON.stringify(dates);
                    var price = $('.nightly-price-input').val();
                    var apartment_id = $('#apartment_id').val();
                    $.ajax({
                        url: '/admin/apartment/nightly-price',
                        type: 'POST',
                        dataType: "JSON",
                        headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                        data: {data: myJsonString, price: price, id: apartment_id},
                        success: function (success) {
                            var blockDay = JSON.parse("[" + success.blockDay.date + "]");

                            $('.calendar').calendar({
                                customDayRenderer: function (element, date) {
                                    for (var i = 0; i < blockDay[0].length; i++) {
                                        if (date.getTime() == blockDay[0][i]) {
                                            $(element).css('border', '2px solid white');
                                            $(element).css('background-color', 'red');
                                            $(element).attr('old-status', 'red');
                                        }
                                    }

                                    for (var n = 0; n < success.nightlyPrice.length; n++) {
                                        var array = JSON.parse("[" + success.nightlyPrice[n]['date'] + "]");

                                        for (var m = 0; m < array[0].length; m++) {
                                            if (date.getTime() == array[0][m]) {
                                                $(element).addClass('existing-nightly-color-' + success.nightlyPrice[n]['nightly_price']);
                                            }
                                        }
                                    }
                                }

                            });
                            $(document).ready(function () {
                                console.log(success.nightlyPrice.length);
                                for (var n = 0; n < success.nightlyPrice.length; n++) {
                                    var r = Math.floor(Math.random() * 255);
                                    var g = Math.floor(Math.random() * (255 - 85)) + 85;
                                    var b = Math.floor(Math.random() * 255);
                                    // console.log();
                                    $('.existing-nightly-color-' + success.nightlyPrice[n]['nightly_price']).css('background-color', "rgb(" + r + "," + g + "," + b + ")");

                                }
                            });

                            var R = Math.floor(Math.random() * 255);
                            var G = Math.floor(Math.random() * (255 - 85)) + 85;
                            var B = Math.floor(Math.random() * 255);

                            $(".existing-nightly-price").append('<div class="nightly-price-color-div">' +
                                '                   <div class="existing-nightly-price-color-div existing-nightly-color-' + price + '"></div>\n' +
                                '                   <p style="display: inline-block"> ' + price + ' <span>$</span></p>' +
                                '</div>');
                            $('.existing-nightly-color-' + price).css('background-color', "rgb(" + R + "," + G + "," + B + ")");
                            // console.log($('.existing-nightly-color-' + price));
                            // $(".calendar").find(".existing-nightly-color-" + price).css('background-color', "rgb(" + R + "," + G + "," + B + ")");

                            $('.nightly-price-input').val('');

                            $('.saved-successfully-bloke-day').css('display', 'block');
                            $('.text-saved').text('Your change saved successfully');
                            setTimeout(function () {
                                $('.saved-successfully-bloke-day').css('display', 'none');
                                $('.text-saved').text('Your change saved successfully');
                            }, 2000);
                            dates=[];
                            redDateTimeArray=[];
                            redDateTimeArray =<?php
                            if ($notAvailableDate) {
                                echo $notAvailableDate;
                            } else {
                                echo 1;
                            }
                            ?>;
                            countOfNotWorkingDays=redDateTimeArray.length;
                            $('.nightly-price-input').val('');

                            // });
                        },
                        error: function (errors) {
                            console.log(errors);


                        }
                    })

                } else {
                    alert('Please choose nightly price');
                }

            } else {
                alert('Please choose date');

            }

        });


        $(document).ready(function () {
            for (var n = 0; n < countNigtlyPrice; n++) {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * (255 - 85)) + 85;
                var b = Math.floor(Math.random() * 255);

                $('.existing-nightly-color-' + nightlyPrice[n]['nightly_price']).css('background-color', "rgb(" + r + "," + g + "," + b + ")");

            }
        })


    </script>
</div>
<!--End price tab-->