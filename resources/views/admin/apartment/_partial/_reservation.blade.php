<!-- Price tab -->
<div class="tab-pane menue-content" id="reservation">

    <div class="form-group">
        <div class="row">
            <div style="position: relative;">
                <br>
                <button class="btn btn-primary " type="button">Upcoming</button>
                <button class="btn btn-primary " type="button">All</button>
                <button class="btn btn-primary filter-hover" type="button">Filter</button>
                <div class="hover-div">
                    <button class="btn btn-primary filter-button" type="button">Request</button>
                    <button class="btn btn-primary filter-button" type="button">Confirmed</button>
                    <button class="btn btn-primary filter-button" type="button">Declined</button>
                    <button class="btn btn-primary filter-button" type="button">Expired</button>
                    <button class="btn btn-primary filter-button" type="button">Canceled</button>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <form action="">

            <div class="div-from-to-search">
                <label for="">From</label><br>
                <input type="date" class="input-from-to-search">
            </div>
            <div class="div-from-to-search div-to-search">
                <label class="label-margin" for="">To</label><br>
                <input type="date" class="input-from-to-search">
                <button type="button" class="btn btn-primary apply-button">Apply</button>
            </div>

        </form>
    </div>
    <br>
    <div class="row">

        <table class="table table-striped table-bordered table-apartment-reservation">

            <thead>
            <tr>

                <th> Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Viber</th>
                <th>Whatsap</th>
                <th>Telegram</th>

                <th>Guests</th>
                <th>Dates</th>
                {{--<th>Booked</th>--}}
                <th>Apartment</th>
                <th>Status</th>
                <th>Earnings</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            @if( isset($reservations))
            @foreach($reservations as $reservation)
                <tr>
                    <td>{{$reservation->first_name. ' '.$reservation->last_name}}</td>
                    <td>{{$reservation->email}}</td>
                    <td>
                        {{'Tell: ' .$reservation->phone}}
                    </td>
                    <td>{{$reservation->viber}}</td>
                    <td>{{$reservation->whatsap}}</td>
                    <td>{{$reservation->telegram}}</td>

                    <td>
                        {{'Adult: '.$reservation->guest_adult}}<br>
                        {{'Child: '.$reservation->guest_child}}

                    </td>
                    <td>{{date("m/d/Y", $reservation->date_from / 1000) . ' - '. date("m/d/Y", $reservation->date_from / 1000)}}</td>
                    <td>{{$apartment->name}}</td>
                    <td id="">
                        @if($reservation->status == \App\Models\ApartmentReservations::STATUS_REQUEST)

                            Request
                        @elseif($reservation->status == \App\Models\ApartmentReservations::STATUS_CANCELED)
                            Canceled
                        @elseif($reservation->status == \App\Models\ApartmentReservations::STATUS_CONFIRMED)
                            Confirmed
                        @elseif($reservation->status == \App\Models\ApartmentReservations::STATUS_EXPAINED)
                            Expained
                        @endif
                    </td>
                    <td></td>
                    <td>
                        <button class="btn btn-primary" type="button">Message</button>
                    </td>
                </tr>
            @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <script>
        $('.filter-hover').hover(function () {
            // $(this).css("", "block");
            $('.hover-div').css("display", "block");
            $('.filter-arrow').css("display", "inline-block");
            $(this).css("background-color", "#67d179");

            $('.hover-div').hover(function () {
                $(this).css("display", "block");
                $('.hover-div').css("display", "block");
                $('.filter-arrow').css("display", "inline-block");
                $('.filter-hover').css("background-color", "#67d179");

            }, function () {
                $(this).css("display", "none");
                $('.hover-div').css("display", "none");
                $('.filter-arrow').css("display", "none");
                $('.filter-hover').css("background-color", "#3097d1");

            })

        }, function () {
            $('.hover-div').css("display", "none");
            $('.filter-arrow').css("display", "none");
            $(this).css("background-color", "#3097d1");
        })
    </script>

</div>
<!--End price tab-->