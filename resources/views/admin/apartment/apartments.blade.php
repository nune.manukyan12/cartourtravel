@extends('admin.layouts.app')

@section('page-header')
    <h1>
        <i class="fa fa-sticky-note-o text-normal"></i>Apartments&nbsp;&nbsp;&nbsp;
        <a href="{{asset('admin/apartment/create')}}" class="btn btn-success update"><i
                    class="fa fa-plus-circle"></i> {{ trans('car.create')}}</a>
    </h1>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped table-bordered">

            <tr>
                <th>ID</th>
                <th>{{ trans('car.name')}}</th>
                <th>city</th>
                <th>country</th>
                <th>{{ trans('car.status')}}</th>
                <th>Show home page</th>
                <th>Action</th>


            </tr>
            @foreach($apartments as $apartment)
                <tr id="{{'apartment-'.$apartment->id}}">
                    <td>
                        {{$apartment->id}}
                    </td>
                    <td>
                        {{($apartment->translateHasOne) ? $apartment->translateHasOne->name : ''}}
                    </td>
                    <td class="">
                        {{$apartment->city}}
                    </td>
                    <td>
                        {{$apartment->country}}
                    </td>
                    <td id="status-{{$apartment->id}}" value="{{$apartment->status}}">
                        @if($apartment->status == \App\Models\Car::STATUS_PENDING)
                            Pending
                        @else
                            Active
                        @endif
                    </td>
                    <td>
                        <input type="checkbox" class="show_homepage" data-id="{{$apartment->id}}" value="{{$apartment->homepage_order}}" {{($apartment->homepage_order === '1')? 'checked' : ''}}>
                    </td>
                    <td>
                        <?php $user = \Auth::user(); ?>
                        @if($user->type == \App\User::SUPER_ADMIN)
                            <button type="button" class="btn btn-success approve-item" data-id="{{$apartment->id}}">
                                Approve
                            </button>
                        @endif

                        <button class="btn btn-xs btn-danger delete" title="Delete"
                                data-id="{{$apartment->id}}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        <a href="{{asset('admin/apartment/show/'.$apartment->id)}}" class="btn btn-xs btn-warning"
                           title="Edit"
                           value="{{$apartment->id}}"><i class="fa fa-edit" aria-hidden="true"></i></a>


                    </td>

                </tr>
            @endforeach

        </table>
    </div>
    <script>
        $(window).on('load', function () {
            $('.menu-item-apartment').addClass('active-item')
        });


        $('.approve-item').on('click', function () {
            var id = $(this).attr('data-id');
            var tt = $('#status-' + id).attr('value');
            console.log(tt);
            if (tt == 2) {
                alert('This item already Active.');
            } else {
                if (confirm('Do you want to approve this item?')) {
                    // var id = $(this).attr('data-id');
                    $('#status-' + id).empty();
                    $('#status-' + id).html("Active");

                    console.log(tt);
                    $.ajax({
                        url: '/admin/apartment/approval/' + id,
                        type: 'GET',
                        success: function (result) {
                            // $('#car-' + id).remove();

                        },
                        error: function (errors) {


                        }
                    })
                }
            }
        });


        $('.show_homepage').on('click', function () {

            var id = $(this).attr('data-id');
            var val = $(this).val();
         if(val == '0'){
          var data = 1;
             $(this).val(1);
         }else{
             var data = 0;
             $(this).val(0);
         }
            $.ajax({
                url: '/admin/apartment/add-home/' + id + '/'+ data,
                type: 'GET',
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},

                success: function (result) {

                },
                error: function (errors) {


                }
            })

        });


        $('.delete').on('click', function () {
            if (confirm('Do you want to delete this item?')) {

                var id = $(this).attr('data-id')
                $.ajax({
                    url: '/admin/apartment/delete/' + id,
                    type: 'GET',
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
//                data: formData,
//                cache: false,
//                contentType: false,
//                processData: false,
                    success: function (result) {
                        $('#apartment-' + id).remove();

                    },
                    error: function (errors) {


                    }
                })
            }
        })
    </script>
@endsection