@extends('admin.layouts.app')

@section('page-header')
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success success-update" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger error-update" style="display:none; padding-top: 10px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    {!! Form::open(['files'=>true, 'id' => 'mainForm','enctype'=>'multipart/form-data']) !!}
    <div class="row">

        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li next="info" back="" tab="apartment" class="menue-tab apartment active">
                        <a href="#apartment" data-toggle="tab" class="page-number-date" id="1">General</a>
                    </li>
                    <li next="price" back="apartment" tab="info" class="menue-tab info ">
                        <a href="#info" data-toggle="tab" id="2" class="page-number-date">Info</a>
                    </li>
                    <li next="pictures" back="info" tab="price" finish="true" class="menue-tab price">
                        <a href="#price" data-toggle="tab" id="3" class="page-number-date">Price</a>
                    </li>
                    <li next="finish" back="price" tab="pictures" class="menue-tab pictures">
                        <a href="#pictures" data-toggle="tab" id="4" class="page-number-date">Picture</a>
                    </li>
                    {{--<li next="inbox" back="pictures" tab="calendar" class="menue-tab calendar"><a href="#calendar" data-toggle="tab">Calendar</a></li>--}}
                    {{--<li next="reservation" back="calendar" tab="inbox" class="menue-tab inbox"><a href="#inbox" data-toggle="tab" >Inbox</a></li>--}}
                    {{--<li next="help" back="inbox" tab="reservation"  class="menue-tab reservation"><a href="#reservation" data-toggle="tab" >Reservations</a></li>--}}
                    {{--<li next="finish" back="reservation" tab="help"  class="menue-tab tab"><a href="#help" data-toggle="tab" >Help</a></li>--}}

                    <li>
                        <button type="button" style="display: none" class="btn btn-danger back-button">Back
                        </button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-success next-button">Next</button>
                    </li>
                    <li>
                        <button type="button" style="" class="btn btn-success save_item">Finish</button>
                    </li>
                </ul>

                <div class="tab-content">
                {{--@include('admin.apartment._partial._calendar')--}}
                {{--@include('admin.apartment._partial._help')--}}
                {{--@include('admin.apartment._partial._inbox')--}}
                {{-- @include('admin.apartment._partial._reservation')--}}
                <!-- Apartment tab -->
                    <div class="tab-pane menue-content active" id="apartment">
                        <div class="form-group">
                            <b style="font-size: 25px;">New Apartment</b> &nbsp;&nbsp;<input type="hidden"
                                                                                             name="lang_count"
                                                                                             value="1" id="lang_count">
                        </div>
                        <div class="row">
                            <div class="col-md-3 div-price-padding-0">

                                <img style="height: 300px;width: 100%" src="{{asset('/img/no-image.jpg')}}"
                                     id="blah" class="image">

                                <div class="col-md-12" style="margin: 10px 0px">
                                <span class="btn btn-success btn-file">
                                Browse For General Picture<input type="file" onchange="readURL(this);"
                                                                 name="general_pic">
                                </span>
                                </div>
                            </div>
                            <div class="col-md-3 div-price-padding-0">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Country</label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="country" class="input-car-general" id="country">
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>City</label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="city" class="input-car-general" id="city">
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>District</label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="district " class="input-car-general" id="area">
                                            <option value="ajapnyak">Ajapnyak</option>
                                            <option value="arabkir">Arabkir</option>
                                            <option value="avan">Avan</option>
                                            <option value="davtashen">Davtashen</option>
                                            <option value="erebuni">Erebuni</option>
                                            <option value="kanaker-zeytun">Kanaker-Zeytun</option>
                                            <option value="kentron">Center</option>
                                            <option value="malatia-sebastia">Malatia-Sebastia</option>
                                            <option value="nork-marash">Nork-Marash</option>
                                            <option value="nor-nork">Nor Nork</option>
                                            <option value="nubarashen">Nubarashen</option>
                                            <option value="shengavit">Shengavit</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Address</label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="address" class="input-car-general" id="address">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-8">
                                        <button type="button" class="btn input-car-general btn-success" id="see_on_map">
                                            See on map
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 div-price-padding-0">
                                <div id="map" style="height: 500px;width: 100%"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-9" id="left-content-event-tab">
                                <!-- Custom Tabs -->
                                <div class="nav-tabs-custom">

                                    <ul class="nav nav-tabs">
                                        <li tab="english" class="menue-tab active">
                                            <a href="#en" data-toggle="tab" class="page-number-date">English</a>
                                        </li>
                                        <li tab="armenian" class="menue-tab translate-tab" lang="hy">
                                            <a href="#hy" data-toggle="tab"  class="page-number-date">Armenian</a>
                                        </li>
                                        <li tab="russian" class="menue-tab translate-tab" lang="ru">
                                            <a href="#ru" data-toggle="tab"  class="page-number-date">Russian</a>
                                        </li>
                                        <li>
                                            <button type="button" lang="" id="translate-btn" style="display: none"
                                                    class="btn btn-info">Translate for english
                                            </button>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="event_tab_content">
                                        <div class="tab-pane active" id="en">                     <!-- Event 1 -->
                                            <div class="form-group">
                                                <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                        type="text"
                                                        class="form-control default-name"
                                                        name="name[]"
                                                        id="en_name">
                                            </div>
                                            <input type="hidden" name="lang[]" value="en">
                                            <div class="form-group">
                                        <textarea name="description[]" class="editor default-desc" id="en_description"
                                                  style="width: 100%;height: 200px;"></textarea>
                                                <style>
                                                    .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                        height: 200px;
                                                    }
                                                </style>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="hy">                     <!-- Event 1 -->
                                            <div class="form-group">
                                                <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                        type="text"
                                                        class="form-control default-name"
                                                        name="name[]"
                                                        id="hy_name">
                                            </div>
                                            <input type="hidden" name="lang[]" value="hy">
                                            <div class="form-group">
                                        <textarea name="description[]" class="editor default-desc" id="hy_description"
                                                  style="width: 100%;height: 200px;"></textarea>
                                                <style>
                                                    .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                        height: 200px;
                                                    }
                                                </style>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="ru">                     <!-- Event 1 -->
                                            <div class="form-group">
                                                <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                        type="text"
                                                        class="form-control default-name"
                                                        name="name[]"
                                                        id="ru_name">
                                            </div>
                                            <input type="hidden" name="lang[]" value="ru">
                                            <div class="form-group">
                                        <textarea name="description[]" class="editor default-desc" id="ru_description"
                                                  style="width: 100%;height: 200px;"></textarea>
                                                <style>
                                                    .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                        height: 200px;
                                                    }
                                                </style>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--  End  Apartment tap-->
                    <!-- Info tab -->
                    <div class="tab-pane menue-content" id="info">
                        <div class="form-group">
                            <b style="font-size: 25px;">Apartment info</b> &nbsp;&nbsp;<input type="hidden"
                                                                                              name="lang_count"
                                                                                              value="1" id="lang_count">
                        </div>
                        <div class="row">
                            <div class="col-md-9">

                                <div class="row" id="content-content">
                                    <div class="col-sm-4">

                                        <div>
                                            <p class="title-title">Property type</p>
                                            <div>
                                                <input type="radio" name="property_type" value="building">
                                                <p class="p-property">Apartement in building</p>
                                            </div>
                                            <div>
                                                <input type="radio" name="property_type" value="house">
                                                <p class="p-property">House</p>
                                            </div>
                                        </div>


                                        <div>
                                            <p class="title-title">Number of guests</p>
                                            <button type="button" class="btn btn-primary buttonminus"><i
                                                        class="fas fa-minus"></i></button>
                                            <input type="text" name="number_of_quests" class="input-num" value="0">
                                            <button type="button" class="btn btn-primary buttonbtn"><i
                                                        class="fas fa-plus"></i></button>
                                        </div>
                                        {{--<div>--}}
                                        {{--<p class="title-title">Total number of beds</p>--}}
                                        {{--<button type="button" class="btn btn-primary buttonminus"><i--}}
                                        {{--class="fas fa-minus"></i></button>--}}
                                        {{--<input type="text" name="total_number_of_beds" class="input-num" value="0">--}}
                                        {{--<button type="button" class="btn btn-primary buttonbtn"><i--}}
                                        {{--class="fas fa-plus"></i></button>--}}
                                        {{--</div>--}}
                                        <div>
                                            <p class="title-title marginBottom-0">Living room</p>
                                            <p class="marginBottom-0">Sofa bed</p>
                                            <button type="button" class="btn btn-primary buttonminus">
                                                <i class="fas fa-minus"></i>
                                            </button>
                                            <input type="text" name="number_sofa_bed" class="input-num" value="0">
                                            <button type="button" class="btn btn-primary buttonbtn">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                        <div>
                                            <p class="title-title">Bathrooms</p>
                                            <button type="button" id="bathrooms-minus" class="btn btn-primary "><i
                                                        class="fas fa-minus"></i></button>
                                            <input type="text" name="bathrooms" class="input-num" value="0">
                                            <button type="button" id="bathrooms-plus" class="btn btn-primary "><i
                                                        class="fas fa-plus"></i></button>
                                        </div>
                                        <p class="title-title">Hygenic things</p>
                                        <ul>
                                            <li>
                                                <p class="p-articale">Towels</p>
                                                {{--<button type="button" class="btn btn-primary buttonminus"><i--}}
                                                {{--class="fas fa-minus"></i></button>--}}
                                                {{--<input type="text" name="towels" class="input-num" value="0">--}}
                                                {{--<button type="button" class="btn btn-primary buttonbtn"><i--}}
                                                {{--class="fas fa-plus"></i></button>--}}
                                                <input type="checkbox" name="towels" value="yes">
                                            </li>
                                            <li>
                                                <p class="p-articale">Bed sheets</p>
                                                {{--<button type="button" class="btn btn-primary buttonminus"><i--}}
                                                {{--class="fas fa-minus"></i></button>--}}
                                                {{--<input type="text" name="bed_sheets" class="input-num" value="0">--}}
                                                {{--<button type="button" class="btn btn-primary buttonbtn"><i--}}
                                                {{--class="fas fa-plus"></i></button>--}}
                                                <input type="checkbox" name="bed_sheets" value="yes">

                                            </li>

                                            <li>
                                                <p class="p-articale">Pillows</p>
                                                {{--<button type="button" class="btn btn-primary buttonminus"><i--}}
                                                {{--class="fas fa-minus"></i></button>--}}
                                                {{--<input type="text" name="pillows" class="input-num" value="0">--}}
                                                {{--<button type="button" class="btn btn-primary buttonbtn"><i--}}
                                                {{--class="fas fa-plus"></i></button>--}}
                                                <input type="checkbox" name="pillows" value="yes">

                                            </li>
                                            <li>
                                                <p class="p-articale">Soap</p>
                                                <input type="checkbox" name="soap" value="yes">
                                            </li>
                                            <li>
                                                <p class="p-articale">Toilet paper</p>
                                                <input type="checkbox" name="toilet_paper" value="yes">
                                            </li>
                                            <li>
                                                <p class="p-articale">Shampoo</p>
                                                <input type="checkbox" name="shampoo" value="yes">
                                            </li>
                                        </ul>
                                        <div>
                                            <p class="title-title">Heating</p>
                                            <div>
                                                <p class="p-radio">Central heating</p>
                                                <input type="radio" name="heating" value="central_heating">
                                            </div>
                                            <div>
                                                <p class="p-radio">Local heating system</p>
                                                <input type="radio" name="heating" value="local_heating_system">
                                            </div>
                                        </div>

                                        <div>
                                            <p class="title-title">Air conditioning</p>
                                            <div>
                                                <p class="p-radio">Central cooling</p>
                                                <input type="checkbox" name="central_cooling" id="central-cooling"
                                                       value="yes">
                                            </div>
                                            <div>
                                                <p class="p-radio">Air conditioner</p>
                                                <input type="checkbox" id="air-conditioner" name="air_conditioning"
                                                       value="yes">
                                                <div class="number-air-conditioner-big">

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-4">

                                        <div>
                                            <p class="title-title">Total number of bedroom</p>
                                            <button type="button" class="btn btn-primary buttonminus remove-bedroom"><i
                                                        class="fas fa-minus"></i></button>
                                            <input type="text" id="total-number-of-bedrooms"
                                                   name="total_number_of_bedrooms" class="input-num" value="0">
                                            <button type="button" class="btn btn-primary buttonbtn add-bedroom "><i
                                                        class="fas fa-plus"></i></button>
                                        </div>
                                        <div class="bedroom-big-body">

                                        </div>
                                        <div>


                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div>
                                            <input type="checkbox" name="hot_water" value="yes">
                                            <p class="p-checkbox">Hot water</p>

                                        </div>
                                        <div>

                                            <input type="checkbox" name="wifi" value="yes">
                                            <p class="p-checkbox">Wi Fi</p>

                                        </div>
                                        <div>
                                            <input type="checkbox" name="tv" value="yes">
                                            <p class="p-checkbox">TV</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="cable_tv" value="yes">
                                            <p class="p-checkbox">Cable TV</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="youtube" value="yes">
                                            <p class="p-checkbox">Youtube</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="hair_dryer" value="yes">
                                            <p class="p-checkbox">Hair dryer</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="iron" value="yes">
                                            <p class="p-checkbox">Iron</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="iron_table" value="yes">
                                            <p class="p-checkbox">Iron table</p>
                                        </div>

                                        <div>
                                            <input type="checkbox" name="washing_machine" value="yes">
                                            <p class="p-checkbox">Washing machine</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="refrigerator" value="yes">
                                            <p class="p-checkbox">Refrigerator</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="micro_wave" value="yes">
                                            <p class="p-checkbox">Micro wave</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="oven" value="yes">
                                            <p class="p-checkbox">Oven</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="kitchen_ware" value="yes">
                                            <p class="p-checkbox">Kitchen ware</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="electric_kettle" value="yes">
                                            <p class="p-checkbox">Electric kettle</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="key_card_access" value="yes">
                                            <p class="p-checkbox">Key card access</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="elevator" value="yes">
                                            <p class="p-checkbox">Elevator</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="balcony" value="yes">
                                            <p class="p-checkbox">Balcony</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="terrace" value="yes">
                                            <p class="p-checkbox">Terrace</p>
                                        </div>
                                        <div>
                                            <p class="title-title">Parking Type</p>
                                            <ul>
                                                <li>
                                                    <input type="checkbox" name="free_parking" value="yes">
                                                    <p class="p-parking">Free Outdoor parking</p>
                                                </li>
                                                <li>
                                                    <input type="checkbox" name="underground_parking" value="yes">
                                                    <p class="p-parking">Underground parking</p>
                                                </li>

                                            </ul>
                                        </div>

                                        <div>
                                            <p class="title-title">View</p>
                                            <ul>
                                                <li>
                                                    <input type="checkbox" name="city_view" value="yes">
                                                    <p class="p-parking">City view</p>
                                                </li>
                                                <li>
                                                    <input type="checkbox" name="street_view" value="yes">
                                                    <p class="p-parking">Street view</p>
                                                </li>
                                                <li>
                                                    <input type="checkbox" name="yard_view" value="yes">
                                                    <p class="p-parking">Yard view</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="garden" value="yes">
                                            <p class="p-checkbox">Garden</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="pool" value="yes">
                                            <p class="p-checkbox">Pool</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="sauna" value="yes">
                                            <p class="p-checkbox">Sauna</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="barbeque_place" value="yes">
                                            <p class="p-checkbox">Barbeque Place</p>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="hot_tube" value="yes">
                                            <p class="p-checkbox">Hot Tube</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Info tab -->

                @include('admin._partial._price')

                <!-- Picute tab -->
                    <div class="tab-pane menue-content" id="pictures">

                        <div class="form-group">
                            <b style="font-size: 25px;">Pictures</b>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Add photos</div>

                                    <div class="panel-body">
                                        <form></form>
                                        {!! Form::open(['url'=>'admin/image-upload','files'=>true ,'enctype'=>'multipart/form-data','class'=>'dropzone','id'=>'myImage','name'=>'image']) !!}

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <script>
                                    Dropzone.options.imageUpload = {
                                        addRemoveLinks: true,
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                        dictRemoveFile: "Remove image",
                                        dictResponseError: 'Error while uploading file!',
                                    };
                                </script>

                            </div>
                        </div>
                    </div>
                    <!--End picture tab-->
                </div>
            </div>
        </div>
    </div>
    <script>
        ymaps.ready(init);

        function init() {
            myMap = new ymaps.Map('map', {
                center: [40.1791857, 44.4991029],
                zoom: 9
            });
        }

        $('#see_on_map').on('click', function () {
            var address = $('#country').val() + ' ' + $('#city').val() + ' ' + $('#area').val() + ' ' + $('#address').val();

            // Поиск координат центра Нижнего Новгорода.
            ymaps.geocode(address, {
                /**
                 * Опции запроса
                 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                 */
                // Сортировка результатов от центра окна карты.
                // boundedBy: myMap.getBounds(),
                // strictBounds: true,
                // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
                // Если нужен только один результат, экономим трафик пользователей.
                results: 1
            }).then(function (res) {
                // Выбираем первый результат геокодирования.
                var firstGeoObject = res.geoObjects.get(0),
                    // Координаты геообъекта.
                    coords = firstGeoObject.geometry.getCoordinates(),
                    // Область видимости геообъекта.
                    bounds = firstGeoObject.properties.get('boundedBy');

                firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
                // Получаем строку с адресом и выводим в иконке геообъекта.
                firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());

                // Добавляем первый найденный геообъект на карту.
                myMap.geoObjects.add(firstGeoObject);
                // Масштабируем карту на область видимости геообъекта.
                myMap.setBounds(bounds, {
                    // Проверяем наличие тайлов на данном масштабе.
                    checkZoomRange: true
                });


            });

        });


        $(window).on('load', function () {
            $('.menu-item-apartment').addClass('active-item')
        });

        $('.save_item').on('click', function () {

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var form = document.getElementById('mainForm');
            var formData = new FormData(form);
            console.log(formData);
            $.ajax({
                url: '/admin/apartment/store',
                type: 'POST',
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $('.error-update').css('display', 'none');
                    $('.success-update').text('');
                    var href = window.location.href;
                    var test = href.split('/');
                    window.location.href = test[0] + '/admin/apartment/show/' + result.id;
                    $('.success-update').css('display', 'block');
                    $('.success-update').text('Apartment Saved successfully')
                },
                error: function (errors) {
                    $('.success-update').css('display', 'none');
                    $('.error-update').css('display', 'none');
                    $('.error-update').text('');
                    var error = errors.responseJSON;
                    $('.error-update').css('display', 'block');
                    $.each(error.errors, function (index, value) {
                        $('.error-update').append(value[0] + '</br>')
                    })


                }
            })

        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.translate-tab').on('click', function () {
            var lang = $(this).attr('lang');
            $('#translate-btn').css('display', 'block')
            $('#translate-btn').attr('lang', lang)
        })


    </script>
    {!! Form::close() !!}

@endsection