@extends('admin.layouts.app')

@section('page-header')
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success success-update" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px; margin-bottom: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger error-update" style="display:none; padding-top: 10px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    {!! Form::open(['files'=>true, 'id' => 'mainForm','enctype'=>'multipart/form-data']) !!}
    <div class="row">

        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li next="info" tab="apartment" class="menue-tab active apartment">
                        <a href="#apartment" data-toggle="tab" id="1" class="page-number-date">General</a>
                    </li>
                    <li next="price" back="apartment" tab="info" class="menue-tab info">
                        <a href="#info" data-toggle="tab" id="2" class="page-number-date">Info</a>
                    </li>
                    <li next="pictures" back="info" tab="price" class="menue-tab price">
                        <a href="#price" data-toggle="tab" id="3" class="page-number-date">Price</a>
                    </li>
                    <li next="calendar" back="price" tab="pictures" class="menue-tab pictures">
                        <a href="#pictures" data-toggle="tab" id="4" class="page-number-date">Picture</a>
                    </li>
                    <li next="inbox" back="pictures" tab="calendar" class="menue-tab calendar">
                        <a href="#calendar" data-toggle="tab" id="5" class="page-number-date calendar-same-class">Calendar</a>
                    </li>
                    <li next="reservation" back="calendar" tab="inbox" class="menue-tab inbox">
                        <a href="#inbox" data-toggle="tab" id="6" class="page-number-date">Inbox</a>
                    </li>
                    <li next="help" back="inbox" tab="reservation" finish="true" class="menue-tab reservation">
                        <a href="#reservation" data-toggle="tab" id="7" class="page-number-date">Reservations</a>
                    </li>
                    <li tab="help" back="reservation" class="menue-tab help">
                        <a href="#help" data-toggle="tab" id="8" class="page-number-date">Help</a>
                    </li>
                    <li>
                        <button type="button" style="display: none" class="btn btn-danger back-button">
                           Back
                        </button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-success next-button">Next

                        </button>
                    </li>

                    <li>
                        <button type="button" class="btn btn-success save_item">Finish</button>
                    </li>
                </ul>

                <div class="tab-content">
                    <?php $prices = $apartment->prices;
                    $price_status = $apartment->price_status;

                    ?>
                    @include('admin._partial._price_edit',$prices)
                    @include('admin.apartment._partial._calendar_edit')
                    @include('admin.apartment._partial._help')
                    @include('admin.apartment._partial._inbox')
                    @include('admin.apartment._partial._reservation')
                    <div class="tab-pane menue-content active" id="apartment">
                        <div class="form-group">
                            <b style="font-size: 25px;">Edit Apartment</b> &nbsp;&nbsp;<input type="hidden" name="id"
                                                                                              id="apartment_id"
                                                                                              value="{{$apartment->id}}">
                        </div>
                        <div class="row">

                            <div class="col-md-3 div-price-padding-0">

                                <img style="height: 300px;width: 100%"
                                     src="{{asset('/uploads/'.$apartment->general_pic)}}" id="blah" class="image">

                                <div class="col-md-12" style="margin: 10px 0px">
                                <span class="btn btn-success btn-file">
                                Browse For General Picture<input type="file" onchange="readURL(this);"
                                                                 name="general_pic" value="{{$apartment->general_pic}}">
                                </span>
                                </div>
                            </div>
                            <div class="col-md-3 div-price-padding-0">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Country</label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="country" value="{{$apartment->country}}" id="country"
                                               class="input-car-general">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>City</label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="city" value="{{$apartment->city}}" id="city"
                                               class="input-car-general">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>District</label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="district" class="input-car-general" id="area">
                                            <option value="ajapnyak" {{($apartment->district == 'ajapnyak')? 'selected' : ''}}>
                                                Ajapnyak
                                            </option>
                                            <option value="arabkir" {{($apartment->district == 'arabkir')? 'selected' : ''}}>
                                                Arabkir
                                            </option>
                                            <option value="avan" {{($apartment->district == 'avan')? 'selected' : ''}}>
                                                Avan
                                            </option>
                                            <option value="davtashen" {{($apartment->district == 'davtashen')? 'selected' : ''}}>
                                                Davtashen
                                            </option>
                                            <option value="erebuni" {{($apartment->district == 'erebuni')? 'selected' : ''}}>
                                                Erebuni
                                            </option>
                                            <option value="kanaker-zeytun" {{($apartment->district == 'kanaker-zeytun')? 'selected' : ''}}>
                                                Kanaker-Zeytun
                                            </option>
                                            <option value="kentron" {{($apartment->district == 'kentron')? 'selected' : ''}}>
                                                Kentron
                                            </option>
                                            <option value="malatia-sebastia" {{($apartment->district == 'malatia-sebastia')? 'selected' : ''}}>
                                                Malatia-Sebastia
                                            </option>
                                            <option value="nork-marash" {{($apartment->district == 'nork-marash')? 'selected' : ''}}>
                                                Nork-Marash
                                            </option>
                                            <option value="nor-nork" {{($apartment->district == 'nor-nork')? 'selected' : ''}}>
                                                Nor Nork
                                            </option>
                                            <option value="nubarashen" {{($apartment->district == 'nubarashen')? 'selected' : ''}}>
                                                Nubarashen
                                            </option>
                                            <option value="shengavit" {{($apartment->district == 'shengavit')? 'selected' : ''}}>
                                                Shengavit
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Address</label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="address" value="{{$apartment->address}}" id="address"
                                               class="input-car-general">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-8">
                                        <button type="button"  class="btn input-car-general btn-success" id="see_on_map">See on map</button>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-5 div-price-padding-0">
                                <div id="map" style="height: 500px;width: 100%"></div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-9" id="left-content-event-tab">
                                <!-- Custom Tabs -->
                                <div class="nav-tabs-custom">

                                    <ul class="nav nav-tabs">
                                        <li tab="english" class="menue-tab active">
                                            <a href="#en" data-toggle="tab" class="page-number-date">English</a>
                                        </li>
                                        <li tab="armenian" class="menue-tab translate-tab" lang="hy">
                                            <a href="#hy" data-toggle="tab"  class="page-number-date">Armenian</a>
                                        </li>
                                        <li tab="russian" class="menue-tab translate-tab" lang="ru">
                                            <a href="#ru" data-toggle="tab"  class="page-number-date">Russian</a>
                                        </li>
                                        <li>
                                            <button type="button" lang="" id="translate-btn"
                                                    class="btn btn-info">Translate for english
                                            </button>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="event_tab_content">
                                        @foreach($apartment->translate as $translate)
                                        <div class="tab-pane {{($translate->locale == \App\Models\ApartmentTranslation::en) ? 'active' : ''}}" id="{{$translate->locale}}">                     <!-- Event 1 -->
                                            <div class="form-group">
                                                <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                        value="{{$translate->name}}"
                                                        type="text"
                                                        value=""
                                                        class="form-control default-name"
                                                        name="name[]"
                                                        id="{{$translate->locale.'_name'}}">
                                            </div>
                                            <input type="hidden" name="lang[]" value="{{$translate->locale}}">
                                            <div class="form-group">
                                        <textarea name="description[]" class="editor default-desc" id="{{$translate->locale.'_description'}}" style="width: 100%;height: 200px;">{{$translate->description}}</textarea>
                                                <style>
                                                    .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                        height: 200px;
                                                    }
                                                </style>

                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane menue-content" id="info">
                        <div class="form-group">
                            <b style="font-size: 25px;">Apartment info</b> &nbsp;&nbsp;<input type="hidden"
                                                                                              name="lang_count"
                                                                                              value="1" id="lang_count">
                        </div>
                        @foreach($apartment->details as $detail)
                            <input name="detail_id" type="hidden" value='{{$detail->id}}'/>
                            <div class="row">
                                <div class="col-md-9">

                                    <div class="row" id="content-content">
                                        <div class="col-sm-4">

                                            <div>
                                                <p class="title-title">Property type</p>
                                                <div>
                                                    <input type="radio" name="property_type"
                                                           {{($detail->property_type=='building')? 'checked' : ''}} value="building">
                                                    <p class="p-property">Apartement in building</p>
                                                </div>
                                                <div>
                                                    <input type="radio" name="property_type"
                                                           {{($detail->property_type=='house')? 'checked' : ''}}
                                                           value="house">
                                                    <p class="p-property">House</p>
                                                </div>
                                            </div>


                                            <div>
                                                <p class="title-title">Number of guests</p>
                                                <button type="button" class="btn btn-primary buttonminus"><i
                                                            class="fas fa-minus"></i></button>
                                                <input type="text" name="number_of_quests" class="input-num"
                                                       value="{{$detail->number_of_quests}}">
                                                <button type="button" class="btn btn-primary buttonbtn"><i
                                                            class="fas fa-plus"></i></button>
                                            </div>
                                            {{--<div>--}}
                                            {{--<p class="title-title">Total number of beds</p>--}}
                                            {{--<button type="button" class="btn btn-primary buttonminus"><i--}}
                                            {{--class="fas fa-minus"></i></button>--}}
                                            {{--<input type="text" name="total_number_of_beds" class="input-num"--}}
                                            {{--value="{{$detail->total_number_of_beds}}">--}}
                                            {{--<button type="button" class="btn btn-primary buttonbtn"><i--}}
                                            {{--class="fas fa-plus"></i></button>--}}
                                            {{--</div>--}}

                                            <div>
                                                <p class="title-title marginBottom-0">Living room</p>
                                                <p class="marginBottom-0">Sofa bed</p>
                                                <button type="button" class="btn btn-primary buttonminus">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="text" name="number_sofa_bed" class="input-num" value="{{($detail->number_sofa_bed) ? $detail->number_sofa_bed : '0' }}">
                                                <button type="button" class="btn btn-primary buttonbtn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div>
                                                <p class="title-title">Bathrooms</p>
                                                <button type="button" class="btn btn-primary" id="bathrooms-minus">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="text" name="bathrooms" class="input-num"
                                                       value="{{$detail->bathrooms}}">
                                                <button type="button" class="btn btn-primary " id="bathrooms-plus">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <p class="title-title">Hygenic things</p>
                                            <ul>
                                                <li>
                                                    <p class="p-articale">Towels</p>
                                                    {{--<button type="button" class="btn btn-primary buttonminus"><i--}}
                                                    {{--class="fas fa-minus"></i></button>--}}
                                                    {{--<input type="text" name="towels" class="input-num"--}}
                                                    {{--value="{{$detail->towels}}">--}}
                                                    {{--<button type="button" class="btn btn-primary buttonbtn"><i--}}
                                                    {{--class="fas fa-plus"></i></button>--}}
                                                    <input type="checkbox"
                                                           {{($detail->towels == 'yes') ? 'checked' : ''}} name="towels"
                                                           value="yes">
                                                </li>
                                                <li>
                                                    <p class="p-articale">Bed sheets</p>
                                                    {{--<button type="button" class="btn btn-primary buttonminus"><i--}}
                                                    {{--class="fas fa-minus"></i></button>--}}
                                                    {{--<input type="text" name="bed_sheets" class="input-num"--}}
                                                    {{--value="{{$detail->bed_sheets}}">--}}
                                                    {{--<button type="button" class="btn btn-primary buttonbtn"><i--}}
                                                    {{--class="fas fa-plus"></i></button>--}}
                                                    <input type="checkbox"
                                                           {{($detail->bed_sheets == 'yes') ? 'checked' : ''}} name="bed_sheets"
                                                           value="yes">
                                                </li>

                                                <li>
                                                    <p class="p-articale">Pillows</p>
                                                    {{--<button type="button" class="btn btn-primary buttonminus"><i--}}
                                                    {{--class="fas fa-minus"></i></button>--}}
                                                    {{--<input type="text" name="pillows" class="input-num"--}}
                                                    {{--value="{{$detail->pillows}}">--}}
                                                    {{--<button type="button" class="btn btn-primary buttonbtn"><i--}}
                                                    {{--class="fas fa-plus"></i></button>--}}
                                                    <input type="checkbox"
                                                           {{($detail->pillows == 'yes') ? 'checked' : ''}} name="pillows"
                                                           value="yes">
                                                </li>
                                                <li>
                                                    <p class="p-articale">Soap</p>
                                                    <input type="checkbox"
                                                           {{($detail->soap == "yes") ? 'checked' : ''}} name="soap"
                                                           value="yes">
                                                </li>
                                                <li>
                                                    <p class="p-articale">Toilet paper</p>
                                                    <input type="checkbox"
                                                           {{($detail->toilet_paper == 'yes') ? 'checked' : ''}} name="toilet_paper"
                                                           value="yes">
                                                </li>
                                                <li>
                                                    <p class="p-articale">Shampoo</p>
                                                    <input type="checkbox"
                                                           {{($detail->shampoo == 'yes') ? 'checked' : ''}} name="shampoo"
                                                           value="yes">
                                                </li>
                                            </ul>
                                            <div>
                                                <p class="title-title">Heating</p>
                                                <div>
                                                    <p class="p-radio">Central heating</p>
                                                    <input type="radio"
                                                           {{($detail->heating == 'central_heating') ? 'checked' : ''}} name="heating"
                                                           value="central_heating">
                                                </div>
                                                <div>
                                                    <p class="p-radio">Local heating system</p>
                                                    <input type="radio"
                                                           {{($detail->heating == 'local_heating_system') ? 'checked' : ''}} name="heating"
                                                           value="local_heating_system">
                                                </div>
                                            </div>

                                            <div>
                                                <p class="title-title">Air conditioning</p>
                                                <div>
                                                    <p class="p-radio">Central cooling</p>
                                                    <input type="checkbox"
                                                           {{($detail->central_cooling == 'yes') ? 'checked' : ''}}
                                                           name="central_cooling" id="central-cooling"
                                                           value="yes">
                                                </div>
                                                <div>
                                                    <p class="p-radio">Air conditioner</p>
                                                    <input type="checkbox"
                                                           {{($detail->air_conditioning == 'yes') ? 'checked' : ''}}
                                                           id="air-conditioner" name="air_conditioning"
                                                           value="yes">
                                                    <div class="number-air-conditioner-big">

                                                        @if($detail->air_conditioning == 'yes')
                                                        <div class="number-air-conditioner">
                                                            <button type="button" class="btn btn-primary buttonminus">
                                                                <i class="fas fa-minus"></i>
                                                            </button>
                                                            <input type="text" name="number_air_conditioner" class="input-num" value="{{$detail->number_air_conditioner}}">
                                                            <button type="button" class="btn btn-primary buttonbtn">
                                                                <i class="fas fa-plus"></i>
                                                            </button>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-4">

                                            <div>
                                                <p class="title-title">Total number of bedroom</p>
                                                <button type="button"
                                                        class="btn btn-primary buttonminus remove-bedroom"><i
                                                            class="fas fa-minus"></i></button>
                                                <input type="text" id="total-number-of-bedrooms"
                                                       name="total_number_of_bedrooms" class="input-num"
                                                       value="{{$detail->total_number_of_bedrooms}}">
                                                <button type="button" class="btn btn-primary buttonbtn add-bedroom "><i
                                                            class="fas fa-plus"></i></button>
                                            </div>
                                            <div class="bedroom-big-body">
                                                <?php $num = 1; ?>
                                                @foreach($detail->bedrooms as $bedroom)
                                                    <div class="bedroom-body-'{{$num}}'">
                                                        <ul>
                                                            <p class="title-title">Bedroom{{' '.$num}}</p>
                                                            <li>
                                                                <p class="p-articale">Double</p>
                                                                <button type="button"
                                                                        class="btn btn-primary buttonminus"><i
                                                                            class="fas fa-minus"></i></button>
                                                                <input type="text" name="double[{{$num}}]"
                                                                       class="input-num" value="{{$bedroom->double}}">
                                                                <button type="button" class="btn btn-primary buttonbtn">
                                                                    <i class="fas fa-plus"></i></button>
                                                            </li>
                                                            <li>
                                                                <p class="p-articale">Queen</p>
                                                                <button type="button"
                                                                        class="btn btn-primary buttonminus"><i
                                                                            class="fas fa-minus"></i></button>
                                                                <input type="text" name="queen[{{$num}}]"
                                                                       class="input-num" value="{{$bedroom->queen}}">
                                                                <button type="button" class="btn btn-primary buttonbtn">
                                                                    <i class="fas fa-plus"></i></button>
                                                            </li>
                                                            <li>
                                                                <p class="p-articale">Single</p>
                                                                <button type="button"
                                                                        class="btn btn-primary buttonminus"><i
                                                                            class="fas fa-minus"></i></button>
                                                                <input type="text" name="single[{{$num}}]"
                                                                       class="input-num" value="{{$bedroom->single}}">
                                                                <button type="button" class="btn btn-primary buttonbtn">
                                                                    <i class="fas fa-plus"></i></button>
                                                            </li>
                                                            <li>
                                                                <p class="p-articale">Sofa bed</p>
                                                                <button type="button"
                                                                        class="btn btn-primary buttonminus"><i
                                                                            class="fas fa-minus"></i></button>
                                                                <input type="text" name="sofa_bed[{{$num}}]"
                                                                       class="input-num" value="{{$bedroom->sofa_bed}}">
                                                                <button type="button" class="btn btn-primary buttonbtn">
                                                                    <i class="fas fa-plus"></i></button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <?php $num++; ?>
                                                @endforeach
                                            </div>

                                        </div>
                                        <div class="col-sm-4">
                                            <div>
                                                <input type="checkbox" name="hot_water"
                                                       value="yes" {{($detail->hot_water == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Hot water</p>

                                            </div>
                                            <div>

                                                <input type="checkbox" name="wifi"
                                                       value="yes" {{($detail->wifi == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Wi Fi</p>

                                            </div>
                                            <div>
                                                <input type="checkbox" name="tv"
                                                       value="yes" {{($detail->tv == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">TV</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="cable_tv"
                                                       value="yes" {{($detail->cable_tv == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Cable TV</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="youtube"
                                                       value="yes" {{($detail->youtube == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Youtube</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="hair_dryer"
                                                       value="yes" {{($detail->hair_dryer == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Hair dryer</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="iron"
                                                       value="yes" {{($detail->iron == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Iron</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="iron_table"
                                                       value="yes" {{($detail->iron_table == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Iron table</p>
                                            </div>

                                            <div>
                                                <input type="checkbox" name="washing_machine"
                                                       value="yes" {{($detail->washing_machine == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Washing machine</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="refrigerator"
                                                       value="yes" {{($detail->refrigerator == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Refrigerator</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="micro_wave"
                                                       value="yes" {{($detail->micro_wave == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Micro wave</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="oven"
                                                       value="yes" {{($detail->oven == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Oven</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="kitchen_ware"
                                                       value="yes" {{($detail->kitchen_ware == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Kitchen ware</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="electric_kettle"
                                                       value="yes" {{($detail->electric_kettle == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">electric_kettle</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="key_card_access"
                                                       value="yes" {{($detail->key_card_access == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Key card access</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="elevator"
                                                       value="yes" {{($detail->elevator == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Elevator</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="balcony"
                                                       value="yes" {{($detail->balcony == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Balcony</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="terrace"
                                                       value="yes" {{($detail->terrace == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Terrace</p>
                                            </div>
                                            <div>
                                                <p class="title-title">Parking Type</p>
                                                <ul>
                                                    {{--<li>--}}
                                                        {{--<input type="radio" name="parking_type"--}}
                                                        {{--value="free_parking" {{($detail->terrace == 'free_parking') ? 'checked' : ''}}>--}}
                                                        {{--<input type="checkbox" name="free_parking"--}}
                                                               {{--value="yes" {{($detail->free_parking == 'yes') ? 'checked' : ''}}>--}}
                                                        {{--<p class="p-parking">Free parking</p>--}}
                                                    {{--</li>--}}
                                                    <li>
                                                        <input type="checkbox" name="free_parking" value="yes" {{($detail->free_parking == 'yes') ? 'checked' : ''}}>
                                                        <p class="p-parking">Free Outdoor parking</p>
                                                    </li>
                                                    <li>
                                                        <input type="checkbox" name="underground_parking" value="yes" {{($detail->underground_parking == 'yes') ? 'checked' : ''}}>
                                                        <p class="p-parking">Underground parking</p>
                                                    </li>
                                                    {{--<li>--}}
                                                        {{--<input type="radio" name="parking_type"--}}
                                                               {{--value="outdoor_parking" {{($detail->parking_type == 'outdoor_parking') ? 'checked' : ''}}>--}}
                                                        {{--<p class="p-parking">Outdoor parking</p>--}}
                                                    {{--</li>--}}
                                                    {{--<li>--}}
                                                        {{--<input type="radio" name="parking_type"--}}
                                                               {{--value="underground_parking" {{($detail->parking_type == 'underground_parking') ? 'checked' : ''}}>--}}
                                                        {{--<p class="p-parking">Underground parking</p>--}}
                                                    {{--</li>--}}
                                                </ul>
                                            </div>

                                            <div>
                                                <p class="title-title">View</p>
                                                <ul>
                                                    <li>
                                                        <input type="checkbox" name="city_view"
                                                               value="yes" {{($detail->city_view == 'yes') ? 'checked' : ''}}>
                                                        <p class="p-parking">City view</p>
                                                    </li>
                                                    <li>
                                                        <input type="checkbox" name="street_view"
                                                               value="yes" {{($detail->street_view == 'yes') ? 'checked' : ''}}>
                                                        <p class="p-parking">Street view</p>
                                                    </li>
                                                    <li>
                                                        <input type="checkbox" name="yard_view"
                                                               value="yes" {{($detail->yard_view == 'yes') ? 'checked' : ''}}>
                                                        <p class="p-parking">Yard view</p>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="garden" value="yes" {{($detail->garden == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Garden</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="pool" value="yes" {{($detail->pool == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Pool</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="sauna" value="yes" {{($detail->sauna == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Sauna</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="barbeque_place" value="yes" {{($detail->barbeque_place == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Barbeque Place</p>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="hot_tube" value="yes" {{($detail->hot_tube == 'yes') ? 'checked' : ''}}>
                                                <p class="p-checkbox">Hot Tube</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>


                    <!-- /.tab-pane -->
                    <div class="tab-pane menue-content" id="pictures">
                        <div class="alert alert-notice">
                            <span class="close">x</span>
                        </div>
                        <div class="form-group">
                            <b style="font-size: 25px;">Pictures</b>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Add photos</div>

                                    <div class="panel-body">
                                        <form></form>
                                        {!! Form::open(['url'=>'admin/image-upload','files'=>true ,'enctype'=>'multipart/form-data','class'=>'dropzone','id'=>'myImage','name'=>'image']) !!}

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <script>
                                    Dropzone.options.imageUpload = {
                                        addRemoveLinks: true,
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                        dictRemoveFile: "Remove image",
                                        dictResponseError: 'Error while uploading file!',
                                    };
                                </script>

                            </div>
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Photos</div>

                                    <div class="panel-body">
                                        @foreach($apartment->picture as $image)
                                            <div id="pic_container{{$image->id}}" class="pic_container">
                                                <img src="{{asset('/uploads/'.$image->name)}}" width="200px">
                                                <button type="button"
                                                        class="btn btn-danger btn-sm delete_button picture-delete-btn"
                                                        style="border-radius: 50%"
                                                        title="Delete photo" data-id="{{$image->id}}" model="apartment">
                                                    X
                                                </button>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
    <script>
        ymaps.ready(init);

        function init() {
            myMap = new ymaps.Map('map', {
                center: [40.1791857, 44.4991029],
                zoom: 9
            });
            gecodeAddresYandexMap();
        }
        $('#see_on_map').on('click', function () {
            gecodeAddresYandexMap()

        });
        function gecodeAddresYandexMap() {
            var address = $('#country').val() + ' ' + $('#city').val() + ' ' + $('#area').val() + ' ' + $('#address').val();

            // Поиск координат центра Нижнего Новгорода.
            ymaps.geocode(address, {
                /**
                 * Опции запроса
                 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                 */
                // Сортировка результатов от центра окна карты.
                // boundedBy: myMap.getBounds(),
                // strictBounds: true,
                // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
                // Если нужен только один результат, экономим трафик пользователей.
                results: 1
            }).then(function (res) {
                // Выбираем первый результат геокодирования.
                var firstGeoObject = res.geoObjects.get(0),
                    // Координаты геообъекта.
                    coords = firstGeoObject.geometry.getCoordinates(),
                    // Область видимости геообъекта.
                    bounds = firstGeoObject.properties.get('boundedBy');

                firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
                // Получаем строку с адресом и выводим в иконке геообъекта.
                firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());

                // Добавляем первый найденный геообъект на карту.
                myMap.geoObjects.add(firstGeoObject);
                // Масштабируем карту на область видимости геообъекта.
                myMap.setBounds(bounds, {
                    // Проверяем наличие тайлов на данном масштабе.
                    checkZoomRange: true
                });


            });
        }
        $(window).on('load', function () {
            $('.menu-item-apartment').addClass('active-item')
        });


        $('.bedroom').on('change', function () {
            var val = $(this).val()
            if (val == 'yes') {
                $('.double-bedroom').removeClass('unselectable')
                $('.double-bedroom').find('fieldset').attr('disabled', false)
                $('.sofa-bedroom').removeClass('unselectable')
                $('.sofa-bedroom').find('fieldset').attr('disabled', false)
                $('.simple-bedroom').find('fieldset').attr('disabled', false)
                $('.simple-bedroom').removeClass('unselectable')
            } else {
                $('.double-bedroom').addClass('unselectable')
                $('.simple-bedroom').addClass('unselectable')
                $('.sofa-bedroom').addClass('unselectable')
                $('.sofa-bedroom').find('fieldset').attr('disabled', true)
                $('.double-bedroom').find('fieldset').attr('disabled', true)
                $('.simple-bedroom').find('fieldset').attr('disabled', true)
            }
        });

        $('.tv').on('click', function () {
            var valeTv = $(this).val();
            if (valeTv == 'yes') {
                $('.tvcabel').attr('disabled', false)
                $('.tvcabel').removeClass('unselectable')
            } else {
                $('.tvcabel').attr('disabled', true)
                $('.tvcabel').addClass('unselectable')
            }
        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.save_item').on('click', function () {

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var form = document.getElementById('mainForm');
            var formData = new FormData(form);

            $.ajax({
                url: '/admin/apartment/update',
                type: 'POST',
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {

                    $('.error-update').css('display', 'none')
                    $('.success-update').text('')
                    $('.success-update').css('display', 'block')
                    $('.success-update').text('Apartment Saved successfully')

                },
                error: function (errors) {
                    $('.success-update').css('display', 'none');
                    $('.error-update').css('display', 'none');
                    $('.error-update').text('');

                    var error = errors.responseJSON;

                    $('.error-update').css('display', 'block')
                    $.each(error.errors, function (index, value) {

                        $('.error-update').append(value[0] + '</br>')
                    })


                }
            })

        })
    </script>

@endsection