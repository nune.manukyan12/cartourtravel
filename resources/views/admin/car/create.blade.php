@extends('admin.layouts.app')

@section('page-header')
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-update alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px; margin-bottom: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger error-update" style="display:none; padding-top: 10px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    {!! Form::open(['files'=>true,  'id' => 'mainForm','enctype'=>'multipart/form-data']) !!}
    <div class="row">

        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li next="info" back=""  tab="car" class="menue-tab active car">
                        <a href="#car" data-toggle="tab" class="page-number-date" id="1">General</a>
                    </li>
                    <li  next="pictures" back="car" tab="info" finish="true" class="menue-tab info">
                        <a href="#info" data-toggle="tab" class="page-number-date">Info</a>
                    </li>
                    {{--<li tab="info" class="menue-tab"><a href="#calendar" data-toggle="tab">Calendar</a></li>--}}

                    <li next="finish" back="info"  tab="pictures" class="menue-tab pictures"><a href="#pictures" data-toggle="tab" class="page-number-date">Pictures</a></li>
                    {{--<li tab="info" class="menue-tab"><a href="#Reservations" data-toggle="tab">Reservations</a></li>--}}

                    <li>
                        <button type="button" style="display: none" class="btn btn-danger back-button">Back</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-success next-button">Next</button>
                    </li>
                    <li >
                        <button type="button" style="display: none" class="btn btn-success save_item" >Finish</button>
                    </li>
                </ul>

                <div class="tab-content">
                    <!-- Car tab-->

                    <div class="tab-pane menue-content active" id="car">
                        <div class="form-group">
                            <b style="font-size: 25px;">Car</b>
                            <input type="hidden" name="lang_count" value="1" id="lang_count">
                        </div>
                        <div class="row">

                            <div class="col-md-4 div-price-padding-0">

                                    <img style="height: 300px;width: 100%" src="{{asset('/img/no-image.jpg')}}"
                                         id="blah" class="image">

                                <div class="col-md-12" style="margin: 10px 0px">
                                <span class="btn btn-success btn-file">
                                Browse For General Picture<input type="file" onchange="readURL(this);"
                                                                 name="general_pic">
                                </span>
                                </div>
                            </div>
                            <div class="col-md-5 div-price-padding-0">
                                <div class="car-car-parametrs-div">
                                    <label>Car type</label>
                                    <div style="margin-top: 8px">
                                        <label>Door count</label>
                                    </div>
                                    <div style="margin-top: 8px">
                                        <label>Persons</label>
                                    </div>
                                    <div style="margin-top: 8px">
                                        <label>Daily price</label><sub class="car-price-sup">*</sub>
                                    </div>
                                </div>
                                <div class="car-info-div-by-input">
                                    <div>
                                        <select name="type_id" id="car_type">
                                            @foreach($types as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="car-car-detail">
                                        <input type="text" name="door_count" class="car-detail-parametr">
                                    </div>
                                    <div class="car-car-detail">
                                        <input type="text" name="persons" class="car-detail-parametr">
                                    </div>
                                    <div class="car-car-detail">
                                        <input type="text" class="car-detail-parametr" name="car_day_prices">
                                        <select name="currency" id="car-currency-select">
                                            <option value="usd">USD</option>
                                            <option value="euro">EURO</option>
                                            <option value="amd" selected>AMD</option>
                                            <option value="rub">RUB</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-3 div-price-padding-0">


                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-9" id="left-content-event-tab">
                                <!-- Custom Tabs -->
                                <div class="nav-tabs-custom">

                                    <ul class="nav nav-tabs">
                                        <li tab="english" class="menue-tab active">
                                            <a href="#en" data-toggle="tab" class="page-number-date">English</a>
                                        </li>
                                        <li tab="armenian" class="menue-tab translate-tab" lang="hy">
                                            <a href="#hy" data-toggle="tab"  class="page-number-date">Armenian</a>
                                        </li>
                                        <li tab="russian" class="menue-tab translate-tab" lang="ru">
                                            <a href="#ru" data-toggle="tab"  class="page-number-date">Russian</a>
                                        </li>
                                        <li>
                                            <button type="button" lang="" id="translate-btn" style="display: none"
                                                    class="btn btn-info">Translate for english
                                            </button>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="event_tab_content">
                                        <div class="tab-pane active" id="en">                     <!-- Event 1 -->
                                            <div class="form-group">
                                                <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                        type="text"
                                                        class="form-control default-name"
                                                        name="name[]"
                                                        id="en_name">
                                            </div>
                                            <input type="hidden" name="lang[]" value="en">
                                            <div class="form-group">
                                        <textarea name="description[]" class="editor default-desc" id="en_description"
                                                  style="width: 100%;height: 200px;"></textarea>
                                                <style>
                                                    .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                        height: 200px;
                                                    }
                                                </style>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="hy">                     <!-- Event 1 -->
                                            <div class="form-group">
                                                <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                        type="text"
                                                        class="form-control default-name"
                                                        name="name[]"
                                                        id="hy_name">
                                            </div>
                                            <input type="hidden" name="lang[]" value="hy">
                                            <div class="form-group">
                                        <textarea name="description[]" class="editor default-desc" id="hy_description"
                                                  style="width: 100%;height: 200px;"></textarea>
                                                <style>
                                                    .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                        height: 200px;
                                                    }
                                                </style>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="ru">                     <!-- Event 1 -->
                                            <div class="form-group">
                                                <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                        type="text"
                                                        class="form-control default-name"
                                                        name="name[]"
                                                        id="ru_name">
                                            </div>
                                            <input type="hidden" name="lang[]" value="ru">
                                            <div class="form-group">
                                        <textarea name="description[]" class="editor default-desc" id="ru_description"
                                                  style="width: 100%;height: 200px;"></textarea>
                                                <style>
                                                    .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                        height: 200px;
                                                    }
                                                </style>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- end Car tab -->

                    <!-- Picture tab -->
                    <div class="tab-pane menue-content" id="pictures">

                        <div class="form-group">
                            <b style="font-size: 25px;">Pictures</b>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Add photos</div>

                                    <div class="panel-body">
                                        <form></form>
                                        {!! Form::open(['url'=>'admin/image-upload','files'=>true ,'enctype'=>'multipart/form-data','class'=>'dropzone','id'=>'myImage','name'=>'image']) !!}

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <script>
                                    Dropzone.options.imageUpload = {
                                        addRemoveLinks: true,
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                        dictRemoveFile: "Remove image",
                                        dictResponseError: 'Error while uploading file!',
                                    };
                                </script>

                            </div>
                        </div>
                    </div>
                    <!-- End picture tab -->
                    {{--info Tab--}}
                    <div class="tab-pane menue-content" id="info">

                        <b style="font-size: 25px;">Info</b>
                        <div class="row div-row-price-general">
                            <div class="col-sm-4 div-price-padding-0">
                                <p class="title-car-info-detail">Car Category- size,cost, power & luxury factor</p>
                                <div class="info-div-car">
                                    <select name="car_category" class="select-car-info">
                                        <option value="M">M-Mlni</option>
                                        <option value="N">N-Mini Elite</option>
                                        <option value="E">E-Econamy</option>
                                        <option value="C">C-Compact</option>
                                        <option value="I">I-Intermediate</option>
                                        <option value="S">S-Standard</option>
                                        <option value="F">F-Full-size</option>
                                        <option value="P">P-Premium</option>
                                        <option value="X">X-Special</option>
                                    </select>
                                </div>
                                <p class="title-car-info-detail">Doors & vehicle type</p>
                                <div class="info-div-car">
                                    <select name="doors_vehicle_type" class="select-car-info">
                                        <option value="B">B-2/3 door</option>
                                        <option value="C">C-2/4 door</option>
                                        <option value="D">D-2/4 Sdoor</option>
                                        <option value="W">W-Wagon/Estate</option>
                                        <option value="S">S-Sport</option>
                                        <option value="T">T-Convertible</option>
                                        <option value="F">F-SUV</option>
                                        <option value="E">E-Coupe</option>
                                        <option value="N">N-Roadster</option>
                                    </select>
                                </div>
                                <p class="title-car-info-detail">Transmission & drive</p>
                                <div class="info-div-car">
                                    <select name="transmission_drive" class="select-car-info">
                                        <option value="M">M-Manual drive</option>
                                        <option value="N">N-Manual, 4WD</option>
                                        <option value="C">C-Manual, AWD</option>
                                        <option value="A">Auto drive</option>
                                        <option value="B">B-Auto, 4WD</option>
                                        <option value="D">D-Auto,AWD</option>
                                    </select>
                                </div>
                                <p class="title-car-info-detail">Fuel & A/C</p>
                                <div class="info-div-car">
                                    <select name="fuel" class="select-car-info">
                                        <option value="N">Unspecifled, no A/C</option>
                                        <option value="R">Unspecifled, fuel A/C</option>
                                        <option value="D">Diesel, A/C</option>
                                        <option value="Q">Diesel, no A/C</option>
                                        <option value="H">Hybrid, A/C</option>
                                        <option value="I">Hybrid, no A/C</option>
                                        <option value="E">Electric A/C</option>
                                        <option value="C">Electric, no A/C</option>
                                        <option value="V">Petrol, A/C</option>
                                        <option value="Z">Petrol, no A/C</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-sm-4 div-price-padding-0">
                                <div class="car-info-title-parametr">
                                    <div class="row">
                                        <p class="title-car-info-detail transmision-p-car">Winter wheel</p>
                                        <select name="winter_wheel" class="select-car-info-2">
                                            <option value="yes">Yes</option>
                                            <option value="no">no</option>
                                        </select>
                                    </div>
                                    <div class="row">
                                        <p class="title-car-info-detail transmision-p-car">Transmission</p>
                                        <select name="transmission" class="select-car-info-2">
                                            <option value="Auto">Auto</option>
                                            <option value="Manual">Manual</option>
                                        </select>
                                    </div>

                                    <div class="row">
                                        <p class="title-car-info-detail transmision-p-car">Conditioner</p>
                                        <div>
                                            <select name="conditioner" class="select-car-info-2">
                                                <option value="yes">Yes</option>
                                                <option value="no">no</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-1 div-price-padding-0">

                            </div>
                            <div class="col-sm-3 div-price-padding-0">

                            </div>
                        </div>
                    </div>
                    {{--end info--}}
                    {{--rezervation content--}}
                    <div class="tab-pane menue-content " id="Reservations">
                        <b style="font-size: 25px;">Reservations</b>
                        <table class="table-car-rezervation table table-hover table-striped table-bordered">
                            <tr class="tr-car-rezervation">
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Viber</th>
                                <th>whatsap</th>
                                <th>Telegram</th>
                                <th>Pick-up date and time</th>
                                <th>Return date and time</th>
                                <th>Pick-up locations</th>
                                <th> Return locations</th>
                                <th>Status</th>
                            </tr>
                        </table>
                    </div>
                    {{--end rezervation--}}
                    {{--calendar content--}}
                    <div class="tab-pane menue-content " id="calendar">
                        <b style="font-size: 25px;">Calendar</b>
                        <div class="calendar-car"></div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <script>
                $('.calendar-car').calendar({
                    startYear: new Date().getFullYear(),
                    showHeaders: false,
                    language: 'en', // or 'fr'
                    allowOverlap: true,
                    displayWeekNumber: false,
                    displayDisabledDataSource: true,
                    displayHeader: true,
                    alwaysHalfDay: false,
                    dataSource: [], // an array of data
                    style: 'border',
                    enableRangeSelection: true,
                    disabledDays: [],
                    disabledWeekDays: [],
                    hiddenWeekDays: [],
                    roundRangeLimits: false,
                    contextMenuItems: [], // an array of menu items,

                });


                $(window).on('load', function () {
                    $('.menu-item-car').addClass('active-item')
                });

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#blah')
                                .attr('src', e.target.result)
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $('.save_item').on('click', function () {

                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                    var form = document.getElementById('mainForm');
                    var formData = new FormData(form);
                    // var test = $('#mainForm').serializeArray();
                    // console.log(test);
                    $.ajax({
                        url: '/admin/car/store',
                        type: 'POST',
                        headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result) {

                            $('.error-update').css('display', 'none');
                            $('.alert-success').css('display', 'block');
                            $('.success-update').text('Car Saved successfully')
                            var href = window.location.href;
                            var test = href.split('/');

                            setTimeout(function () {
                                window.location.href = test[0] + '/admin/car/show/' + result.id;

                            }, 2000)

                        },
                        error: function (errors) {
                            $('.success-update').css('display', 'none');
                            $('.error-update').css('display', 'none');
                            $('.error-update').text('');

                            var error = errors.responseJSON;
                            $('.error-update').css('display', 'block');
                            $.each(error.errors, function (index, value) {
                                console.log(index);
                                console.log(value);

                                $('.error-update').append(value[0] + '</br>')
                            })


                        }
                    })

                })
            </script>

@endsection