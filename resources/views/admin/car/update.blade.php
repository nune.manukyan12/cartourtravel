@extends('admin.layouts.app')

@section('page-header')
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish success-update alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px; margin-bottom: 10px">
        <div class="col-md-10 col-md-offset faild-publish error-update alert-danger" style="display:none; padding-top: 10px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    {!! Form::open(['files'=>true, 'id' => 'mainForm','enctype'=>'multipart/form-data']) !!}
    <div class="row">

        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li next="info" tab="car" class="menue-tab active car">
                        <a href="#car" data-toggle="tab" id="1" class="page-number-date">General</a>
                    </li>
                    <li next="calendar" back="car" tab="info" class="menue-tab info">
                        <a href="#info" data-toggle="tab" id="2" class="page-number-date">Info</a>
                    </li>
                    <li next="pictures" back="info" tab="calendar" class="menue-tab calendar">
                        <a href="#calendar" data-toggle="tab" id="3" class="page-number-date">Calendar</a>
                    </li>

                    <li  next="reservation" back="calendar"  tab="pictures" finish="true" class="menue-tab pictures">
                        <a href="#pictures" data-toggle="tab" id="4" class="page-number-date">Pictures</a>
                    </li>
                    <li next="finish" back="pictures" tab="reservation" class="menue-tab reservation">
                        <a href="#reservation" data-toggle="tab" id="5" class="page-number-date">Reservations</a>
                    </li>

                    <li>
                        <button type="button" style="display: none" class="btn btn-danger back-button">
                            Back
                        </button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-success next-button">Next

                        </button>
                    </li>

                    <li>
                        <button type="button" class="btn btn-success save_item">Finish</button>
                    </li>
                </ul>

                <div class="tab-content">

                    {{--Car contet--}}
                    <div class="tab-pane menue-content active" id="car">
                        <div class="form-group">
                            <b style="font-size: 25px;">Car</b>
                            <input type="hidden" name="lang_count" value="1" id="lang_count">
                        </div>
                        <div class="row">

                            <div class="col-md-3 div-price-padding-0">
                                <img style="height: 300px;width: 100%"
                                     src="{{asset('/uploads/'.$car->general_pic)}}"
                                     id="blah" class="image">

                                <div class="col-md-12" style="margin: 10px 0px">
                                <span class="btn btn-success btn-file">
                                Browse For General Picture<input type="file" onchange="readURL(this);"
                                                                 name="general_pic">
                                </span>
                                </div>
                            </div>
                            <div class="col-md-6 div-price-padding-0">

                                <div class="car-car-parametrs-div">
                                    <label>Car type</label>
                                    <div style="margin-top: 8px">
                                        <label>Door count</label>
                                    </div>
                                    <div style="margin-top: 8px">
                                        <label>Persons</label>
                                    </div>
                                    <div style="margin-top: 8px">
                                        <label>Daily price</label><sup class="car-price-sup">*</sup>
                                    </div>
                                </div>

                                <div class="car-info-div-by-input">
                                    <select name="type_id" id="car_type">
                                        @foreach($types as $type)
                                            <option value="{{$type->id}}" {{($type->id == $car->type_id)? 'selected' : ''}}>{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                    <div style="margin-top: 8px">

                                        <input type="text" name="door_count" value="{{$car->door_count}}"
                                               class="car-detail-parametr">

                                    </div>
                                    <div style="margin-top: 10px">
                                        <input type="text" name="persons" value="{{$car->persons}}" class="car-detail-parametr">

                                    </div>
                                    <input type="hidden" value="{{$car->id}}"  name="id" class="car-detail-parametr">

                                    <div style="margin-top: 10px">
                                        <input type="text" class="car-detail-parametr" name="car_day_prices"
                                               value="{{$car->car_day_prices}}">
                                        <select name="currency" id="car-currency-select">
                                            <option value="usd" {{($car->currency == 'usd')? 'selected' : ''}}>USD
                                            </option>
                                            <option value="euro" {{($car->currency == 'euro')? 'selected' : ''}} >
                                                EURO
                                            </option>
                                            <option value="amd" {{($car->currency == 'amd')? 'selected' : ''}}>AMD
                                            </option>
                                            <option value="rub" {{($car->currency == 'rub')? 'selected' : ''}} >RUB
                                            </option>
                                        </select>
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-3 div-price-padding-0">


                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-9" id="left-content-event-tab">
                                <!-- Custom Tabs -->
                                <div class="nav-tabs-custom">

                                    <ul class="nav nav-tabs">
                                        <li tab="english" class="menue-tab active">
                                            <a href="#en" data-toggle="tab" class="page-number-date">English</a>
                                        </li>
                                        <li tab="armenian" class="menue-tab translate-tab" lang="hy">
                                            <a href="#hy" data-toggle="tab"  class="page-number-date">Armenian</a>
                                        </li>
                                        <li tab="russian" class="menue-tab translate-tab" lang="ru">
                                            <a href="#ru" data-toggle="tab"  class="page-number-date">Russian</a>
                                        </li>
                                        <li>
                                            <button type="button" lang="" id="translate-btn"
                                                    class="btn btn-info">Translate for english
                                            </button>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="event_tab_content">
                                        @foreach($car->translate as $translate)
                                            <div class="tab-pane {{($translate->locale == \App\Models\CarTranslation::en) ? 'active' : ''}}" id="{{$translate->locale}}">                     <!-- Event 1 -->
                                                <div class="form-group">
                                                    <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                            value="{{$translate->name}}"
                                                            type="text"
                                                            value=""
                                                            class="form-control default-name"
                                                            name="name[]"
                                                            id="{{$translate->locale.'_name'}}">
                                                </div>
                                                <input type="hidden" name="lang[]" value="{{$translate->locale}}">
                                                <div class="form-group">
                                                    <textarea name="description[]" class="editor default-desc" id="{{$translate->locale.'_description'}}" style="width: 100%;height: 200px;">{{$translate->description}}</textarea>
                                                    <style>
                                                        .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                            height: 200px;
                                                        }
                                                    </style>

                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- pictures content -->
                    <div class="tab-pane menue-content" id="pictures">

                        <div class="form-group">
                            <b style="font-size: 25px;">Pictures</b>
                        </div>
                        <div class="alert alert-notice">
                            <span class="close">x</span>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Add photos</div>

                                    <div class="panel-body">
                                        <form></form>
                                        {!! Form::open(['url'=>'admin/image-upload','files'=>true ,'enctype'=>'multipart/form-data','class'=>'dropzone','id'=>'myImage','name'=>'image']) !!}

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <script>
                                    Dropzone.options.imageUpload = {
                                        addRemoveLinks: true,
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                        dictRemoveFile: "Remove image",
                                        dictResponseError: 'Error while uploading file!',
                                    };
                                </script>

                            </div>
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Photos</div>

                                    <div class="panel-body">
                                        @foreach($car->picture as $image)
                                            <div id="pic_container{{$image->id}}" class="pic_container">
                                                <img src="{{asset('/uploads/'.$image->name)}}" width="200px">
                                                <button type="button"
                                                        class="btn btn-danger btn-sm delete_button picture-delete-btn"
                                                        style="border-radius: 50%"
                                                        title="Delete photo" data-id="{{$image->id}}" model="car">X
                                                </button>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    {{--Info content--}}
                    <div class="tab-pane menue-content" id="info">
                        <b style="font-size: 25px;">Info</b>

                        <div class="row">
                            <div class="col-sm-4 div-price-padding-0">

                                <p class="title-car-info-detail">Car Category- size,cost, power & luxury factor</p>
                                <div class="info-div-car">
                                    <select name="car_category" class="select-car-info">
                                        <option value="M" {{($car->car_category == 'M')? 'selected' : ''}}>M-Mlni
                                        </option>
                                        <option value="N" {{($car->car_category == 'N')? 'selected' : ''}}>N-Mini Elite
                                        </option>
                                        <option value="E" {{($car->car_category == 'E')? 'selected' : ''}}>E-Econamy
                                        </option>
                                        <option value="C" {{($car->car_category == 'C')? 'selected' : ''}}>C-Compact
                                        </option>
                                        <option value="I" {{($car->car_category == 'I')? 'selected' : ''}}>
                                            I-Intermediate
                                        </option>
                                        <option value="S" {{($car->car_category == 'S')? 'selected' : ''}}>S-Standard
                                        </option>
                                        <option value="F" {{($car->car_category == 'F')? 'selected' : ''}}>F-Full-size
                                        </option>
                                        <option value="P" {{($car->car_category == 'P')? 'selected' : ''}}>P-Premium
                                        </option>
                                        <option value="X" {{($car->car_category == 'X')? 'selected' : ''}}>X-Special
                                        </option>
                                    </select>
                                </div>
                                <p class="title-car-info-detail">Doors & vehicle type</p>
                                <div class="info-div-car">
                                    <select name="doors_vehicle_type" class="select-car-info">
                                        <option value="B" {{($car->doors_vehicle_type == 'B')? 'selected' : ''}}>B-2/3
                                            door
                                        </option>
                                        <option value="C" {{($car->doors_vehicle_type == 'C')? 'selected' : ''}}>C-2/4
                                            door
                                        </option>
                                        <option value="D" {{($car->doors_vehicle_type == 'D')? 'selected' : ''}}>D-2/4
                                            Sdoor
                                        </option>
                                        <option value="W" {{($car->doors_vehicle_type == 'W')? 'selected' : ''}}>
                                            W-Wagon/Estate
                                        </option>
                                        <option value="S" {{($car->doors_vehicle_type == 'S')? 'selected' : ''}}>
                                            S-Sport
                                        </option>
                                        <option value="T" {{($car->doors_vehicle_type == 'T')? 'selected' : ''}}>
                                            T-Convertible
                                        </option>
                                        <option value="F" {{($car->doors_vehicle_type == 'F')? 'selected' : ''}}>F-SUV
                                        </option>
                                        <option value="E" {{($car->doors_vehicle_type == 'E')? 'selected' : ''}}>
                                            E-Coupe
                                        </option>
                                        <option value="N" {{($car->doors_vehicle_type == 'N')? 'selected' : ''}}>
                                            N-Roadster
                                        </option>
                                    </select>
                                </div>
                                <p class="title-car-info-detail">Transmission & drive</p>
                                <div class="info-div-car">
                                    <select name="transmission_drive" class="select-car-info">
                                        <option value="M" {{($car->transmission_drive == 'M')? 'selected' : ''}}>
                                            M-Manual
                                            drive
                                        </option>
                                        <option value="N" {{($car->transmission_drive == 'N')? 'selected' : ''}}>
                                            N-Manual, 4WD
                                        </option>
                                        <option value="C" {{($car->transmission_drive == 'C')? 'selected' : ''}}>
                                            C-Manual, AWD
                                        </option>
                                        <option value="A" {{($car->transmission_drive == 'A')? 'selected' : ''}}>Auto
                                            drive
                                        </option>
                                        <option value="B" {{($car->transmission_drive == 'B')? 'selected' : ''}}>B-Auto,
                                            4WD
                                        </option>
                                        <option value="D" {{($car->transmission_drive == 'D')? 'selected' : ''}}>
                                            D-Auto,AWD
                                        </option>
                                    </select>
                                </div>
                                <p class="title-car-info-detail">Fuel & A/C</p>
                                <div class="info-div-car">
                                    <select name="fuel" class="select-car-info">
                                        <option value="N" {{($car->fuel == 'N')? 'selected' : ''}}>Unspecifled, no A/C
                                        </option>
                                        <option value="R" {{($car->fuel == 'R')? 'selected' : ''}}>Unspecifled, fuel
                                            A/C
                                        </option>
                                        <option value="D" {{($car->fuel == 'D')? 'selected' : ''}}>Diesel, A/C</option>
                                        <option value="Q" {{($car->fuel == 'Q')? 'selected' : ''}}>Diesel, no A/C
                                        </option>
                                        <option value="H" {{($car->fuel == 'H')? 'selected' : ''}}>Hybrid, A/C</option>
                                        <option value="I" {{($car->fuel == 'I')? 'selected' : ''}}>Hybrid, no A/C
                                        </option>
                                        <option value="E" {{($car->fuel == 'E')? 'selected' : ''}}>Electric A/C</option>
                                        <option value="C" {{($car->fuel == 'C')? 'selected' : ''}}>Electric, no A/C
                                        </option>
                                        <option value="V" {{($car->fuel == 'V')? 'selected' : ''}}>Petrol, A/C</option>
                                        <option value="Z" {{($car->fuel == 'Z')? 'selected' : ''}}>Petrol, no A/C
                                        </option>
                                    </select>

                                </div>

                            </div>
                            <div class="col-sm-4 div-price-padding-0">
                                <div class="car-info-title-parametr">
                                    <div>
                                        <p class="title-car-info-detail transmision-p-car">Winter wheel</p>
                                        <select name="winter_wheel" id="" class="select-car-info-2">
                                            <option value="yes" {{($car->winter_wheel == 'yes')? 'selected' : ''}}>Yes
                                            </option>
                                            <option value="no" {{($car->winter_wheel == 'no')? 'selected' : ''}}>no
                                            </option>
                                        </select>
                                    </div>
                                    <div >
                                        <div>
                                            <p class="title-car-info-detail transmision-p-car">Transmission</p>
                                            <select name="transmission" id="" class="select-car-info-2">
                                                <option value="Auto" {{($car->transmission == 'auto')? 'selected' : ''}}>
                                                    Auto
                                                </option>
                                                <option value="Manual" {{($car->transmission == 'manual')? 'selected' : ''}}>
                                                    Manual
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div >
                                        <div>
                                            <p class="title-car-info-detail transmision-p-car">Conditioner</p>
                                        </div>
                                        <select name="conditioner" id="" class="select-car-info-2">
                                            <option value="yes" {{($car->conditioner == 'yes')? 'selected' : ''}}>Yes
                                            </option>
                                            <option value="no" {{($car->conditioner == 'no')? 'selected' : ''}}>no
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div style="display: inline-block">

                                    <div>

                                    </div>
                                    <div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 div-price-padding-0">

                            </div>
                            <div class="col-sm-2 div-price-padding-0">

                            </div>
                        </div>
                    </div>
                    {{--end info--}}
                    {{--rezervation content--}}
                    <div class="tab-pane menue-content " id="reservation">
                        <b style="font-size: 25px;">Reservations</b>

                        <table class="table-car-rezervation table table-hover table-striped table-bordered">
                            <tr class="tr-car-rezervation">
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Viber</th>
                                <th>whatsap</th>
                                <th>Telegram</th>
                                <th>Pick-up date and time</th>
                                <th>Return date and time</th>
                                <th>Pick-up locations</th>
                                <th> Return locations</th>
                                <th>Status</th>

                            </tr>
                            @foreach($car->reservations as $reservations)
                                <tr class="tr-td-car-table-reservation">

                                    <td>{{$reservations->first_name}}</td>
                                    <td>{{$reservations->last_name}}</td>
                                    <td>{{$reservations->email}}</td>
                                    <td>{{$reservations->phone}}</td>
                                    <td>{{$reservations->viber}}</td>
                                    <td>{{$reservations->whatsap}}</td>
                                    <td>{{$reservations->telegram}}</td>
                                    <td>{{$reservations->pick_up_date}} {{$reservations->pick_up_time}}</td>
                                    <td>{{$reservations->return_date}} {{$reservations->return_time}}</td>
                                    <td>{{$reservations->pick_up_locations}}</td>
                                    <td>{{$reservations->return_locations}}</td>
                                    <td>{{$reservations->status}}</td>
                                </tr>

                            @endforeach

                        </table>
                    </div>
                    {{--calendar content--}}
                    <div class="tab-pane menue-content " id="calendar">
                        <b style="font-size: 25px;">Calendar</b>
                        <div class="calendar-car"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <script>
        var reservationTimeArray = [];
        reservationTimeArray = <?php
        if ($reservationTimeArray) {
            echo json_encode($reservationTimeArray);
        } else {
            echo 1;
        }
        ?>;

        $('.calendar-car').calendar({
            startYear: new Date().getFullYear(),
            showHeaders: false,
            language: 'en', // or 'fr'
            allowOverlap: true,
            displayWeekNumber: false,
            displayDisabledDataSource: true,
            displayHeader: true,
            alwaysHalfDay: false,
            dataSource: [], // an array of data
            style: 'border',
            enableRangeSelection: true,
            disabledDays: [],
            disabledWeekDays: [],
            hiddenWeekDays: [],
            roundRangeLimits: false,
            contextMenuItems: [], // an array of menu items,
            customDayRenderer: function (element, date) {

                for (var i = 0; i < reservationTimeArray.length; i++) {
                    var n = new Date(reservationTimeArray[i]).getTime()
                    if (date.getTime() == n) {
                        $(element).css('border', '2px solid white');
                        $(element).css('background-color', 'red');
                        $(element).attr('old-status', 'red');
                    }
                }

            },

        });


        $(window).on('load', function () {
            $('.menu-item-car').addClass('active-item')
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.save_item').on('click', function () {

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var form = document.getElementById('mainForm');
            // var test = $('#mainForm').serializeArray();
            var formData = new FormData(form);
            // console.log(test);

            $.ajax({
                url: '/admin/car/update',
                type: 'POST',
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {

                    $('.error-update').css('display', 'none')
                    $('.success-update').text('')

                    $('.success-update').css('display', 'block')
                    $('.success-update').text('Car Saved successfully')

                },
                error: function (errors) {
                    $('.success-update').css('display', 'none');
                    $('.error-update').css('display', 'none');
                    $('.error-update').text('');

                    var error = errors.responseJSON;

                    $('.error-update').css('display', 'block')
                    $.each(error.errors, function (index, value) {

                        $('.error-update').append(value[0] + '</br>')
                    })


                }
            })

        })
    </script>

@endsection