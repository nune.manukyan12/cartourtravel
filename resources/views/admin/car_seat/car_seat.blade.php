@extends('admin.layouts.app')

@section('page-header')

    <h1>
        <i class="fa fa-sticky-note-o text-normal"></i> &nbsp;{{ trans('Car Seat')}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="{{asset('admin/car-seat/create')}}" class="btn btn-success update"><i
                    class="fa fa-plus-circle"></i> Add</a>
    </h1>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')

    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped table-bordered">

            <tr>
                <th>ID</th>
                <th>{{ trans('car.name')}}</th>
                <th>Child's age</th>
                <th>Daily price</th>
                <th>Action</th>

            </tr>
            @foreach($carSeats as $seat)
                <tr id="{{'car-seat-'.$seat->id}}">
                    <td>
                        {{$seat->id}}
                    </td>
                    <td>
                        {{$seat->name}}
                    </td>
                    <td class="">
                        {{$seat->child_age}}
                    </td>
                    <td>
                        {{$seat->car_seat_price}}
                    </td>
                    <td>
                        <?php $user = \Auth::user(); ?>
                        @if($user->type == \App\User::SUPER_ADMIN)
                            <button type="button" class="btn btn-success approve-item" data-id="{{$seat->id}}">Approve
                            </button>
                        @endif
                        <button class="btn btn-xs btn-danger delete" title="Delete"
                                data-id="{{$seat->id}}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        <a href="{{asset('admin/car-seat/show/'.$seat->id)}}" class="btn btn-xs btn-warning"
                           title="Edit"
                           value="{{$seat->id}}"><i class="fa fa-edit" aria-hidden="true"></i></a>


                    </td>

                </tr>
            @endforeach
        </table>
    </div>
    <script>
        $(window).on('load', function () {
            $('.menu-item-car-seat').addClass('active-item')
        });

        $('.approve-item').on('click', function () {
            var id = $(this).attr('data-id');
            var tt = $('#status-' + id).attr('value');
            console.log(tt);
            if (tt == 2) {
                alert('This item already Active.');
            } else {
                if (confirm('Do you want to approve this item?')) {
                    // var id = $(this).attr('data-id');
                    $('#status-' + id).empty();
                    $('#status-' + id).html("Active");

                    console.log(tt);
                    $.ajax({
                        url: '/admin/car/approval/' + id,
                        type: 'GET',
                        success: function (result) {
                            // $('#car-' + id).remove();

                        },
                        error: function (errors) {

                        }
                    })
                }
            }
        });

        $('.delete').on('click', function () {
            if (confirm('Do you want to delete this item?')) {
                var id = $(this).attr('data-id')
                $.ajax({
                    url: '/admin/car-seat/delete/' + id,
                    type: 'GET',
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},

                    success: function (result) {
                        $('#car-seat-' + id).remove();

                    },
                    error: function (errors) {


                    }
                })
            }
        })
    </script>
@endsection
