@extends('admin.layouts.app')

@section('page-header')
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish success-update alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px; margin-bottom: 10px">
        <div class="col-md-10 col-md-offset faild-publish error-update alert-danger" style="display:none; padding-top: 10px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    {!! Form::open(['files'=>true, 'id' => 'mainForm','enctype'=>'multipart/form-data']) !!}
    <div class="row">

        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li tab="car" class="menue-tab active"><a href="#car" data-toggle="tab">General</a></li>
                    <li tab="pictures" class="menue-tab"><a href="#pictures" data-toggle="tab">Pictures</a></li>
                    <li tab="info" class="menue-tab"><a href="#Reservations" data-toggle="tab">Reservations</a></li>

                    <li>
                        <button type="button" class="btn btn-success save_car">Save</button>
                    </li>
                </ul>

                <div class="tab-content">

                    {{--Car contet--}}
                    <div class="tab-pane menue-content active" id="car">
                        <div class="form-group">
                            <b style="font-size: 25px;">Car Seat</b>
                            <input type="hidden" name="lang_count" value="1" id="lang_count">
                        </div>
                        <div class="row">

                            <div class="col-md-3 div-price-padding-0">
                                <img style="height: 300px;width: 100%"
                                     src="{{asset('/uploads/'.$carSeat->general_pic)}}"
                                     id="blah" class="image">

                                <div class="col-md-12" style="margin: 10px 0px">
                                <span class="btn btn-success btn-file">
                                Browse For General Picture<input type="file" onchange="readURL(this);"
                                                                 name="general_pic">
                                </span>
                                </div>
                            </div>
                            <div class="col-md- div-price-padding-0">
                                <div class="car-car-parametrs-div">

                                    <div style="margin-top: 8px">
                                        <label> Child's age</label>
                                    </div>
                                    <div style="margin-top: 8px">
                                        <label>Daily price</label><sub class="car-price-sup">*</sub>
                                    </div>
                                </div>
                                <div class="car-info-div-by-input">

                                    <div class="car-car-detail">
                                        <input type="text" name="child_age" class="car-detail-parametr"
                                               value="{{$carSeat->child_age}}">
                                    </div>
                                    <div class="car-car-detail">
                                        <input type="text" class="car-detail-parametr" name="car_seat_price"
                                               value="{{$carSeat->car_seat_price}}">
                                        <select name="currency" id="car-currency-select">
                                            <option value="usd">USD</option>
                                            <option value="euro">EURO</option>
                                            <option value="amd" selected>AMD</option>
                                            <option value="rub">RUB</option>
                                        </select>
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-3 div-price-padding-0">


                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-9" id="left-content-event-tab">
                                <!-- Tab panes -->
                                <div class="tab-content" id="event_tab_content">
                                    <div class="tab-pane active" id="event1-r">                     <!-- Event 1 -->
                                        <div class="form-group">
                                            <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                    type="text"
                                                    class="form-control default-name"
                                                    name="name"
                                                    id="name" value="{{$carSeat->name}}">
                                            <input type="hidden" name="id" value="{{$carSeat->id}}">
                                        </div>

                                        <div class="form-group">
                                        <textarea name="description" class="editor default-desc" id="translate"
                                                  style="width: 100%;height: 200px;">{{$carSeat->description}}</textarea>
                                            <style>
                                                .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                    height: 200px;
                                                }
                                            </style>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- pictures content -->
                    <div class="tab-pane menue-content" id="pictures">

                        <div class="form-group">
                            <b style="font-size: 25px;">Pictures</b>
                        </div>
                        <div class="alert alert-notice">
                            Files maximum size is 10Mb
                            <span class="close">x</span>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Add photos</div>

                                    <div class="panel-body">
                                        <form></form>
                                        {!! Form::open(['url'=>'admin/image-upload','files'=>true ,'enctype'=>'multipart/form-data','class'=>'dropzone','id'=>'myImage','name'=>'image']) !!}

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <script>
                                    Dropzone.options.imageUpload = {
                                        addRemoveLinks: true,
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                        dictRemoveFile: "Remove image",
                                        dictResponseError: 'Error while uploading file!',
                                    };
                                </script>

                            </div>
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Photos</div>

                                    <div class="panel-body">
                                        @foreach($carSeat->picture as $image)
                                        <div id="pic_container{{$image->id}}" class="pic_container">
                                        <img src="{{asset('/uploads/'.$image->name)}}" width="200px">
                                        <button type="button"
                                        class="btn btn-danger btn-sm delete_button picture-delete-btn"
                                        style="border-radius: 50%"
                                        title="Delete photo" data-id="{{$image->id}}" model="car">X
                                        </button>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    {{--rezervation content--}}
                    <div class="tab-pane menue-content " id="Reservations">
                        <b style="font-size: 25px;">Reservations</b>

                        <table class="table-car-rezervation table table-hover table-striped table-bordered">
                            <tr class="tr-car-rezervation">
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Viber</th>
                                <th>whatsap</th>
                                <th>Telegram</th>
                                <th>Pick-up date and time</th>
                                <th>Return date and time</th>
                                <th>Pick-up locations</th>
                                <th> Return locations</th>
                                <th>Status</th>

                            </tr>
                            {{--@foreach($carSeat->reservations as $reservations)--}}
                            {{--<tr class="tr-td-car-table-reservation">--}}

                            {{--<td>{{$reservations->first_name}}</td>--}}
                            {{--<td>{{$reservations->last_name}}</td>--}}
                            {{--<td>{{$reservations->email}}</td>--}}
                            {{--<td>{{$reservations->phone}}</td>--}}
                            {{--<td>{{$reservations->viber}}</td>--}}
                            {{--<td>{{$reservations->whatsap}}</td>--}}
                            {{--<td>{{$reservations->telegram}}</td>--}}
                            {{--<td>{{$reservations->pick_up_date}} {{$reservations->pick_up_time}}</td>--}}
                            {{--<td>{{$reservations->return_date}} {{$reservations->return_time}}</td>--}}
                            {{--<td>{{$reservations->pick_up_locations}}</td>--}}
                            {{--<td>{{$reservations->return_locations}}</td>--}}
                            {{--<td>{{$reservations->status}}</td>--}}
                            {{--</tr>--}}

                            {{--@endforeach--}}

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <script>



        $(window).on('load', function () {
            $('.menu-item-car-seat').addClass('active-item')
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.save_car').on('click', function () {

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var form = document.getElementById('mainForm');
            // var test = $('#mainForm').serializeArray();
            var formData = new FormData(form);

            $.ajax({
                url: '/admin/car-seat/update',
                type: 'POST',
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $('.error-update').css('display', 'none')
                    $('.success-update').text('')
                    $('.success-update').css('display', 'block')
                    $('.success-update').text('Car seat saved successfully')

                },
                error: function (errors) {
                    $('.success-update').css('display', 'none');
                    $('.error-update').css('display', 'none');
                    $('.error-update').text('');
                    var error = errors.responseJSON;
                    $('.error-update').css('display', 'block')
                    $.each(error.errors, function (index, value) {
                        $('.error-update').append(value[0] + '</br>')
                    })


                }
            })

        })
    </script>

@endsection