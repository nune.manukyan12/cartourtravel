@extends('admin.layouts.app')

@section('page-header')
    <h1>
        <small>Dashboard</small>
    </h1>
@endsection

@section('content')

        <style>
            @media (max-width: 1650px) {
                a.small-box-footer {
                    min-height: 45px;
                }
            }
        </style>
        <div class="box box-success">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                       Adminstrator page
                    </div>
                </div>
            </div>
        </div>

@endsection