@extends('admin.layouts.app')

@section('page-header')
    <h1>
        <i class="fa fa-sticky-note-o text-normal"></i>Hotels&nbsp;&nbsp;&nbsp;
        <a href="{{asset('admin/hotel/create')}}" class="btn btn-success update"><i
                    class="fa fa-plus-circle"></i> {{ trans('car.create')}}</a>
    </h1>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped table-bordered">

            <tr>
                <th>ID</th>
                <th>{{ trans('car.name')}}</th>
                <th>city</th>
                <th>country</th>
                <th>{{ trans('car.status')}}</th>
                <th>Action</th>


            </tr>
            @foreach($hotels as $hotel)
                <tr id="{{'hotel-'.$hotel->id}}">
                    <td>
                        {{$hotel->id}}
                    </td>
                    <td>
                        {{$hotel->name}}
                    </td>
                    <td class="">
                        {{$hotel->city}}
                    </td>
                    <td>
                        {{$hotel->country}}
                    </td>
                    <td id="status-{{$hotel->id}}" value="{{$hotel->status}}">
                        @if($hotel->status == \App\Models\Car::STATUS_PENDING)
                            Pending
                        @else
                            Active
                        @endif
                    </td>
                    <td>
                        <?php $user = \Auth::user(); ?>
                        @if($user->type == \App\User::SUPER_ADMIN)
                            <button type="button" class="btn btn-success approve-item" data-id="{{$hotel->id}}">Approve</button>
                        @endif

                        <button class="btn btn-xs btn-danger delete" title="Delete"
                                data-id="{{$hotel->id}}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        <a href="{{asset('admin/hotel/show/'.$hotel->id)}}" class="btn btn-xs btn-warning" title="Edit"
                                value="{{$hotel->id}}"><i class="fa fa-edit" aria-hidden="true"></i></a>


                    </td>

                </tr>
            @endforeach
        </table>
    </div>
    <script>
        $(window).on('load',function () {
            $('.menu-item-hotel').addClass('active-item')
        });


        $('.approve-item').on('click', function () {
            var id = $(this).attr('data-id');
            var tt= $('#status-'+ id).attr('value');
            console.log(tt);
            if (tt == 2) {
                alert('This item already Active.');
            }else {
                if (confirm('Do you want to approve this item?')) {
                    // var id = $(this).attr('data-id');
                    $('#status-'+ id).empty();
                    $('#status-'+ id).html("Active");

                    console.log(tt);
                    $.ajax({
                        url: '/admin/hotel/approval/' + id,
                        type: 'GET',
                        success: function (result) {
                            // $('#car-' + id).remove();

                        },
                        error: function (errors) {


                        }
                    })
                }
            }
        });











        $('.delete').on('click', function () {
            if(confirm('Do you want to delete this item?')) {
                var id = $(this).attr('data-id')
                $.ajax({
                    url: '/admin/hotel/delete/' + id,
                    type: 'GET',
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
//                data: formData,
//                cache: false,
//                contentType: false,
//                processData: false,
                    success: function (result) {
                        $('#hotel-' + id).remove();

                    },
                    error: function (errors) {


                    }
                })
            }
        })
    </script>
@endsection