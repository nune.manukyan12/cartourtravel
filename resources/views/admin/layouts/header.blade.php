<header class="main-header-admin">

    <a href="{{ asset('/admin/dashboard')}}" class="logo">
        <span class="logo-mini">

        </span>

        <span class="logo-lg">

        </span>
    </a>

    <nav class="navbar navbar-expand-lg navbar-light">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            {{--<span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>--}}
        </a>
        @if(\Illuminate\Support\Facades\Auth::user())
            <div class="navbar-custom-menu" style="position: absolute;right: 85px;">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs">{{  \Illuminate\Support\Facades\Auth::user()->name }}</span>
                        </a>

                        <ul class="dropdown-menu">
                            <li class="">
                                <div class="">
                                    <a href="{!! route('auth.logout') !!}" class="btn btn-danger btn-flat">
                                        <i class="fa fa-sign-out"></i>
                                        {{ trans('admin.logout') }}
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-custom-menu -->
        @endif
    </nav>
</header>
