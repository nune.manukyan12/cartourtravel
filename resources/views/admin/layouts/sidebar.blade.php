<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
            </div>
            <div class="pull-left info">
                <a href="#"><i class="fa fa-circle text-success"></i>
                    {{\Auth::user()->name}}
                </a>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="menu-item-apartment">
                <a href="{{asset('/admin/apartment')}}">
                    <i class="fa fa-home text-normal"></i>
                    <span>Apartment</span>
                </a>
            </li>

            <li class="menu-item-car">
                <a href="{{ asset('/admin/car') }}">
                    <i class="fa fa-car" aria-hidden="true"></i>
                    <span>Cars</span>
                </a>
            </li>
            <li class="menu-item-car-seat">
                <a href="{{ asset('/admin/car-seat') }}">
                    <img src="{{ asset("img/car-seat.png")}}" alt="" style="width: 19px; height: 16px;">
                    <span>Cars seat</span>
                </a>
            </li>
            <li class="menu-item-transfer">
                <a href="{{ asset('/admin/transfer') }}">
                    <i class="fa fa-bus" aria-hidden="true"></i>
                    <span>Transfer</span>
                </a>
            </li>
            <li class="menu-item-chauffeur">
                <a href="{{ asset('/admin/chauffeur') }}">
                    <i class="fa fa-bus" aria-hidden="true"></i>
                    <span>Chauffeur Service</span>
                </a>
            </li>
            <li class="menu-item-hotel">
                <a href="{{ asset('/admin/hotel') }}">
                    <i class="fa fa-users text-normal"></i>
                    <span>Hotels</span>
                </a>

            </li>

            <li class="menu-item-tour">
                <a href="#" class="menu-link-tour">
                    <i class="fa fa-globe" aria-hidden="true"></i>
                    <span>Tour</span>
                </a>
            </li>
            <li class="menu-tour-child menu-tour-child-tour" style="display: none">
                <a href="{{ asset('/admin/tour') }}">
                    {{--<i class="fa fa-globe" aria-hidden="true"></i>--}}
                    <span class="span-tour-child"> Tours</span>
                </a>
            </li>
            <li class="menu-tour-child menu-tour-child-package" style="display: none">
                <a href="{{ asset('/admin/tour/package') }}">
                    {{--<i class="fa fa-globe" aria-hidden="true"></i>--}}
                    <span class="span-tour-child"> Tour Package</span>
                </a>
            </li>

            <li class="menu-item-slider">
                <a href="{{asset('/admin/slider')}}">
                    <i class="far fa-file-image text-normal"></i>
                    <span>Slider Photo</span>
                </a>
            </li>
            {{--@role('Super admin')--}}
            <li class="menu-item-user">
                <a href="{{asset('/admin/users')}}">
                    <i class="fa fa-users text-normal"></i>
                    <span>Users</span>
                </a>
            </li>
            <li class="menu-item-user">
                <a href="{{asset('/admin/about')}}">
                    <i class="fa fa-users text-normal"></i>
                    <span>About</span>
                </a>
            </li>
            <li class="menu-item-user">
                <a href="{{asset('/admin/contact')}}">
                    <i class="fa fa-users text-normal"></i>
                    <span>Contact us</span>
                </a>
            </li>
            <li class="menu-item-news">
                <a href="{{asset('/admin/news')}}">
                    <i class="fa fa-newspaper text-normal"></i>
                    <span>News Feed</span>
                </a>
            </li>
            <li class="menu-item-medical">
                <a href="{{asset('/admin/medical')}}">
                    <i class="fa fa-user-md text-normal"></i>
                    <span>Medical Tourism</span>
                </a>
            </li>
            <li class="menu-item-tourism">
                <a href="{{asset('/admin/tourism')}}">
                    <i class="fa fa-user-md text-normal"></i>
                    <span>Tourism Tourism</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
<script>
    var filtrNumber = 2;
    $('.menu-link-tour').click(function () {
        if (filtrNumber % 2 == 0) {
            $('.menu-tour-child').show();
        } else {
            $('.menu-tour-child').hide();
        }
        filtrNumber++;
    });
</script>