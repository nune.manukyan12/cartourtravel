@extends('admin.layouts.app')

@section('page-header')
    <h1>
        <i class="fa fa-sticky-note-o text-normal"></i>Medical Service&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="{{asset('admin/medical/create')}}" class="btn btn-success update"><i
                    class="fa fa-plus-circle"></i> {{ trans('car.create')}}</a>
    </h1>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    <div class="box-body table-responsive no-paddin
    g">
        <table class="table table-hover table-striped table-bordered">

            <tr>
                <th>ID</th>
                <th>{{ trans('car.name')}}</th>
                <th>Action</th>

            </tr>
            @foreach($medicals as $medical)
                <tr id="{{'medical-'.$medical->id}}">
                    <td>
                        {{$medical->id}}
                    </td>
                    <td>
                        {{$medical->name}}
                    </td>
                    <td>
                        <?php $user = \Auth::user(); ?>
                        <button class="btn btn-xs btn-danger delete" title="Delete"
                                data-id="{{$medical->id}}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        <a href="{{asset('admin/medical/show/'.$medical->id)}}" class="btn btn-xs btn-warning" title="Edit"
                           value="{{$medical->id}}"><i class="fa fa-edit" aria-hidden="true"></i></a>


                    </td>

                </tr>
            @endforeach
        </table>
    </div>
    <script>
        $(window).on('load', function () {
            $('.menu-item-medical').addClass('active-item');
            $('.menu-medical-child').show();
            $('.menu-medical-child-medical').css('background-color','#7e9dbc');
        });

        $('.delete').on('click', function () {
            if(confirm('Do you want to delete this item?')) {
                var id = $(this).attr('data-id')
                $.ajax({
                    url: '/admin/medical/delete/' + id,
                    type: 'GET',
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    success: function (result) {
                        $('#medical-' + id).remove();

                    },
                    error: function (errors) {


                    }
                })
            }
        })


    </script>
@endsection