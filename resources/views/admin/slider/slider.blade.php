@extends('admin.layouts.app')

@section('page-header')
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success success-update" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px; margin-bottom: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger error-update" style="display:none; padding-top: 10px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li tab = "pictures" class="menue-tab active"><a href="#pictures" data-toggle="tab">Pictures</a></li>
                </ul>

                <div class="tab-content">

                    <!-- /.tab-pane -->
                    <div class="tab-pane menue-content active" id="pictures">
                        <div class="alert alert-notice">
                            {{--Files maximum size is 10Mb--}}
                            <span class="close">x</span>
                        </div>
                        <div class="form-group">
                            <b style="font-size: 25px;">Pictures</b>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Add photos</div>

                                    <div class="panel-body">
                                        <form></form>
                                        {!! Form::open(['url'=>'admin/slider/create','files'=>true ,'enctype'=>'multipart/form-data','class'=>'dropzone','id'=>'myImage','name'=>'image']) !!}

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <script>
                                    Dropzone.options.imageUpload = {
                                        addRemoveLinks: true,
                                        acceptedFiles: ".mp4",
                                        dictRemoveFile: "Remove image",
                                        dictResponseError: 'Error while uploading file!',
                                    };
                                </script>

                            </div>
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Photos</div>

                                    <div class="panel-body">
                                        @foreach($slider_Photos as $image)
                                            <div id="pic_container{{$image->id}}" class="pic_container">
                                                <img src="{{asset('/uploads/'.$image->photo)}}" width="200px">
                                                <button type="button"
                                                        class="btn btn-danger btn-sm delete_button picture-delete-btn"
                                                        style="border-radius: 50%"
                                                        title="Delete photo" data-id="{{$image->id}}" model="slider">X
                                                </button>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(window).on('load',function () {
            $('.menu-item-slider').addClass('active-item')
        });


    </script>

@endsection