@extends('admin.layouts.app')

@section('page-header')
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success success-update" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px;margin-bottom: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger error-update" style="display:none; padding-top: 10px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    {!! Form::open(['files'=>true,  'id' => 'mainForm','enctype'=>'multipart/form-data']) !!}
    <div class="row">

        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li tab="car" class="menue-tab active"><a href="#car" data-toggle="tab">General</a></li>
                    <li tab="info" class="menue-tab"><a href="#info" data-toggle="tab">Info</a></li>
                    {{--<li tab="price" class="menue-tab"><a href="#price" data-toggle="tab">Price</a></li>--}}
                    <li tab="pictures" class="menue-tab"><a href="#pictures" data-toggle="tab">Pictures</a></li>
                    <li>
                        <button type="button" class="btn btn-success save_tour">Create Tour</button>
                    </li>
                </ul>

                <div class="tab-content">

                    <div class="tab-pane menue-content active" id="car">
                        <div class="form-group">
                            <b style="font-size: 25px;">New Tour</b> &nbsp;&nbsp;<input type="hidden" name="lang_count"
                                                                                        value="1" id="lang_count">
                        </div>
                        <div class="row">

                            <div class="col-md-3 div-price-padding-0">
                                <div class="col-md-12 div-price-padding-0">
                                    <img style="height: 300px;width: 100%" src="{{asset('/img/no-image.jpg')}}"
                                         id="blah" class="image">
                                </div>
                                <div class="col-md-12" style="margin: 10px 0px">
                                <span class="btn btn-success btn-file">
                                Browse For General Picture<input type="file" onchange="readURL(this);"
                                                                 name="general_pic">
                                </span>
                                </div>
                            </div>
                            <div class="col-md-9 div-price-padding-0">
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Tour type</label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">

                                        {{--<select class=" select-tour-type input-car-general" name="type_id" id="">--}}


                                        <select class=" select-tour-type input-car-general" name="type_id" id="">


                                            @foreach($types as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Country</label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="country" class="input-car-general">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>City</label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="city" class="input-car-general">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Address</label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="address" class="input-car-general">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-9" id="left-content-event-tab">
                                <!-- Tab panes -->
                                <div class="tab-content" id="event_tab_content">
                                    <div class="tab-pane active" id="event1-r">                     <!-- Event 1 -->
                                        <div class="form-group">
                                            <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                    type="text"
                                                    class="form-control default-name"
                                                    name="name"
                                                    id="name">
                                        </div>

                                        <div class="form-group">
                                        <textarea name="description" class="editor default-desc" id="translate"
                                                  style="width: 100%;height: 200px;"></textarea>
                                            <style>
                                                .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                    height: 200px;
                                                }
                                            </style>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>

                    <!-- Price tab -->
                @include('admin._partial._price')
                <!-- End price -->
                    <!-- /.tab-pane -->
                    <div class="tab-pane menue-content" id="pictures">
                        <div class="alert alert-notice">
                            Files maximum size is 10Mb
                            <span class="close">x</span>
                        </div>
                        <div class="form-group">
                            <b style="font-size: 25px;">Pictures</b>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Add photos</div>

                                    <div class="panel-body">
                                        <form></form>
                                        {!! Form::open(['url'=>'admin/image-upload','files'=>true ,'enctype'=>'multipart/form-data','class'=>'dropzone','id'=>'myImage','name'=>'image']) !!}

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <script>
                                    Dropzone.options.imageUpload = {
                                        addRemoveLinks: true,
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                        dictRemoveFile: "Remove image",
                                        dictResponseError: 'Error while uploading file!',
                                    };
                                </script>

                            </div>
                        </div>
                    </div>
                    {{--end Pictures--}}
                    {{--Info--}}
                    <div class="tab-pane menue-content " id="info">
                        <div class="form-group">
                            <b style="font-size: 25px;">Info</b>
                        </div>
                        <div class="row text-center">
                           <div class="col-md-12">
                               <table class="table table-hover table-striped table-bordered table-tour extream-type"
                                      id="table-tour-info"
                                      style="display: none">
                                   <tr>
                                       <th class="text-center text-destination-group"  >
                                           <h4>Tour destination</h4>
                                       </th>
                                   </tr>
                                   <tr class="tour-info-tr">
                                       <td>
                                           <input type="text" class="input-tour-info" name="tour_destination[0]">
                                       </td>
                                   </tr>
                               </table>

                               <table class="table table-hover table-striped table-bordered table-tour group-regular-type"
                                      id="table-tour-info">
                                   <tr>
                                       <th class="text-center text-destination-group"  >
                                           <h4>Tour destination</h4>
                                       </th>
                                       <th class="text-center"  >
                                           <h4>Km</h4>
                                       </th>
                                       <th class="text-center"  >
                                           <h4 >Date</h4>
                                       </th>
                                       <th class="text-center" >
                                           <h4 >O'clock</h4>
                                       </th>
                                       <th class="text-center" >
                                           <h4>Duration</h4>
                                       </th>
                                       <th class="text-center" >
                                           <h4 >Price person</h4>
                                       </th>
                                       <th class="text-center" >
                                           <button type="button" class="btn btn-primary" id="regular_type">+</button>
                                       </th>
                                   </tr>

                                   <tr class="tour-info-tr">
                                       <td>
                                           <input type="text" class="input-tour-info" name="regular_tour_destination[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="regular_km[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="regular_date[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="regular_clock[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="regular_duration[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="regular_price_person[0]">
                                       </td>
                                   </tr>
                               </table>



                               <table class="table table-hover table-striped table-bordered table-tour private-type"
                                      id="table-tour-info"
                                        style="display: none">
                                   <tr>
                                       <th rowspan="2" class="text-center tour-info-th-by-destination">
                                           <h4>Tour destination</h4>
                                       </th>
                                       <th  rowspan="2" class="text-center tour-info-th-by-km">
                                           <h4>Km</h4>
                                       </th>
                                       <th class="text-center tour-info-th-by-km" rowspan="2" >
                                           <h4 class="tour-type-info-title">Duration</h4>
                                           <p>hours</p>
                                       </th>
                                       <th class="text-center" colspan="2" >
                                           <h4 class="tour-type-info-title" >Sedan</h4>
                                           <p>(2-3 persons)</p>
                                       </th>
                                       <th class="text-center" colspan="2">
                                           <h4 class="tour-type-info-title">Minivan</h4>
                                           <p>(4-7 persons)</p>
                                       </th>
                                       <th colspan="2">
                                           <h4 class="tour-type-info-title" >Mini bus</h4>
                                       </th>
                                       <th colspan="2">
                                           <h4 class="tour-type-info-title" >Bus</h4>
                                       </th>
                                       <th class="info-tour-button-th">
                                           <button type="button" class="btn btn-primary" id="tour_info">+</button>
                                       </th>
                                   </tr>
                                   <tr>
                                       <th class="text-center">
                                           <p class="tour-guide-p">Without guide</p>
                                       </th>
                                       <th class="text-center">
                                           <p class="tour-guide-p">With guide</p>
                                       </th>
                                       <th class="text-center">
                                           <p class="tour-guide-p">Without guide</p>
                                       </th>
                                       <th class="text-center">
                                           <p class="tour-guide-p">With guide</p>
                                       </th>
                                       <th class="text-center">
                                           <p class="tour-guide-p">Without guide</p>
                                       </th>
                                       <th class="text-center">
                                           <p class="tour-guide-p">With guide</p>
                                       </th>
                                       <th class="text-center">
                                           <p class="tour-guide-p">Without guide</p>
                                       </th>
                                       <th class="text-center">
                                           <p class="tour-guide-p">With guide</p>
                                       </th>
                                   </tr>
                                   <tr class="tour-info-tr">
                                       <td>
                                           <input type="text" class="input-tour-info" name="tour_destination[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="km[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="duration[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="sedan_price[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="sedan_price_guide[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="minivan_price[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="minivan_price_guide[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="mini_bus[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="mini_bus_guide[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="bus[0]">
                                       </td>
                                       <td>
                                           <input type="text" class="input-tour-info" name="bus_guide[0]">
                                       </td>
                                   </tr>
                               </table>
                           </div>
                        </div>
                    </div>
                    {{--end Info--}}
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        <script>
            var numberTour='';
            $(window).on('load', function () {
                $('.menu-item-tour').addClass('active-item');
                $('.menu-tour-child').show();
                $('.menu-tour-child-tour').css('background-color','#7e9dbc');
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah')
                            .attr('src', e.target.result)
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.save_tour').on('click', function () {

                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                var form = document.getElementById('mainForm');
                var formData = new FormData(form);

                $.ajax({
                    url: '/admin/tour/store',
                    type: 'POST',
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result) {

                        $('.error-update').css('display', 'none');
                        $('.success-update').text('');
                        var href = window.location.href;
                        var test = href.split('/');
                        $('.success-update').css('display', 'block');
                        $('.success-update').text('Tour Saved successfully');
                        setTimeout(function () {
                            window.location.href = test[0] + '/admin/tour/show/'+ result.id;
                        }, 2000)

                    },
                    error: function (errors) {
                        $('.success-update').css('display', 'none');
                        $('.error-update').css('display', 'none');
                        $('.error-update').text('');

                        var error = errors.responseJSON;
                        $('.error-update').css('display', 'block');
                        $.each(error.errors, function (index, value) {

                            $('.error-update').append(value[0] + '</br>')
                        })
                    }
                })
            });

        </script>

@endsection