@extends('admin.layouts.app')

@section('page-header')
    <h1>
        <i class="fa fa-sticky-note-o text-normal"></i>Tour Package
        {{--<a href="{{asset('admin/tour/create')}}" class="btn btn-success update"><i--}}
        {{--class="fa fa-plus-circle"></i> {{ trans('car.create')}}</a>--}}
    </h1>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection




@section('content')
    <div class="box-body table-responsive no-padding">
        <div class="row">
            <div class="col-sm-12">
                <h3>About user</h3>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <p><span class="span-bold">First name: </span> {{$data->first_name}} </p>
                <p><span class="span-bold">Last name: </span> {{$data->last_name}}</p>
                <p><span class="span-bold">Email: </span> {{$data->email}}</p>
                <p><span class="span-bold">Phone:</span> {{$data->phone}}</p>
                <p><span class="span-bold">Viber:</span> {{$data->viber}}</p>
                <p><span class="span-bold">Whatsapp:</span> {{$data->whatsapp}}</p>
            </div>
            <div class="col-sm-7">
                <p><span class="span-bold">Country:</span> {{$data->country}}</p>
                <p><span class="span-bold">City:</span> {{$data->city}}</p>
                <p><span class="span-bold">Address:</span> {{$data->address}}</p>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <h3>About tour</h3>

                @if($data->date_arrival)
                    <p><span class="span-bold">Date arrival: </span> {{$data->date_arrival}}</p>
                @endif
                @if($data->date_departure)
                    <p><span class="span-bold">Date departure: </span> {{$data->date_departure}}</p>
                @endif
                @if($data->adults)
                    <p><span class="span-bold">Adults: </span> {{$data->adults}}</p>
                @endif
                @if($data->kids)
                    <p><span class="span-bold">Kids: </span> {{$data->kids}}</p>
                @endif

            </div>
            <div class="col-sm-7">
                <br><br>
                <br>
                @if($data->people_arrival)
                    <p><span class="span-bold">People arrival: </span> {{$data->people_arrival}}</p>
                @endif
                @if($data->people_departure)
                    <p><span class="span-bold">People departure: </span> {{$data->people_departure}}</p>
                @endif
                <p class="span-bold">Notes:</p>
                <p>{{$data->notes}}</p>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-12">
                <h3>Accommodation</h3>

                @if($apartment)
                    <div class="package-accommodation">
                        <img src="{{asset("uploads/".$apartment->general_pic)}}" style="width: 240px; height: 150px">
                        <div class="div-padding-package">
                            <p>{{$apartment->name}}</p>
                            <p>
                                <span class="span-bold">Address: </span>{{$apartment->country}} {{$apartment->city}} {{$apartment->address}}
                            </p>
                        </div>
                        <p class="div-padding-package">Apartment <a href="{{asset('/apartment/'.$apartment->id)}}" target="_blank"
                                        class="see-link-package">See</a></p>
                    </div>
                @endif
                @if($house)
                    <div class="package-accommodation">
                        <img src="{{asset("uploads/".$house->general_pic)}}" style="width: 240px; height: 150px">
                        <div class="div-padding-package">
                            <p>{{$house->name}}</p>
                            <p>
                                <span class="span-bold">Address: </span>{{$house->country}} {{$house->city}} {{$house->address}}
                            </p>
                            <p class="div-padding-package">House <a href="{{asset('/apartment/'.$house->id)}}" target="_blank"
                                        class="see-link-package">See</a></p>
                        </div>
                    </div>
                @endif
            </div>

        </div>
        <div class="row">

            <div class="col-sm-12">
                <h3>Tour</h3>
                <table class="table table-hover table-striped table-bordered">
                    <tr>
                        <th class="text-center">Destination</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Car</th>
                        <th class="text-center">With guid</th>
                        <th class="text-center">Price</th>
                    </tr>
                    @foreach($tour as $value)


                        @foreach($destinations as $destination)

                            @if($value->type=="minivan_private"&&$destination->id==$value->minivan_private_id)
                                <tr>
                                    <td>{{$destination->destination}}</td>
                                    <td>Private Tour</td>
                                    <td>Minivan</td>
                                    <td>
                                        @if($value->guid==1)
                                            <p class="text-center"><i class="fas fa-check"></i></p>

                                        @endif
                                    </td>
                                    <td>{{$value->guid==1?$destination->minivan_price_guide:$destination->minivan_price}}</td>
                                </tr>
                            @elseif($value->type=="sedan_private"&&$destination->id==$value->sedan_private_id)
                                <tr>
                                    <td>{{$destination->destination}}</td>
                                    <td>Private Tour</td>
                                    <td>Sedan</td>
                                    <td>

                                        @if($value->guid==1)
                                            <p class="text-center"><i class="fas fa-check"></i></p>

                                        @endif
                                    </td>
                                    <td>{{$value->guid==1?$destination->sedan_price_guide:$destination->sedan_price}}</td>
                                </tr>
                            @elseif($value->type=="minibus_private"&&$destination->id==$value->minibus_private_id)
                                <tr>
                                    <td>{{$destination->destination}}</td>
                                    <td>Private Tour</td>
                                    <td>Mini bus</td>
                                    <td>
                                        @if($value->guid==1)
                                            <p class="text-center"><i class="fas fa-check"></i></p>

                                        @endif
                                    </td>
                                    <td>{{$value->guid==1?$destination->minibus_price_guide:$destination->minibus_price}}</td>
                                </tr>
                            @elseif($value->type=="bus_private"&&$destination->id==$value->bus_private_id)
                                <tr>
                                    <td>{{$destination->destination}}</td>
                                    <td>Private Tour</td>
                                    <td>Bus</td>
                                    <td>
                                        @if($value->guid==1)
                                            <p class="text-center"><i class="fas fa-check"></i></p>
                                        @endif
                                    </td>
                                    <td>{{$value->guid==1?$destination->bus_price_guide:$destination->bus_price}}</td>
                                </tr>
                            @elseif($value->type=="group"&&$destination->id==$value->group_id)
                                <tr>
                                    <td>{{$destination->destination}}</td>
                                    <td>Group</td>
                                    <td></td>
                                    <td>

                                    </td>
                                    <td>{{$destination->price_person}}</td>
                                </tr>
                            @endif

                        @endforeach
                    @endforeach
                </table>
            </div>
        </div>


    </div>
    <script>
        $(window).on('load', function () {
            $('.menu-item-tour').addClass('active-item');
            $('.menu-tour-child').show();
            $('.menu-tour-child-package').css('background-color', '#7e9dbc');
        });

    </script>
@endsection