@extends('admin.layouts.app')

@section('page-header')
    <h1>
        <i class="fa fa-sticky-note-o text-normal"></i>Tour Packages&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {{--<a href="{{asset('admin/tour/create')}}" class="btn btn-success update"><i--}}
                    {{--class="fa fa-plus-circle"></i> {{ trans('car.create')}}</a>--}}
    </h1>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped table-bordered">

            <tr >
                <th class="text-center id-package-tour">ID</th>
                <th class="text-center">Name</th>
                <th class="text-center">Email</th>
                <th class="text-center">Phone</th>
                <th class="text-center">Country</th>
                <th class="text-center">City</th>
                <th class="text-center">Address</th>
                <th class="text-center status-package-tour">Status</th>
                <th class="text-center id-package-tour"></th>
            </tr>
            @foreach($packages as $package)
                <tr>
                    <th>{{$package->id}}</th>
                    <th>{{$package->last_name}}{{$package->first_name}}</th>
                    <th>{{$package->email}}</th>
                    <th>{{$package->phone}}</th>
                    <th>{{$package->country}}</th>
                    <th>{{$package->city}}</th>
                    <th>{{$package->address}}</th>
                    <th>{{$package->status}}</th>
                    <th> <a href="{{asset('admin/tour/package/show/'.$package->id)}}"><i class="far fa-eye color-green"></i></a></th>
                </tr>
                @endforeach
        </table>
    </div>
    <script>
        $(window).on('load',function () {
            $('.menu-item-tour').addClass('active-item');
            $('.menu-tour-child').show();
            $('.menu-tour-child-package').css('background-color','#7e9dbc');
        });

    </script>
@endsection