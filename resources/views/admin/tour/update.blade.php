@extends('admin.layouts.app')

@section('page-header')
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success success-update" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px;margin-bottom: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger error-update" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    {!! Form::open(['files'=>true,  'id' => 'mainForm','enctype'=>'multipart/form-data']) !!}
    <div class="row">


        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li tab="car" class="menue-tab active"><a href="#car" data-toggle="tab">General</a></li>
                    <li tab="info" class="menue-tab"><a href="#info" data-toggle="tab">Info</a></li>
                    {{--<li tab="price" class="menue-tab"><a href="#price" data-toggle="tab">Price</a></li>--}}
                    <li tab="pictures" class="menue-tab"><a href="#pictures" data-toggle="tab">Pictures</a></li>
                    <li>
                        <button type="button" class="btn btn-success save_tour">Update Tour</button>
                    </li>
                </ul>

                <div class="tab-content">

                    <?php $prices = $tour->prices;
                    $price_status = $tour->price_status;
                    ?>
                    @include('admin._partial._price_edit',$prices)


                    <div class="tab-pane menue-content active" id="car">
                        <div class="form-group">
                            <b style="font-size: 25px;">Edit Tour</b> &nbsp;
                            <input type="hidden" value="{{$tour->id}}" name="id">
                        </div>
                        <div class="row">

                            <div class="col-md-3 div-price-padding-0">
                                <div class="col-md-12 div-price-padding-0">
                                    <img style="height: 300px;width: 100%"
                                         src="{{asset('/uploads/'.$tour->general_pic)}}" id="blah" class="image">
                                </div>
                                <div class="col-md-12" style="margin: 10px 0px">
                                <span class="btn btn-success btn-file">
                                Browse For General Picture<input type="file" onchange="readURL(this);"
                                                                 name="general_pic">
                                </span>
                                </div>
                            </div>
                            <div class="col-md-9 div-price-padding-0">
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Tour type</label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <select class="select-tour-type input-car-general" name="type_id" id="">

                                            @foreach($types as $type)
                                                <option value="{{$type->id}}" {{($type->id == $tour->type_id)? 'selected' : ''}}>{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Country</label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="input-car-general" type="text" name="country"
                                               value="{{$tour->country}}">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>City</label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="input-car-general" type="text" name="city"
                                               value="{{$tour->city}}">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Address</label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="input-car-general" name="address"
                                               value="{{$tour->address}}">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-9" id="left-content-event-tab">
                                <!-- Tab panes -->
                                <div class="tab-content" id="event_tab_content">
                                    <div class="tab-pane active" id="event1-r">                     <!-- Event 1 -->
                                        <div class="form-group">
                                            <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                    type="text"
                                                    class="form-control default-name"
                                                    name="name"
                                                    id="name" value="{{$tour->name}}">
                                        </div>

                                        <div class="form-group">
                                        <textarea name="description" class="editor default-desc" id="translate"
                                                  style="width: 100%;height: 200px;">{{$tour->description}}</textarea>
                                            <style>
                                                .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                    height: 200px;
                                                }
                                            </style>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>


                    <!-- /.tab-pane -->
                    <div class="tab-pane menue-content" id="pictures">
                        <div class="alert alert-notice">
                            Files maximum size is 10Mb
                            <span class="close">x</span>
                        </div>
                        <div class="form-group">
                            <b style="font-size: 25px;">Pictures</b>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Add photos</div>

                                    <div class="panel-body">
                                        <form></form>
                                        {!! Form::open(['url'=>'admin/image-upload','files'=>true ,'enctype'=>'multipart/form-data','class'=>'dropzone','id'=>'myImage','name'=>'image']) !!}

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <script>
                                    Dropzone.options.imageUpload = {
                                        addRemoveLinks: true,
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                        dictRemoveFile: "Remove image",
                                        dictResponseError: 'Error while uploading file!',
                                    };
                                </script>

                            </div>
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Photos</div>

                                    <div class="panel-body">
                                        @foreach($tour->picture as $image)
                                            <div id="{{$image->id}}" class="pic_container">
                                                <img src="{{asset('/uploads/'.$image->name)}}"
                                                     width="200px">
                                                <button type="button"
                                                        class="btn btn-danger btn-sm delete_button picture-delete-btn"
                                                        style="border-radius: 50%"
                                                        title="Delete photo" data-id="{{$image->id}}" model="tour">X
                                                </button>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    {{--end pictures--}}
                    {{--info--}}
                    <div class="tab-pane menue-content " id="info">
                        <div class="form-group">
                            <b style="font-size: 25px;">Info</b>
                        </div>
                        <div class="row text-center">
                            <div class="col-sm-12">
                                <table class="table table-hover table-striped table-bordered table-tour extream-type"
                                       id="table-tour-info"
                                       style="display: none">
                                    <tr>
                                        <th class="text-center text-destination-group">
                                            <h4>Tour destination</h4>
                                        </th>
                                    </tr>
                                    <tr class="tour-info-tr">
                                        {{--<td>--}}
                                        {{--<input type="text" class="input-tour-info" name="tour_destination[0]">--}}
                                        {{--</td>--}}
                                    </tr>
                                </table>

                                <table class="table table-hover table-striped table-bordered table-tour group-regular-type"
                                       id="table-tour-info"
                                       style="display: none">
                                    <tr>
                                        <th class="text-center text-destination-group">
                                            <h4>Tour destination</h4>
                                        </th>
                                        <th class="text-center">
                                            <h4>Km</h4>
                                        </th>
                                        <th class="text-center">
                                            <h4>Date</h4>
                                        </th>
                                        <th class="text-center">
                                            <h4>O'clock</h4>
                                        </th>
                                        <th class="text-center">
                                            <h4>Duration</h4>
                                        </th>
                                        <th class="text-center">
                                            <h4>Price person</h4>
                                        </th>
                                        <th>Action</th>
                                        <th class="text-center">
                                            <button type="button" class="btn btn-primary" id="regular_type">+</button>
                                        </th>
                                    </tr>

                                    <?php $num = 0; ?>

                                    @if($tour->type_id==\App\Models\Tour::TYPE_REGULAR)

                                        @foreach($tour->destination as $destination )
                                            <input type="hidden" value="{{$destination->id}}" name="destination_id[{{$num}}]">
                                            <tr class="tour-info-tr">
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="regular_tour_destination[{{$num}}]"
                                                           value="{{$destination->destination}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="regular_km[{{$num}}]" value="{{$destination->km}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="regular_date[{{$num}}]" value="{{$destination->date}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="regular_clock[{{$num}}]"
                                                           value="{{$destination->clock}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="regular_duration[{{$num}}]"
                                                           value="{{$destination->duration}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="regular_price_person[{{$num}}]"
                                                           value="{{$destination->price_person}}">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info add-picture-destination"
                                                            data-toggle="modal" data-target="#myModal"
                                                            destination_id="{{$destination->id}}"><i
                                                                class="fa fa-plus"></i>Add
                                                        pictures
                                                    </button>
                                                    <input type="hidden" value="{{$destination->id}}" id="parent_id">

                                                </td>
                                                <td>

                                                    <button type="button" attr="regular"
                                                            class="btn btn-danger tour_info_delete"
                                                            id="{{$destination->id}}"><i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <?php $num++; ?>
                                        @endforeach
                                    @endif

                                </table>

                                <table class="table table-hover table-striped table-bordered table-tour private-type"
                                       {{--id="table-tour-info"--}}
                                       style="display: none">
                                    <tr>
                                        <th class="text-center tour-info-th-by-destination" rowspan="2">
                                            <h4>Tour destination</h4>
                                        </th>
                                        <th class="text-center tour-info-th-by-km" rowspan="2">
                                            <h4>Km</h4>
                                        </th>
                                        <th class="text-center tour-info-th-by-km" rowspan="2">
                                            <h4 class="tour-type-info-title">Duration</h4>
                                            <p>hours</p>
                                        </th>
                                        <th class="text-center tour-info-th-by-sedan" colspan="2">
                                            <h4 class="tour-type-info-title">Sedan</h4>
                                            <p>(2-3 persons)</p>
                                        </th>
                                        <th class="text-center tour-info-th-by-sedan" colspan="2">
                                            <h4 class="tour-type-info-title">Minivan</h4>
                                            <p>(4-7 persons)</p>
                                        </th>
                                        <th class="text-center tour-info-th-by-sedan" colspan="2">
                                            <h4 class="tour-type-info-title">Mini bus</h4>
                                        </th>
                                        <th class="text-center tour-info-th-by-sedan" colspan="2">
                                            <h4 class="tour-type-info-title">Bus</h4>
                                        </th>
                                        <th>Action</th>
                                        <th class="info-tour-button-th">
                                            <button type="button" class="btn btn-primary" id="tour_info">+</button>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">
                                            <p class="tour-guide-p">Without guide</p>
                                        </th>
                                        <th class="text-center">
                                            <p class="tour-guide-p">With guide</p>
                                        </th>
                                        <th class="text-center">
                                            <p class="tour-guide-p">Without guide</p>
                                        </th>
                                        <th class="text-center">
                                            <p class="tour-guide-p">With guide</p>
                                        </th>
                                        <th class="text-center">
                                            <p class="tour-guide-p">Without guide</p>
                                        </th>
                                        <th class="text-center">
                                            <p class="tour-guide-p">With guide</p>
                                        </th>
                                        <th class="text-center">
                                            <p class="tour-guide-p">Without guide</p>
                                        </th>
                                        <th class="text-center">
                                            <p class="tour-guide-p">With guide</p>
                                        </th>
                                    </tr>
                                    @if($tour->type_id==\App\Models\Tour::TYPE_PRIVATE)
                                        <?php $num = 0; ?>
                                        @foreach($tour->destination as $destination )
                                                <input type="hidden" value="{{$destination->id}}" name="destination_id[{{$num}}]">
                                            <tr class="tour-info-tr">
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="tour_destination[{{$num}}]"
                                                           value="{{$destination->destination}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info" name="km[{{$num}}]"
                                                           value="{{$destination->km}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info" name="duration[{{$num}}]"
                                                           value="{{$destination->duration}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="sedan_price[{{$num}}]"
                                                           value="{{$destination->sedan_price}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="sedan_price_guide[{{$num}}]"
                                                           value="{{$destination->sedan_price_guide}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="minivan_price[{{$num}}]"
                                                           value="{{$destination->minivan_price}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="minivan_price_guide[{{$num}}]"
                                                           value="{{$destination->minivan_price_guide}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info" name="mini_bus[{{$num}}]"
                                                           value="{{$destination->mini_bus}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="mini_bus_guide[{{$num}}]"
                                                           value="{{$destination->mini_bus_guide}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info" name="bus[{{$num}}]"
                                                           value="{{$destination->bus}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="input-tour-info"
                                                           name="bus_guide[{{$num}}]"
                                                           value="{{$destination->bus_guide}}">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info add-picture-destination"
                                                            data-toggle="modal" data-target="#myModal"
                                                            destination_id="{{$destination->id}}"><i
                                                                class="fa fa-plus"></i>Add
                                                        pictures
                                                    </button>
                                                    <input type="hidden" value="{{$destination->id}}" id="parent_id">
                                                </td>
                                                <td>
                                                    <button type="button" attr="private"
                                                            class="btn btn-danger tour_info_delete"
                                                            onclick="deleteDestination()" id="{{$destination->id}}"><i
                                                                class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            <?php $num++;  ?>
                                        @endforeach
                                    @endif


                                </table>
                            </div>
                        </div>
                    </div>
                    {{--end info--}}
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add info and pictures</h4>
                        <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="POST">
                            <div class="row">

                                <div class="col-md-12 form-group">
                                    <label for="">Add description</label>
                                    <textarea name="destination_description" class="form-control"
                                              id="destination_description"></textarea>
                                </div>
                            </div>
                            <div class="row exist_pictures">

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="">Add pictures</label>
                                    <div class="uploader__box js-uploader__box l-center-box" id="fine-uploader">


                                        <div class="uploader__contents">

                                            <label class="button button--secondary" for="fileinput">Select Files</label>

                                            <input id="fileinput" class="uploader__file-input" type="file" multiple
                                                   value="Select Files">

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="js-uploader__submit-button uploader__submit-button">Finish</button>--}}
                    {{--</div>--}}
                </div>

            </div>
        </div>
    </div>
    {!! Form::close() !!}
    <script>


        $('.add-picture-destination').on('click', function () {

            var id = $(this).attr('destination_id');
            $.ajax({
                url: '/admin/tour/destination_picture/' + id,
                type: 'GET',
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    var picture = result.destinationPictures;
                    var description =result.description;
                    console.log(result);
                    $('#destination_description').val(description.description);
                    $('.exist_pictures').html('')
                    $.each(picture, function (index, value) {
                        var content = '<div class="col-md-3" style="margin: 3px;height: 70px"><img class="exist_pic" style="height: 70%;" src="/uploads/' + value.picture + '" alt=""></div>'

                        $('.exist_pictures').append(content)
                    });
                },
                error: function (errors) {


                }
            })
            var options = {

                instructionsCopy: 'Drag and Drop, or',

                furtherInstructionsCopy: 'Your can also drop more files, or',

                selectButtonCopy: 'Select Files',

                secondarySelectButtonCopy: 'Select More Files',

                dropZone: $(this),

                fileTypeWhiteList: ['jpg', 'png', 'jpeg', 'gif', 'pdf'],

                badFileTypeMessage: 'Sorry, we\'re unable to accept this type of file.',

                ajaxUrl: '/admin/tour/destination/info',
            };
            $('#fine-uploader').uploader(options);
            $(".js-uploader__submit-button").on('click', function (e) {
                e.preventDefault();

            });
        });

        var numberTour =<?php echo $num; ?>;
        var typeTour = <?php echo $tour->type_id; ?>;

        if (typeTour == 2) {
            $('.group-regular-type').show();
        } else if (typeTour == 3) {
            $('.private-type').show();

        } else {
            $('.extream-type').show();

        }

        $(window).on('load', function () {
            $('.menu-item-tour').addClass('active-item');
            $('.menu-tour-child').show();
            $('.menu-tour-child-tour').css('background-color', '#7e9dbc');
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.save_tour').on('click', function () {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var form = document.getElementById('mainForm');
            var formData = new FormData(form);
            $.ajax({
                url: '/admin/tour/update',
                type: 'POST',
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $('.error-update').css('display', 'none')
                    $('.success-update').text('')
                    $('.success-update').css('display', 'block')
                    $('.success-update').text('Tour Saved successfully')

                },
                error: function (errors) {
                    $('.success-update').css('display', 'none');
                    $('.error-update').css('display', 'none');
                    $('.error-update').text('');
                    var error = errors.responseJSON;
                    $('.error-update').css('display', 'block')
                    $.each(error.errors, function (index, value) {
                        $('.error-update').append(value[0] + '</br>')
                    })


                }
            })

        });

    </script>

@endsection