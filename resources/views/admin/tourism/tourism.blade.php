@extends('admin.layouts.app')

@section('page-header')
    <h1>
        <i class="fa fa-sticky-note-o text-normal"></i>Tourism Service&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="{{asset('admin/tourism/create')}}" class="btn btn-success update"><i
                    class="fa fa-plus-circle"></i> {{ trans('car.create')}}</a>
    </h1>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    <div class="box-body table-responsive no-paddin
    g">
        <table class="table table-hover table-striped table-bordered">

            <tr>
                <th>ID</th>
                <th>{{ trans('car.name')}}</th>
                <th>Action</th>

            </tr>
            @foreach($tourisms as $tourism)
                <tr id="{{'tourism-'.$tourism->id}}">
                    <td>
                        {{$tourism->id}}
                    </td>
                    <td>
                        {{$tourism->name}}
                    </td>
                    <td>
                        <?php $user = \Auth::user(); ?>
                        <button class="btn btn-xs btn-danger delete" title="Delete"
                                data-id="{{$tourism->id}}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        <a href="{{asset('admin/tourism/show/'.$tourism->id)}}" class="btn btn-xs btn-warning" title="Edit"
                           value="{{$tourism->id}}"><i class="fa fa-edit" aria-hidden="true"></i></a>


                    </td>

                </tr>
            @endforeach
        </table>
    </div>
    <script>
        $(window).on('load', function () {
            $('.menu-item-tourism').addClass('active-item');
            $('.menu-tourism-child').show();
            $('.menu-tourism-child-tourism').css('background-color','#7e9dbc');
        });

        $('.delete').on('click', function () {
            if(confirm('Do you want to delete this item?')) {
                var id = $(this).attr('data-id')
                $.ajax({
                    url: '/admin/tourism/delete/' + id,
                    type: 'GET',
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    success: function (result) {
                        $('#tourism-' + id).remove();

                    },
                    error: function (errors) {


                    }
                })
            }
        })


    </script>
@endsection