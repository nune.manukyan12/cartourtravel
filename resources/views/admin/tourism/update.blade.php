@extends('admin.layouts.app')

@section('page-header')
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success success-update" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px;margin-bottom: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger error-update" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    {!! Form::open(['files'=>true,  'id' => 'mainForm','enctype'=>'multipart/form-data']) !!}
    <div class="row">


        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li tab="car" class="menue-tab active"><a href="#car" data-toggle="tab">General</a></li>
                    {{--<li tab="price" class="menue-tab"><a href="#price" data-toggle="tab">Price</a></li>--}}
                    <li>
                        <button type="button" class="btn btn-success save_tourism">Update Tourism</button>
                    </li>
                </ul>

                <div class="tab-content">

                    <div class="tab-pane menue-content active" id="car">
                        <div class="form-group">
                            <b style="font-size: 25px;">Edit Tourism</b> &nbsp;
                            <input type="hidden" value="{{$tourism->id}}" name="id">
                        </div>
                        <div class="row">

                            <div class="col-md-3 div-price-padding-0">
                                <div class="col-md-12 div-price-padding-0">
                                    <img style="height: 300px;width: 100%"
                                         src="{{asset('/img/'.$tourism->img)}}" id="blah" class="image">
                                </div>
                                <div class="col-md-12" style="margin: 10px 0px">
                                <span class="btn btn-success btn-file">
                                Browse For General Picture<input type="file" onchange="readURL(this);"
                                                                 name="general_pic">
                                </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-9" id="left-content-event-tab">
                                <!-- Tab panes -->
                                <div class="tab-content" id="event_tab_content">
                                    <div class="tab-pane active" id="event1-r">                     <!-- Event 1 -->
                                        <div class="form-group">
                                            <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                    type="text"
                                                    class="form-control default-name"
                                                    name="name"
                                                    id="name" value="{{$tourism->name}}">
                                        </div>

                                        <div class="form-group">
                                        <textarea name="description" class="editor default-desc" id="translate"
                                                  style="width: 100%;height: 200px;">{{$tourism->description}}</textarea>
                                            <style>
                                                .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                    height: 200px;
                                                }
                                            </style>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add info and pictures</h4>
                        <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="POST">
                            <div class="row">

                                <div class="col-md-12 form-group">
                                    <label for="">Add description</label>
                                    <textarea name="destination_description" class="form-control"
                                              id="destination_description"></textarea>
                                </div>
                            </div>
                            <div class="row exist_pictures">

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="">Add pictures</label>
                                    <div class="uploader__box js-uploader__box l-center-box" id="fine-uploader">


                                        <div class="uploader__contents">

                                            <label class="button button--secondary" for="fileinput">Select Files</label>

                                            <input id="fileinput" class="uploader__file-input" type="file" multiple
                                                   value="Select Files">

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="js-uploader__submit-button uploader__submit-button">Finish</button>--}}
                    {{--</div>--}}
                </div>

            </div>
        </div>
    </div>
    {!! Form::close() !!}
    <script>


        $(window).on('load', function () {
            $('.menu-item-tourism').addClass('active-item');
            $('.menu-tourism-child').show();
            $('.menu-tourism-child-tourism').css('background-color','#7e9dbc');
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.save_tourism').on('click', function () {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var form = document.getElementById('mainForm');
            var formData = new FormData(form);
            $.ajax({
                url: '/admin/tourism/update',
                type: 'POST',
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $('.error-update').css('display', 'none')
                    $('.success-update').text('')
                    $('.success-update').css('display', 'block')
                    $('.success-update').text('Tour Saved successfully')

                },
                error: function (errors) {
                    $('.success-update').css('display', 'none');
                    $('.error-update').css('display', 'none');
                    $('.error-update').text('');
                    var error = errors.responseJSON;
                    $('.error-update').css('display', 'block')
                    $.each(error.errors, function (index, value) {
                        $('.error-update').append(value[0] + '</br>')
                    })


                }
            })

        });

    </script>

@endsection