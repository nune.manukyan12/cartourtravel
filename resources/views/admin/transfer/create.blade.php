@extends('admin.layouts.app')

@section('page-header')
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px; margin-bottom: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger error-update" style="display:none; padding-top: 10px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    {!! Form::open(['files'=>true,  'id' => 'mainForm','enctype'=>'multipart/form-data']) !!}
    <div class="row">

        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li tab="car" class="menue-tab active"><a href="#car" data-toggle="tab">General</a></li>
                    <li tab="price" class="menue-tab "><a href="#price" data-toggle="tab">Price</a></li>
                    <li tab="pictures" class="menue-tab"><a href="#pictures" data-toggle="tab">Pictures</a></li>
                    <li>
                        <button type="button" class="btn btn-success save_car">Save</button>
                    </li>
                </ul>

                <div class="tab-content">
                    <!-- Price tab -->
                @include('admin._partial._price')
                <!-- End price -->
                    <div class="tab-pane menue-content active" id="car">
                        <div class="form-group">
                            <b style="font-size: 25px;">Transfer</b> &nbsp;&nbsp;<input type="hidden" name="lang_count"
                                                                                   value="1" id="lang_count">
                        </div>
                        <div class="row">

                            <div class="col-md-3 div-price-padding-0">
                                <div class="col-md-12 div-price-padding-0">
                                    <img style="height: 300px;width: 100%" src="{{asset('/img/no-image.jpg')}}"
                                         id="blah" class="image">
                                </div>
                                <div class="col-md-12" style="margin: 10px 0px">
                                <span class="btn btn-success btn-file">
                                Browse For General Picture<input type="file" onchange="readURL(this);"
                                                                 name="general_pic">
                                </span>
                                </div>
                            </div>
                            <div class="col-md-9 div-price-padding-0">

                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12 div-price-padding-0">
                                        <div class="form-group car-car-parametrs-div">
                                            <label>Persons</label>
                                        </div>
                                        <select class="select-car-info-2" name="persons" id="">
                                            @for($i=1; $i<11; $i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-9" id="left-content-event-tab">
                                <!-- Tab panes -->
                                <div class="tab-content" id="event_tab_content">
                                    <div class="tab-pane active" id="event1-r">                     <!-- Event 1 -->
                                        <div class="form-group">
                                            <label for="name"><b style="font-size: 25px;">Name</b></label><input
                                                    type="text"
                                                    class="form-control default-name"
                                                    name="name"
                                                    id="name">
                                        </div>

                                        <div class="form-group">
                                        <textarea name="description" class="editor default-desc" id="translate"
                                                  style="width: 100%;height: 200px;"></textarea>
                                            <style>
                                                .ck-editor__editable.ck-rounded-corners, .ck-rounded-corners .ck-editor__editable {
                                                    height: 200px;
                                                }
                                            </style>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>


                    <!-- /.tab-pane -->
                    <div class="tab-pane menue-content" id="pictures">
                        <div class="alert alert-notice">
                            Files maximum size is 10Mb
                            <span class="close">x</span>
                        </div>
                        <div class="form-group">
                            <b style="font-size: 25px;">Pictures</b>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading">Add photos</div>

                                    <div class="panel-body">
                                        <form></form>
                                        {!! Form::open(['url'=>'admin/image-upload','files'=>true ,'enctype'=>'multipart/form-data','class'=>'dropzone','id'=>'myImage','name'=>'image']) !!}

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <script>
                                    Dropzone.options.imageUpload = {
                                        addRemoveLinks: true,
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                        dictRemoveFile: "Remove image",
                                        dictResponseError: 'Error while uploading file!',
                                    };
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
    <script>
        $(window).on('load',function () {
            $('.menu-item-transfer').addClass('active-item')
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $('.save_car').on('click', function () {

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var form = document.getElementById('mainForm');
            var formData = new FormData(form);
            console.log(formData);
            $.ajax({
                url: '/admin/transfer/store',
                type: 'POST',
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {

                    $('.error-update').css('display', 'none')
                    $('.success-update').text('')
                    var href = window.location.href;
                    var test =  href.split('/');
                    window.location.href = test[0]+ '/admin/transfer'
                    $('.success-update').css('display', 'block')
                    $('.success-update').text('Car Saved successfully')


                },
                error: function (errors) {
                    $('.success-update').css('display', 'none');
                    $('.error-update').css('display', 'none');
                    $('.error-update').text('');

                    var error = errors.responseJSON;
                    $('.error-update').css('display', 'block');
                    $.each(error.errors, function (index, value) {
                        console.log(index);
                        console.log(value);

                        $('.error-update').append(value[0] + '</br>')
                    })


                }
            })

        })
    </script>

@endsection