@extends('admin.layouts.app')

@section('page-header')
    <h1>
        <i class="fa fa-sticky-note-o text-normal"></i> Transfer&nbsp;&nbsp;
        <a href="{{asset('admin/transfer/create')}}" class="btn btn-success update"><i
                    class="fa fa-plus-circle"></i> {{ trans('car.create')}}</a>
    </h1>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped table-bordered">

            <tr>
                <th>ID</th>
                <th>{{ trans('car.name')}}</th>
                <th>{{ trans('car.person')}}</th>
                <th>{{ trans('car.winter_wheel')}}</th>
                <th>{{ trans('car.status')}}</th>
                <th>Action</th>

            </tr>
            @foreach($transfers as $transfer)
                <tr id="{{'car-'.$transfer->id}}">
                    <td>
                        {{$transfer->id}}
                    </td>
                    <td>
                        {{$transfer->name}}
                    </td>
                    <td class="">
                        {{$transfer->persons}}
                    </td>
                    <td>
                        {{$transfer->winter_wheel}}
                    </td>
                    <td>
                        @if($transfer->status == \App\Models\Car::STATUS_PENDING)
                            Pending
                        @else
                            Active
                        @endif
                    </td>
                    <td>

                        <?php $user = \Auth::user(); ?>
                        @if($user->type == \App\User::SUPER_ADMIN)
                            <button type="button" class="btn btn-success approve-item" data-id="{{$transfer->id}}">Approve</button>
                        @endif
                        <button class="btn btn-xs btn-danger delete" title="Delete"
                                data-id="{{$transfer->id}}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        <a href="{{asset('admin/transfer/show/'.$transfer->id)}}" class="btn btn-xs btn-warning"
                           title="Edit"
                           value="{{$transfer->id}}"><i class="fa fa-edit" aria-hidden="true"></i></a>


                    </td>

                </tr>
            @endforeach
        </table>
    </div>
    <script>
        $(window).on('load',function () {
            $('.menu-item-transfer').addClass('active-item')
        });

        $('.approve-item').on('click', function () {
            if (confirm('Do you want to delete this item?')) {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: '/admin/transfer/approval/' + id,
                    type: 'GET',
                    success: function (result) {
                        // $('#car-' + id).remove();

                    },
                    error: function (errors) {


                    }
                })
            }
        });

        $('.delete').on('click', function () {
            if(confirm('Do you want to delete this item?')) {
                var id = $(this).attr('data-id')
                $.ajax({
                    url: '/admin/transfer/delete/' + id,
                    type: 'GET',
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
//                data: formData,
//                cache: false,
//                contentType: false,
//                processData: false,
                    success: function (result) {
                        $('#car-' + id).remove();

                    },
                    error: function (errors) {


                    }
                })
            }
        })
    </script>
@endsection