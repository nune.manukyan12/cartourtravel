@extends('admin.layouts.app')

@section('page-header')
    <h1>
        <i class="fa fa-sticky-note-o text-normal"></i> Users&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

    </h1>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset success-publish alert-success" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-10 col-md-offset faild-publish alert-danger" style="display:none; padding-top: 10px;height: 40px;
    margin-left: 127px;"></div>
    </div>
@endsection

@section('content')
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped table-bordered">

            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>status</th>
                <th>Action</th>

            </tr>
            @foreach($users as $user)
                <tr id="">
                    <td>
                        {{$user->id}}
                    </td>
                    <td>
                        {{$user->name}}
                    </td>
                    <td class="">
                        {{$user->email}}
                    </td>
                    <td class="status-{{$user->id}}">

                        @if($user->status == \App\User::STATUS_ACTIVE)
                            <p style="color: #06ab66">Active</p>                        @endif
                        @if($user->status == \App\User::STATUS_PENDING)
                            <p style="color: #9b8a30">Pending </p>                            @endif
                        @if($user->status == \App\User::STATUS_DELETED)
                            <p style="color: #990000">Deleted</p>                             @endif
                    </td>
                    <td>

                        <button class="btn btn-xs btn-success activate"
                                data-id="{{$user->id}}">Activate
                        </button>
                        <button class="btn btn-xs btn-warning deactivate" data-id="{{$user->id}}">Deactivate
                        </button>


                    </td>

                </tr>
            @endforeach
        </table>
    </div>
    <script>
        $(window).on('load', function () {
            $('.menu-item-user').addClass('active-item')
        });
        $('.deactivate').on('click', function () {
            if (confirm('Do you want to delete this item?')) {
                var id = $(this).attr('data-id')
                $.ajax({
                    url: '/admin/users/deactivate/' + id,
                    type: 'GET',
//                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    success: function (result) {
                        $('.status-' + id).html('   <p style="color: #990000">Deleted</p>  ');

                    },
                    error: function (errors) {


                    }
                })
            }
        })
        $('.activate').on('click', function () {
            if (confirm('Do you want to activate this user?')) {
                var id = $(this).attr('data-id')
                $.ajax({
                    url: '/admin/users/activate/' + id,
                    type: 'GET',
//                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    success: function (result) {
                        $('.status-' + id).html('  <p style="color: #06ab66">Active</p>    ');
                    },
                    error: function (errors) {


                    }
                })
            }
        })
    </script>
@endsection