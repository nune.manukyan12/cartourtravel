@extends('layouts.app')

@section('content')

    <div class="body-image" >


        <div class="row car-book-general" style="background-image: url({{ asset("img/bg-apartment3.jpg")}});">
            {{--<img src="{{ asset("img/car-back-img.jpg")}} " alt="" style="width:100%;height: 450px;">--}}

        </div>

        {{--<div class="container-fluid big-background-img">--}}
            {{--<img src="{{ asset("img/beg.jpg")}} " alt="Girl in a jacket" style="width:100%;height: 550px;">--}}
        {{--</div>--}}
        <div class="container-fluid">
            <div class="row" style="margin-bottom: 5%">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    {{--                @include('_partial.search_apartment')--}}
                </div>
                {{--<div class="col-md-4">--}}
                {{--<div class="description">--}}
                {{--<p>--}}
                {{--<p>Car Tour Travel agency with reliable partners to give you the best selection of short-term, long-term and luxury rentals. They also provide a specialty rentals such as vans, one-way rentals and chauffeur services.</p>--}}
                {{--<p><br />Wherever you want to go, whatever car you need, we've got the keys. Just enter your destination, pick-up, and drop-off dates and we'll get you all the deals from the top car rental companies in Armenia.</p>--}}
                {{--</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-1">--}}
                {{--</div>--}}
            </div>
            {{--<div class="row">--}}
            {{--<div class="col-md-1">--}}
            {{--</div>--}}
            {{--<div class="col-md-11">--}}
            {{--<article id="page-277">--}}
            {{--<h1>Appartments</h1>--}}
            {{--</article>--}}
            <div class="row">
                <div class="col-lg-3 mb-4">
                    @include('_partial.search_apartment')

                </div>
                <div class="col-lg-9 apartment-general-col-div bg-white" style="margin-left: -4%">
                    @foreach($apartments as $apartment)
                        <div class="row div-by-car-car">
                            <div class="col-sm-12 mb-2">
                                {{--<h2 class="car-name-car">{{($apartment->translateHasOne)? $apartment->translateHasOne->name : ''}}</h2>--}}

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="row p-3">
                                            {{--<img class="img-apartment" src="{{asset('uploads/'.$car->general_pic)}}" alt=""--}}
                                            {{--style="width: 450px; height: 300px;">--}}
                                            {{--<div class="owl-theme owl-height  owl-carousel">--}}
                                            <a href="{{asset('apartment/'.$apartment->id)}}" class="a-no-style">

                                                <div>
                                                    <img class="img-apartment"
                                                         src="{{asset('uploads/'.$apartment->general_pic)}}" alt=""
                                                         style="width: 100%; height: 210px;">
                                                </div>
                                            </a>
                                            {{--@if($car->picture)--}}
                                            {{--@foreach($car->picture as $picture)--}}
                                            {{--<div>--}}
                                            {{--<img src="{{asset('uploads/'.$picture->name)}}"--}}
                                            {{--style="width: 80%; height: 300px;">--}}
                                            {{--</div>--}}
                                            {{--@endforeach--}}
                                            {{--@endif--}}
                                            {{--</div>--}}
                                        </div>
                                        <div class="row div-row-car-detail">
                                            <div class="col-sm-12">
                                                <div class="row div-car-icons">
                                                    @if ($apartment->details[0]->wifi == 'yes')
                                                        <div class="col-2 col-sm-2 div-by-icons-snow text-center pl-0">
                                                            <i class='fa fa-wifi'
                                                               style='    font-size: 16px;color: #51b7f1;'></i>
                                                        </div>
                                                    @endif
                                                    @if ($apartment->details[0]->central_cooling == 'yes' || $apartment->details[0]->air_conditioning == 'yes')
                                                        <div class="col-2 col-sm-2 div-by-icons-snow text-center pl-0">
                                                            <i class='fa fa-snowflake'
                                                               style='font-size: 16px;color: #51b7f1;'></i>
                                                        </div>
                                                    @endif

                                                    <?php $numberBedroom = 1; ?>
                                                    @foreach($bedrooms as $bedroom)
                                                        @if($apartment->details[0]->id == $bedroom->apartment_detail_id)
                                                            @if($bedroom->double!=0)
                                                                <div class="col-2 col-sm-2 div-by-icons-snow text-center pl-0">
                                                                    <p class="p-rooms-class"><span class="color-black-blod">
                                                                        </span>
                                                                        <img src="{{asset("img/queen-bed-blue.png")}}"
                                                                             style="width: 16px; height: 16px"
                                                                             alt="">
                                                                        @if($bedroom->double != 1)
                                                                            <span> +{{$bedroom->double-1}}</span>
                                                                        @endif
                                                                        {{--                                <img src="{{asset("img/single-bed.png")}}" style="width: 20px; height: 20px" alt="">--}}
                                                                    </p>
                                                                </div>
                                                            @endif

                                                            @if($bedroom->queen!=0)
                                                                <div class="col-2 col-sm-2 div-by-icons-snow text-center pl-0">
                                                                    <p class="p-rooms-class"><span
                                                                                class="color-black-blod"></span> <img
                                                                                src="{{asset("img/queen-bed-blue.png")}}"
                                                                                style="width: 16px; height: 16px"
                                                                                alt="">
                                                                        @if($bedroom->queen != 1)
                                                                            <span> +{{$bedroom->queen-1}}</span>
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                            @endif
                                                            @if($bedroom->single!=0)
                                                                <div class="col-2 col-sm-2 div-by-icons-snow text-center pl-0">
                                                                    <p class="p-rooms-class"><span class="color-black-blod">
                                                                        </span>
                                                                        <img src="{{asset("img/single-bed-blue.png")}}"
                                                                             style="width: 16px; height: 16px"
                                                                             alt="">
                                                                        @if($bedroom->single != 1)
                                                                            <span> +{{$bedroom->single-1}}</span>
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                            @endif
                                                            @if($bedroom->sofa_bed!=0)
                                                                <div class="col-2 col-sm-2 div-by-icons-snow text-center pl-0">
                                                                    <p class="p-rooms-class"><span
                                                                                class="color-black-blod"></span> <img
                                                                                src="{{asset("img/sofa-bed-blue.png")}}"
                                                                                style="width: 16px; height: 16px"
                                                                                alt="">
                                                                        @if($bedroom->sofa_bed != 1)
                                                                            <span> +{{$bedroom->sofa_bed-1}}</span>
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                            @endif
                                                            <?php $numberBedroom++; ?>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-8 d-flex align-items-center">
                                        <div class="row">
                                                <div class="col-md-9 pt-3">
                                                    <a href="{{asset('apartment/'.$apartment->id)}}" class="car-offer-front">{{($apartment->translateHasOne)? $apartment->translateHasOne->name : ''}}</a>
                                                {{--<p >{{($apartment->translateHasOne)? $apartment->translateHasOne->name : ''}}</p>--}}
                                                <?php echo ($apartment->translateHasOne) ? $apartment->translateHasOne->description : '';?>
                                            </div>
                                            <div class="col-md-3 price-col-color  price-div-car">

                                                <h2 class="h4-price-car">{{ (\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? strtoupper(session()->get('currency_symbol')).' ' : '$ '}}{{(\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price):$apartment->prices[0]->base_price}}</h2>
                                                <p>(per day)</p>
                                                <a href="{{asset('apartment/'.$apartment->id)}}" class="btn btn-success btn-lg btn-block book-button apartment-book-now" id="{{$apartment->id}}">BOOK NOW</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">


                                </div>
                            </div>
                            <div class="col-sm-2"></div>
                        </div>
                    @endforeach
                </div>
            </div>

            {{--</div>--}}
            {{--</div>--}}
            <section class="full-width">
                <nav class="page-navigation bottom-nav">
                    <!--back up button-->
                    <a class="scroll-to-top pull-right" title="Back up"><i class="fas fa-arrow-up"></i></a>
                    <!--//back up button-->
                    <!--pager-->
                    <div class="pager">
                    </div>
                </nav>
            </section>
        </div>

    </div>
    <script>
        $('.owl-carousel').owlCarousel({
            items: 1,
            loop: true,
            nav: true,
            autoplay: true,
            smartSpeed: 900,
            navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            navClass: ['owl-prev', 'owl-next'],

        });

        $(document).ready(function () {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth() + 1;
            var y = date.getFullYear();
            $(".input-by-pick-up").attr("placeholder", d + '/' + m + '/' + y);
            $(".input-by-return").attr("placeholder", d + '/' + m + '/' + y);

        });
        $('#datepicker-pick-up').Zebra_DatePicker({
            always_visible: $('#datepicker-pick-up-div'),
            format: 'd/m/Y',
            custom_classes: {
                'not-close-calendar': ['1-31']
            }
        });
        $('#datepicker-return').Zebra_DatePicker({
            always_visible: $('#datepicker-return-div'),
            format: 'd/m/Y',
            custom_classes: {
                'not-close-calendar': ['1-31']
            }
        });
        $('.all-body').on('click', function (e) {
            if (!$(e.target).hasClass('not-close-calendar')) {

                if ($('.input-hidden-by-return').val() > 0) {
                    $('#datepicker-return-div').css('display', 'none');
                    $('.input-hidden-by-return').val(0);

                }
                if ($('.input-hidden-by-pick-up').val() > 0) {
                    $('#datepicker-pick-up-div').css('display', 'none');
                    $('.input-hidden-by-pick-up').val(0);
                }

            }
        });
        $('.input-by-return').click(function () {
            if ($('.input-hidden-by-return').val() < 1) {
                $('#datepicker-return-div').css('display', 'block');
                $('.input-hidden-by-return').val(1);
            }
            if ($('.input-hidden-by-pick-up').val() > 0) {
                $('#datepicker-pick-up-div').css('display', 'none');
                $('.input-hidden-by-pick-up').val(0);
            }
        });
        $('.input-by-pick-up').click(function () {

            if ($('.input-hidden-by-pick-up').val() < 1) {
                $('#datepicker-pick-up-div').css('display', 'block');
                $('.input-hidden-by-pick-up').val(1);
            }
            if ($('.input-hidden-by-return').val() > 0) {
                $('#datepicker-return-div').css('display', 'none');
                $('.input-hidden-by-return').val(0);

            }
        });


        $(document).ready(function () {
            $('.timepicker-pick-up').qcTimepicker({
                'format': 'H:mm',
                'minTime': '0:00:00',
                'maxTime': '23:59:59',
                'step': '0:30:00',
                'placeholder': '12:00'
            });
        });
        $(document).ready(function () {
            $('.timepicker-return').qcTimepicker({
                'format': 'H:mm',
                'minTime': '0:00:00',
                'maxTime': '23:59:59',
                'step': '0:30:00',
                'placeholder': '12:00'
            });
        });

        $(document).ready(function () {
            $(window).scroll(function(){
                if ($(document).scrollTop() > 0) {
                    $('.car-book-general').css("background-image", "");
                    $('.body-image').css("background-image", " url({{ asset("img/bg-apartment3.jpg")}})");
                } else {
                    $('.body-image').css("background-image", "");
                    $('.car-book-general').css("background-image", " url({{ asset("img/bg-apartment3.jpg")}})");
                }
            })
        })
    </script>

    <style>
        .owl-nav {
            width: 96%;
            top: 41%;
            position: absolute;
            font-size: 30px;
        }

        .owl-carousel {
            width: 80%;
        }
        .h4-price-car {
            margin-top: 0px;
        }
        .price-div-car {
            position: absolute;
            right: 0;
            bottom: 0;
        }
        .body-image {
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
        }
        .div-by-days-car{
            width: 100%;
        }
        .div-by-car-car {
            background-color: transparent !important;
            border-bottom: none !important;
        }
        .car-book-general{
            background-attachment: fixed;
            background-position-y: bottom;
            background-repeat: no-repeat;
        }
    </style>
@endsection
