@extends('layouts.app')
@section('content')
    @include('_partial.book_apartment')
    <div class="container-fluid">
        <div class="container">
            <p><br></p>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-5">
                            <h4 class="comp-tour-travel">{{$apartment->name}}</h4>
                            <p><?=$apartment->description?></p>
                            @foreach($apartment->details as $detail)
                                <p class="text-capitalize font-weight-bold">{{$detail->property_type}}</p>
                            @endforeach
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <div style="float: right">
                                <img src="{{ asset("img/ctt_main_png.png")}}" alt="" class="img-by-admin">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <p><span class="tell-mail-apartment">Tell:</span> +37498421212</p>
                            <p><span class="tell-mail-apartment">Email:</span> cartourtravel.info@gmail.com</p>
                        </div>
                    </div>
                    <p><br></p>
                    <div class="row">

                        <div class="col-sm-8">
                            <?php $numberBedroom = 1; ?>
                            @foreach($apartment->details[0]->bedrooms as $bedroom)

                                @if($bedroom->double!=0)
                                    <p class="p-rooms-class"><span class="color-black-blod">Bedroom {{$numberBedroom}}
                                            :</span> {{$bedroom->double}} <span>double bed</span>
                                        <img src="{{asset("img/queen-bed.png")}}" style="width: 20px; height: 20px"
                                             alt="">
                                        {{--                                <img src="{{asset("img/single-bed.png")}}" style="width: 20px; height: 20px" alt="">--}}
                                    </p>
                                @endif

                                @if($bedroom->queen!=0)
                                    <p class="p-rooms-class"><span class="color-black-blod">Bedroom {{$numberBedroom}}
                                            :</span> {{$bedroom->queen}} <span>queen bed</span> <img
                                                src="{{asset("img/queen-bed.png")}}" style="width: 20px; height: 20px"
                                                alt="">
                                    </p>
                                @endif
                                @if($bedroom->single!=0)
                                    <p class="p-rooms-class"><span class="color-black-blod">Bedroom {{$numberBedroom}}
                                            :</span> {{$bedroom->single}} <span>single bed</span>
                                        <img src="{{asset("img/single-bed.png")}}" style="width: 20px; height: 20px"
                                             alt="">
                                        @if($bedroom->single ==2)
                                            <img src="{{asset("img/single-bed.png")}}" style="width: 20px; height: 20px"
                                                 alt="">
                                        @endif
                                        @if($bedroom->single ==3)
                                            <img src="{{asset("img/single-bed.png")}}" style="width: 20px; height: 20px"
                                                 alt="">
                                        @endif
                                    </p>
                                @endif
                                @if($bedroom->sofa_bed!=0)
                                    <p class="p-rooms-class"><span class="color-black-blod">Bedroom {{$numberBedroom}}
                                            :</span> {{$bedroom->sofa_bed}} <span>sofa bed</span> <img
                                                src="{{asset("img/sofa-bed.png")}}" style="width: 20px; height: 20px"
                                                alt="">
                                    </p>
                                @endif
                                <?php $numberBedroom++; ?>
                            @endforeach
                            @if($apartment->details[0]->number_sofa_bed != 0)
                                <p class="p-rooms-class"><span class="color-black-blod">Living space
                                    :</span> {{$apartment->details[0]->number_sofa_bed}} <span>sofa bed</span> <img
                                            src="{{asset("img/sofa-bed.png")}}" style="width: 20px; height: 20px"
                                            alt="">
                                </p>
                            @endif
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                    <p><br></p>
                    <div class="row">
                        <p class="apartment-title">Tour this apartment</p>
                    </div>
                    <p><br></p>
                    <div class="row">
                        <?php $imgNumber = 0; ?>
                        @foreach($apartment->picture as $picture)
                            <?php $imgNumber++; ?>
                            <div class="img-apartment-this img-apartment-view col-md-4">
                                <img class="img-apartment" src="{{asset('uploads/'.$picture->name)}}" alt=""
                                     style="width: 370px; height: 200px;">
                                @if($imgNumber==3)
                                    <div class="div-apartment-img-opacity ">
                                        <h3 class="text-center view-more-img">View more</h3>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                    <p><br></p>
                    <div class="row">
                        <p class="apartment-title amenities-p">Amenities</p>
                    </div>
                    <div class="row">
                        <p>These amenites are available to you.</p>
                    </div>

                    <div class="row div-by-img-serch">

                        <?php
                        $detail = $apartment->details[0]->toArray();
                        $array = array_sort_recursive($detail);
                        $see_detalis = 0;
                        ?>
                        @foreach($sortDetails as $key => $value)
                            @if ($value == "yes" ||($key == 'heating' && $value))
                                <?php $see_detalis++; ?>
                                @if($see_detalis>7)
                                    <div class="div-detail-8" style="display: none">
                                        @endif
                                        <div class="div-by-img">
                                            <img src="{{ asset("img/$key.jpg")}}" alt=""
                                                 style="width: 146px; height: 130px;">
                                            <p class="text-capitalize name-amenities{{$see_detalis}}" id="{{$key}}"></p>
                                        </div>
                                        @if($see_detalis>7)
                                    </div>
                                @endif
                            @endif

                        @endforeach


                    </div>
                    <div class="row row-by-button-see-all-detail">
                        <button type="button" class="btn  button-by-button-see-all-detail">See more</button>
                        <input type="hidden" value="0" class="hidden-by-see-all">
                    </div>
                    <p><br></p>
                    <div class="row ">
                        <p class="apartment-title">Location</p>
                    </div>
                </div>
                {{--<div class="col-sm-4">--}}
                {{--@include('_partial.book_apartment')--}}
                {{--</div>--}}
            </div>


            <div>

                @foreach($apartment->details as $detail)
                    <div class="row">
                        <p>{{$apartment->address}},{{$apartment->city}}, {{$apartment->country}}</p>
                    </div>
                @endforeach

                <div class="row">
                    <div class="content-tab" id="location" style=" height: auto">
                        <div id="map" style="height: 500px;width: 100%"></div>
                        <input id="country" value="{{$apartment->country}}" type="hidden">
                        <input id="city" value="{{$apartment->city}}" type="hidden">
                        <input id="area" value="{{$apartment->district}}" type="hidden">
                        <input id="street" value="{{$apartment->address}}" type="hidden">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-lg ">

                <!-- Modal content-->
                <div class="modal-content" style="width: 1000px">
                    <div class="modal-body modal-picture-apartment">
                        <div class="owl-theme owl-height  owl-carousel photo-slider">


                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade" id="book-Modal" role="dialog">
            <div class="modal-dialog book-dialog">
                <!-- Modal content-->
                <div class="modal-content book-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Please enter your info for booking. </h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="div-modal-book div-first-block ">
                                    <label for="">Last name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control" id="last-name" placeholder="Last name">
                                    <p class="error-last-name"></p>

                                </div>
                                <div class="div-modal-book div-second-block ">
                                    <label for="">First name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control" id="first-name" placeholder="First name">
                                    <p class="error-first-name"></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="div-modal-book div-first-block ">
                                    <label for="">Email address<sup><i
                                                    class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="email" class="form-control email" id="email"
                                           placeholder="Email address"
                                           name="email">
                                    <p class="error-email"></p>

                                </div>
                                <div class="div-modal-book div-second-block">
                                    <label for="">Phone<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                    <input type="text" class="form-control phone" id="phone" placeholder="Phone"
                                           name="phone">
                                    <p class="error-phone"></p>

                                </div>
                            </div>
                            <div>
                                <label for="">Viber:</label>
                                <input type="text" class="form-control viber" id="viber" placeholder="Viber">
                            </div>
                            <div class="">
                                <label for="">Telegram:</label>
                                <input type="text" class="form-control" id="telegram" placeholder="Telegram">
                            </div>
                            <div class="">
                                <label for="">Whatsapp:</label>
                                <input type="text" class="form-control whatsapp" id="whatsapp" placeholder="Whatsapp">
                            </div>
                            <p class="date-from"></p>
                            <p class="date-to"></p>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="book-book">Book</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade" id="modal-messenger" role="dialog">
            <div class="modal-dialog modal-messege-dialog">
                <!-- Modal content-->
                <div class="modal-content modal-messege">
                    <div class="modal-body text-center">
                        <p class="book-successful">Your booking successful</p>
                        <button type="button" class="close " data-dismiss="modal"><i
                                    class="fas fa-times close-button-x"></i></button>
                    </div>

                </div>

            </div>
        </div>
        <script>
            $(document).ready(function () {

                ymaps.ready(init);

                function init() {
                    myMap = new ymaps.Map('map', {
                        center: [40.1791857, 44.4991029],
                        zoom: 9
                    });
                    var address = $('#country').val() + ' ' + $('#city').val() + ' ' + $('#area').val() + ' ' + $('#address').val();

                    // Поиск координат центра Нижнего Новгорода.
                    ymaps.geocode(address, {
                        /**
                         * Опции запроса
                         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                         */
                        // Сортировка результатов от центра окна карты.
                        // boundedBy: myMap.getBounds(),
                        // strictBounds: true,
                        // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
                        // Если нужен только один результат, экономим трафик пользователей.
                        results: 1
                    }).then(function (res) {
                        // Выбираем первый результат геокодирования.
                        var firstGeoObject = res.geoObjects.get(0),
                            // Координаты геообъекта.
                            coords = firstGeoObject.geometry.getCoordinates(),
                            // Область видимости геообъекта.
                            bounds = firstGeoObject.properties.get('boundedBy');

                        firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
                        // Получаем строку с адресом и выводим в иконке геообъекта.
                        firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());

                        // Добавляем первый найденный геообъект на карту.
                        myMap.geoObjects.add(firstGeoObject);
                        // Масштабируем карту на область видимости геообъекта.
                        myMap.setBounds(bounds, {
                            // Проверяем наличие тайлов на данном масштабе.
                            checkZoomRange: true
                        });


                    });

                }

                var seeDetail =<?php echo $see_detalis; ?>;
                seeDetail = seeDetail + 1;
                console.log(seeDetail);
                for (var i = 1; i < seeDetail; i++) {
                    var text = $('.name-amenities' + i + '').attr('id');
                    // console.log(text);
                    var arrayText = text.split("_");
                    for (var m = 0; m < arrayText.length; m++) {
                        var text = $('.name-amenities' + i + '').append(arrayText[m] + ' ');

                    }

                }

            });
            $(document).ready(function () {
                var seeDetail =<?php echo $see_detalis; ?>;
                if (seeDetail > 7) {
                    $('.div-detail-8').css('display', 'none');
                } else {
                    $('.row-by-button-see-all-detail').css('display', 'none');
                }
            });
            $('.button-by-button-see-all-detail').click(function () {
                if ($('.hidden-by-see-all').val() == 0) {
                    $('.div-detail-8').show(2000);
                    $('.hidden-by-see-all').val(1);
                    $('.button-by-button-see-all-detail').text('Close');
                } else {
                    $('.div-detail-8').hide(1000);
                    $('.hidden-by-see-all').val(0);
                    $('.button-by-button-see-all-detail').text('See more');
                }

            });

            $("#last-name").click(function () {
                $(".error-last-name").empty();
            });
            $("#first-name").click(function () {
                $(".error-first-name").empty();
            });
            $("#email").click(function () {
                $(".error-email").empty();
            });
            $("#phone").click(function () {
                $(".error-phone").empty();
            });
            $(document).ready(function () {
                $(".book-button").click(function () {
                    $("#book-Modal").modal("show");
                });
            });
            // $(document).ready(function () {
            //     $(".img-apartment").click(function () {
            //         $("#myModal").modal({backdrop: true});
            //     });
            // });
            $('#book-book').click(function () {
                var data = $('#book-form').serialize();
                console.log(data);
                $.ajax({
                    url: '/reservation/store',
                    type: 'POST',
                    dataType: "JSON",
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    data: data,
                    success: function (success) {
                        // console.log('Yes');
                        $("#book-Modal").modal("hide");
                        $("#modal-messenger").modal("show");
                        setTimeout(function () {
                            $("#modal-messenger").modal("hide");
                        }, 10000);
                    },
                    error: function (errors) {
                        // console.log(errors.responseJSON.errors.lastName);
                        if (errors.responseJSON.errors.last_name) {
                            $(".error-last-name").html(errors.responseJSON.errors.last_name);
                        }
                        if (errors.responseJSON.errors.first_name) {
                            $(".error-first-name").html(errors.responseJSON.errors.first_name);
                        }
                        if (errors.responseJSON.errors.email) {
                            $(".error-email").html(errors.responseJSON.errors.email);
                        }
                        if (errors.responseJSON.errors.phone) {
                            $(".error-phone").html(errors.responseJSON.errors.phone);
                        }
                        if (errors.responseJSON.errors.date_from) {
                            $(".date-from").html(errors.responseJSON.errors.date_from);
                        }
                        console.log(errors.responseJSON.errors);

                        if (errors.responseJSON.errors.date_to) {
                            $(".date-to").html(errors.responseJSON.errors.date_to);
                        }

                    }
                })

            });

            $(document).keydown(function (e) {
                switch (e.which) {
                    case 37: // left
                        $('.owl-next').click();
                        break;

                    case 38: // up
                        break;

                    case 39: // right
                        $('.owl-prev').click();
                        break;

                    case 40: // down
                        break;

                    default:
                        return;
                }
                e.preventDefault();
            });

            function init_carousel() {
                $('.owl-carousel').owlCarousel({
                    items: 1,
                    loop: true,
                    nav: true,
                    smartSpeed: 900,
                    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                    navClass: ['owl-prev', 'owl-next'],

                });
            }

            var request = 'yes'; // for first request

            $('.img-apartment-view').click(function () {
                var id = '<?php echo $apartment->id ?>';
                if (request == 'yes') {
                    $.ajax({
                        url: '/apartments/img',
                        type: 'POST',
                        dataType: "JSON",
                        headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                        data: {
                            id: id
                        },
                        success: function (result) {
                            for (var i = 0; i < result.length; i++) {
                                $('.owl-carousel').append(
                                    '<div>' +
                                    '                <img alt="" src="/uploads/' + result[i].name + '" style="height:620px; width: 1000px">' +
                                    '           </div>'
                                )
                            }
                            console.log(7894);
//                            setTimeout(function () {
                                init_carousel();
//                            }, 100);
                            $("#myModal").modal({backdrop: true});
                            request = '';

                        },
                        error: function (errors) {

                        }
                    })

                } else {
                    $("#myModal").modal({backdrop: true});

                }

            })

        </script>

@endsection