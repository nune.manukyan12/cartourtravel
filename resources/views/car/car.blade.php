@extends('layouts.app')

@section('content')

    <div class="body-image" style="background-image: url({{ asset("img/bg-car-chauffeur3.jpg")}});">


        <div class="row car-book-general" {{--style="background-image: url({{ asset("img/car-back-img.jpg")}});"--}}>
            {{--<img src="{{ asset("img/car-back-img.jpg")}} " alt="" style="width:100%;height: 450px;">--}}

        </div>
        <div class="row w-100 m-0 mt-5">
            <div class="col-md-12 col-lg-3">
                {{--<div class="table-car-rent">--}}
                    {{--<form action="{{url('/cars/search')}}" method="GET">--}}
                        {{--{{ csrf_field() }}--}}
                        {{--<p class="book-car">Book a car</p>--}}
                        {{--<div class="parametr-book-car">--}}
                            {{--<p class="pick-up-return-location"> Pick-up & Return locations <sup class="red-icon">*</sup></p>--}}
                            {{--<select name="pick_up_locations" id="pick_up_locations" class="select-option-by-car">--}}
                                {{--<option value="yerevan-downtown">Yerevan, Downtown</option>--}}
                                {{--<option value="yerevan-international-airport">Yerevan, International Airport</option>--}}
                                {{--<option value="tbilisi-city-center">Tbilisi City Center</option>--}}
                                {{--<option value="tbilisi-airport">Tbilisi, Airport</option>--}}
                                {{--<option value="gyumri-international-airport">Gyumri International Airport</option>--}}
                            {{--</select>--}}
                            {{--<select name="return_locations" id="return_locations" class="select-option-by-car">--}}
                                {{--<option value="yerevan-downtown">Yerevan, Downtown</option>--}}
                                {{--<option value="yerevan-international-airport">Yerevan, International Airport</option>--}}
                                {{--<option value="tbilisi-city-center">Tbilisi City Center</option>--}}
                                {{--<option value="tbilisi-airport">Tbilisi, Airport</option>--}}
                                {{--<option value="gyumri-international-airport">Gyumri International Airport</option>--}}
                            {{--</select>--}}
                            {{--<div class="div-by-days-car">--}}
                                {{--<div class="calendar-div-button">--}}
                                    {{--<p class="pick-up-return-location"> Pick-up date and time <sup class="red-icon">*</sup>--}}
                                    {{--</p>--}}
                                    {{--<input type="text" class="input-car-calendar input-by-pick-up not-close-calendar"--}}
                                           {{--id="datepicker-pick-up"--}}
                                           {{--data-zdp_direction="[1,365]">--}}

                                    {{--<div id="datepicker-pick-up" style="display: none"></div>--}}
                                    {{--<div id="datepicker-pick-up-div" style="display: none"></div>--}}
                                    {{--<input type="hidden" class="input-hidden-by-pick-up" value="0">--}}

                                    {{--<div class="time-div">--}}
                                        {{--<input type="time" class="timepicker-pick-up" value="12:00">--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="calendar-div-button">--}}
                                    {{--<p class="pick-up-return-location"> Return date and time<sup class="red-icon">*</sup>--}}
                                    {{--</p>--}}
                                    {{--<input type="text" class="input-car-calendar input-by-return not-close-calendar"--}}
                                           {{--id="datepicker-return"--}}
                                           {{--data-zdp_direction="[1,365]">--}}

                                    {{--<div id="datepicker-return" style="display: none"></div>--}}
                                    {{--<div id="datepicker-return-div" style="display: none"></div>--}}
                                    {{--<input type="hidden" class="input-hidden-by-return" value="0">--}}

                                    {{--<div class="time-div">--}}
                                        {{--<input type="time" class="timepicker-return" value="12:00">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<p class="pick-up-return-location"> Voucher Code </p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="more-search-detalis-car" style="display: none">--}}
                        {{--<div style="" class="more-search-div">--}}
                            {{--<p>Fuel</p>--}}
                            {{--<select name="fuel" id="" class="select-car-search">--}}
                                {{--<option value=""></option>--}}
                                {{--<option value="N">Unspecifled, no A/C</option>--}}
                                {{--<option value="R">Unspecifled, fuel A/C</option>--}}
                                {{--<option value="D">Diesel, A/C</option>--}}
                                {{--<option value="Q">Diesel, no A/C</option>--}}
                                {{--<option value="H">Hybrid, A/C</option>--}}
                                {{--<option value="I">Hybrid, no A/C</option>--}}
                                {{--<option value="E">Electric A/C</option>--}}
                                {{--<option value="C">Electric, no A/C</option>--}}
                                {{--<option value="V">Petrol, A/C</option>--}}
                                {{--<option value="Z">Petrol, no A/C</option>--}}
                            {{--</select>--}}
                            {{--<p>Transmission & drive</p>--}}
                            {{--<select name="transmission_drive" id="" class="select-car-search">--}}
                                {{--<option value=""></option>--}}
                                {{--<option value="M">M-Manual drive</option>--}}
                                {{--<option value="N">N-Manual, 4WD</option>--}}
                                {{--<option value="C">C-Manual, AWD</option>--}}
                                {{--<option value="A">Auto drive</option>--}}
                                {{--<option value="B">B-Auto, 4WD</option>--}}
                                {{--<option value="D">D-Auto,AWD</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<div style="" class="more-search-div-1">--}}
                            {{--<p>Doors & vehicle type</p>--}}
                            {{--<select name="doors_vehicle_type" id="" class="select-car-search">--}}
                                {{--<option value=""></option>--}}
                                {{--<option value="B">B-2/3 door</option>--}}
                                {{--<option value="C">C-2/4 door</option>--}}
                                {{--<option value="D">D-2/4 Sdoor</option>--}}
                                {{--<option value="W">W-Wagon/Estate</option>--}}
                                {{--<option value="S">S-Sport</option>--}}
                                {{--<option value="T">T-Convertible</option>--}}
                                {{--<option value="F">F-SUV</option>--}}
                                {{--<option value="E">E-Coupe</option>--}}
                                {{--<option value="N">N-Roadster</option>--}}

                            {{--</select>--}}
                            {{--<p>Car Category-size,cost, power & luxury factor</p>--}}
                            {{--<select name="car_category" id="" class="select-car-search">--}}
                                {{--<option value=""></option>--}}
                                {{--<option value="M">M-Mlni</option>--}}
                                {{--<option value="N">N-Mini Elite</option>--}}
                                {{--<option value="E">E-Econamy</option>--}}
                                {{--<option value="C">C-Compact</option>--}}
                                {{--<option value="I">I-Intermediate</option>--}}
                                {{--<option value="S">S-Standard</option>--}}
                                {{--<option value="F">F-Full-size</option>--}}
                                {{--<option value="P">P-Premium</option>--}}
                                {{--<option value="X">X-Special</option>--}}

                            {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="buttons-div-by-serch-car">--}}
                            {{--<div>--}}
                                {{--<input type="hidden" class="see-more-filter" value="0">--}}
                            {{--</div>--}}
                            {{--<button type="submit" class="btn btn-primary button-car-s">FIND A CAR</button>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
                <div class="table-car-rent">
                    <form action="{{url('/cars/search')}}" method="GET" style="padding: 10px 10px 10px 20px">
                        {{--<p class="book-car">Book a car</p>--}}
                        <div class="row m-0">
                            <div class="col-md-12 pr-0">
                                <div class="parametr-book-car">
                                    <div class="row">
                                    <p class="pick-up-return-location"> Pick-up & Return locations <sup class="red-icon">*</sup></p>
                                    <select name="pick_up_locations" id="pick_up_locations" class="select-option-by-car">
                                        <option value="yerevan-downtown">Yerevan, Downtown</option>
                                        <option value="yerevan-international-airport">Yerevan, International Airport</option>
                                        <option value="tbilisi-city-center">Tbilisi City Center</option>
                                        <option value="tbilisi-airport">Tbilisi, Airport</option>
                                        <option value="gyumri-international-airport">Gyumri International Airport</option>
                                    </select>
                                    <select name="return_locations" id="return_locations" class="select-option-by-car">
                                        <option value="yerevan-downtown">Yerevan, Downtown</option>
                                        <option value="yerevan-international-airport">Yerevan, International Airport</option>
                                        <option value="tbilisi-city-center">Tbilisi City Center</option>
                                        <option value="tbilisi-airport">Tbilisi, Airport</option>
                                        <option value="gyumri-international-airport">Gyumri International Airport</option>
                                    </select>
                                    <p class="pick-up-return-location">Car Category-size,cost, power & luxury facto  <sup class="red-icon">*</sup></p>
                                    <select name="car_category" id="" class="select-car-search">
                                        <option value=""></option>
                                        <option value="M">M-Mlni</option>
                                        <option value="N">N-Mini Elite</option>
                                        <option value="E">E-Econamy</option>
                                        <option value="C">C-Compact</option>
                                        <option value="I">I-Intermediate</option>
                                        <option value="S">S-Standard</option>
                                        <option value="F">F-Full-size</option>
                                        <option value="P">P-Premium</option>
                                        {{--<option value="X">X-Special</option>--}}
                                        <option value="V">V-Van</option>
                                    </select>
                                    <div class="div-by-days-car w-100">
                                        <div class="calendar-div-button">
                                            <p class="pick-up-return-location"> Pick-up date and time <sup class="red-icon">*</sup>
                                            </p>
                                            <div class="row" style="width: 107%">
                                                <div class="col-7 col-sm-7">
                                                    <input type="text" class="input-car-calendar input-by-pick-up not-close-calendar"
                                                           id="datepicker-pick-up"
                                                           data-zdp_direction="[1,365]">

                                                    <div id="datepicker-pick-up" style="display: none"></div>
                                                    <div id="datepicker-pick-up-div" style="display: none"></div>
                                                    <input type="hidden" class="input-hidden-by-pick-up" value="0">
                                                </div>
                                                <div class="col-5 col-sm-5">
                                                    <div class="time-div">
                                                        <input type="time" class="timepicker-pick-up" value="12:00">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="calendar-div-button">
                                            <p class="pick-up-return-location"> Return date and time<sup class="red-icon">*</sup>
                                            </p>
                                            <div class="row" style="width: 107%">
                                                <div class="col-7">
                                                    <input type="text" class="input-car-calendar input-by-return not-close-calendar"
                                                           id="datepicker-return"
                                                           data-zdp_direction="[1,365]">

                                                    <div id="datepicker-return" style="display: none"></div>
                                                    <div id="datepicker-return-div" style="display: none"></div>
                                                    <input type="hidden" class="input-hidden-by-return" value="0">
                                                </div>
                                                <div class="col-5">
                                                    <div class="time-div">
                                                        <input type="time" class="timepicker-return" value="12:00">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="pick-up-return-location"> Voucher Code </p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="col-md-4 p-0">--}}
                                {{--<div class="more-search-detalis-car" style="display: none">--}}
                                {{--<div style="" class="more-search-div">--}}
                                    {{--<div class="row m-0">--}}
                                        {{--<div class="col-sm-6 col-md-12 pl-0">--}}
                                            {{--<p>Fuel</p>--}}
                                            {{--<select name="fuel" id="" class="select-car-search">--}}
                                                {{--<option value=""></option>--}}
                                                {{--<option value="N">Unspecifled, no A/C</option>--}}
                                                {{--<option value="R">Unspecifled, fuel A/C</option>--}}
                                                {{--<option value="D">Diesel, A/C</option>--}}
                                                {{--<option value="Q">Diesel, no A/C</option>--}}
                                                {{--<option value="H">Hybrid, A/C</option>--}}
                                                {{--<option value="I">Hybrid, no A/C</option>--}}
                                                {{--<option value="E">Electric A/C</option>--}}
                                                {{--<option value="C">Electric, no A/C</option>--}}
                                                {{--<option value="V">Petrol, A/C</option>--}}
                                                {{--<option value="Z">Petrol, no A/C</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-6 col-md-12 pl-0">--}}
                                            {{--<p>Transmission & drive</p>--}}
                                            {{--<select name="transmission_drive" id="" class="select-car-search">--}}
                                                {{--<option value=""></option>--}}
                                                {{--<option value="M">M-Manual drive</option>--}}
                                                {{--<option value="N">N-Manual, 4WD</option>--}}
                                                {{--<option value="C">C-Manual, AWD</option>--}}
                                                {{--<option value="A">Auto drive</option>--}}
                                                {{--<option value="B">B-Auto, 4WD</option>--}}
                                                {{--<option value="D">D-Auto,AWD</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div style="" class="more-search-div-1">--}}
                                    {{--<div class="row m-0">--}}
                                        {{--<div class="col-sm-6 col-md-12 pl-0">--}}
                                            {{--<p>Doors & vehicle type</p>--}}
                                            {{--<select name="doors_vehicle_type" id="" class="select-car-search">--}}
                                                {{--<option value=""></option>--}}
                                                {{--<option value="B">B-2/3 door</option>--}}
                                                {{--<option value="C">C-2/4 door</option>--}}
                                                {{--<option value="D">D-2/4 Sdoor</option>--}}
                                                {{--<option value="W">W-Wagon/Estate</option>--}}
                                                {{--<option value="S">S-Sport</option>--}}
                                                {{--<option value="T">T-Convertible</option>--}}
                                                {{--<option value="F">F-SUV</option>--}}
                                                {{--<option value="E">E-Coupe</option>--}}
                                                {{--<option value="N">N-Roadster</option>--}}

                                            {{--</select>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-6 col-md-12 pl-0">--}}
                                            {{--<p>Car Category-size,cost, power & luxury factor</p>--}}
                                            {{--<select name="car_category" id="" class="select-car-search">--}}
                                                {{--<option value=""></option>--}}
                                                {{--<option value="M">M-Mlni</option>--}}
                                                {{--<option value="N">N-Mini Elite</option>--}}
                                                {{--<option value="E">E-Econamy</option>--}}
                                                {{--<option value="C">C-Compact</option>--}}
                                                {{--<option value="I">I-Intermediate</option>--}}
                                                {{--<option value="S">S-Standard</option>--}}
                                                {{--<option value="F">F-Full-size</option>--}}
                                                {{--<option value="P">P-Premium</option>--}}
                                                {{--<option value="X">X-Special</option>--}}
                                                {{--<option value="V">V-Van</option>--}}

                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="row m-0">
                            <div class="col-md-12 pl-0">
                                <div class="buttons-div-by-serch-car">
                                    <div>
                                        <input type="hidden" class="see-more-filter" value="0">
                                    </div>
                                    <button type="submit" class="btn btn-primary w-100">FIND A CAR</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-12 col-lg-9 bg-white">
                @foreach($cars as $car)


                    <div class="row div-by-car-car">
                        <div class="offset-md-2 col-sm-8">
                            <h2 class="car-name-car">{{($car->translateHasOne)? $car->translateHasOne->name : ''}} or similar</h2>
    {{--                        <h3 style="margin: 0px">{{$car->car_category}}{{$car->doors_vehicle_type}}{{$car->transmission_drive}}{{$car->fuel}}</h3>--}}
                            <h4 class="text-capitalize h4-car-type" style="margin: 0px">{{$car->type->name}} Car</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        {{--<img class="img-apartment" src="{{asset('uploads/'.$car->general_pic)}}" alt=""--}}
                                        {{--style="width: 450px; height: 300px;">--}}
                                        <div class="owl-theme owl-height  owl-carousel">
                                            <div>
                                                <img class="img-apartment" src="{{asset('uploads/'.$car->general_pic)}}" alt=""
                                                     style="width: 80%; height: 300px;">
                                            </div>
                                            @if($car->picture)
                                                @foreach($car->picture as $picture)
                                                    <div>
                                                        <img src="{{asset('uploads/'.$picture->name)}}"
                                                             style="width: 80%; height: 300px;">
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row div-row-car-detail">
                                        <div class="col-12 col-sm-12">

                                            <div class="row div-car-icons m-0 w-100">
                                                <div class="col-3 col-sm-3 div-by-icons">
                                                    <div class="display-inline-block">
                                                        <img src="{{ asset("img/transmission.png")}}" alt=""
                                                             style="width: 25px; background-color: white; height: 22px; ">
                                                    </div>
                                                    <div class="display-inline-block">
                                                        <span class="number-or-type-car a-or-m-car"><?php if ($car->transmission == 'Auto') {
                                                                echo 'A';
                                                            } else {
                                                                echo 'M';
                                                            }?></span>
                                                    </div>
                                                </div>
                                                <div class="col-3 col-sm-3 div-by-icons-sit">
                                                    <img class="img-icon-car" src="{{ asset("img/sit.png")}}" alt=""
                                                         style="width: 20px; background-color: white; height: 22px;">
                                                    <span class="number-or-type-car count-car-sit"><?php echo $car->persons; ?></span>
                                                </div>
                                                <div class="col-3 col-sm-3 div-by-icons-door">
                                                    <img class="img-icon-car" src="{{ asset("img/door.png")}}" alt=""
                                                         style="width: 25px; background-color: white; height: 20px;">
                                                    <span class="number-or-type-car count-car-sit"><?php echo $car->door_count; ?></span>
                                                </div>
                                                <div class="col-3 col-sm-3 div-by-icons-snow">
                                                    <img class="img-snow-car-icon" src="{{ asset("img/snow.png")}}" alt=""
                                                         style="width: 20px; background-color: white; height: 20px;">
                                                    <span class="number-or-type-car"><?php if ($car->winter_wheel == 'yes') {
                                                            echo 'Yes';
                                                        } else {
                                                            echo 'No';
                                                        }?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6"></div>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="row mb-2 h-100">
                                        <div class="col-md-6 col-lg-10">
                                            <p class="car-offer-front">This offer includes:</p>
                                            <ul class="ul-offer-car">

                                                <li>Collision Damage Waiver</li>
                                                <li>Third Party Liability</li>
                                                <li>Theft Protection</li>
                                                <li>Free Cancelation</li>
                                                <li>Roadside Assistance</li>

                                            </ul>
                                        </div>
                                        <div class="col-sm-6 price-col-color div-h4-price-car">
                                            <div >
                                                <h2 class="h4-price-car">{{\App\Services\ForgeApi::getRate(session()->get('currency'),$car->car_day_prices)}} {{strtoupper(session()->get('currency_symbol')).' '}}</h2>
                                                <p>(per day)</p>
                                                <button type="button" class="btn btn-success book-button" id="{{$car->id}}">BOOK NOW
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <?php echo ($car->translateHasOne)? $car->translateHasOne->description : '';?>

                            </div>
                        </div>
                        <div class="col-sm-2"></div>
                    </div>

                @endforeach


                @foreach($cars as $car)


                        <div class="row div-by-car-car">
                            {{--<div class="col-md-12 col-lg-4" style="background-color: #e9e6e0"></div>--}}
                            <div class="offset-md-2 col-sm-8">
                                <h2 class="car-name-car">{{($car->translateHasOne)? $car->translateHasOne->name : ''}} or similar</h2>
                                <h3 style="margin: 0px">{{$car->car_category}}{{$car->doors_vehicle_type}}{{$car->transmission_drive}}{{$car->fuel}}</h3>
                                <h4 class="text-capitalize h4-car-type" style="margin: 0px">{{$car->type->name}} Car</h4>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            {{--<img class="img-apartment" src="{{asset('uploads/'.$car->general_pic)}}" alt=""--}}
                                            {{--style="width: 450px; height: 300px;">--}}
                                            <div class="owl-theme owl-height  owl-carousel">
                                                <div>
                                                    <img class="img-apartment" src="{{asset('uploads/'.$car->general_pic)}}" alt=""
                                                         style="width: 80%; height: 300px;">
                                                </div>
                                                @if($car->picture)
                                                    @foreach($car->picture as $picture)
                                                        <div>
                                                            <img src="{{asset('uploads/'.$picture->name)}}"
                                                                 style="width: 80%; height: 300px;">
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row div-row-car-detail">
                                            <div class="col-sm-12">

                                                <div class="row div-car-icons w-100 m-0">
                                                    <div class="col-3 col-sm-3 div-by-icons">
                                                        <div class="display-inline-block">
                                                            <img src="{{ asset("img/transmission.png")}}" alt=""
                                                                 style="width: 25px; background-color: white; height: 22px; ">
                                                        </div>
                                                        <div class="display-inline-block">
                                                            <span class="number-or-type-car a-or-m-car"><?php if ($car->transmission == 'Auto') {
                                                                    echo 'A';
                                                                } else {
                                                                    echo 'M';
                                                                }?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-3 col-sm-3 div-by-icons-sit">
                                                        <img class="img-icon-car" src="{{ asset("img/sit.png")}}" alt=""
                                                             style="width: 20px; background-color: white; height: 22px;">
                                                        <span class="number-or-type-car count-car-sit"><?php echo $car->persons; ?></span>
                                                    </div>
                                                    <div class="col-3 col-sm-3 div-by-icons-door">
                                                        <img class="img-icon-car" src="{{ asset("img/door.png")}}" alt=""
                                                             style="width: 25px; background-color: white; height: 20px;">
                                                        <span class="number-or-type-car count-car-sit"><?php echo $car->door_count; ?></span>
                                                    </div>
                                                    <div class="col-3 col-sm-3 div-by-icons-snow">
                                                        <img class="img-snow-car-icon" src="{{ asset("img/snow.png")}}" alt=""
                                                             style="width: 20px; background-color: white; height: 20px;">
                                                        <span class="number-or-type-car"><?php if ($car->winter_wheel == 'yes') {
                                                                echo 'Yes';
                                                            } else {
                                                                echo 'No';
                                                            }?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--<div class="col-sm-4 col-md-6"></div>--}}
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row mb-2 h-100">
                                            <div class="col-md-6 col-lg-10">
                                                <p class="car-offer-front">This offer includes:</p>
                                                <ul class="ul-offer-car">

                                                    <li>Collision Damage Waiver</li>
                                                    <li>Third Party Liability</li>
                                                    <li>Theft Protection</li>
                                                    <li>Free Cancelation</li>
                                                    <li>Roadside Assistance</li>

                                                </ul>
                                            </div>
                                            <div class="col-sm-6 price-col-color div-h4-price-car">
                                                <div>
                                                    <h2 class="h4-price-car">{{\App\Services\ForgeApi::getRate(session()->get('currency'),$car->car_day_prices)}} {{strtoupper(session()->get('currency_symbol')).' '}}</h2>
                                                    <p>(per day)</p>
                                                    <button type="button" class="btn btn-success book-button" id="{{$car->id}}">BOOK NOW
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <?php echo ($car->translateHasOne)? $car->translateHasOne->description : '';?>

                                </div>
                            </div>
                            <div class="col-sm-2"></div>
                        </div>

                    @endforeach
            </div>

            {{--<div class="col-md-12 col-lg-4">--}}
                {{--<div class="description">--}}
                    {{--<p>--}}
                    {{--<p>Car Tour Travel agency with reliable partners to give you the best selection of short-term, long-term and luxury rentals. They also provide a specialty rentals such as vans, one-way rentals and chauffeur services.</p>--}}
                    {{--<p><br />Wherever you want to go, whatever car you need, we've got the keys. Just enter your destination, pick-up, and drop-off dates and we'll get you all the deals from the top car rental companies in Armenia.</p>--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>

    <nav class="page-navigation bottom-nav">
        <!--back up button-->
        <a class="scroll-to-top pull-right" title="Back up"><i class="fas fa-arrow-up"></i></a>
        <!--//back up button-->
        <!--pager-->
        <div class="pager">
        </div>
    </nav>
    <div class="modal fade" id="book-car" role="dialog">
        <div class="modal-dialog book-dialog">
            <!-- Modal content-->
            <div class="modal-content book-content">
                <div class="modal-header">
                    <h4 class="modal-title">Please enter your info for booking. </h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="div-modal-book div-first-block ">
                                <label for="">Last name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control" id="last-name" placeholder="Last name">
                                <p class="error-last-name"></p>

                            </div>
                            <div class="div-modal-book div-second-block ">
                                <label for="">First name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control" id="first-name" placeholder="First name">
                                <p class="error-first-name"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="div-modal-book div-first-block ">
                                <label for="">Email address<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="email" class="form-control email" id="email" placeholder="Email address">
                                <p class="error-email"></p>

                            </div>
                            <div class="div-modal-book div-second-block">
                                <label for="">Phone<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control phone" id="phone" placeholder="Phone">
                                <p class="error-phone"></p>

                            </div>
                        </div>
                        <div>
                            <label for="">Viber:</label>
                            <input type="text" class="form-control" id="viber" placeholder="Viber">
                        </div>
                        <div class="">
                            <label for="">Telegram:</label>
                            <input type="text" class="form-control" id="telegram" placeholder="Telegram">
                        </div>
                        <div class="">
                            <label for="">Whatsapp:</label>
                            <input type="text" class="form-control" id="whatsapp" placeholder="Whatsapp">
                        </div>
                        <p class="date-from"></p>
                        <p class="date-to"></p>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="book-book">Book</button>
                </div>
            </div>

        </div>
    </div>

    {{--Modal error return and puke up date--}}
    <div class="modal fade" id="modal_error" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <p class="error-puke-up-date-car"></p>
                    <p class="error-return-date-car"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-messenger" role="dialog">
        <div class="modal-dialog modal-messege-dialog">
            <!-- Modal content-->
            <div class="modal-content modal-messege">
                <div class="modal-body text-center">
                    <p class="book-successful">Your booking successful</p>
                    <button type="button" class="close " data-dismiss="modal"><i
                                class="fas fa-times close-button-x"></i></button>
                </div>

            </div>

        </div>
    </div>

    <script>

        $('.owl-carousel').owlCarousel({
            items: 1,
            loop: true,
            nav: true,
            autoplay:true,
            smartSpeed: 900,
            navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            navClass: ['owl-prev', 'owl-next'],

        });
        $(document).ready(function () {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth() + 1;
            var y = date.getFullYear();
            $(".input-by-pick-up").attr("placeholder", d + '/' + m + '/' + y);
            $(".input-by-return").attr("placeholder", d + '/' + m + '/' + y);

        });
        $('#datepicker-pick-up').Zebra_DatePicker({
            always_visible: $('#datepicker-pick-up-div'),
            format: 'd/m/Y',
            custom_classes: {
                'not-close-calendar': ['1-31']
            }
        });
        $('#datepicker-return').Zebra_DatePicker({
            always_visible: $('#datepicker-return-div'),
            format: 'd/m/Y',
            custom_classes: {
                'not-close-calendar': ['1-31']
            }
        });
        $('.all-body').on('click', function (e) {
            if (!$(e.target).hasClass('not-close-calendar')) {

                if ($('.input-hidden-by-return').val() > 0) {
                    $('#datepicker-return-div').css('display', 'none');
                    $('.input-hidden-by-return').val(0);

                }
                if ($('.input-hidden-by-pick-up').val() > 0) {
                    $('#datepicker-pick-up-div').css('display', 'none');
                    $('.input-hidden-by-pick-up').val(0);
                }

            }
        });
        $('.input-by-return').click(function () {
            if ($('.input-hidden-by-return').val() < 1) {
                $('#datepicker-return-div').css('display', 'block');
                $('.input-hidden-by-return').val(1);
            }
            if ($('.input-hidden-by-pick-up').val() > 0) {
                $('#datepicker-pick-up-div').css('display', 'none');
                $('.input-hidden-by-pick-up').val(0);
            }
        });
        $('.input-by-pick-up').click(function () {

            if ($('.input-hidden-by-pick-up').val() < 1) {
                $('#datepicker-pick-up-div').css('display', 'block');
                $('.input-hidden-by-pick-up').val(1);
            }
            if ($('.input-hidden-by-return').val() > 0) {
                $('#datepicker-return-div').css('display', 'none');
                $('.input-hidden-by-return').val(0);

            }
        });


        $(document).ready(function () {
            $('.timepicker-pick-up').qcTimepicker({
                'format': 'H:mm',
                'minTime': '0:00:00',
                'maxTime': '23:59:59',
                'step': '0:30:00',
                'placeholder': '12:00'
            });
        });
        $(document).ready(function () {
            $('.timepicker-return').qcTimepicker({
                'format': 'H:mm',
                'minTime': '0:00:00',
                'maxTime': '23:59:59',
                'step': '0:30:00',
                'placeholder': '12:00'
            });
        });
        var id;
        $(".book-button").click(function () {
            $("#book-car").modal("show");
            id = $(this).attr('id');

        });

        $('#book-book').click(function () {
            var firstName = $('#first-name').val();
            var lastName = $('#last-name').val();
            var email = $('.email').val();
            var phone = $('.phone').val();
            var viber = $('#viber').val();
            var telegram = $('#telegram').val();
            var whatsapp = $('#whatsapp').val();
            var pick_up_locations = $('#pick_up_locations').find(":selected").text();
            var return_locations = $('#return_locations').find(":selected").text();
            var pick_up_date = $('#datepicker-pick-up').val();
            var return_date = $('#datepicker-return').val();
            var pick_up_time = $('#qcTimepicker-0').find(":selected").text();
            var return_time = $('#qcTimepicker-1').find(":selected").text();
            $.ajax({
                url: '/reservation/store_car',
                type: 'POST',
                dataType: "JSON",
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: {

                    last_name: lastName,
                    first_name: firstName,
                    email: email,
                    viber: viber,
                    telegram: telegram,
                    whatsapp: whatsapp,
                    phone: phone,
                    car_id: id,
                    pick_up_locations: pick_up_locations,
                    return_locations: return_locations,
                    pick_up_date: pick_up_date,
                    return_date: return_date,
                    pick_up_time: pick_up_time,
                    return_time: return_time,

                },
                success: function (success) {
                    // console.log('Yes');
                    $("#book-car").modal("hide");
                    $("#modal-messenger").modal("show");
                    setTimeout(function () {
                        $("#modal-messenger").modal("hide");
                    }, 10000);
                },
                error: function (errors) {

                    if (errors.responseJSON.errors.last_name) {
                        $(".error-last-name").html(errors.responseJSON.errors.last_name);
                    }
                    if (errors.responseJSON.errors.first_name) {
                        $(".error-first-name").html(errors.responseJSON.errors.first_name);
                    }
                    if (errors.responseJSON.errors.email) {
                        $(".error-email").html(errors.responseJSON.errors.email);
                    }
                    if (errors.responseJSON.errors.phone) {
                        $(".error-phone").html(errors.responseJSON.errors.phone);
                    }
                    if (errors.responseJSON.errors.pick_up_date || errors.responseJSON.errors.return_date) {

                        $("#modal_error").modal("show");
                        if (errors.responseJSON.errors.pick_up_date) {
                            $(".error-puke-up-date-car").html(errors.responseJSON.errors.pick_up_date);

                        }
                        console.log(errors.responseJSON.errors.return_date);
                        if (errors.responseJSON.errors.return_date) {
                            $(".error-return-date-car").html(errors.responseJSON.errors.return_date);

                        }
                        setTimeout(function () {
                            $("#modal_error").modal("hide");
                        }, 5000);
                    }


                }
            })
        });
        $('#see_more_filter').click(function () {
            if ($('.see-more-filter').val() == 0) {
                $(".table-car-rent").animate({width: '700px'}, 1000);
                $(".button-car-s").animate({width: '640px'}, 1000);
                $('#see_more_filter').text('CLOSE');
                $('.see-more-filter').val(1);
                // $('.more-search-detalis-car').show(500);
                setTimeout(function () {
                    $('.more-search-div').show(700);
                    setTimeout(function () {
                        $('.more-search-div-1').show(1000);

                    }, 200);

                }, 200);
            } else {
                $(".table-car-rent").animate({width: '366px'}, 1000);
                $(".button-car-s").animate({width: '297px'}, 1000);
                $('#see_more_filter').text('MORE');
                $('.see-more-filter').val(0);

                $('.more-search-div-1').hide(1000);

                setTimeout(function () {
                    $('.more-search-div').hide(800);
                }, 200);

            }
        })
    </script>


    <style>
        .owl-nav {
            width: 96%;
            top: 41%;
            position: absolute;
            font-size: 30px;
        }
        .owl-carousel{
            width: 80%;
        }
        .h4-price-car{
            margin-top: 0px;
        }
        .body-image {
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
        }
        .div-by-days-car{
            width: 100%;
        }
        .div-by-car-car {
            background-color: transparent !important;
            border-bottom: none !important;
        }

    </style>


@endsection
