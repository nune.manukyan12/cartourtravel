@extends('layouts.app')

@section('content')

    @foreach($carSeats as $car)


        <div class="row div-by-car-car">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <h2 class="car-name-car">{{$car->name}}</h2>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <img class="img-apartment" src="{{asset('uploads/'.$car->general_pic)}}" alt=""
                                 style="width: 450px; height: 300px;">
                        </div>
                        <div class="row div-row-car-detail">
                            <div class="col-sm-6">


                            </div>
                            <div class="col-sm-6"></div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="row">

                            <div class="col-sm-6 price-col-color">

                                <h2 class="h4-price-car">{{$car->car_seat_price}} <?php if ($car->currency == 'usd') {
                                        echo 'USD';
                                    } elseif ($car->currency == 'amd') {
                                        echo 'AMD';
                                    } elseif ($car->currency == 'euro') {
                                        echo 'EURO';
                                    } elseif ($car->currency == 'rub') {
                                        echo 'RUB';
                                    } ?></h2>
                                <p>(per day)</p>
                                <button type="button" class="btn btn-success book-button" id="{{$car->id}}">Add
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <?php echo $car->description;?>

                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>

    @endforeach


    <div class="modal fade" id="book-car" role="dialog">
        <div class="modal-dialog book-dialog">
            <!-- Modal content-->
            <div class="modal-content book-content">
                <div class="modal-header">
                    <h4 class="modal-title">Please enter your info for booking. </h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="div-modal-book div-first-block ">
                                <label for="">Last name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control" id="last-name" placeholder="Last name">
                                <p class="error-last-name"></p>

                            </div>
                            <div class="div-modal-book div-second-block ">
                                <label for="">First name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control" id="first-name" placeholder="First name">
                                <p class="error-first-name"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="div-modal-book div-first-block ">
                                <label for="">Email address<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="email" class="form-control" id="email" placeholder="Email address">
                                <p class="error-email"></p>

                            </div>
                            <div class="div-modal-book div-second-block">
                                <label for="">Phone<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control" id="phone" placeholder="Phone">
                                <p class="error-phone"></p>

                            </div>
                        </div>
                        <div>
                            <label for="">Viber:</label>
                            <input type="text" class="form-control" id="viber" placeholder="Viber">
                        </div>
                        <div class="">
                            <label for="">Telegram:</label>
                            <input type="text" class="form-control" id="telegram" placeholder="Telegram">
                        </div>
                        <div class="">
                            <label for="">Whatsapp:</label>
                            <input type="text" class="form-control" id="whatsapp" placeholder="Whatsapp">
                        </div>
                        <p class="date-from"></p>
                        <p class="date-to"></p>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="book-book">Book</button>
                </div>
            </div>

        </div>
    </div>

    {{--Modal error return and puke up date--}}
    <div class="modal fade" id="modal_error" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <p class="error-puke-up-date-car"></p>
                    <p class="error-return-date-car"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth() + 1;
            var y = date.getFullYear();
            $(".input-by-pick-up").attr("placeholder", d + '/' + m + '/' + y);
            $(".input-by-return").attr("placeholder", d + '/' + m + '/' + y);

        });
        $('#datepicker-pick-up').Zebra_DatePicker({
            always_visible: $('#datepicker-pick-up-div'),
            format: 'd/m/Y',
            custom_classes: {
                'not-close-calendar': ['1-31']
            }
        });
        $('#datepicker-return').Zebra_DatePicker({
            always_visible: $('#datepicker-return-div'),
            format: 'd/m/Y',
            custom_classes: {
                'not-close-calendar': ['1-31']
            }
        });
        $('.all-body').on('click', function (e) {
            if (!$(e.target).hasClass('not-close-calendar')) {

                if ($('.input-hidden-by-return').val() > 0) {
                    $('#datepicker-return-div').css('display', 'none');
                    $('.input-hidden-by-return').val(0);

                }
                if ($('.input-hidden-by-pick-up').val() > 0) {
                    $('#datepicker-pick-up-div').css('display', 'none');
                    $('.input-hidden-by-pick-up').val(0);
                }

            }
        });
        $('.input-by-return').click(function () {
            if ($('.input-hidden-by-return').val() < 1) {
                $('#datepicker-return-div').css('display', 'block');
                $('.input-hidden-by-return').val(1);
            }
            if ($('.input-hidden-by-pick-up').val() > 0) {
                $('#datepicker-pick-up-div').css('display', 'none');
                $('.input-hidden-by-pick-up').val(0);
            }
        });
        $('.input-by-pick-up').click(function () {

            if ($('.input-hidden-by-pick-up').val() < 1) {
                $('#datepicker-pick-up-div').css('display', 'block');
                $('.input-hidden-by-pick-up').val(1);
            }
            if ($('.input-hidden-by-return').val() > 0) {
                $('#datepicker-return-div').css('display', 'none');
                $('.input-hidden-by-return').val(0);

            }
        });


        $(document).ready(function () {
            $('.timepicker-pick-up').qcTimepicker({
                'format': 'H:mm',
                'minTime': '0:00:00',
                'maxTime': '23:59:59',
                'step': '0:30:00',
                'placeholder': '12:00'
            });
        });
        $(document).ready(function () {
            $('.timepicker-return').qcTimepicker({
                'format': 'H:mm',
                'minTime': '0:00:00',
                'maxTime': '23:59:59',
                'step': '0:30:00',
                'placeholder': '12:00'
            });
        });
        var id;
        $(".book-button").click(function () {
            $("#book-car").modal("show");
            id = $(this).attr('id');

        });

        $('#book-book').click(function () {
            var firstName = $('#first-name').val();
            var lastName = $('#last-name').val();
            var email = $('#email').val();
            var phone = $('#phone').val();
            var viber = $('#viber').val();
            var telegram = $('#telegram').val();
            var whatsapp = $('#whatsapp').val();
            var pick_up_locations = $('#pick_up_locations').find(":selected").text();
            var return_locations = $('#return_locations').find(":selected").text();
            var pick_up_date = $('#datepicker-pick-up').val();
            var return_date = $('#datepicker-return').val();
            var pick_up_time = $('#qcTimepicker-0').find(":selected").text();
            var return_time = $('#qcTimepicker-1').find(":selected").text();
            $.ajax({
                url: '/reservation/store_car',
                type: 'POST',
                dataType: "JSON",
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: {

                    last_name: lastName,
                    first_name: firstName,
                    email: email,
                    viber: viber,
                    telegram: telegram,
                    whatsap: whatsapp,
                    phone: phone,
                    car_id: id,
                    pick_up_locations: pick_up_locations,
                    return_locations: return_locations,
                    pick_up_date: pick_up_date,
                    return_date: return_date,
                    pick_up_time: pick_up_time,
                    return_time: return_time,

                },
                success: function (success) {
                    // console.log('Yes');
                    // $("#book-Modal").modal("hide");
                    // $("#modal-messenger").modal("show");
                    // setTimeout(function () {
                    //     $("#modal-messenger").modal("hide");
                    // }, 10000);
                },
                error: function (errors) {

                    if (errors.responseJSON.errors.last_name) {
                        $(".error-last-name").html(errors.responseJSON.errors.last_name);
                    }
                    if (errors.responseJSON.errors.first_name) {
                        $(".error-first-name").html(errors.responseJSON.errors.first_name);
                    }
                    if (errors.responseJSON.errors.email) {
                        $(".error-email").html(errors.responseJSON.errors.email);
                    }
                    if (errors.responseJSON.errors.phone) {
                        $(".error-phone").html(errors.responseJSON.errors.phone);
                    }
                    if (errors.responseJSON.errors.pick_up_date || errors.responseJSON.errors.return_date) {

                        $("#modal_error").modal("show");
                        if (errors.responseJSON.errors.pick_up_date) {
                            $(".error-puke-up-date-car").html(errors.responseJSON.errors.pick_up_date);

                        }
                        console.log(errors.responseJSON.errors.return_date);
                        if (errors.responseJSON.errors.return_date) {
                            $(".error-return-date-car").html(errors.responseJSON.errors.return_date);

                        }
                        setTimeout(function () {
                            $("#modal_error").modal("hide");
                        }, 5000);
                    }


                }
            })
        });
        $('#see_more_filter').click(function () {
            if ($('.see-more-filter').val() == 0) {
                $(".table-car-rent").animate({width: '700px'}, 1000);
                $(".button-car-s").animate({width: '640px'}, 1000);
                $('#see_more_filter').text('CLOSE');
                $('.see-more-filter').val(1);
                // $('.more-search-detalis-car').show(500);
                setTimeout(function () {
                    $('.more-search-div').show(700);
                    setTimeout(function () {
                        $('.more-search-div-1').show(1000);

                    }, 200);

                }, 200);
            } else {
                $(".table-car-rent").animate({width: '366px'}, 1000);
                $(".button-car-s").animate({width: '297px'}, 1000);
                $('#see_more_filter').text('MORE');
                $('.see-more-filter').val(0);

                $('.more-search-div-1').hide(1000);

                setTimeout(function () {
                    $('.more-search-div').hide(800);
                }, 200);

            }
        })
    </script>





@endsection
