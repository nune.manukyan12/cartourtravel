@extends('layouts.app')

@section('content')
        <div class="page_top_wrap">
            <h1 class="page_top_title">
                Contact us </h1>

            {{--<h2 class="page_top_desc">--}}
            {{--Affordable, comfortable, enjoyable </h2>--}}
        </div>

        <section class="sections_content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 cust_link_cols">
                        <div class="content_left_link_wrap">
                            <div class="title">
                                Products &amp; Services
                            </div>
                            <a class="item active_link" href="{{asset('contact')}}">
                                Contact Us </a>
                            <a class="item" href="{{asset('about')}}">
                                About Us </a>

                        </div>
                    </div>

                    <div class="col-lg-9 col-md-8">
                        <div class="content_right">
                            <form id="mainForm">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">First name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                        <input type="text" class="form-control" id="first-name" name="first_name" placeholder="First name">
                                        <p class="error-first-name"></p>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Last name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                        <input type="text" class="form-control" id="last-name" name="last_name" placeholder="Last name">
                                        <p class="error-last-name"></p>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Email address<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                        <input type="email" class="form-control email" id="email" name="email" placeholder="Email address">
                                        <p class="error-email"></p>

                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Phone<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                        <input type="text" class="form-control phone" id="phone" name="phone" placeholder="Phone">
                                        <p class="error-phone"></p>

                                    </div>
                                </div>
                                <div>
                                    <textarea class="form-control" name="message" cols="50" rows="10"></textarea>
                                </div>

                                    <button type="button" class="btn btn-primary pull-right" id="contact">Send</button>

                            </form>
                            <div class="main_content_text">
                                <p style="margin-left:.25in;">

                                        <h4 class="comp-tour-travel">Car Tour Travel Club</h4>
                                <p><span class="tell-mail-apartment">Tell:</span> +37498421212</p>
                                <p><span class="tell-mail-apartment">Email:</span> cartourtravel.info@gmail.com</p>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
            $('#contact').click(function () {
                var data = $('#mainForm').serializeArray();
                $.ajax({
                    url: '/contacts/email',
                    type: 'POST',
                    dataType: "JSON",
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                    data: data,
                    success: function (success) {
                        // console.log('Yes');
//                        $("#book-car").modal("hide");
//                        $("#modal-messenger").modal("show");
//                        setTimeout(function () {
//                            $("#modal-messenger").modal("hide");
//                        }, 10000);
                    },
                    error: function (errors) {

//                        if (errors.responseJSON.errors.last_name) {
//                            $(".error-last-name").html(errors.responseJSON.errors.last_name);
//                        }
//                        if (errors.responseJSON.errors.first_name) {
//                            $(".error-first-name").html(errors.responseJSON.errors.first_name);
//                        }
//                        if (errors.responseJSON.errors.email) {
//                            $(".error-email").html(errors.responseJSON.errors.email);
//                        }
//                        if (errors.responseJSON.errors.phone) {
//                            $(".error-phone").html(errors.responseJSON.errors.phone);
//                        }
//                        if (errors.responseJSON.errors.pick_up_date || errors.responseJSON.errors.return_date) {
//
//                            $("#modal_error").modal("show");
//                            if (errors.responseJSON.errors.pick_up_date) {
//                                $(".error-puke-up-date-car").html(errors.responseJSON.errors.pick_up_date);
//
//                            }
//                            console.log(errors.responseJSON.errors.return_date);
//                            if (errors.responseJSON.errors.return_date) {
//                                $(".error-return-date-car").html(errors.responseJSON.errors.return_date);
//
//                            }
//                            setTimeout(function () {
//                                $("#modal_error").modal("hide");
//                            }, 5000);
//                        }


                    }
                })
            });
        </script>
    <style>
        .page_top_wrap {
            text-align: center;
            padding-top: 5%;
            padding-bottom: 22px;
        }

        .page_top_wrap .page_top_title {
            color: #343434;
            font-size: 34px;
            font-family: 'sans_semibold';
            line-height: 46px;
            margin: 0;
            padding: 0;
        }

        .page_top_wrap .page_top_desc {
            color: #343434;
            font-family: 'sans_reg';
            font-size: 20px;
            line-height: 28px;
            margin: 0;
            padding: 0;
        }

        .sections_content {
            border-top: 6px solid #2979b2;
        }

        .content_left_link_wrap {
            background-color: #ededed;
        }

        .content_left_link_wrap .title {
            color: #343434;
            font-family: 'sans_semibold';
            font-size: 20px;
            padding-left: 23px;
            padding-bottom: 9px;
            padding-top: 8px;
            border-bottom: 2px solid #ddd;
        }

        .content_left_link_wrap .item {
            display: block;
            text-decoration: none;
            color: #525252;
            font-size: 16px;
            line-height: 20px;
            font-family: 'sans_semibold';
            padding-left: 30px;
            padding-right: 10px;
            padding-top: 15px;
            padding-bottom: 16px;
            border-bottom: 1px solid #ddd;
        }

        .content_left_link_wrap .active_link {
            background-color: white;
            color: #2979b2;
        }
    </style>
@endsection
