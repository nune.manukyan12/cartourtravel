<footer class="footer" role="contentinfo">
    <div id="footer-sidebar" class="footer-sidebar widget-area wrap" role="complementary">
        <ul style="display: flex;justify-content: space-around;">
            <li class="widget widget-sidebar one-fourth">
                <article class="byt_address_widget BookYourTravel_Address_Widget">
                    <div class="col-sm-12" style="margin-top: 7%">
                        <div class="col-sm-3 footer-logo">
                            <div style="float: right">
                                <img src="{{ asset("img/ctt_main_png.png")}}" alt="" class="img-by-admin">
                            </div>
                        </div>

                        <div class="col-sm-9">
                            <h4 class="comp-tour-travel">Car Tour Travel Club</h4>
                            <p><span class="tell-mail-apartment">Tell:</span> +37498421212</p>
                            <p><span class="tell-mail-apartment">Email:</span>cartourtravel.info@gmail.com</p>
                        </div>
                    </div>
                </article>
            </li>
            <li class="widget widget-sidebar one-fourth"><h6>Subscribe to our Newsletter</h6>
                <div class="widget_wysija_cont">
                    <div id="msg-form-wysija-2" class="wysija-msg ajax"></div>
                    <form id="form-wysija-2" method="post" action="#wysija" class="widget_wysija">
                        <p class="wysija-paragraph">
                            <label>Email <span class="wysija-required">*</span></label>

                            <input type="text" name="rmail"
                                   class="wysija-input validate[required,custom[email]]" title="Email" value="">

                                                    </p>

                        <input class="wysija-submit wysija-submit-field" type="submit" value="Subscribe!">

                        <input type="hidden" name="form_id" value="1">
                        <input type="hidden" name="action" value="save">
                        <input type="hidden" name="controller" value="subscribers">
                        <input type="hidden" value="1" name="wysija-page">


                        <input type="hidden" name="wysija[user_list][list_ids]" value="1">

                    </form>
                </div>
            </li>
        </ul>
    </div><!-- #secondary -->

</footer>