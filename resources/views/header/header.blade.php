<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #ffffffa8;">
    <a href="{{asset('/')}}" style="margin-left: 2%;

"><img src="{{ asset("img/ctt_main_png.png")}}" alt="CarTourTravel" style="height: 60px;width: 90px;object-fit: contain">

    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav" style="margin-left: auto">
            <li class="home">
                <a class="nav-link mega-menue-link" href="{{asset('/')}}"><span
                            class="flag-icon flag-icon-gr"></span>@lang('header.home')<span
                            class="sr-only">(current)</span></a>
            </li>
            <li class="">
                <a class="nav-link mega-menue-link" href="{{asset('/apartments')}}">@lang('header.apartment')<span
                            class="sr-only">(current)</span></a>
            </li>
            <li class="">
                <a class="nav-link mega-menue-link" href="{{asset('/hotels')}}">@lang('header.hotel')<span
                            class="sr-only">(current)</span></a>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle nav-link mega-menue-link" data-toggle="dropdown" href="#"
                   aria-expanded="true">@lang('header.tour')</a>
                <ul class="dropdown-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{asset('/tours/'. \App\Models\Tour::TYPE_REGULAR)}}"
                           title="Regular Tours">Regular Tours</a></li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{asset('/tours/'.\App\Models\Tour::TYPE_PRIVATE)}}"
                           title="Private Tours">Private Tours</a></li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{asset('/tours/'.\App\Models\Tour::TYPE_EXTREAM)}}"
                           title="Extream Tours">Extream Tours</a></li>
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{asset('/tours/packages')}}"--}}
                           {{--title="Packages Tours">Ready Packages</a></li>--}}
                    <li class="nav-item">
                        {{--<a class="nav-link crete-tour"--}}
                        {{--title="Create Your Tour Package">Create Your Tour Package</a>--}}
                        <a class="nav-link" href="{{asset('/tour/package/create')}}"
                           title="Create Your Tour Package">Create Your Tour Package</a>
                    </li>
                </ul>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle nav-link mega-menue-link" data-toggle="dropdown" href="#"
                   aria-expanded="true">@lang('header.car')</a>
                <ul class="dropdown-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{asset('/cars')}}">Car Rental</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{asset('/transfer')}}">Transfers & Chauffeur Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{asset('/car-seats')}}">Car Seats</a>
                    </li>
                </ul>
            </li>
            <li >
                <a class="nav-link mega-menue-link" href="{{asset('/medical/service')}}">@lang('header.medical')</a>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle nav-link mega-menue-link" data-toggle="dropdown" href="#"
                   aria-expanded="true">@lang('header.info')</a>
                <ul class="dropdown-menu " style="left: -95px">
                    <li class="nav-item">
                        <a class="nav-link" target="_blank" href="https://en.wikipedia.org/wiki/Armenia">About
                            Armenia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" target="_blank"
                           href="https://www.accuweather.com/en/am/yerevan/16890/weather-forecast/16890">Wheater in
                            Armenia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" target="_blank"
                           href="http://rate.am/en/armenian-dram-exchange-rates/banks/cash">Currency</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{asset('/about')}}">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{asset('/contact')}}">Contact us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{asset('/news')}}">News feed</a>
                    </li>

                </ul>
            </li>

            <li style="margin: 10px 0px; padding: 0px">
                @if($local_lang = \App::getLocale())
                    <a
                            href="{{'/lang/'.$local_lang}}" title="{{$local_lang}}">
                        <img style="height: 20px;" src="{{asset("img/".$local_lang.".png")}}"
                             alt="CarTourTravel"></a>
                @else
                    <a
                            href="{{'/lang/en'}}" title="{{'en'}}">
                        <img style="height: 20px;" src="{{asset("img/en.png")}}"
                             alt="CarTourTravel"></a>
                @endif
            </li>
            <?php $currency = session()->get('currency');?>
            @if($currency)
                <li style="margin: 10px 10px"><a href="{{'/currency/'.$currency}}" title="USD"><span class="text-uppercase">{{$currency}}</span></a></li>
            @else
                <li style="margin: 10px 10px"><a href="{{'/currency/USD'}}" title="USD">USD</a></li>
            @endif
        </ul>
    </div>
</nav>
<style>
    .active {
        color: #2579a9 !important;
    }

    ul > li {
        float: left;
    }

    ul {
        list-style-type: none;
    }

    .language-ul li {
        width: 20%;
    }

    ul > li > a {
        color: rgba(41, 32, 32, 0.99);
    }

    .menu-active {
        text-align: center;
        padding: 2px;
        background-color: #ffffffa8;
    }

    .menu-active img {
        width: 30px;
    }
</style>
<script>
    $(".navbar-nav a").click(function (e) {
        var link = $(this);
        var item = link.parent("li");
        if (item.hasClass("active")) {
            item.removeClass("active").children("a").removeClass("active");
        } else {
        }

    })
        .each(function () {
            var link = $(this);
            if (link.get(0).href === location.href) {
                link.addClass("active").parents("li").addClass("active");
                return false;
            }
        });
    $('.ribbon').hover(function () {
            $(this).find('ul.menu').css({'visibility': 'initial', 'top': '45px', 'left': '255px'})
        },
        function () {
            $(this).find('ul.menu').css({'visibility': 'hidden', 'top': '45px', 'left': '255px'})
        }
    );

</script>
<style>
    .pic-figure img {
        width: 100%;
        height: 100%;
    }
</style>

