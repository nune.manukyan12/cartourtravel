@extends('layouts.app')

@section('content')
    @include('_partial._global_search')
    <div class="container">
        <div class="row">
            <section class="full-width">
                <article class="post-477 page type-page status-publish hentry" id="page-477">
                </article>

                <section class="home-content-sidebar">
                    <ul class="ul-sidebar">
                        <li class="widget widget-sidebar"><!--deals-->
                            <div class="offers">
                                <header class="s-title">
                                    <h2 class="title-h2">Explore our latest Apartment</h2></header>
                                <div class="row"><!--post-->
                                    @foreach($apartments as $apartment)

                                        <article class="accommodation_item one-fourth">
                                            <div>
                                                <figure class="pic-figure" style="position: relative">
                                                    <a href="{{asset('apartment/'.$apartment->id)}}" target="_blank"
                                                       title="City loft">
                                                        <img src="{{asset('uploads/'.$apartment->general_pic)}}"
                                                             alt="City loft">

                                                        <hr>
                                                    </a>
                                                </figure>
                                                <div class="details">
                                                    <a href="{{asset('apartment/'.$apartment->id)}}" class="a-no-style"
                                                       target="_blank">
                                                        <p class="apartment-name">{{$apartment->name}}</p>
                                                    </a>
                                                    <i class="fas fa-map-marker-alt map-icon"></i><span
                                                            class="address">  {{$apartment->country.' '. $apartment->city.' '.$apartment->address}}</span>
                                                    <hr>
                                                    {{--<div class="price-block">--}}

                                                        {{--@if(isset($apartment->prices[0]))--}}
                                                            {{--<span class="amount">--}}
                                                                {{--<span class="curr">{{ (\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? strtoupper(session()->get('currency_symbol')).' ' : '$ '}}</span>--}}
                                                                {{--{{(\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price):$apartment->prices[0]->base_price}}--}}
                                                            {{--</span>--}}
                                                        {{--@endif--}}

                                                    {{--</div>--}}
                                                    {{--<hr>--}}
                                                    {{--<div >--}}
                                                        {{--<a href="{{asset('apartment/'.$apartment->id)}}" target="_blank"--}}
                                                           {{--class="a-no-style">--}}
                                                            {{--<p class="with_read_more_dots">{{$apartment->description}}</p>--}}
                                                        {{--</a>--}}

                                                    {{--</div>--}}

                                                    <div class="clearfix apartment-parameter-div">
                                                        <div class="text-wrap d-flex">
                                                            <span class="apartment-parameter">Max people</span>{{$apartment->details[0]->number_of_quests}}
                                                        </div>
                                                        <div class="text-wrap d-flex">
                                                            <span class="apartment-parameter">Bedroom</span>{{$apartment->details[0]->total_number_of_bedrooms}}
                                                        </div>
                                                        @if(isset($apartment->prices[0]))
                                                        <div class="text-wrap d-flex curr">
                                                            <span class="apartment-parameter">Per day</span>
                                                            {{ (\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? strtoupper(session()->get('currency_symbol')).' ' : '$ '}}
                                                        {{(\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price))? \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$apartment->prices[0]->base_price):$apartment->prices[0]->base_price}}
                                                        </div>
                                                        @endif
                                                        {{--<div class="text-wrap d-flex">--}}
                                                            {{--<span class="apartment-parameter">Wifi</span>{{$apartment->details[0]->wifi}}--}}
                                                        {{--</div>--}}
                                                        {{--<div class="text-wrap d-flex">--}}
                                                            {{--<span class="apartment-parameter">Conditioner</span>{{($apartment->details[0]->central_cooling == 'yes' || $apartment->details[0]->air_conditioning == 'yes')?'yes':'no'}}--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    <hr>
                                                    <div class="link-more">
                                                        <a class="text-center" target="_blank"
                                                           href="{{asset('apartment/'.$apartment->id)}}">More info</a>
                                                    </div>
                                                    {{--<div class="actions">--}}
                                                        {{--<div class="line-black"></div>--}}
                                                        {{--<a href="{{asset('apartment/'.$apartment->id)}}" target="_blank"--}}
                                                           {{--class="gradient-button clearfix" title="Book now">Book--}}
                                                            {{--now</a>--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>
                                        </article>
                                @endforeach
                                <!--//post-->
                                </div>
                            </div><!--//deals--></li>
                        <li class="widget widget-sidebar"><!--deals-->
                        {{--<div class="deals">--}}
                        {{--<header class="s-title"><h2 class="title-h2">Explore our latest hotels</h2></header>--}}
                        {{--<div class="row"><!--accommodation item-->--}}
                        {{--@foreach($hotels as $hotel)--}}
                        {{--<article class="accommodation_item one-fourth">--}}
                        {{--<div>--}}
                        {{--<figure class="pic-figure">--}}
                        {{--<a href="{{asset('hotel/'.$hotel->id)}}"--}}
                        {{--title="City loft">--}}
                        {{--<img src="{{asset('uploads/'.$hotel->general_pic)}}"--}}
                        {{--alt="City loft">--}}
                        {{--</a>--}}
                        {{--</figure>--}}
                        {{--<div class="details details-hotel">--}}
                        {{--<p class="apartment-name">--}}
                        {{--{{$hotel->name}}<span class="stars"></span>--}}
                        {{--</p>--}}
                        {{--<span class="address">{{$hotel->country.' '. $hotel->city.' '.$hotel->address}}</span>--}}
                        {{--<div class="price price-hotel">--}}
                        {{--Price from <em>--}}
                        {{--@if(isset($hotel->prices[0]))--}}

                        {{--@if($hotel->prices[0]->single_price)--}}
                        {{--<div>--}}
                        {{--Single <span class="amount">{{$hotel->prices[0]->single_price}}<span class="curr">$</span></span>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--@if($hotel->prices[0]->double_price)--}}
                        {{--<div>--}}

                        {{--Double/Twin <span class="amount">{{$hotel->prices[0]->double_price}}<span class="curr">$</span></span>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--@if($hotel->prices[0]->superior_price)--}}
                        {{--<div>--}}

                        {{--Superior Single<span class="amount">{{$hotel->prices[0]->superior_price}}<span class="curr">$</span></span>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--@if($hotel->prices[0]->superior_double_price)--}}
                        {{--<div>--}}

                        {{--Superior Double <span class="amount">{{$hotel->prices[0]->superior_double_price}}<span class="curr">$</span></span>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--@if($hotel->prices[0]->family_price)--}}
                        {{--<div>--}}

                        {{--Family Suite<span class="amount">{{$hotel->prices[0]->family_price}}<span class="curr">$</span></span>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--@if($hotel->prices[0]->executive_price)--}}
                        {{--<div>--}}

                        {{--Executive Suite <span class="amount">{{$hotel->prices[0]->executive_price}}<span class="curr">$</span></span>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--@if($hotel->prices[0]->deluxe_price)--}}
                        {{--<div>--}}

                        {{--Deluxe Single  <span class="amount">{{$hotel->prices[0]->deluxe_price}}<span class="curr">$</span></span>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--@if($hotel->prices[0]->deluxe_double_price)--}}
                        {{--<div>--}}

                        {{--Deluxe Double  <span class="amount">{{$hotel->prices[0]->deluxe_double_price}}<span class="curr">$</span></span>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--@else--}}
                        {{--<span class="amount">see</span>--}}
                        {{--@endif--}}
                        {{--</em>--}}
                        {{--</div>--}}
                        {{--<div class="info-data">--}}
                        {{--<p class="with_read_more_dots">{{$hotel->description}}</p>--}}
                        {{--<a href="{{asset('hotel/'.$hotel->id)}}">More info</a>--}}
                        {{--</div>--}}
                        {{--<div class="actions"><a--}}
                        {{--href="{{asset('hotel/'.$hotel->id)}}"--}}
                        {{--class="gradient-button clearfix" title="Book now">Book--}}
                        {{--now</a>--}}
                        {{--</div>--}}


                        {{--<div class="link-more">--}}
                        {{--<a class="text-center" href="{{asset('hotel/'.$hotel->id)}}">More info</a>--}}
                        {{--</div>--}}
                        {{--<div class="actions">--}}
                        {{--<div class="line-black"></div>--}}
                        {{--<a href="{{asset('hotel/'.$hotel->id)}}"--}}
                        {{--class="gradient-button clearfix" title="Book now">Book now</a>--}}
                        {{--</div>--}}







                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</article>--}}
                        {{--@endforeach--}}
                        {{--<!--//accommodation item-->--}}
                        {{--</div>--}}
                        {{--</div><!--//deals--></li>--}}
                        <li class="widget widget-sidebar"><!--deals-->
                            <div class="deals">
                                <header class="s-title">
                                    <h2 class="title-h2">Explore our latest tours</h2></header>
                                <div class="row"><!--tour item-->
                                    @foreach($tours as $tour)
                                        <article class="accommodation_item one-fourth">
                                            <div>
                                                <figure class="pic-figure">
                                                    <a href="{{'tours/'.$tour->type_id}}" target="_blank"
                                                       title="City loft">
                                                        <img src="{{asset('uploads/'.$tour->general_pic)}}"
                                                             alt="City loft">
                                                    </a>
                                                </figure>
                                                <div class="details">
                                                    <a href="{{asset('tours/'.$tour->type_id)}}" class="a-no-style"
                                                       target="_blank">
                                                        <p class="apartment-name">{{$tour->name}}<span
                                                                    class="stars"></span>
                                                        </p>
                                                    </a>
                                                    <i class="fas fa-map-marker-alt map-icon"></i>
                                                    <span class="address">{{$tour->country.' '. $tour->city.' '.$tour->address}}</span>
                                                    {{--<hr>--}}
                                                    {{--<div class="price">--}}
                                                    {{--Price from <em>--}}
                                                    {{--@if(isset($tour->prices[0]))--}}
                                                    {{--<span class="curr">$</span>--}}
                                                    {{--<span class="amount">{{$tour->prices[0]->price}}</span>--}}
                                                    {{--@else--}}
                                                    {{--<span class="amount">see</span>--}}
                                                    {{--@endif--}}
                                                    {{--</em>--}}
                                                    {{--</div>--}}
                                                    <hr>
                                                    {{--<div class="info-data">--}}
                                                    {{--<a href="{{asset('tour/'.$tour->type_id)}}" class="a-no-style" target="_blank">--}}
                                                    {{--<p class="with_read_more_dots">{{$tour->description}}</p>--}}
                                                    {{--</a>--}}
                                                    {{--<a href="{{'tour/'.$tour->id}}">More info</a>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="actions"><a--}}
                                                    {{--href="{{'tour/'.$tour->id}}"--}}
                                                    {{--class="gradient-button clearfix" title="Book now">Book now</a>--}}
                                                    {{--</div>--}}


                                                    <div class="link-more">
                                                        <a class="text-center" href="{{'tours/'.$tour->type_id}}"
                                                           target="_blank">More
                                                            info</a>
                                                    </div>
                                                    {{--<div class="actions">--}}
                                                    {{--<div class="line-black"></div>--}}
                                                    {{--<a href="{{'tours/'.$tour->type_id}}" target="_blank"--}}
                                                    {{--class="gradient-button clearfix" title="Book now">Book--}}
                                                    {{--now</a>--}}
                                                    {{--</div>--}}


                                                </div>
                                            </div>
                                        </article>
                                    @endforeach

                                </div>
                            </div><!--//deals--></li>
                        <li class="widget widget-sidebar"><!--deals-->
                            <div class="deals">
                                <header class="s-title">
                                    <h2 class="title-h2">Explore our latest cars</h2></header>
                                <div class="row"><!--cruise item-->
                                    @foreach($cars as $car)
                                        <article class=" accommodation_item  car_rental_item one-fourth">
                                            <div>
                                                <figure class="pic-figure">
                                                    <a href="{{asset('cars')}}" target="_blank"
                                                       title="Audi A4">
                                                        <img src="{{asset('uploads/'.$car->general_pic)}}"
                                                             alt="Audi A4">
                                                    </a>
                                                </figure>
                                                <div class="details-car cars">
                                                    <a href="{{asset('cars')}}" class="a-no-style" target="_blank">
                                                        <p class="apartment-name">{{$car->name.' ' }}or similar</p>
                                                    </a>
                                                    {{--<div class="price-block">--}}

                                                            {{--<span class="curr"></span>--}}
                                                            {{--<span class="amount">--}}
                                                                {{--<span class="curr">{{ (\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$car->car_day_prices))? strtoupper(session()->get('currency_symbol')).' ' : '$ '}}</span>--}}
                                                                {{--{{(\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$car->car_day_prices))? \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$car->car_day_prices) : $car->car_day_prices}}--}}
                                                            {{--</span>--}}

                                                    {{--</div>--}}
                                                    <hr>
                                                    <div class="description-car clearfix ">
                                                        <div class="text-wrap car_type">
                                                            <span >Car type</span>{{$car->type->name}}
                                                        </div>
                                                        <div class="text-wrap max_people">
                                                            <span class="car-parameter">Max people</span>{{$car->persons}}
                                                        </div>
                                                        {{--<div class="text-wrap door_count">--}}
                                                        {{--<span class="car-parameter">Door count</span>{{$car->door_count}}--}}
                                                        {{--</div>--}}
                                                        {{--<div class="text-wrap transmission">--}}
                                                            {{--<span class="car-parameter">Transmission</span>{{$car->transmission}}--}}
                                                        {{--</div>--}}
                                                        {{--<div class="text-wrap winter_wheel">--}}
                                                        {{--<span class="car-parameter">Winter Wheel</span>{{$car->winter_wheel}}--}}
                                                        {{--</div>--}}
                                                        {{--<div class="text-wrap conditioner">--}}
                                                            {{--<span class="car-parameter">Conditioner</span>{{$car->conditioner}}--}}
                                                        {{--</div>--}}

                                                        @if(isset($car->car_day_prices))
                                                            <div class="text-wrap d-flex curr">
                                                                <span class="apartment-parameter">Per day</span>
                                                                {{ (\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$car->car_day_prices))? strtoupper(session()->get('currency_symbol')).' ' : '$ '}}
                                                                {{(\App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$car->car_day_prices))? \App\Services\ForgeApi::changeCurrencyFromUsd(session()->get('currency'),$car->car_day_prices) : $car->car_day_prices}}
                                                            </div>
                                                        @endif
                                                    </div>

                                                    <hr>

                                                    <div class="link-more">
                                                        <a class="text-center" href="{{'cars'}}">More info</a>
                                                    </div>
                                                    {{--<div class="actions">--}}
                                                        {{--<div class="line-black"></div>--}}
                                                        {{--<a href="{{'cars'}}"--}}
                                                           {{--class="gradient-button clearfix" title="Book now">Book--}}
                                                            {{--now</a>--}}
                                                    {{--</div>--}}


                                                </div>
                                            </div>
                                        </article>
                                    @endforeach
                                </div>
                            </div><!--//deals--></li>
                        <li class="widget widget-sidebar"><!--deals-->
                            <div class="deals">
                                <header class="s-title">
                                    @if(count($transfers))
                                        <h2 class="title-h2">Explore our latest transfers</h2></header>
                                @endif
                                <div class="row"><!--tour item-->
                                    @foreach($transfers as $transfer)
                                        <article class="accommodation_item one-fourth">
                                            <div>
                                                <figure class="pic-figure">
                                                    <a href="{{'transfer'}}" target="_blank"
                                                       title="City loft">
                                                        <img src="{{asset('uploads/'.$transfer->general_pic)}}"
                                                             alt="City loft">
                                                    </a>
                                                </figure>
                                                <div class="details">
                                                    <a href="{{asset('transfer')}}" class="a-no-style" target="_blank">
                                                        <p class="apartment-name">{{$transfer->name}}<span
                                                                    class="stars"></span>
                                                        </p>
                                                    </a>
                                                    <i class="fas fa-map-marker-alt map-icon"></i>
                                                    <span class="address">{{$transfer->country.' '. $transfer->city.' '.$transfer->address}}</span>
                                                    {{--<hr>--}}
                                                    {{--<div class="price">--}}
                                                    {{--Price from <em>--}}
                                                    {{--@if(isset($tour->prices[0]))--}}
                                                    {{--<span class="curr">$</span>--}}
                                                    {{--<span class="amount">{{$tour->prices[0]->price}}</span>--}}
                                                    {{--@else--}}
                                                    {{--<span class="amount">see</span>--}}
                                                    {{--@endif--}}
                                                    {{--</em>--}}
                                                    {{--</div>--}}
                                                    <hr>
                                                    <div class="info-data">
                                                        <a href="{{asset('transfer')}}" class="a-no-style"
                                                           target="_blank">
                                                            <p class="with_read_more_dots">{{$transfer->description}}</p>
                                                        </a>
                                                        {{--<a href="{{'tour/'.$tour->id}}">More info</a>--}}
                                                    </div>
                                                    {{--<div class="actions"><a--}}
                                                    {{--href="{{'tour/'.$tour->id}}"--}}
                                                    {{--class="gradient-button clearfix" title="Book now">Book now</a>--}}
                                                    {{--</div>--}}


                                                    <div class="link-more">
                                                        <a class="text-center" href="{{'transfer'}}" target="_blank">More
                                                            info</a>
                                                    </div>
                                                    {{--<div class="actions">--}}
                                                    {{--<div class="line-black"></div>--}}
                                                    {{--<a href="{{'tours/'.$tour->type_id}}" target="_blank"--}}
                                                    {{--class="gradient-button clearfix" title="Book now">Book--}}
                                                    {{--now</a>--}}
                                                    {{--</div>--}}


                                                </div>
                                            </div>
                                        </article>
                                    @endforeach

                                </div>
                            </div><!--//deals--></li>

                    </ul>
                </section><!-- #secondary -->

            </section>
        </div>
        <nav class="page-navigation bottom-nav">
            <!--back up button-->
            <a class="scroll-to-top pull-right" title="Back up"><i class="fas fa-arrow-up"></i></a>
            <!--//back up button-->
            <!--pager-->
            <div class="pager">
            </div>
        </nav>
    </div>
    <style>
        hr{
            margin: 10px;
        }
        .myVideo {
            position: fixed;
            right: 0;
            bottom: 0;
            min-width: 100%;
            min-height: 100%;
        }

        .global-search-block li.active {
            border: 2px solid #2579a9;
            border-bottom: none;
        }

        .tab-private-general .active {
            border: 2px solid #2579a9;
            padding: 6px;
        }

        .text-wrap span {
            width: 77% !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('.home').addClass('active');
            $('.search-adults').on('input',function () {
                if (/\D/g.test(this.value))
                {
                    // Filter non-digits from input value.
                    this.value = this.value.replace(/\D/g, '')+' adult';
                    if ( this.value[0] == 0){
                        this.value = this.value.slice(1)
                    }

                }
                var length = this.value.replace(/\D/g, '').length
                $(this)[0].setSelectionRange(length, length);
            })


            $('.search-child').on('input',function () {
                if (/\D/g.test(this.value))
                {
                    // Filter non-digits from input value.
                    this.value = this.value.replace(/\D/g, '')+' child';
                    if ( this.value[0] == 0){
                        this.value = this.value.slice(1)
                    }
                }
                var length = this.value.replace(/\D/g, '').length
                $(this)[0].setSelectionRange(length, length);
            })


            $( ".apartment-name" ).each(function() {
                var myTag = $( this ).text()
                console.log(myTag,myTag.trim().length)
                if (myTag.length > 40) {
                    var truncated = myTag.trim().substring(0, 40).split(" ").slice(0, -1).join(" ") + "…";
                    $(this).text(truncated);
                }
            });
        });



        $(window).scroll(function () {
            if ($(window).scrollTop() > 300) {

                $('.video').addClass('myVideo')
            } else {
                $('.video').removeClass('myVideo')
            }
        });
    </script>
@endsection
