@extends('layouts.app')

@section('content')
    <div class="wrap">
        <!--main content--><!--breadcrumbs-->
        <nav role="navigation" class="breadcrumbs">
            <ul>
                <li><a href="https://www.themeenergy.com/themes/wordpress/book-your-travel/" title="Home">Home</a>
                </li>
                <li>{{$hotel->name}}</li>
            </ul>
        </nav><!--//breadcrumbs-->

        <section class="three-fourth">

            <div class="loading" id="wait_loading" style="display:none">
                <div class="ball"></div>
                <div class="ball1"></div>
            </div>
            <form method="post"
                  action="https://www.themeenergy.com/themes/wordpress/book-your-travel/accommodations/house-brandon?accommodation=house-brandon&amp;post_type=accommodation&amp;name=house-brandon"
                  class="booking accommodation-confirmation-form" style="display:none">
                <fieldset>
                    <h3>Confirmation</h3>
                    <div class="text-wrap">
                        <p>Thank you. We will get back you with regards your reservation within 24 hours.</p>
                    </div>
                    <h3>Traveller info</h3>
                    <div class="row">
                        <div class="output one-half">
                            <p>First name:
                                <strong class="confirm_first_name_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>Last name:
                                <strong class="confirm_last_name_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>Company:
                                <strong class="confirm_company_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>Email:
                                <strong class="confirm_email_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>Phone:
                                <strong class="confirm_phone_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>Address:
                                <strong class="confirm_address_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>Address 2:
                                <strong class="confirm_address_2_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>City:
                                <strong class="confirm_town_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>Zip:
                                <strong class="confirm_zip_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>State:
                                <strong class="confirm_state_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>Country:
                                <strong class="confirm_country_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>Special requirements:
                                <strong class="confirm_special_requirements_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="output one-half">
                            <p>Adults:
                                <strong class="confirm_adults_p"></strong>
                            </p>
                        </div>
                        <div class="output one-half">
                            <p>Children:
                                <strong class="confirm_children_p"></strong>
                            </p>
                        </div>
                        <div class="output one-half">
                            <p>Date from:
                                <strong class="confirm_date_from_p"></strong>
                            </p>
                        </div>
                        <div class="output one-half">
                            <p>Date to:
                                <strong class="confirm_date_to_p"></strong>
                            </p>
                        </div>
                        <div class="output one-half">
                            <p>Reservation total:
                                <strong class="confirm_reservation_total_p"></strong>
                            </p>
                        </div>
                        <div class="output one-half">
                            <p>Extra items total:
                                <strong class="confirm_extra_items_total_p"></strong>
                            </p>
                        </div>
                        <div class="output one-half">
                            <p>Total price:
                                <strong class="confirm_total_price_p"></strong>
                            </p>
                        </div>
                    </div>
                    <div class="text-wrap">
                        <p><strong>We wish you a pleasant stay</strong><br><i>your Book Your Travel LLC team</i>
                        </p>
                    </div>
                </fieldset>
            </form>



            <section class="jd-slider example">

                <div class="slide-inner">

                    <ul class="slide-area">
                        <li><img
                                    src="{{asset('uploads/'.$hotel->general_pic)}}">
                        </li>
                        @foreach($hotel->picture as $picture)
                            <li><img
                                        src="{{asset('uploads/'.$picture->name)}}">
                            </li>
                        @endforeach

                    </ul>

                </div>

                <a class="prev" href="#">

                    <i class="fas fa-angle-left fa-2x"></i>

                </a>

                <a class="next" href="#">

                    <i class="fas fa-angle-right fa-2x"></i>

                </a>

                <div class="controller">

                    <div class="indicate-area"></div>
                </div>

            </section>

            <div class="div-navbar row">
                <div class="col-md-3">
                    <div class="inner-navigation">
                        <div class="nav-center-div">
                            <div class=" info-tab-new activ-link" tab-content="description">
                                <p class="text-nav-link">Description</p>
                            </div>
                            <div class="info-tab-new" tab-content="location">
                                <p class="text-nav-link">Location</p>
                            </div>
                            <div class="info-tab-new" tab-content="facilities">
                                <p class="text-nav-link">Facilities</p>
                            </div>
                            <div class="info-tab-new" tab-content="reviews">
                                <p class="text-nav-link">Reviews</p>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-9">
                    <div class="content-tab" id="description">
                        <div class="title-general ">
                            <p class="general">General</p>
                        </div>
                        <div class="description-text-div">
                            <p class="description-text-text"> <?= $hotel->description ?></p>
                        </div>
                    </div>

                    <div class="content-tab" id="location" style="display: none; height: auto">
                        <div class="gmap" id="map">

                        </div><!--//map-->
                        <input id="country" value="{{$hotel->country}}" type="hidden">
                        <input id="city" value="{{$hotel->city}}" type="hidden">
                        <input id="street" value="{{$hotel->address}}" type="hidden">
                    </div>
                </div>
            </div>


        </section>
        <aside id="secondary" class="right-sidebar widget-area one-fourth" role="complementary">
            <ul>
                <li>
                    <article class="accommodation-details hotel-details">
                        <h1>{{$hotel->name}}<span class="stars">
											</span>
                        </h1>
                        <span class="address">{{$hotel->address}},{{$hotel->city}}, {{$hotel->country}}</span>


                        <div class="price">
                            Price from <em>
                                <span class="curr">$</span>
                                <span class="amount">{{$hotel->price}}</span>
                            </em>
                        </div>
                        <div class="description"><p>Far far away, behind the word mountains, far from the countries
                                Vokalia and Consonantia, there live </p></div>

                    </article>
                </li>

                <li class="widget widget-sidebar"><!--deals-->
                    <div class="deals">
                        <header class="s-title"><h4>Deal of the day</h4></header>
                        <div class="row"><!--accommodation item-->
                            <article class="accommodation_item full-width">

                            </article>
                            <!--//accommodation item-->
                        </div>
                    </div><!--//deals--></li>
            </ul>
        </aside>
    </div><!--//wrap-->
@endsection