<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

{!! SEOMeta::generate() !!}
{!! OpenGraph::generate() !!}
{!! Twitter::generate() !!}
<!-- OR -->
{!! SEO::generate() !!}

<!-- MINIFIED -->
{!! SEO::generate(true) !!}


<!-- LUMEN -->
    {!! app('seotools')->generate() !!}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CarTourTravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/site1908.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugin/jquery.jdSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugin/bootstrap-year-calendar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugin/zebra_datepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugin/nouislider.css') }}" rel="stylesheet">


    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <script src="https://api-maps.yandex.ru/2.1/?apikey=5fa504f6-95e2-4249-9e1f-d929bd1cd061&lang&lang=ru_RU" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/site0207.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>

    <script src="{{ asset('js/plugin/jquery.jdSlider-latest.js') }}"></script>
    <script src="{{ asset('js/plugin/bootstrap-year-calendar.js') }}"></script>
    <script src="{{ asset('js/plugin/zebra_datepicker.src.js') }}"></script>
    <script src="{{ asset('js/plugin/qcTimepicker.js') }}"></script>
    <script src="{{ asset('js/plugin/nouislider.js') }}"></script>


    <style>

    </style>

</head>
<body class="all-body">
<div id="app">

    @include('header.header')

    <?php
    $url = url()->current();
    $explode_url = explode('/', $url);
    ?>

    @if(!isset($explode_url[3]))



        <div style="width: 100%; height: 560px; overflow: hidden;">
            <video class="video" width="100%" autoplay loop muted style="    width: 100%; height: 100%; object-fit: cover;
    z-index: -100;">
                <source src="{{asset('uploads/'.$slider->photo)}}" type="video/mp4">

            </video>



        </div>

        <script>

            $(function () {


            });
        </script>
    @endif

    @yield('content')
</div>

</body>
@include('footer.footer')
</html>
