@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" style="margin-top: 10%;">
            <section class="full-width">
                <article id="page-277">
                    <h1>News Feed</h1>
                </article>

                <div class="deals row">

                    <!--accommodation item-->
                    @foreach($news as $new)
                        <div class="col-md-4">
                            <article class="accommodation_item">
                                <div>
                                    <figure class="pic-figure">
                                        <a href="#" class="a-no-style"
                                           title="City loft">
                                            <img src="{{asset("img").'/'.$new->img}}"
                                                 alt="City loft">
                                        </a>
                                    </figure>
                                    <div class="detail-etxream">

                                        <p class="apartment-name">{{ $new->name }}</p>

                                    </div>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </section>
            <section class="full-width">
                <nav class="page-navigation bottom-nav">
                    <!--back up button-->
                    <a class="scroll-to-top pull-right" title="Back up"></a>
                    <!--//back up button-->
                    <!--pager-->
                    <div class="pager">
                    </div>
                </nav>
            </section>

        </div>
    </div>

@endsection
