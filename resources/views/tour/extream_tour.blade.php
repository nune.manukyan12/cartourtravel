@extends('layouts.app')

@section('content')
    {{--<div class="container" style="height: 100px; background-color: red" >--}}

    {{--</div>--}}

    <div class="container">
        <div class="row" style="margin-top: 10%;">
            {{--<section class="full-width">--}}
            {{--<div class="row" style="margin: 15px">--}}
            {{--<div class="col-md-12 col-xs-12">--}}
            {{--<div class="search-tab-content" style="position: relative">--}}
            {{--<div class="tab-pane fade active in" id="hotels-tab">--}}

            {{--<div class="search-tab-content" style="position: relative">--}}
            {{--<div class="tab-pane fade active in" id="hotels-tab">--}}

            {{--<input type="hidden" class="search-category" name="search-category" value="">--}}

            {{--<div class="col-md-6">--}}
            {{--<div id="test-list" class="search-bar col-md-12">--}}
            {{--<input type="text" autocomplete="off" id="GeoFilterInput" class="search-input fuzzy-search" placeholder="Search by name, description, city...">--}}
            {{--<i id="search-activity" class="fa fa-search icon-search"></i>--}}
            {{--<div style="display: none" class="result-list-default">--}}
            {{--<i class="fa fa-close close-btn pull-right" style="font-size:36px"></i>--}}
            {{--<ul class="list result-list">--}}


            {{--</ul>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<script>--}}

            {{--$('.close-btn').on('click', function () {--}}

            {{--$('.result-list-default').removeClass('result-list-display')--}}
            {{--});--}}
            {{--$(document).on('click', function (event) {--}}
            {{--if (!$(event.target).closest('#GeoFilterInput').length) {--}}
            {{--if (!$(event.target).closest('.search-result-block').length) {--}}
            {{--$('.result-list-default').removeClass('result-list-display')--}}
            {{--}--}}
            {{--}--}}
            {{--});--}}
            {{--</script>--}}
            {{--</div>--}}

            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</section>--}}
            <section class="full-width">
                <article id="page-277">
                    <h1>Extream Tours</h1>
                </article>

                <div class="deals row">

                    <!--accommodation item-->

                    <div class="col-md-4">
                        <article class="accommodation_item">
                            <div>
                                <figure class="pic-figure">
                                    <a href="#" class="a-no-style"
                                       title="City loft">
                                        <img src="{{asset("img/winter-tour.jpg")}}"
                                             alt="City loft">
                                    </a>
                                </figure>
                                <div class="detail-etxream">

                                    <p class="apartment-name">Winter Tours</p>


                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="accommodation_item">
                            <div>
                                <figure class="pic-figure">
                                    <a href="#" class="a-no-style"
                                       title="City loft">
                                        <img src="{{asset("img/hiking-tour.jpg")}}"
                                             alt="City loft">
                                    </a>
                                </figure>
                                <div class="detail-etxream">

                                    <p class="apartment-name">Hiking Tours</p>

                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="accommodation_item">
                            <div>
                                <figure class="pic-figure">
                                    <a href="#" class="a-no-style"
                                       title="City loft">
                                        <img src="{{asset("img/jeep-tour.jpg")}}"
                                             alt="City loft">
                                    </a>
                                </figure>
                                <div class="detail-etxream">

                                    <p class="apartment-name">Jeep Tours</p>


                                    <hr>

                                </div>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="deals row">

                    <div class="col-md-4">
                        <article class="accommodation_item">
                            <div>
                                <figure class="pic-figure">
                                    <a href="#" class="a-no-style"
                                       title="City loft">
                                        <img src="{{asset("img/bike-tour.jpg")}}"
                                             alt="City loft">
                                    </a>
                                </figure>
                                <div class="detail-etxream">

                                    <p class="apartment-name">Bike Tours</p>


                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="accommodation_item">
                            <div>
                                <figure class="pic-figure">
                                    <a href="#" class="a-no-style"
                                       title="City loft">
                                        <img src="{{asset("img/zipline-tour.jpg")}}"
                                             alt="City loft">
                                    </a>
                                </figure>
                                <div class="detail-etxream">

                                    <p class="apartment-name">Zipline Tours</p>

                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <article class="accommodation_item">
                            <div>
                                <figure class="pic-figure">
                                    <a href="#" class="a-no-style"
                                       title="City loft">
                                        <img src="{{asset("img/enduro-tour.jpg")}}"
                                             alt="City loft">
                                    </a>
                                </figure>
                                <div class="detail-etxream">

                                    <p class="apartment-name">Enduro Tours</p>


                                </div>
                            </div>
                        </article>
                    </div>





                    <!--//accommodation item-->


                </div><!--//deals-->
            </section>
            <section class="full-width">
                <nav class="page-navigation bottom-nav">
                    <!--back up button-->
                    <a class="scroll-to-top pull-right" title="Back up"></a>
                    <!--//back up button-->
                    <!--pager-->
                    <div class="pager">
                    </div>
                </nav>
            </section>

        </div>
    </div>

@endsection
