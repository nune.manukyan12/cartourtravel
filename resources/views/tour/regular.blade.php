@extends('layouts.app')

@section('content')
    <div class="row car-book-general regular-tour-general-head"
         style="background-image: url({{ asset("uploads/".$tours->general_pic)}});">
        <div class="row" style="background-color: #ffffff69;padding: 3%">
            <div class="col-sm-12">
                <p class=""><?= $tours->description?></p>
            </div>
            <hr>
        </div>
    </div>

    <div class="container">
        <div class="row cars">

        </div>

        @if($tours)
            <section class="full-width">
                <div class="regular-deals">
                    @foreach($tours->destination  as $key => $value)

                        <article class="regular-tour wows fadeInUp tour-archive" style="height: 210px">
                            <div class="regular-tour-title"><a href="#">{{$value->destination}}</a></div>
                            <div class="regular-tour-clearfix">
                                <div class="col-md-2 col-sm-2 no-dotted">
                                    <div class="reg-tour-img">
                                        <a href="#">
                                            <img width="180" height="136" data-original="#"
                                                 src="https://onewaytour.com/wp-content/uploads/2019/02/featured-khndzoresk-180x136.jpg"
                                                 class="lazy" style="display: inline;">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-4 tour-info">
                                    <div class="reg-tour-details multiline">
                                        <p class="reg-tour-date"><span>Date</span> <span
                                                    class="hidden-xs"></span> <span></span></p>
                                        <p class="price">&nbsp;{{$value->date}}&nbsp;</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-4 tour-info">
                                    <div class="reg-tour-details">
                                        <p>Start</p>
                                        <p class="date">{{$value->clock}}</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-4 tour-duration tour-info">
                                    <div class="reg-tour-details">
                                        <p>Duration</p>
                                        <p>{{$value->duration}}</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12 regular-tour-price no-dotted">
                                    <div class="reg-tour-details">
                                        <p>Price Per Person</p>
                                        <p>
                                            {{$value->price_person}}AMD</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12 no-dotted">
                                    <div class="reg-tour-details book-regular-tour" regulra_tour_id="{{$value->id}}">
                                        <a href="#"
                                           class="more-information">Book</a>
                                    </div>
                                </div>
                            </div>
                        </article>

                    @endforeach
                </div>
            </section>
        @else
            <div class="row">
                <div class="col-md-12" style="text-align: center">
                    <h1>
                        NO Tours
                    </h1>
                </div>
            </div>

        @endif
    </div>
    @include('_partial.tour_modals')
    <script>
        $('.book-regular-tour').on('click', function () {
            $('#tour_type').val('regular');
          var tour =  $(this).attr('regulra_tour_id');
            $('#regulra_tour').val(tour);
            $('#book').modal('show');
        });
        $('.link-private').click(function () {
            var name = $(this).attr('id');
            console.log(name);
            $('.hide-class-div').hide();

            $(".div-" + name).show();

        })
    </script>
    <style>
        li .active {
            color: #10415d !important;
            background-color: #e9e6e0bd !important;
        }
    </style>
@endsection

