@extends('layouts.app')

@section('content')
    <div class="container">
        @if($tours)
            <section class="full-width" style="margin-top: 10%">

                @include('_partial.tour_include')

                <div class="deals font-private-tour">
                    <div class="row">
                        <div class="col-sm-12" style="padding:0px;">
                            <button id="book_tour" class="btn btn-info pull-right book-private-tour">
                                <div class="div-private-number" style="display: none"></div>
                                <i class="	fas fa-cart-arrow-down"></i> Book
                            </button>

                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-12">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tab-private ">
                                <li class="nav-item li-tab-private active">
                                <a class="nav-link active link-private" id="sedan_img" data-toggle="tab"
                                       href="#sedan_general">Sedan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link link-private" id="minivan_img" data-toggle="tab"
                                       href="#minivan_general">Minivan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link link-private" id="mini_bus_img" data-toggle="tab"
                                       href="#mini_bus_general">Mini bus</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link link-private" id="bus_img" data-toggle="tab" href="#bus_general">Bus</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content tab-private-general">
                                <div id="sedan_general" class="tab-private-div container tab-pane active">
                                    <table class="table ">
                                        <thead class="thead-private-tour">
                                        <tr>
                                            <th></th>
                                            <th class="th-destination-private text-center">Destination</th>
                                            <th class="text-center">Duration</th>
                                            <th class="text-center">Km</th>
                                            <th class="text-center">Without guide</th>
                                            <th class="text-center">With guide</th>

                                        </tr>
                                        </thead>
                                        <tbody class="tbody-private-tour">
                                        <?php $number = 1; ?>
                                        @foreach($tours->destination  as $key => $value)
                                            <tr class="sedan-private-tr">
                                                <th class="tbody-number text-center">{{$number}}</th>
                                                <th class="destination_info"
                                                    destination_id="{{$value->id}}">{{$value->destination}}</th>
                                                <th class="text-center">{{$value->duration}}</th>
                                                <th class="text-center">{{$value->km}}</th>
                                                <th class="true-td text-center">

                                                    {{$value->sedan_price }}
                                                    @if($value->sedan_price)
                                                        <img style="display: none" src="/img/icon-check.png" alt="Check"
                                                             class="choose-check-img-private" car="sedan" id="{{$value->id}}">
                                                        <button type="button" tour="private" car="sedan" guide="false"
                                                                class="btn choose-tour-button" id="{{$value->id}}">
                                                            Choose
                                                        </button>
                                                    @endif
                                                </th>
<!--                                                --><?php //$change_sedan_price_guide = \App\Services\ForgeApi::changeCurrencyFromAmd(session()->get('currency'),$value->sedan_price_guide) ?>
                                                <th class="false-td text-center">{{$value->sedan_price_guide }}
                                                    @if($value->sedan_price_guide)
                                                        <img style="display: none" src="/img/icon-check.png" alt="Check"
                                                             class="choose-check-img-private" car="sedan" id="{{$value->id}}">
                                                        <button type="button" tour="private" car="sedan" guide="true"
                                                                class="btn choose-tour-button" id="{{$value->id}}">
                                                            Choose
                                                        </button>
                                                    @endif
                                                </th>
                                            </tr>
                                            <?php $number++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div id="minivan_general" class="tab-private-div container tab-pane fade">
                                    <table class="table ">
                                        <thead class="thead-private-tour">
                                        <tr>
                                            <th></th>
                                            <th class="th-destination-private text-center">Destination</th>
                                            <th class="text-center">Duration</th>
                                            <th class="text-center">Km</th>
                                            <th class="text-center">Without guide</th>
                                            <th class="text-center">With guide</th>

                                        </tr>
                                        </thead>
                                        <tbody class="tbody-private-tour">
                                        <?php $minivan_number = 1; ?>
                                        @foreach($tours->destination  as $key => $value)
                                            <tr class="sedan-private-tr">
                                                <th class="tbody-number text-center">{{$minivan_number}}</th>
                                                <th class="destination_info"
                                                    destination_id="{{$value->id}}">{{$value->destination}}</th>
                                                <th class="text-center">{{$value->duration}}</th>
                                                <th class="text-center">{{$value->km}}</th>
                                                <th class="true-td text-center">
                                                    {{$value->minivan_price}}
                                                    @if($value->sedan_price)
                                                        <img style="display: none" src="/img/icon-check.png" alt="Check"
                                                             class="choose-check-img-private" car="sedan" id="{{$value->id}}">
                                                        <button type="button" tour="private" car="sedan" guide="false"
                                                                class="btn choose-tour-button" id="{{$value->id}}">
                                                            Choose
                                                        </button>
                                                    @endif
                                                </th>
                                                <th class="false-td text-center">
                                                    {{$value->minivan_price_guide}}
                                                    @if($value->sedan_price_guide)
                                                        <img style="display: none" src="/img/icon-check.png" alt="Check"
                                                             class="choose-check-img-private" car="sedan" id="{{$value->id}}">
                                                        <button type="button" tour="private" car="sedan" guide="true"
                                                                class="btn choose-tour-button" id="{{$value->id}}">
                                                            Choose
                                                        </button>
                                                    @endif
                                                </th>
                                            </tr>
                                            <?php $minivan_number++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div id="mini_bus_general" class="tab-private-div container tab-pane fade">
                                    <table class="table ">
                                        <thead class="thead-private-tour">
                                        <tr>
                                            <th></th>
                                            <th class="th-destination-private text-center">Destination</th>
                                            <th class="text-center">Duration</th>
                                            <th class="text-center">Km</th>
                                            <th class="text-center">Without guide</th>
                                            <th class="text-center">With guide</th>

                                        </tr>
                                        </thead>
                                        <tbody class="tbody-private-tour">
                                        <?php $mini_bus_number = 1; ?>
                                        @foreach($tours->destination  as $key => $value)
                                            <tr class="sedan-private-tr">
                                                <th class="tbody-number text-center">{{$mini_bus_number}}</th>
                                                <th class="destination_info"
                                                    destination_id="{{$value->id}}">{{$value->destination}}</th>
                                                <th class="text-center">{{$value->duration}}</th>
                                                <th class="text-center">{{$value->km}}</th>
                                                <th class="true-td text-center">
                                                    {{ $value->mini_bus }}
                                                    @if($value->mini_bus)
                                                        <img style="display: none" src="/img/icon-check.png" alt="Check"
                                                             class="choose-check-img-private" car="sedan" id="{{$value->id}}">
                                                        <button type="button" tour="private" car="sedan" guide="false"
                                                                class="btn choose-tour-button" id="{{$value->id}}">
                                                            Choose
                                                        </button>
                                                    @endif
                                                </th>
                                                <th class="false-td text-center">
                                                    {{ $value->mini_bus_guide }}
                                                    @if($value->sedan_price_guide)
                                                        <img style="display: none" src="/img/icon-check.png" alt="Check"
                                                             class="choose-check-img-private" car="sedan" id="{{$value->id}}">
                                                        <button type="button" tour="private" car="sedan" guide="true"
                                                                class="btn choose-tour-button" id="{{$value->id}}">
                                                            Choose
                                                        </button>
                                                    @endif
                                                </th>
                                            </tr>
                                            <?php $mini_bus_number++; ?>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <div id="bus_general" class="tab-private-div container tab-pane fade">
                                    <table class="table ">
                                        <thead class="thead-private-tour">
                                        <tr>
                                            <th></th>
                                            <th class="th-destination-private text-center">Destination</th>
                                            <th class="text-center">Duration</th>
                                            <th class="text-center">Km</th>
                                            <th class="text-center">Without guide</th>
                                            <th class="text-center">With guide</th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbody-private-tour">
                                        <?php $bus_number = 1; ?>
                                        @foreach($tours->destination  as $key => $value)
                                            <tr class="sedan-private-tr">
                                                <th class="tbody-number text-center">{{$bus_number}}</th>
                                                <th class="destination_info"
                                                    destination_id="{{$value->id}}">{{$value->destination}}</th>
                                                <th class="text-center">{{$value->duration}}</th>
                                                <th class="text-center">{{$value->km}}</th>
                                                <th class="true-td text-center">
                                                    {{($value->bus)}}
                                                    @if($value->bus)
                                                        <img style="display: none" src="/img/icon-check.png" alt="Check"
                                                             class="choose-check-img-private" car="sedan" id="{{$value->id}}">
                                                        <button type="button" tour="private" car="sedan" guide="false"
                                                                class="btn choose-tour-button" id="{{$value->id}}">
                                                            Choose
                                                        </button>
                                                    @endif
                                                </th>
                                                <th class="false-td text-center">
                                                    {{$value->bus_guide }}
                                                    @if($value->sedan_price_guide)
                                                        <img style="display: none" src="/img/icon-check.png" alt="Check"
                                                             class="choose-check-img-private" car="sedan" id="{{$value->id}}">
                                                        <button type="button" tour="private" car="sedan" guide="true"
                                                                class="btn choose-tour-button" id="{{$value->id}}">
                                                            Choose
                                                        </button>
                                                    @endif
                                                </th>
                                            </tr>
                                            <?php $bus_number++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        @else
            <div class="row">
                <div class="col-md-12" style="text-align: center">

                    <h1>
                        NO Tours
                    </h1>
                </div>
            </div>

        @endif
    </div>

    @include('_partial.tour_modals')
    <script>
        var dataPrivate = [];

        $('.choose-tour-button').click(function () {

            var id = $(this).attr('id');
            var typeCar = $(this).attr('car');

            var guide = $(this).attr('guide');
            var typeTour = $(this).attr('tour');

            if (dataPrivate.length > 0) {
                var bool = true;
                for (var i = 0; i < dataPrivate.length; i++) {
                    if (dataPrivate[i].id == id && dataPrivate[i].type_car == typeCar) {
                        $(this).hide();
                        $(this).parent().find('.choose-check-img-private').show();
                        $(this).parent().find('.choose-id').val(id);
                        $(this).parent().parent().find('.' + guide + '-td').children('img').hide();
                        $(this).parent().parent().find('.' + guide + '-td').children('button').show();
                        dataPrivate[i].guide = guide;
                        bool = false;
                    }
                }
                if (bool === true) {
                    var tour = {id: id, guide: guide, type_car: typeCar, type_tour: typeTour};
                    $(this).hide();
                    $(this).parent().find('.choose-check-img-private').show();
                    $(this).parent().find('.choose-id').val(id);
                    dataPrivate.push(tour);
                }
            } else {
                var tour = {id: id, guide: guide, type_car: typeCar, type_tour: typeTour};
                $(this).hide();
                $(this).parent().find('.choose-check-img-private').show();
                $(this).parent().find('.choose-id').val(id);
                dataPrivate.push(tour);
            }
            $('.div-private-number').html(dataPrivate.length);
            $('.div-private-number').show();
        });
        $('.choose-check-img-private').click(function () {
            var id = $(this).attr('id');
            var typeCar = $(this).attr('car');
            $(this).hide();
            $(this).parent().find('button').show();
            for (var i = 0; i < dataPrivate.length; i++) {
                if (dataPrivate[i].id == id && dataPrivate[i].type_car == typeCar) {
                    dataPrivate.splice(i,1)
                }
            }
            $('.div-private-number').html(dataPrivate.length);

        });

        $('.destination_info').on('click', function () {
            var destination = $(this).attr('destination_id')
            $.ajax({
                url: '/tour/info/' + destination,
                type: 'GET',
                success: function (result) {
                    if (result.destinationInfo.length != 0) {

                        $('#modalDestination').modal('show');
                        $('.destination-description').text(result.destinationInfo[0].description);
                        $('.photo-slider').html('');
                        $.each(result.destinationInfo, function (index, value) {
                            $('.photo-slider').append(
                                '          <div>\n' +
                                '                <img alt="" src="/uploads/' + value.picture + '">\n' +
                                '           </div>'
                            );
                        });
                        $('.photo-slider').promise().done(function() {
                            $('.owl-carousel').owlCarousel({
                                items: 1,
                                loop: true,
                                nav: true,
                                smartSpeed: 900,
                                navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                                navClass: ['owl-prev', 'owl-next'],

                            });
                        });
                    }
                },


                error: function (errors) {

                }
            })
        });
        $('#book_tour').on('click', function () {
            $('#book').modal('show');

        });

        $('.link-private').click(function () {
            var name = $(this).attr('id');
            console.log(name);
            $('.hide-class-div').hide();

            $(".div-" + name).show();

        })
    </script>
    <style>
        li .active {
            color: #10415d !important;
            background-color: #e9e6e0bd !important;
        }
    </style>
@endsection

