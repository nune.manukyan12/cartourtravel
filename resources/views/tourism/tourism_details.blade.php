@extends('layouts.app')

@section('content')
    @if($tourism)
        <div class="page_top_wrap">
            <h1 class="page_top_title">
                {{$tourism->name}}    </h1>

            <h2 class="page_top_desc">
                Affordable, comfortable, enjoyable </h2>
        </div>

        <section class="sections_content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-right mt-2 mb-2">
                        <button type="button" class="btn btn-success book-button" id="{{$tourism->id}}">
                            <i class="fa fa-shopping-basket" aria-hidden="true">
                                <span class="basket ml-2">
                                @if(session()->exists('tourism_basket'))
                                    {{  count(session()->get('tourism_basket')) }}
                                @else
                                    0
                                @endif
                                </span>
                            </i>
                        </button>
                    </div>
                    <div class="col-md-12 ">
                            <div class="content_right">
                                <div class="row">
                                    <div class="col-md-1 pt-30">
                                        @if(!is_null($prev))
                                            <a href="{{ route('tourism.details',['id' => $prev->id]) }}" class="pull-left btn btn-success ">Prev</a>
                                        @endif
                                    </div>
                                    <div class="col-md-10">
                                        <img src="{{asset('img/'.$tourism->img)}}"
                                             alt="car rental in yerevan chauffeur service" title="" class="img-responsive mx-auto">
                                        <div class="main_content_text">
                                            <p style="margin-left:.25in;">{!! $tourism->description !!}
                                        </div>
                                    </div>
                                    <div class="col-md-1 pt-30">
                                        @if(!is_null($next))
                                            <a href="{{ route('tourism.details',['id' => $next->id]) }}" class="pull-right btn btn-success ">Next</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif


    <style>
        .page_top_wrap {
            text-align: center;
            padding-top: 6%;
            padding-bottom: 22px;
        }

        .page_top_wrap .page_top_title {
            color: #343434;
            font-size: 34px;
            font-family: 'sans_semibold';
            line-height: 46px;
            margin: 0;
            padding: 0;
        }

        .page_top_wrap .page_top_desc {
            color: #343434;
            font-family: 'sans_reg';
            font-size: 20px;
            line-height: 28px;
            margin: 0;
            padding: 0;
        }

        .sections_content {
            border-top: 6px solid #2979b2;
        }

        .content_left_link_wrap {
            background-color: #ededed;
        }

        .content_left_link_wrap .title {
            color: #343434;
            font-family: 'sans_semibold';
            font-size: 20px;
            padding-left: 23px;
            padding-bottom: 9px;
            padding-top: 8px;
            border-bottom: 2px solid #ddd;
        }

        .content_left_link_wrap .item {
            display: block;
            text-decoration: none;
            color: #525252;
            font-size: 16px;
            line-height: 20px;
            font-family: 'sans_semibold';
            padding-left: 30px;
            padding-right: 10px;
            padding-top: 15px;
            padding-bottom: 16px;
            border-bottom: 1px solid #ddd;
        }

        .content_left_link_wrap .active_link {
            background-color: white;
            color: #2979b2;
        }
        .content_right{
            width: 95%;
            margin: auto;
        }
        .pt-30{
            padding-top: 30%;
            padding-bottom: 10%;
        }
    </style>

    <script>
        var id;
        $(".book-button").click(function () {
            id = $(this).attr('id');
            $.ajax({
                url: '/tourism/basket',
                type: 'POST',
                dataType: "JSON",
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: {
                    id: id,
                },
                success: function (success) {
                    $('.basket').text(success)
                },
            })
        });
    </script>
@endsection
