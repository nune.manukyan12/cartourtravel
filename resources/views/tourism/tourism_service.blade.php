@extends('layouts.app')

@section('content')


    <section class="sections_content">
        <div class="container">
            <div class="row" style="margin-top: 10%;">
                <div class="col-lg-3 col-md-4"></div>
                <div class="col-lg-9 col-md-8 text-right mt-2 mb-2">
                    <button type="button" class="pull-right btn btn-success book-button">
                        <i class="fa fa-shopping-basket" aria-hidden="true">
                          <span class="basket ml-2">
                                @if(session()->exists('tourism_basket'))
                                  {{  count(session()->get('tourism_basket')) }}
                              @else
                                  0
                              @endif
                        </span>
                        </i>
                    </button>
                </div>
                <div class="col-lg-3 col-md-4 cust_link_cols">
                    <div class="content_left_link_wrap">
                        <a class="item " href="{{asset('/medical/service')}}">
                                @lang('medical.medical_service')</a>
                        <a class="item active_link">
                            @lang('medical.tourist_service') </a>
                    </div>
                </div>

                <div class="col-lg-9 col-md-8">
                    <div class="content_right">
                        <div class="deals row">

                            <!--accommodation item-->
                            @foreach($tourisms as $tourism)
                                <div class="col-md-4">
                                    <article class="accommodation_item">
                                        <div>
                                            <figure class="pic-figure">
                                                <a  href="{{ route('tourism.details',['id' => $tourism->id]) }}" class="a-no-style"
                                                    title="City loft">
                                                    <img src="{{asset("img").'/'.$tourism->img}}"
                                                         alt="City loft">
                                                </a>
                                            </figure>
                                            <div class="detail-etxream">

                                                <p class="apartment-name">{{ $tourism->name }}</p>

                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="modal fade" id="book-car" role="dialog">
        <div class="modal-dialog book-dialog">
            <!-- Modal content-->
            <div class="modal-content book-content">
                <div class="modal-header">
                    <h4 class="modal-title">Please enter your info for booking. </h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="div-modal-book div-first-block ">
                                <label for="">Last name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control" id="last-name" placeholder="Last name">
                                <p class="error-last-name"></p>

                            </div>
                            <div class="div-modal-book div-second-block ">
                                <label for="">First name<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control" id="first-name" placeholder="First name">
                                <p class="error-first-name"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="div-modal-book div-first-block ">
                                <label for="">Email address<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="email" class="form-control" id="email" placeholder="Email address">
                                <p class="error-email"></p>

                            </div>
                            <div class="div-modal-book div-second-block">
                                <label for="">Phone<sup><i class="fas fa-asterisk red-icon"></i></sup>:</label>
                                <input type="text" class="form-control" id="phone" placeholder="Phone">
                                <p class="error-phone"></p>

                            </div>
                        </div>
                        <div>
                            <label for="">Viber:</label>
                            <input type="text" class="form-control" id="viber" placeholder="Viber">
                        </div>
                        <div class="">
                            <label for="">Telegram:</label>
                            <input type="text" class="form-control" id="telegram" placeholder="Telegram">
                        </div>
                        <div class="">
                            <label for="">Whatsapp:</label>
                            <input type="text" class="form-control" id="whatsapp" placeholder="Whatsapp">
                        </div>
                        <p class="date-from"></p>
                        <p class="date-to"></p>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="book-book">Book</button>
                </div>
            </div>

        </div>
    </div>

    {{--Modal error return and puke up date--}}
    <div class="modal fade" id="modal_error" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <p class="error-puke-up-date-car"></p>
                    <p class="error-return-date-car"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <style>
        .sections_content {
            border-top: 6px solid #2979b2;
        }

        .content_left_link_wrap {
            background-color: #ededed;
        }

        .content_left_link_wrap .title {
            color: #343434;
            font-family: 'sans_semibold';
            font-size: 20px;
            padding-left: 23px;
            padding-bottom: 9px;
            padding-top: 8px;
            border-bottom: 2px solid #ddd;
        }

        .content_left_link_wrap .item {
            display: block;
            text-decoration: none;
            color: #525252;
            font-size: 16px;
            line-height: 20px;
            font-family: 'sans_semibold';
            padding-left: 30px;
            padding-right: 10px;
            padding-top: 15px;
            padding-bottom: 16px;
            border-bottom: 1px solid #ddd;
        }

        .content_left_link_wrap .active_link {
            background-color: white;
            color: #2979b2;
        }
    </style>
    <script>
        $(".book-button").click(function () {
            $("#book-car").modal("show");
        });

        $('#book-book').click(function () {
            var firstName = $('#first-name').val();
            var lastName = $('#last-name').val();
            var email = $('#email').val();
            var phone = $('#phone').val();
            var viber = $('#viber').val();
            var telegram = $('#telegram').val();
            var whatsapp = $('#whatsapp').val();
            $.ajax({
                url: '/tourism/store_tourism',
                type: 'POST',
                dataType: "JSON",
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: {

                    last_name: lastName,
                    first_name: firstName,
                    email: email,
                    viber: viber,
                    telegram: telegram,
                    whatsapp: whatsapp,
                    phone: phone,
                },
                success: function (success) {
                    $("#book-car").modal("hide");
                    $("#modal-messenger").modal("show");
                    setTimeout(function () {
                        $("#modal-messenger").modal("hide");
                        $('.basket').text('0')
                    }, 10000);
                },
                error: function (errors) {

                    if (errors.responseJSON.errors.last_name) {
                        $(".error-last-name").html(errors.responseJSON.errors.last_name);
                    }
                    if (errors.responseJSON.errors.first_name) {
                        $(".error-first-name").html(errors.responseJSON.errors.first_name);
                    }
                    if (errors.responseJSON.errors.email) {
                        $(".error-email").html(errors.responseJSON.errors.email);
                    }
                    if (errors.responseJSON.errors.phone) {
                        $(".error-phone").html(errors.responseJSON.errors.phone);
                    }
                    if (errors.responseJSON.errors.pick_up_date || errors.responseJSON.errors.return_date) {

                        $("#modal_error").modal("show");
                        if (errors.responseJSON.errors.pick_up_date) {
                            $(".error-puke-up-date-car").html(errors.responseJSON.errors.pick_up_date);

                        }
                        console.log(errors.responseJSON.errors.return_date);
                        if (errors.responseJSON.errors.return_date) {
                            $(".error-return-date-car").html(errors.responseJSON.errors.return_date);

                        }
                        setTimeout(function () {
                            $("#modal_error").modal("hide");
                        }, 5000);
                    }


                }
            })
        });
    </script>
@endsection
