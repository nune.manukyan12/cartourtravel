@extends('layouts.app')

@section('content')

    <div class="body-image" style="background-image: url({{ asset("img/car-back-img3.png")}});">

        <div class="row car-book-general" {{--style="background-image: url({{ asset("img/car-back-img3.png")}});"--}}>
            {{--<img src="{{ asset("img/car-back-img.jpg")}} " alt="" style="width:100%;height: 450px;">--}}

        </div>
        {{--<div class="row">--}}
            {{--<div class="col-md-2"></div>--}}
            {{--<div class="col-md-8">--}}
                {{--<div class="" style="padding: 30px">--}}

                    {{--Are you taking a holiday, booking your business trip, traveling as a couple, with a family, or as part--}}
                    {{--of a large group? So, your reason for travel we have a vehicle to suit your needs in Armenia.--}}


                    {{--Arpi Travel offers chauffeur services in Armenia. We are able to work with local suppliers to set up a--}}
                    {{--customized itinerary with a private driver at your disposal for the length of your trip.--}}


                    {{--Our transport transfers are available with licensed, trusted and professional drivers. We will do the--}}
                    {{--hard work for your comfortable journey.--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-1">--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="row div-by-car-car" >
            <div class="col-lg-3">
                <div class="table-car-rent">
                    <form action="{{url('/transfer/search')}}" method="GET" style="padding: 10px 10px 10px 20px">
                        {{--<p class="book-car">Book a car</p>--}}
                        <div class="row m-0">
                            <div class="col-md-12 pr-0">
                                <div class="parametr-book-car">
                                    <div class="row pt-3" style="color: white">
                                        <div class="col-sm-6">
                                            <input type="checkbox" class="" value="transfer" name="transfer">
                                            <p class="p-checkbox" style="width: auto;font-size: 14px">Airport Transfer</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="checkbox" class="" value="chauffeur" name="chauffeur">
                                            <p class="p-checkbox" style="width: auto;font-size: 14px">Chauffeur Service </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <p class="pick-up-return-location"> Pick-up & Return locations <sup class="red-icon">*</sup></p>
                                        <select name="pick_up_locations" id="pick_up_locations" class="select-option-by-car">
                                            <option value="yerevan-downtown">Yerevan, Downtown</option>
                                            <option value="yerevan-international-airport">Yerevan, International Airport</option>
                                            <option value="tbilisi-city-center">Tbilisi City Center</option>
                                            <option value="tbilisi-airport">Tbilisi, Airport</option>
                                            <option value="gyumri-international-airport">Gyumri International Airport</option>
                                        </select>
                                        <select name="return_locations" id="return_locations" class="select-option-by-car">
                                            <option value="yerevan-downtown">Yerevan, Downtown</option>
                                            <option value="yerevan-international-airport">Yerevan, International Airport</option>
                                            <option value="tbilisi-city-center">Tbilisi City Center</option>
                                            <option value="tbilisi-airport">Tbilisi, Airport</option>
                                            <option value="gyumri-international-airport">Gyumri International Airport</option>
                                        </select>
                                        <p class="pick-up-return-location">Car Category-size,cost, power & luxury factor <sup class="red-icon">*</sup></p>
                                        <select name="car_category" id="" class="select-car-search">
                                            <option value=""></option>
                                            <option value="M">M-Mlni</option>
                                            <option value="N">N-Mini Elite</option>
                                            <option value="E">E-Econamy</option>
                                            <option value="C">C-Compact</option>
                                            <option value="I">I-Intermediate</option>
                                            <option value="S">S-Standard</option>
                                            <option value="F">F-Full-size</option>
                                            <option value="P">P-Premium</option>
                                            {{--<option value="X">X-Special</option>--}}
                                            <option value="V">V-Van</option>

                                        </select>
                                        <div class="div-by-days-car">
                                            <div class="calendar-div-button">
                                                <p class="pick-up-return-location"> Pick-up date and time <sup class="red-icon">*</sup>
                                                </p>
                                                <div class="row " style="width: 107%">
                                                    <div class="col-7 col-sm-7">
                                                        <input type="text" class="input-car-calendar input-by-pick-up not-close-calendar"
                                                               id="datepicker-pick-up"
                                                               data-zdp_direction="[1,365]">

                                                        <div id="datepicker-pick-up" style="display: none"></div>
                                                        <div id="datepicker-pick-up-div" style="display: none"></div>
                                                        <input type="hidden" class="input-hidden-by-pick-up" value="0">
                                                    </div>
                                                    <div class="col-5 col-sm-5">
                                                        <div class="time-div">
                                                            <input type="time" class="timepicker-pick-up" value="12:00">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="calendar-div-button">
                                                <p class="pick-up-return-location"> Drop off <sup class="red-icon">*</sup>
                                                </p>
                                                <div class="row" style="width: 107%">
                                                    <div class="col-7">
                                                        <input type="text" class="input-car-calendar input-by-return not-close-calendar"
                                                               id="datepicker-return"
                                                               data-zdp_direction="[1,365]">

                                                        <div id="datepicker-return" style="display: none"></div>
                                                        <div id="datepicker-return-div" style="display: none"></div>
                                                        <input type="hidden" class="input-hidden-by-return" value="0">
                                                    </div>
                                                    <div class="col-5">
                                                        <div class="time-div">
                                                            <input type="time" class="timepicker-return" value="12:00">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="pick-up-return-location"> Voucher Code </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="col-md-4 p-0">--}}
                                {{--<div class="more-search-detalis-car" style="display: none">--}}
                                {{--<div style="" class="more-search-div-1">--}}
                                    {{--<div class="row m-0">--}}
                                        {{--<div class="col-sm-6 col-md-12 pl-0">--}}
                                            {{--<p>Doors & vehicle type</p>--}}
                                            {{--<select name="doors_vehicle_type" id="" class="select-car-search">--}}
                                                {{--<option value=""></option>--}}
                                                {{--<option value="B">B-2/3 door</option>--}}
                                                {{--<option value="C">C-2/4 door</option>--}}
                                                {{--<option value="D">D-2/4 Sdoor</option>--}}
                                                {{--<option value="W">W-Wagon/Estate</option>--}}
                                                {{--<option value="S">S-Sport</option>--}}
                                                {{--<option value="T">T-Convertible</option>--}}
                                                {{--<option value="F">F-SUV</option>--}}
                                                {{--<option value="E">E-Coupe</option>--}}
                                                {{--<option value="N">N-Roadster</option>--}}

                                            {{--</select>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-6 col-md-12 pl-0">--}}
                                            {{--<p>Car Category-size,cost, power & luxury factor</p>--}}
                                            {{--<select name="car_category" id="" class="select-car-search">--}}
                                                {{--<option value=""></option>--}}
                                                {{--<option value="M">M-Mlni</option>--}}
                                                {{--<option value="N">N-Mini Elite</option>--}}
                                                {{--<option value="E">E-Econamy</option>--}}
                                                {{--<option value="C">C-Compact</option>--}}
                                                {{--<option value="I">I-Intermediate</option>--}}
                                                {{--<option value="S">S-Standard</option>--}}
                                                {{--<option value="F">F-Full-size</option>--}}
                                                {{--<option value="P">P-Premium</option>--}}
                                                {{--<option value="X">X-Special</option>--}}
                                                {{--<option value="V">V-Van</option>--}}

                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="row m-0">
                            <div class="col-md-12 pl-0">
                                <div class="buttons-div-by-serch-car">
                                    <div>
                                        <input type="hidden" class="see-more-filter" value="0">
                                    </div>
                                    <button type="submit" class="btn btn-primary w-100">FIND A CAR</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-8 mb-blue">
            @foreach($transfers as $transfer)
                {{--<div class="row div-by-car-car">--}}
                    {{--<div class="col-sm-2"></div>--}}
                    {{--<div class="col-sm-8">--}}
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="car-name-car">{{$transfer->name}} </h2>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <img class="img-apartment" src="{{asset('uploads/'.$transfer->general_pic)}}" alt=""
                                             style="width: 450px; height: 300px;">
                                    </div>
                                    <div class="row div-row-car-detail">
                                        <div class="col-sm-6">
                                        </div>
                                        <div class="col-sm-6"></div>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10">
                                         <p class="with_read_more_dots"> <?php echo $transfer->description;?></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 price-col-color">
                                            <a href="{{asset('transfer/details/'.$transfer->id)}}" type="button" class="btn btn-success book-button pull-right" id="{{$transfer->id}}">Details
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
            </div>
        </div>
        @if($chauffeurService)
            <div class="row div-by-car-car">
                <div class="col-lg-3"></div>
                <div class="col-lg-8 mb-blue">
                    <h2 class="car-name-car">{{$chauffeurService->name}} </h2>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <img class="img-apartment" src="{{asset('uploads/'.$chauffeurService->general_pic)}}" alt=""
                                     style="width: 450px; height: 300px;">
                            </div>
                            <div class="row div-row-car-detail">
                                <div class="col-sm-6">
                                </div>
                                <div class="col-sm-6"></div>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-12">
                                <div class="with_read_more_dots_chauffeur"><?= $chauffeurService->description ?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 price-col-color">
                                    <a href="{{asset('chauffeur/details/'.$chauffeurService->id)}}" type="button" class="btn btn-success book-button pull-right"
                                       id="{{$chauffeurService->id}}">Details
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <nav class="page-navigation bottom-nav">
        <!--back up button-->
        <a class="scroll-to-top pull-right" title="Back up"><i class="fas fa-arrow-up"></i></a>
        <!--//back up button-->
        <!--pager-->
        <div class="pager">
        </div>
    </nav>


    {{--Modal error return and puke up date--}}
    <div class="modal fade" id="modal_error" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <p class="error-puke-up-date-car"></p>
                    <p class="error-return-date-car"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-messenger" role="dialog">
        <div class="modal-dialog modal-messege-dialog">
            <!-- Modal content-->
            <div class="modal-content modal-messege">
                <div class="modal-body text-center">
                    <p class="book-successful">Your booking successful</p>
                    <button type="button" class="close " data-dismiss="modal"><i
                                class="fas fa-times close-button-x"></i></button>
                </div>

            </div>

        </div>
    </div>
    <style>
        .body-image {
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
        }
        .div-by-days-car{
            width: 100%;
        }
        .div-by-car-car {
            background-color: transparent !important;
            border-bottom: none !important;
        }
        .mb-blue{
            background-color:white;
            padding: 0 35px;
            border-bottom: solid 4px #4379a9;
        }
    </style>
    <script>
        $(document).ready(function () {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth() + 1;
            var y = date.getFullYear();
            $(".input-by-pick-up").attr("placeholder", d + '/' + m + '/' + y);
            $(".input-by-return").attr("placeholder", d + '/' + m + '/' + y);

        });
        $('#datepicker-pick-up').Zebra_DatePicker({
            always_visible: $('#datepicker-pick-up-div'),
            format: 'd/m/Y',
            custom_classes: {
                'not-close-calendar': ['1-31']
            }
        });
        $('#datepicker-return').Zebra_DatePicker({
            always_visible: $('#datepicker-return-div'),
            format: 'd/m/Y',
            custom_classes: {
                'not-close-calendar': ['1-31']
            }
        });
        $('.all-body').on('click', function (e) {
            if (!$(e.target).hasClass('not-close-calendar')) {

                if ($('.input-hidden-by-return').val() > 0) {
                    $('#datepicker-return-div').css('display', 'none');
                    $('.input-hidden-by-return').val(0);

                }
                if ($('.input-hidden-by-pick-up').val() > 0) {
                    $('#datepicker-pick-up-div').css('display', 'none');
                    $('.input-hidden-by-pick-up').val(0);
                }

            }
        });
        $('.input-by-return').click(function () {
            if ($('.input-hidden-by-return').val() < 1) {
                $('#datepicker-return-div').css('display', 'block');
                $('.input-hidden-by-return').val(1);
            }
            if ($('.input-hidden-by-pick-up').val() > 0) {
                $('#datepicker-pick-up-div').css('display', 'none');
                $('.input-hidden-by-pick-up').val(0);
            }
        });
        $('.input-by-pick-up').click(function () {

            if ($('.input-hidden-by-pick-up').val() < 1) {
                $('#datepicker-pick-up-div').css('display', 'block');
                $('.input-hidden-by-pick-up').val(1);
            }
            if ($('.input-hidden-by-return').val() > 0) {
                $('#datepicker-return-div').css('display', 'none');
                $('.input-hidden-by-return').val(0);

            }
        });


        $(document).ready(function () {
            $('.timepicker-pick-up').qcTimepicker({
                'format': 'H:mm',
                'minTime': '0:00:00',
                'maxTime': '23:59:59',
                'step': '0:30:00',
                'placeholder': '12:00'
            });
        });
        $(document).ready(function () {
            $('.timepicker-return').qcTimepicker({
                'format': 'H:mm',
                'minTime': '0:00:00',
                'maxTime': '23:59:59',
                'step': '0:30:00',
                'placeholder': '12:00'
            });
        });
        var id;
        $(".book-button").click(function () {
            $("#book-car").modal("show");
            id = $(this).attr('id');

        });

        $('#book-book').click(function () {
            var firstName = $('#first-name').val();
            var lastName = $('#last-name').val();
            var email = $('.email').val();
            var phone = $('.phone').val();
            var viber = $('#viber').val();
            var telegram = $('#telegram').val();
            var whatsapp = $('#whatsapp').val();
            var pick_up_locations = $('#pick_up_locations').find(":selected").text();
            var return_locations = $('#return_locations').find(":selected").text();
            var pick_up_date = $('#datepicker-pick-up').val();
            var return_date = $('#datepicker-return').val();
            var pick_up_time = $('#qcTimepicker-0').find(":selected").text();
            var return_time = $('#qcTimepicker-1').find(":selected").text();
            $.ajax({
                url: '/reservation/store_car',
                type: 'POST',
                dataType: "JSON",
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                data: {

                    last_name: lastName,
                    first_name: firstName,
                    email: email,
                    viber: viber,
                    telegram: telegram,
                    whatsapp: whatsapp,
                    phone: phone,
                    car_id: id,
                    pick_up_locations: pick_up_locations,
                    return_locations: return_locations,
                    pick_up_date: pick_up_date,
                    return_date: return_date,
                    pick_up_time: pick_up_time,
                    return_time: return_time,

                },
                success: function (success) {
                    // console.log('Yes');
                    $("#book-car").modal("hide");
                    $("#modal-messenger").modal("show");
                    setTimeout(function () {
                        $("#modal-messenger").modal("hide");
                    }, 10000);
                },
                error: function (errors) {

                    if (errors.responseJSON.errors.last_name) {
                        $(".error-last-name").html(errors.responseJSON.errors.last_name);
                    }
                    if (errors.responseJSON.errors.first_name) {
                        $(".error-first-name").html(errors.responseJSON.errors.first_name);
                    }
                    if (errors.responseJSON.errors.email) {
                        $(".error-email").html(errors.responseJSON.errors.email);
                    }
                    if (errors.responseJSON.errors.phone) {
                        $(".error-phone").html(errors.responseJSON.errors.phone);
                    }
                    if (errors.responseJSON.errors.pick_up_date || errors.responseJSON.errors.return_date) {

                        $("#modal_error").modal("show");
                        if (errors.responseJSON.errors.pick_up_date) {
                            $(".error-puke-up-date-car").html(errors.responseJSON.errors.pick_up_date);

                        }
                        console.log(errors.responseJSON.errors.return_date);
                        if (errors.responseJSON.errors.return_date) {
                            $(".error-return-date-car").html(errors.responseJSON.errors.return_date);

                        }
                        setTimeout(function () {
                            $("#modal_error").modal("hide");
                        }, 5000);
                    }


                }
            })
        });
        $('#see_more_filter').click(function () {
            if ($('.see-more-filter').val() == 0) {
                $(".table-car-rent").animate({width: '700px'}, 1000);
                $(".button-car-s").animate({width: '640px'}, 1000);
                $('#see_more_filter').text('CLOSE');
                $('.see-more-filter').val(1);
                // $('.more-search-detalis-car').show(500);
                setTimeout(function () {
                    $('.more-search-div').show(700);
                    setTimeout(function () {
                        $('.more-search-div-1').show(1000);

                    }, 200);

                }, 200);
            } else {
                $(".table-car-rent").animate({width: '366px'}, 1000);
                $(".button-car-s").animate({width: '297px'}, 1000);
                $('#see_more_filter').text('MORE');
                $('.see-more-filter').val(0);

                $('.more-search-div-1').hide(1000);

                setTimeout(function () {
                    $('.more-search-div').hide(800);
                }, 200);

            }
        })
    </script>



@endsection
