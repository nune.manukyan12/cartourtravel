<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', '');

    //Reservation//
Route::group(['prefix' => 'reservation', 'as' => 'reservation.'], function () {
    Route::post('/store','Frontend\ReservationController@store')->name('store');
    Route::post('/store_car','Frontend\ReservationController@store_car')->name('store_car');
    Route::post('/calculation','Frontend\ReservationController@calculation')->name('calculation');
    Route::post('/package/save', 'Frontend\ReservationController@packageSave')->name('tour.package-save');
    Route::post('/tour/save', 'Frontend\ReservationController@tourSave')->name('tour.save');

});

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {

    // Slider //
    Route::group(['prefix' => 'slider', 'as' => 'slider.'], function () {
        Route::get('/', 'SliderController@index')->name('show');
        Route::post('/create', 'SliderController@store')->name('store');
        Route::get('/image/delete/{id}', 'SliderController@delete')->name('image-delete');
    });

//    translation
    Route::post('/translate', 'ApartmentController@translate')->name('translate');

    // User //
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/', 'UserController@index')->name('show');
        Route::get('/activate/{id}', 'UserController@activate')->name('activate');
        Route::get('/deactivate/{id}', 'UserController@deactivate')->name('deactivate');

    });

    // Car //

    Route::group(['prefix' => 'car', 'as' => 'car.'], function () {
        Route::get('/', 'CarController@index')->name('car');
        Route::get('/create', 'CarController@create')->name('create');
        Route::post('/store', 'CarController@store')->name('store');
        Route::get('/show/{id}', 'CarController@show')->name('show');
        Route::post('/update', 'CarController@update')->name('update');
        Route::get('/delete/{id}', 'CarController@destroy')->name('delete');
        Route::get('/image/delete/{id}', 'CarController@deleteImage')->name('image-delete');
        Route::get('/approval/{id}', 'CarController@approval')->name('approval');

    });
    //Car seat//
    Route::group(['prefix' => 'car-seat', 'as' => 'car-seat.'], function () {

        Route::get('/', 'CarSeatController@index')->name('car-seat');
        Route::get('/create', 'CarSeatController@create')->name('create');
        Route::post('/store', 'CarSeatController@store')->name('store');
        Route::get('/show/{id}', 'CarSeatController@show')->name('show');
        Route::post('/update', 'CarSeatController@update')->name('update');
        Route::get('/delete/{id}', 'CarSeatController@destroy')->name('delete');



    });

    // transfer //

    Route::group(['prefix' => 'transfer', 'as' => 'transfer.'], function () {
        Route::get('/', 'TransferController@index')->name('transfer');
        Route::get('/create', 'TransferController@create')->name('create');
        Route::post('/store', 'TransferController@store')->name('store');
        Route::get('/show/{id}', 'TransferController@show')->name('show');
        Route::post('/update', 'TransferController@update')->name('update');
        Route::get('/delete/{id}', 'TransferController@destroy')->name('delete');
        Route::get('/image/delete/{id}', 'TransferController@deleteImage')->name('image-delete');
        Route::get('/approval/{id}', 'TransferController@approval')->name('approval');
        Route::post('/store_transfer','Frontend\TransferController@store_transfer')->name('store_transfer');

    });

    Route::group(['prefix' => 'chauffeur', 'as' => 'chauffeur.'], function () {
        Route::get('/', 'TransferController@chauffeur')->name('chauffeur');
        Route::get('/create', 'TransferController@chauffeurCreate')->name('chauffeur');
        Route::post('/store', 'TransferController@chauffeurStore')->name('chauffeur-store');
        Route::post('/update', 'TransferController@chauffeurEdit')->name('chauffeur-edit');
    });
    // hotel //
    Route::group(['prefix' => 'hotel', 'as' => 'hotel.'], function () {
        Route::get('/', 'HotelController@index')->name('car');
        Route::get('/create', 'HotelController@create')->name('create');
        Route::post('/store', 'HotelController@store')->name('store');
        Route::get('/show/{id}', 'HotelController@show')->name('show');
        Route::post('/update', 'HotelController@update')->name('update');
        Route::get('/delete/{id}', 'HotelController@destroy')->name('delete');
        Route::get('/image/delete/{id}', 'HotelController@deleteImage')->name('image-delete');
        Route::get('/approval/{id}', 'HotelController@approval')->name('approval');

    });

    // tour //
    Route::group(['prefix' => 'tour', 'as' => 'tour.'], function () {
        Route::get('/', 'TourController@index')->name('car');
        Route::get('/package', 'TourController@indexPackage')->name('packages');
        Route::get('/package/show/{id}', 'TourController@showPackage')->name('package-show');
        Route::get('/create', 'TourController@create')->name('create');
        Route::post('/store', 'TourController@store')->name('store');
        Route::get('/show/{id}', 'TourController@show')->name('show');
        Route::post('/update', 'TourController@update')->name('update');
        Route::get('/delete/{id}', 'TourController@destroy')->name('delete');
        Route::get('/image/delete/{id}', 'TourController@deleteImage')->name('image-delete');
        Route::get('/approval/{id}', 'TourController@approval')->name('approval');
        Route::post('/destination/delete', 'TourController@deleteDestination')->name('delete-destination');
        Route::post('/destination/info', 'TourController@storeDestinationInfo')->name('info-destination');
        Route::get('/destination_picture/{id}', 'TourController@showDestinationInfo')->name('picture-destination');

    });

    // apartments //
    Route::group(['prefix' => 'apartment', 'as' => 'apartment.'], function () {
        Route::get('/', 'ApartmentController@index')->name('car');
        Route::get('/create', 'ApartmentController@create')->name('create');
        Route::post('/store', 'ApartmentController@store')->name('store');
        Route::get('/show/{id}', 'ApartmentController@show')->name('show');
        Route::post('/update', 'ApartmentController@update')->name('update');
        Route::get('/delete/{id}', 'ApartmentController@destroy')->name('delete');
        Route::get('/add-home/{id}/{val}', 'ApartmentController@addHome')->name('add-home');
        Route::get('/image/delete/{id}', 'ApartmentController@deleteImage')->name('image-delete');
        Route::get('/approval/{id}', 'ApartmentController@approval')->name('approval');
        Route::post('/available','ApartmentController@available')->name('available');
        Route::post('/nightly-price','ApartmentController@nightlyPrice')->name('nightly-price');

    });


    // news //
    Route::group(['prefix' => 'news', 'as' => 'news.'], function () {
        Route::get('/', 'NewsController@index')->name('index');
        Route::get('/create', 'NewsController@create')->name('create');
        Route::post('/store', 'NewsController@store')->name('store');
        Route::get('/show/{id}', 'NewsController@show')->name('show');
        Route::post('/update', 'NewsController@update')->name('update');
        Route::get('/delete/{id}', 'NewsController@destroy')->name('delete');
    });

    // medical //
    Route::group(['prefix' => 'medical', 'as' => 'medical.'], function () {
        Route::get('/', 'MedicalController@index')->name('index');
        Route::get('/create', 'MedicalController@create')->name('create');
        Route::post('/store', 'MedicalController@store')->name('store');
        Route::get('/show/{id}', 'MedicalController@show')->name('show');
        Route::post('/update', 'MedicalController@update')->name('update');
        Route::get('/delete/{id}', 'MedicalController@destroy')->name('delete');
    });

    // tourism //
    Route::group(['prefix' => 'tourism', 'as' => 'tourism.'], function () {
        Route::get('/', 'TourismController@index')->name('index');
        Route::get('/create', 'TourismController@create')->name('create');
        Route::post('/store', 'TourismController@store')->name('store');
        Route::get('/show/{id}', 'TourismController@show')->name('show');
        Route::post('/update', 'TourismController@update')->name('update');
        Route::get('/delete/{id}', 'TourismController@destroy')->name('delete');
    });

    Route::get('/about', 'InfoController@about')->name('about');
    Route::POST('/about/store', 'InfoController@storeAbout')->name('about-store');
    Route::get('/contact', 'InfoController@contact')->name('contact');

    Route::get('/', 'AdminController@dashboard')->name('admin');
    Route::post('/image-upload', 'AdminController@uploadImage');

});


Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('auth.logout');

Route::get('/', 'Frontend\HomeController@index')->name('home');

Route::get('/apartments/{type?}', 'Frontend\ApartmentController@show')->name('apartment.show');
Route::post('/apartments/search', 'Frontend\ApartmentController@search')->name('apartment.search');
Route::post('/apartments/img', 'Frontend\ApartmentController@imgView')->name('apartment.img-view');

Route::get('/apartment/{id}', 'Frontend\ApartmentController@detail')->name('apartment.detail');



Route::get('/cars/search', 'Frontend\CarController@search')->name('car.search');
Route::get('/cars/{type?}', 'Frontend\CarController@show')->name('car.show');
Route::get('/car/{id}', 'Frontend\CarController@detail')->name('car.detail');


Route::get('/tours/{type}', 'Frontend\TourController@show')->name('tour.show');
Route::get('/tour/{id}', 'Frontend\TourController@detail')->name('tour.detail');

Route::get('/tour/package/create', 'Frontend\TourController@createPackage')->name('tour.create');


Route::get('/medical/service',  'Frontend\MedicalController@show')->name('medical.show');
Route::get('/medical/details/{id}',  'Frontend\MedicalController@details')->name('medical.details');
Route::post('/medical/basket',  'Frontend\MedicalController@basket')->name('medical.basket');
Route::post('/medical/store_medical',  'Frontend\MedicalController@store_medical')->name('medical.store_medical');

Route::get('/tourism/service',  'Frontend\TourismController@show')->name('tourism.show');
Route::get('/tourism/details/{id}',  'Frontend\TourismController@details')->name('tourism.details');
Route::post('/tourism/basket',  'Frontend\TourismController@basket')->name('tourism.basket');
Route::post('/tourism/store_tourism',  'Frontend\TourismController@store_tourism')->name('tourism.store_medical');

Route::get('/news',  'Frontend\NewsController@show')->name('news.show');


Route::post('/package/date/search', 'Frontend\ApartmentController@searchPackageDate')->name('search-package-date');
Route::get('/accommodation/package/{id}', 'Frontend\TourController@apartmentPackage')->name('apartment.package');
Route::post('/accommodation/package/price', 'Frontend\TourController@apartmentPricePackage')->name('apartment-price.package');
Route::post('/tour/package/price', 'Frontend\TourController@tourPricePackage')->name('tour-price.package');
Route::get('/tour/info/{id}', 'Frontend\TourController@getDestinationInfo')->name('destination.get');



Route::get('/hotels', 'Frontend\HotelController@show')->name('hotel.show');
Route::get('/hotel/{id}', 'Frontend\HotelController@detail')->name('hotel.detail');


Route::get('/car-seats', 'Frontend\CarSeatController@show')->name('car-seat.show');

Route::get('/transfer', 'Frontend\TransferController@show')->name('transfer.show');

Route::get('/transfer/details/{id}', 'Frontend\TransferController@transferDetails')->name('transfer.details');
Route::get('/chauffeur/details/{id}', 'Frontend\TransferController@chauffeurDetails')->name('chauffeur.details');
Route::post('transfer/store_transfer','Frontend\TransferController@store_transfer')->name('store_transfer');
Route::post('transfer/store_chauffeur','Frontend\TransferController@store_chauffeur')->name('store_chauffeur');

Route::get('/about', 'Frontend\InfoController@about')->name('about.show');

Route::get('/contact', 'Frontend\InfoController@contact')->name('contact.show');
Route::post('/contacts/email', 'Frontend\InfoController@contactEmail')->name('contact.email');



Route::get('lang/{lang}', 'Frontend\LanguageController@swap');
Route::get('currency/{currency}', 'Frontend\CurrencyController@change');
